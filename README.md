# 稠云智能办公系统

#### 介绍
本项目为V3.1密文版生产环境代码！！！主打安全，项目90%开源项目，用户可以用户开发学习，用户可自主开发除工作流以外的业务系统。

#### 软件架构
本项目JDK8x64+SpringBoot+MyBatis+Redis+Druid+Beetl+Shrio+Vue3的框架组合，自研工作流引擎，支持可视化表单设计与流程设计。支技分布式部署。功能完善能够满足中大型企业办公需要。
全面支持mysql8+、PostgreSQL 15+、Oracle 12C，同时全面支持国产达梦数据库8.0,人大金仓V8R3,神州通用v7
本项目中暂不包含工作流与收发文相关的前后台代码。相关代码正在整理中...

安装最新版测试请加群索取或WX联系！


演示版本为前后端分离V3.1版本，采用VUE3+SpringBoot技术路线。
正式版演示地址：http://oa.cyoasoft.com 账户：admin 密码：123456

下载地址：http://www.cyoasoft.com/cyoa3.1_setup20240317.exe
注意:在安装时,请在安装文件上右击->选择“管理员身份运行”。否则会出权限不足导致后台服务无法正常运行。

#### 国产化
系统已全面支持国产化，统信UOS,中标麒麟操作系统+openJdk8x64bit+达梦8数据库、人大金仓V8R3、神州通用V7+东方通tongWeb中间件、中创中间件,金蝶天燕,宝兰德,文档支持在线WPS,OFFICE,福昕ofd,数科ofd,pdf等。若项目需要可联系我们索取相关源代码。
![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/gc.jpg)

#### 使用说明

1.  演示地址：http://oa.cyoasoft.com 账户：admin 密码：123456
2.  若有问题请联系：
	QQ:68311718 
	WX:13814042966 
	邮件:cyunoa@qq.com
3.  本产品的工作流手册与用户使用手册请联系我们索取。
4.  演示系统支持Shrio权限框架http://oa.cyoasoft.com

#### 安装教程
先决条件：安装MySql8 并导入lib/cyunsoft3.sql  安装redis并设置密码为"myredis"

后端：
      注意：之前有不在小伙伴说项目不能正确的部署，经过我们的测试发现问题出在Maven上,好多小伙伴都是使用的idea的maven,最终不能正确启动项目。现在我把我们的使用的maven程序放在了lib中，idea中指定我们的maven,并覆盖setting.xml文件。
只有这样，项目的maven才能正确依赖。

1.  mvn install lib/目录中的jar 文件拷贝到D盘根目录下，
      mvn install:install-file -DgroupId=cyunsoft.utils -DartifactId=cyunsoft-utils -Dversion=3.1.0 -Dpackaging=jar -Dfile=d:/cyunsoft-utils-3.1.0.jar
      mvn install:install-file -DgroupId=cyunsoft.common -DartifactId=cyunsoft-common -Dversion=3.1.0 -Dpackaging=jar -Dfile=d:/cyunsoft-common-3.1.0.jar  
      mvn install:install-file -DgroupId=cyunsoft.gen -DartifactId=cyunsoft-gen -Dversion=3.1.0 -Dpackaging=jar -Dfile=d:/cyunsoft-gen-3.1.0.jar.jar

2.  项目若提示缺少jar包的话，可以lib/cyunsoft-common-3.1.0.jar文中找到，用解压工具打开，MATE-INF/maven/cyunsoft.commmon/cyunsoft-common/pom.xml文件中找到。
注：要是启动时提示缺少国密相关的“hutool”的类，请检查maven仓库里面是否存在。
      <dependency>
            <groupId>cn.hutool</groupId>
            <artifactId>hutool-all</artifactId>
            <version>5.8.11</version>
      </dependency>
      <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcprov-jdk18on</artifactId>
            <version>1.73</version>
      </dependency>
3.  安装MYSQL8.0.21版本 后导入lib/cyunsoft.sql
4.  运行cyunsoft-appservice中的AppGo.java 即可启动项目。
项目启动时需要d:/cyunsoft/attach和d:/cyunsoft/lic目录，建议事件下载安装测试版，一切需求的目录与静态文件都有了。


前端
      1、安装Nodejs 16 
      2、设置淘宝镜像 npm config set registry https://registry.npmmirror.com/ 
      3、npm install 


#### 项目界面预览

  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main0.png)
  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main1.png)
  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main1-1.png)
  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main2.png)
  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main3.png)
  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main4.png)
  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main5.png)
  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main6.png)
  ![image](https://gitee.com/cysoft_1/oa/raw/master/lib/img/main7.png)
#### APP界面预览
<img src="https://gitee.com/cysoft_1/oa/raw/master/lib/img/app0.jpg" width="200"/>
<img src="https://gitee.com/cysoft_1/oa/raw/master/lib/img/app1.jpg" width="200"/>
<img src="https://gitee.com/cysoft_1/oa/raw/master/lib/img/app2.jpg" width="200"/>
<img src="https://gitee.com/cysoft_1/oa/raw/master/lib/img/app3.jpg" width="200"/>
<img src="https://gitee.com/cysoft_1/oa/raw/master/lib/img/app4.jpg" width="200"/>
<img src="https://gitee.com/cysoft_1/oa/raw/master/lib/img/app5.jpg" width="200"/>
<img src="https://gitee.com/cysoft_1/oa/raw/master/lib/img/app6.jpg" width="200"/>
<img src="https://gitee.com/cysoft_1/oa/raw/master/lib/img/app7.jpg" width="200"/>


#### 开源宗旨
1. 本项目主要用于开发者了解企业办系统的基本功能，共同开发适合本国国情的工作流引擎。
2. 技术交流群QQ:660214195 群内不定时的发放技术文档。
3. 开源项目不容易，大家感觉可以加个星。

