const DEFAULT_CONFIG = {
	//标题
	APP_NAME: "",
	//首页地址
	DASHBOARD_URL: "/dashboard",

	//版本号
	APP_VER: "V3.1",

	//内核版本号
	CORE_VER: "V3.1",

	//接口地址
	//API_URL: process.env.NODE_ENV === 'development' && process.env.VUE_APP_PROXY === 'true' ? "/capi" : process.env.VUE_APP_API_BASEURL,
	API_URL: "/capi",
	//请求超时
	TIMEOUT: 5*60*1000,

	//TokenName
	TOKEN_NAME: "Authorization",

	//Token前缀，注意最后有个空格，如不需要需设置空字符串
	TOKEN_PREFIX: "Bearer ",

	//追加其他头
	HEADERS: {},

	//请求是否开启缓存
	REQUEST_CACHE: false,

	//布局 默认：default | 通栏：header | 经典：menu | 功能坞：dock
	//dock将关闭标签和面包屑栏
	LAYOUT: 'default',

	//菜单是否折叠
	MENU_IS_COLLAPSE: false,

	//菜单是否启用手风琴效果
	MENU_UNIQUE_OPENED: false,

	//是否开启多标签
	LAYOUT_TAGS: true,

	//语言
	LANG: 'zh-cn',

	//主题颜色
	COLOR: '',

	//是否加密localStorage, 为空不加密，可填写AES(模式ECB,移位Pkcs7)加密
	LS_ENCRYPTION: '',

	//localStorageAES加密秘钥，位数建议填写8的倍数
	LS_ENCRYPTION_key: '2XNN4K8LC0ELVWN4',

	//控制台首页默认布局
	DEFAULT_GRID: {
		//默认分栏数量和宽度 例如 [24] [18,6] [8,8,8] [6,12,6]
		layout: [12, 6, 6],
		//小组件分布，com取值:views/home/components 文件名
		copmsList: [["welcome"],["about", "ver"],["time", "progress"]]
	},
	BPM_FIELD:true,

	VARIANT_FORM_VERSION : '3.1.0',
	MOCK_CASE_URL : '',
	ACE_BASE_PATH:'/global/',
	BEAUTIFIER_PATH:'/global/beautifier.min.js',
	DOC_FILE:['.docx','.doc','.dot','.wps','.wpt'],
	EXCEL_FILE:['.xlsx','.xls','.et','.ett','.xlt'],
	PPT_FILE:['.ppt','.pptx','.dpt','.dps','.pot','.pps'],
	PDF_FILE:['.pdf','.ofd'],
	IMG_FILE:['.png','.jpg','.jpeg','.bmp','.gif','.tif','.webp'],
	VIDEO_FILE:['.mp4','.mkv','.mov','.flv','.mp4','.avi','.wmv'],
	AUDIO_FILE:['.wav','.mp3','.ogg'],
	//当SM4_PRIVATE_KEY不为空时，则启用密文传参，此时需要注意的是后面也需同步启用。确保前后密码一致。
	SM4_PRIVATE_KEY:"www.cyoasoft.com",
	//数据库敏感信息加密设置 1为AES方式。
	DATA_ENCRYPT_MODE:"1",
	DATA_ENCRYPT_KEY:"www.cyoasoft.com",
}

// 如果生产模式，就合并动态的APP_CONFIG
// public/config.js
if(process.env.NODE_ENV === 'production'){
	Object.assign(DEFAULT_CONFIG, APP_CONFIG)
}

module.exports = DEFAULT_CONFIG
