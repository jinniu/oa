import {isMobile} from "@/utils/util";
import sysConfig from "@/config";
import tool from "@/utils/tool";
export default {
	methods: {
		down(fileItem) {
			if (sysConfig.SM4_PRIVATE_KEY != '') {
				this.$nextTick(() => {
					window.open(sysConfig.API_URL+'/get/file/getFileDown?attachId=' + tool.crypto.createParam(fileItem.attachId) + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
				})
			}else{
				this.$nextTick(() => {
					window.open(sysConfig.API_URL+'/get/file/getFileDown?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
				})
			}
		},
		preview(fileItem) {
			if(isMobile())
			{
				let url="/app/file/preview";
				if(sysConfig.DOC_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName=='.docx')
					{
						url="/app/file/preview/word";
					}
				}else if(sysConfig.EXCEL_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName=='.xlsx')
					{
						url="/app/file/preview/excel";
					}
				}else if(sysConfig.PPT_FILE.includes(fileItem.extName))
				{
					url="/app/file/preview/ppt";
				}else if(sysConfig.PDF_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName==".ofd") {
						url="/app/file/preview/ofd";
					}
				}else if (sysConfig.IMG_FILE.includes(fileItem.extName)) {
					url='/app/file/preview/image';
				}
				this.$nextTick(() => {
					this.$router.push({
						path: url,
						query: {
							attachId:fileItem.attachId,
							extName:fileItem.extName,
						}
					})
				})
			}else {
				if(this.$TOOL.data.get("OPEN_ON_LINE_FLAG")=='0')
				{
					this.$nextTick(() => {
						window.open('/#/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
					})
				}else if(this.$TOOL.data.get("OPEN_ON_LINE_FLAG")=='2')
				{
					if(sysConfig.DOC_FILE.includes(fileItem.extName))
					{
						if(fileItem.extName=='.docx')
						{
							this.$nextTick(() => {
								window.open('/#/file/preview/word?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}else {
							this.$nextTick(() => {
								window.open('/#/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}
					}else if(sysConfig.EXCEL_FILE.includes(fileItem.extName))
					{
						if(fileItem.extName==".xlsx")
						{
							this.$nextTick(() => {
								window.open('/#/file/preview/excel?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}else {
							this.$nextTick(() => {
								window.open('/#/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}
					}else if(sysConfig.PPT_FILE.includes(fileItem.extName))
					{
						if(fileItem.extName==".pptx") {
							this.$nextTick(() => {
								window.open('/#/file/preview/ppt?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}else{
							this.$nextTick(() => {
								window.open('/#/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}
					}else if(sysConfig.PDF_FILE.includes(fileItem.extName))
					{
						if(fileItem.extName=='.ofd')
						{
							this.$nextTick(() => {
								window.open('/#/file/preview/ofd?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}else{
							this.$nextTick(() => {
								window.open('/#/file/preview/pdf?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}
					}else if (sysConfig.IMG_FILE.includes(fileItem.extName)) {
						const image = new Image()
						if (sysConfig.SM4_PRIVATE_KEY != '') {
							image.src = sysConfig.API_URL+'/get/file/getFileDown?attachId=' + tool.crypto.createParam(fileItem.attachId) + "&extName=" + tool.crypto.createParam(fileItem.extName)+"&ddtab=true"
						}else{
							image.src = sysConfig.API_URL+'/get/file/getFileDown?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true"
						}
						image.onload = () => {
							image.style.margin = "0 auto"
							image.style.display = "block"
							const newWin = window.open("", "_blank")
							newWin.document.write(image.outerHTML)
						}
					}
				}
			}
		},
		async createFolder(folderId, newName) {
			var res = await this.$API.file.publicFile.insertPublicFileFolder.post({parentId: folderId, folderName: newName});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getPublicFileFolderGroup();
				this.getPublicFileList(this.folderId);
				this.getPublicFolderRoleInfo(this.folderId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async paste(fileType, optType, optId, currentLocation) {
			if (optId == "" || optId == null) {
				this.$alert('请选择需要粘贴的文件', "提示", {type: 'error'});
				return;
			}
			var res;
			if (optType == "2") {
				res = await this.$API.file.publicFile.pastePublicFile.post({fileId: optId, type: fileType, currentLocation: currentLocation});
				if (res.code == 200) {
					this.$message.success("操作成功");
					this.getPublicFileFolderGroup();
					this.getPublicFileList(this.folderId);
					this.getPublicFolderRoleInfo(this.folderId);
				} else {
					this.$alert(res.message, "提示", {type: 'error'})
				}
			} else if (optType == "1") {
				if (optId == "" || optId == null) {
					this.$alert('请选择需要粘贴的文件', "提示", {type: 'error'});
					return;
				}
				res = await this.$API.file.publicFile.shearPublicFile.post({fileId: optId, type: fileType, currentLocation: currentLocation});
				if (res.code == 200) {
					this.$message.success("操作成功");
					this.getPublicFileFolderGroup();
					this.getPublicFileList(this.folderId);
					this.getPublicFolderRoleInfo(this.folderId);
				} else {
					this.$alert(res.message, "提示", {type: 'error'})
				}
			}
		},
		async renameFolder(fileItem, newName) {
			var res = await this.$API.file.publicFile.updatePublicFileFolder.post({folderId: fileItem.fileId, folderName: newName});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getPublicFileFolderGroup();
				this.getPublicFileList(this.folderId);
				this.getPublicFolderRoleInfo(this.folderId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async rename(fileItem, newName) {
			var res = await this.$API.file.publicFile.updatePublicFile.post({fileId: fileItem.fileId, fileName: newName+fileItem.extName});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getPublicFileFolderGroup();
				this.getPublicFileList(this.folderId);
				this.getPublicFolderRoleInfo(this.folderId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async delete(fileItem) {
			var res = await this.$API.file.publicFile.deletePublicFile.post({fileId: fileItem.fileId});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getPublicFileFolderGroup();
				this.getPublicFileList(this.folderId);
				this.getPublicFolderRoleInfo(this.folderId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async deleteFolder(fileItem) {
			var res = await this.$API.file.publicFile.deletePublicFileFolderAndFile.post({folderId: fileItem.fileId});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getPublicFileFolderGroup();
				this.getPublicFileList(this.folderId);
				this.getPublicFolderRoleInfo(this.folderId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
	}
}
