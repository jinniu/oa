import {isMobile} from "@/utils/util";
import sysConfig from "@/config";
import tool from "@/utils/tool";
export default {
	methods: {
		down(fileItem) {
			this.$nextTick(() => {
				window.open(sysConfig.API_URL+'/get/file/getFileDown?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
			})
		},
		preview(fileItem) {
			if(isMobile())
			{
				if(sysConfig.DOC_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName=='.docx')
					{
						this.$nextTick(() => {
							window.open('/#/app/file/preview/word?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
						})
					}else {
						this.$nextTick(() => {
							window.open('/#/app/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
						})
					}
				}else if(sysConfig.EXCEL_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName=='.xlsx')
					{
						this.$nextTick(() => {
							window.open('/#/app/file/preview/excel?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
						})
					}else {
						this.$nextTick(() => {
							window.open('/#/app/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
						})
					}
				}else if(sysConfig.PPT_FILE.includes(fileItem.extName))
				{
					this.$nextTick(() => {
						window.open('/#/app/file/preview/ppt?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
					})
				}else if(sysConfig.PDF_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName==".ofd") {
						this.$nextTick(() => {
							window.open('/#/app/file/preview/ofd?netDiskId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
						})
					}else {
						this.$nextTick(() => {
							window.open('/#/app/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
						})
					}
				}else if (sysConfig.IMG_FILE.includes(fileItem.extName)) {
					const image = new Image()
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						image.src = sysConfig.API_URL + '/get/file/getFileDown?attachId=' + tool.crypto.createParam(fileItem.attachId) + "&extName=" + tool.crypto.createParam(fileItem.extName) + "&ddtab=true";
					}else{
						image.src = sysConfig.API_URL+'/get/file/getFileDown?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true"
					}
					image.onload = () => {
						image.style.margin = "0 auto"
						image.style.display = "block"
						const newWin = window.open("", "_blank")
						newWin.document.write(image.outerHTML)
					}
				}
			}else{
				if(this.$TOOL.data.get("OPEN_ON_LINE_FLAG")=='0')
				{
					this.$nextTick(() => {
						window.open('/#/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
					})
				}else if(this.$TOOL.data.get("OPEN_ON_LINE_FLAG")=='2')
				{
					if(sysConfig.DOC_FILE.includes(fileItem.extName))
					{
						if(fileItem.extName=='.docx')
						{
							this.$nextTick(() => {
								window.open('/#/file/preview/word?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}else {
							this.$nextTick(() => {
								window.open('/#/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}
					}else if(sysConfig.EXCEL_FILE.includes(fileItem.extName))
					{
						if(fileItem.extName==".xlsx")
						{
							this.$nextTick(() => {
								window.open('/#/file/preview/excel?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}else {
							this.$nextTick(() => {
								window.open('/#/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}
					}else if(sysConfig.PPT_FILE.includes(fileItem.extName))
					{
						if(fileItem.extName==".pptx") {
							this.$nextTick(() => {
								window.open('/#/file/preview/ppt?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}else{
							this.$nextTick(() => {
								window.open('/#/file/preview?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}
					}else if(sysConfig.PDF_FILE.includes(fileItem.extName))
					{
						if(fileItem.extName==".ofd") {
							this.$nextTick(() => {
								window.open('/#/file/preview/ofd?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}else{
							this.$nextTick(() => {
								window.open('/#/file/preview/pdf?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true", "_blank");
							})
						}
					}else if (sysConfig.IMG_FILE.includes(fileItem.extName)) {
						const image = new Image()
						if (sysConfig.SM4_PRIVATE_KEY != '') {
							image.src = sysConfig.API_URL + '/get/file/getFileDown?attachId=' + tool.crypto.createParam(fileItem.attachId) + "&extName=" + tool.crypto.createParam(fileItem.extName) + "&ddtab=true";
						}else{
							image.src = sysConfig.API_URL+'/get/file/getFileDown?attachId=' + fileItem.attachId + "&extName=" + fileItem.extName+"&ddtab=true"
						}
						image.onload = () => {
							image.style.margin = "0 auto"
							image.style.display = "block"
							const newWin = window.open("", "_blank")
							newWin.document.write(image.outerHTML)
						}
					}
				}
			}
		},
		async delete(fileItem) {
			var res = await this.$API.file.personalFile.deletePersonalFile.post({fileId: fileItem.fileId});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.$emit('changefoldersuccess',this.fileItem.fileId);
				this.getPersonalFileList(this.folderId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async deleteFolder(fileItem) {
			var res = await this.$API.file.personalFile.deletePersonalFileFolder.post({folderId: fileItem.fileId});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.$emit('changefoldersuccess');
				this.getPersonalFileList(this.folderId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
	}
}
