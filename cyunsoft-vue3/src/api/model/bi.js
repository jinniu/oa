import config from "@/config"
import http from "@/utils/request"

export default {
	excel:{
		insertBiExcelData:{
			url: `${config.API_URL}/set/bi/insertBiExcelData`,
			name: "填报EXCEL报表",
			post: async function (data) {
				return await http.bigPost(this.url, data,{});
			},
		},
		updateBiExcelData:{
			url: `${config.API_URL}/set/bi/updateBiExcelData`,
			name: "更新excel报表记录",
			post: async function (data) {
				return await http.bigPost(this.url, data,{});
			},
		},
		deleteBiExcelData:{
			url: `${config.API_URL}/set/bi/deleteBiExcelData`,
			name: "删除Execel报表记录",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		deleteBiExcelDataByIds:{
			url: `${config.API_URL}/set/bi/deleteBiExcelDataByIds`,
			name: "批量删除Execel报表记录",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		getBiExcelDataById:{
			url: `${config.API_URL}/get/bi/getBiExcelDataById`,
			name: "获取EXCEL报表记录详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBiExcelDataList:{
			url: `${config.API_URL}/get/bi/getBiExcelDataList`,
			name: "获取填报记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyBiExcelManageList:{
			url: `${config.API_URL}/get/bi/getMyBiExcelManageList`,
			name: "获取个人填报列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyBiExcelManageForSelect:{
			url: `${config.API_URL}/get/bi/getMyBiExcelManageForSelect`,
			name: "获取报表模版列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBiExcelSort:{
			url: `${config.API_URL}/set/bi/insertBiExcelSort`,
			name: "创建Excel分类",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		updateBiExcelSort:{
			url: `${config.API_URL}/set/bi/updateBiExcelSort`,
			name: "更新分类",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		deleteBiExcelSort:{
			url: `${config.API_URL}/set/bi/deleteBiExcelSort`,
			name: "删除Excel分类",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		getBiExcelSortTree:{
			url: `${config.API_URL}/get/bi/getBiExcelSortTree`,
			name: "获取Excel分类结构",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getExcelManageTemplateById:{
			url: `${config.API_URL}/get/bi/getExcelManageTemplateById`,
			name: "获取报表模版",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBiExcelManage:{
			url: `${config.API_URL}/set/bi/insertBiExcelManage`,
			name: "创建Excel报表",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		updateBiExcelManage: {
			url: `${config.API_URL}/set/bi/updateBiExcelManage`,
			name: "保存EXCEL模版",
			post: async function (data) {
				return await http.bigPost(this.url, data,{});
			},
		},
		downExcelTemplate:{
			url: `${config.API_URL}/get/bi/downExcelTemplate`,
			name: "导出EXCEL文件",
			fileDownload: async function (data) {
				return await http.fileDownload(this.url, data,{
					headers: {}
				})
			}
		},
		downExcelReport:{
			url: `${config.API_URL}/get/bi/downExcelReport`,
			name: "导出EXCEL文件",
			fileDownload: async function (data) {
				return await http.fileDownload(this.url, data,{})
			}
		},
		deleteBiExcelManage:{
			url: `${config.API_URL}/set/bi/deleteBiExcelManage`,
			name: "删除Execel模版",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		deleteBiExcelManageByIds:{
			url: `${config.API_URL}/set/bi/deleteBiExcelManageByIds`,
			name: "批量删除Execel模版",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		getBiExcelManageList:{
			url: `${config.API_URL}/get/bi/getBiExcelManageList`,
			name: "获取管理报表列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	}
}
