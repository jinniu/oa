import config from "@/config"
import http from "@/utils/request"

export default {
	sysGen:{
		createControllerGetFile:{
			url: `${config.API_URL}/get/gen/createControllerGetFile`,
			name: "获取代码生成后台CONTROLLER GET代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createControllerSetFile:{
			url: `${config.API_URL}/get/gen/createControllerSetFile`,
			name: "获取代码生成后台CONTROLLER SET代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createServiceFile:{
			url: `${config.API_URL}/get/gen/createServiceFile`,
			name: "获取代码生成后台SERVICE代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createBeanFile:{
			url: `${config.API_URL}/get/gen/createBeanFile`,
			name: "获取代码生成后台BEAN代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createMapperFile:{
			url: `${config.API_URL}/get/gen/createMapperFile`,
			name: "获取代码生成后台MAPPER代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createMapperXmlFile:{
			url: `${config.API_URL}/get/gen/createMapperXmlFile`,
			name: "获取代码生成后台MAPPER XML代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createVueApiJsFile:{
			url: `${config.API_URL}/get/gen/createVueApiJsFile`,
			name: "获取Vue 前台API 接口代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createVueManageFile:{
			url: `${config.API_URL}/get/gen/createVueManageFile`,
			name: "获取Vue 前台记录管理页面代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createVueSaveFile:{
			url: `${config.API_URL}/get/gen/createVueSaveFile`,
			name: "获取Vue 前台记录编辑页面代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createVueDetailsFile:{
			url: `${config.API_URL}/get/gen/createVueDetailsFile`,
			name: "获取Vue 前台记录详情页面代码",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	sysGenConfig:{
		insertSysGenConfig:{
			url: `${config.API_URL}/set/gen/insertSysGenConfig`,
			name: "创建代码生成配置",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateSysGenConfig:{
			url: `${config.API_URL}/set/gen/updateSysGenConfig`,
			name: "更新代码生成配置",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getSysGenConfig:{
			url: `${config.API_URL}/get/gen/getSysGenConfig`,
			name: "获取代码生成器配置",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	sysGenTemplate:{
		getSysGenTemplate:{
			url: `${config.API_URL}/get/gen/getSysGenTemplate`,
			name: "获取代码模版信息",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	sysGenDb:{
		getGenTableNameList:{
			url: `${config.API_URL}/get/gen/getGenTableNameList`,
			name: "获取所有数据库表列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	}
}
