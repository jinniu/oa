import config from "@/config"
import http from "@/utils/request"

export default {
	expenseClaim:{
		getExpenseClaimList:{
			url: `${config.API_URL}/get/finance/getExpenseClaimList`,
			name: "获取人员报销列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	expense:{
		insertExpenseAccount:{
			url: `${config.API_URL}/set/finance/insertExpenseAccount`,
			name: "创建报用报销名称",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteExpenseAccount:{
			url: `${config.API_URL}/set/finance/deleteExpenseAccount`,
			name: "删除费用报销名称",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteExpenseAccountByIds:{
			url: `${config.API_URL}/set/finance/deleteExpenseAccountByIds`,
			name: "批量删除费用科目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateExpenseAccount:{
			url: `${config.API_URL}/set/finance/updateExpenseAccount`,
			name: "更新费用报销名称",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getExpenseAccountTree:{
			url: `${config.API_URL}/get/finance/getExpenseAccountTree`,
			name: "获取科目树型结构",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getExpenseAccountForSelect:{
			url: `${config.API_URL}/get/finance/getExpenseAccountForSelect`,
			name: "获取报销费用类型",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertExpenseComConfig:{
			url: `${config.API_URL}/set/finance/insertExpenseComConfig`,
			name: "创建公司年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteExpenseComConfig:{
			url: `${config.API_URL}/set/finance/deleteExpenseComConfig`,
			name: "删除公司年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteExpenseComConfigByIds:{
			url: `${config.API_URL}/set/finance/deleteExpenseComConfigByIds`,
			name: "批量删除公司年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateExpenseComConfig:{
			url: `${config.API_URL}/set/finance/updateExpenseComConfig`,
			name: "更新公司年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getExpenseComConfigList:{
			url: `${config.API_URL}/get/finance/getExpenseComConfigList`,
			name: "获取公司年度费用预算列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertExpenseUserConfig:{
			url: `${config.API_URL}/set/finance/insertExpenseUserConfig`,
			name: "创建人员年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteExpenseUserConfig:{
			url: `${config.API_URL}/set/finance/deleteExpenseUserConfig`,
			name: "删除人员年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteExpenseUserConfigByIds:{
			url: `${config.API_URL}/set/finance/deleteExpenseUserConfigByIds`,
			name: "批量删除人员年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateExpenseUserConfig:{
			url: `${config.API_URL}/set/finance/updateExpenseUserConfig`,
			name: "更新人员年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getExpenseUserConfigList:{
			url: `${config.API_URL}/get/finance/getExpenseUserConfigList`,
			name: "获取人员年度预算列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertExpenseDeptConfig:{
			url: `${config.API_URL}/set/finance/insertExpenseDeptConfig`,
			name: "创建部门年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteExpenseDeptConfig:{
			url: `${config.API_URL}/set/finance/deleteExpenseDeptConfig`,
			name: "删除部门年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteExpenseDeptConfigByIds:{
			url: `${config.API_URL}/set/finance/deleteExpenseDeptConfigByIds`,
			name: "批量删除部门年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateExpenseDeptConfig:{
			url: `${config.API_URL}/set/finance/updateExpenseDeptConfig`,
			name: "更新部门年度费用预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getExpenseDeptConfigList:{
			url: `${config.API_URL}/get/finance/getExpenseDeptConfigList`,
			name: "获取部门年度预算列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	contract:{
		getContractTop:{
			url: `${config.API_URL}/get/finance/getContractTop`,
			name: "获取近期的合同列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getContractBillTop:{
			url: `${config.API_URL}/get/finance/getContractBillTop`,
			name: "获取近期发票列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getPayableRecordTop:{
			url: `${config.API_URL}/get/finance/getPayableRecordTop`,
			name: "近期付款记录",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getReceivablesRecordTop:{
			url: `${config.API_URL}/get/finance/getReceivablesRecordTop`,
			name: "近期收款记录",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getContractDeliveryList:{
			url: `${config.API_URL}/get/finance/getContractDeliveryList`,
			name: "获取发货列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertContractDelivery:{
			url: `${config.API_URL}/set/finance/insertContractDelivery`,
			name: "添加发货记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractDelivery:{
			url: `${config.API_URL}/set/finance/deleteContractDelivery`,
			name: "删除发货记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractDeliveryByIds:{
			url: `${config.API_URL}/set/finance/deleteContractDeliveryByIds`,
			name: "批量删除发货记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateContractDelivery:{
			url: `${config.API_URL}/set/finance/updateContractDelivery`,
			name: "更新发货记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getContractBillList:{
			url: `${config.API_URL}/get/finance/getContractBillList`,
			name: "获取票据列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertContractBill:{
			url: `${config.API_URL}/set/finance/insertContractBill`,
			name: "添加票据",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractBill:{
			url: `${config.API_URL}/set/finance/deleteContractBill`,
			name: "删除票据",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractBillByIds:{
			url: `${config.API_URL}/set/finance/deleteContractBillByIds`,
			name: "批量删除票据",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateContractBill:{
			url: `${config.API_URL}/set/finance/updateContractBill`,
			name: "更新票据记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getContractPayableRecordList:{
			url: `${config.API_URL}/get/finance/getContractPayableRecordList`,
			name: "获取付款记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getAllContractPayableRecordList:{
			url: `${config.API_URL}/get/finance/getAllContractPayableRecordList`,
			name: "获取历史付款记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertContractPayableRecord:{
			url: `${config.API_URL}/set/finance/insertContractPayableRecord`,
			name: "添加付款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractPayableRecord:{
			url: `${config.API_URL}/set/finance/deleteContractPayableRecord`,
			name: "删除付款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateContractPayableRecord:{
			url: `${config.API_URL}/set/finance/updateContractPayableRecord`,
			name: "更新付款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getContractPayableList:{
			url: `${config.API_URL}/get/finance/getContractPayableList`,
			name: "获取应付款列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertContractPayable:{
			url: `${config.API_URL}/set/finance/insertContractPayable`,
			name: "创建应付款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractPayableByIds:{
			url: `${config.API_URL}/set/finance/deleteContractPayableByIds`,
			name: "批量删除应付款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractPayable:{
			url: `${config.API_URL}/set/finance/deleteContractPayable`,
			name: "删除应付款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateContractPayable:{
			url: `${config.API_URL}/set/finance/updateContractPayable`,
			name: "更新应付款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getContractReceivablesRecordList:{
			url: `${config.API_URL}/get/finance/getContractReceivablesRecordList`,
			name: "获取收款记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getAllContractReceivablesRecordList:{
			url: `${config.API_URL}/get/finance/getAllContractReceivablesRecordList`,
			name: "查询历史收款记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertContractReceivablesRecord:{
			url: `${config.API_URL}/set/finance/insertContractReceivablesRecord`,
			name: "添加收款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractReceivablesRecord:{
			url: `${config.API_URL}/set/finance/deleteContractReceivablesRecord`,
			name: "删除收款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateContractReceivablesRecord:{
			url: `${config.API_URL}/set/finance/updateContractReceivablesRecord`,
			name: "更新收款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		insertContractReceivables:{
			url: `${config.API_URL}/set/finance/insertContractReceivables`,
			name: "创建应收款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractReceivables:{
			url: `${config.API_URL}/set/finance/deleteContractReceivables`,
			name: "删除应收款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractReceivablesByIds:{
			url: `${config.API_URL}/set/finance/deleteContractReceivablesByIds`,
			name: "批量删除应收款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateContractReceivables:{
			url: `${config.API_URL}/set/finance/updateContractReceivables`,
			name: "更新应收款记录",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getContractReceivablesList:{
			url: `${config.API_URL}/get/finance/getContractReceivablesList`,
			name: "获取应收款列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		queryContract:{
			url: `${config.API_URL}/get/finance/queryContract`,
			name: "合同查询列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getContractById:{
			url: `${config.API_URL}/get/finance/getContractById`,
			name: "合同详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getContractForSelect:{
			url: `${config.API_URL}/get/finance/getContractForSelect`,
			name: "获取合同管理列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getContractManageList:{
			url: `${config.API_URL}/get/finance/getContractManageList`,
			name: "获取合同管理列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertContract:{
			url: `${config.API_URL}/set/finance/insertContract`,
			name: "添加合同",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContract:{
			url: `${config.API_URL}/set/finance/deleteContract`,
			name: "删除合同",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractByIds:{
			url: `${config.API_URL}/set/finance/deleteContractByIds`,
			name: "批量删除合同",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateContract:{
			url: `${config.API_URL}/set/finance/updateContract`,
			name: "编辑合同",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		insertContractSort:{
			url: `${config.API_URL}/set/finance/insertContractSort`,
			name: "创建合同分类",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateContractSort:{
			url: `${config.API_URL}/set/finance/updateContractSort`,
			name: "更新合同分类信息",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteContractSort:{
			url: `${config.API_URL}/set/finance/deleteContractSort`,
			name: "删除合同分类",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getContractSortTree:{
			url: `${config.API_URL}/get/finance/getContractSortTree`,
			name: "获取合同分类树结构",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	budget: {
		getBudgetCostList:{
			url: `${config.API_URL}/get/finance/getBudgetCostList`,
			name: "获取项目预算费用支出列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBudgetCost:{
			url: `${config.API_URL}/set/finance/insertBudgetCost`,
			name: "录入费用",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteBudgetCost:{
			url: `${config.API_URL}/set/finance/deleteBudgetCost`,
			name: "删除费用",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateBudgetCost:{
			url: `${config.API_URL}/set/finance/updateBudgetCost`,
			name: "更新费用",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getChildBudgetCostList:{
			url: `${config.API_URL}/get/finance/getChildBudgetCostList`,
			name: "获取子项目费用支出列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBudgetCostApprovalList:{
			url: `${config.API_URL}/get/finance/getBudgetCostApprovalList`,
			name: "获取预算费用审批记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBudgetCostApplyList:{
			url: `${config.API_URL}/get/finance/getBudgetCostApplyList`,
			name: "获取预算申请记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBudgetCostApply:{
			url: `${config.API_URL}/set/finance/insertBudgetCostApply`,
			name: "申请项目预算费用",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteBudgetCostApply:{
			url: `${config.API_URL}/set/finance/deleteBudgetCostApply`,
			name: "删除预算费用申请",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateBudgetCostApply:{
			url: `${config.API_URL}/set/finance/updateBudgetCostApply`,
			name: "更新预算费用申请",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		setCostApprovalStatus:{
			url: `${config.API_URL}/set/finance/setCostApprovalStatus`,
			name: "预算费用申请审批",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getAdjustmentApprovalList:{
			url: `${config.API_URL}/get/finance/getAdjustmentApprovalList`,
			name: "获取等审批列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getAdjustmentApprovalUser:{
			url: `${config.API_URL}/get/finance/getAdjustmentApprovalUser`,
			name: "获取预算调整审批人员列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertBudgetAdjustment:{
			url: `${config.API_URL}/set/finance/insertBudgetAdjustment`,
			name: "添加费用调整申请",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteBudgetAdjustment:{
			url: `${config.API_URL}/set/finance/deleteBudgetAdjustment`,
			name: "删除预算调整申请",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateBudgetAdjustment:{
			url: `${config.API_URL}/set/finance/updateBudgetAdjustment`,
			name: "费用调整申请",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		setBudgetAdjustmentApprovalStatus:{
			url: `${config.API_URL}/set/finance/setBudgetAdjustmentApprovalStatus`,
			name: "审批时更新项目预算",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getCostApprovalUser:{
			url: `${config.API_URL}/get/finance/getCostApprovalUser`,
			name: "获取费用预算申请审批人",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getAdjustmentApplyList:{
			url: `${config.API_URL}/get/finance/getAdjustmentApplyList`,
			name: "获取申请列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getAllParentBudgetProject:{
			url: `${config.API_URL}/get/finance/getAllParentBudgetProject`,
			name: "获取所有父级项目",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getAllBudgetProjectList:{
			url: `${config.API_URL}/get/finance/getAllBudgetProjectList`,
			name: "获取预算项目列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertChildBudgetProject:{
			url: `${config.API_URL}/set/finance/insertChildBudgetProject`,
			name: "创建预算子项目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		insertBudgetProject:{
			url: `${config.API_URL}/set/finance/insertBudgetProject`,
			name: "创建预算项目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteBudgetProject:{
			url: `${config.API_URL}/set/finance/deleteBudgetProject`,
			name: "删除预算项目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteBudgetProjectByIds:{
			url: `${config.API_URL}/set/finance/deleteBudgetProjectByIds`,
			name: "批量删除预算项目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateBudgetProject:{
			url: `${config.API_URL}/set/finance/updateBudgetProject`,
			name: "更新预算项目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getBudgetAccountForSelect:{
			url: `${config.API_URL}/get/finance/getBudgetAccountForSelect`,
			name: "获取账套列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBudgetProjectList:{
			url: `${config.API_URL}/get/finance/getBudgetProjectList`,
			name: "获取项目列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBudgetAccount:{
			url: `${config.API_URL}/set/finance/insertBudgetAccount`,
			name: "添加预算科目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteBudgetAccount:{
			url: `${config.API_URL}/set/finance/deleteBudgetAccount`,
			name: "删除预算科目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteBudgetAccountIds:{
			url: `${config.API_URL}/set/finance/deleteBudgetAccountIds`,
			name: "批量删除预算科目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateBudgetAccount:{
			url: `${config.API_URL}/set/finance/updateBudgetAccount`,
			name: "更新预算科目",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getAllBudgetAccountList:{
			url: `${config.API_URL}/get/finance/getAllBudgetAccountList`,
			name: "获取账套树结构",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBudgetConfig: {
			url: `${config.API_URL}/get/finance/getBudgetConfig`,
			name: "获取费用预算配置详情",
			get: async function () {
				return await http.get(this.url);
			}
		},
		setBudgetConfig:{
			url: `${config.API_URL}/set/finance/setBudgetConfig`,
			name: "创建配置",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		}
	}
}
