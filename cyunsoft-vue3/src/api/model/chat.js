import config from "@/config"
import http from "@/utils/request"

export default {
	dingDing:{
		getDdJSAPITicket:{
			url: `${config.API_URL}/get/chat/getDdJSAPITicket`,
			name: "获取微信用户的JSAPITicket",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDdPcLogin:{
			url: `${config.API_URL}/get/chat/getDdPcLogin`,
			name: "获取钉钉用户信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDingDingUserInfo:{
			url: `${config.API_URL}/get/chat/getDingDingUserInfo`,
			name: "获取钉钉用户信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDdConfig:{
			url: `${config.API_URL}/get/chat/getDdConfig`,
			name: "获取钉钉APP-KEY配置信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	weiXin:{
		getWxJSAPITicket:{
			url: `${config.API_URL}/get/chat/getWxJSAPITicket`,
			name: "获取微信用户的JSAPITicket",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getWxPcLogin:{
			url: `${config.API_URL}/get/chat/getWxPcLogin`,
			name: "获取微信用户信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getWxUserInfo:{
			url: `${config.API_URL}/get/chat/getWxUserInfo`,
			name: "获取微信用户信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getWxConfig:{
			url: `${config.API_URL}/get/chat/getWxConfig`,
			name: "获取微信-KEY配置信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	app:{
		doAppLogin:{
			url: `${config.API_URL}/get/chat/doAppLogin`,
			name: "APP用户登陆",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getAppInfo:{
			url: `${config.API_URL}/get/chat/getAppInfo`,
			name: "获取APP信息信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	appConfig:{
		getMyAppListInfo:{
			url: `${config.API_URL}/get/chat/getMyAppListInfo`,
			name: "获取权限内APP功能列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertAppConfig:{
			url: `${config.API_URL}/set/chat/insertAppConfig`,
			name: "添加APP应用",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		deleteAppConfig:{
			url: `${config.API_URL}/set/chat/deleteAppConfig`,
			name: "删除APP应用",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateAppConfig:{
			url: `${config.API_URL}/set/chat/updateAppConfig`,
			name: "更新APP应用",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getAppParentListForSelect:{
			url: `${config.API_URL}/get/chat/getAppParentListForSelect`,
			name: "获取全部群",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getAllAppConfigTree:{
			url: `${config.API_URL}/get/chat/getAllAppConfigTree`,
			name: "获取全部APP功能列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	group:{
		addChatGroup:{
			url: `${config.API_URL}/set/chat/addChatGroup`,
			name: "创建群",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		updateChatGroup:{
			url: `${config.API_URL}/set/chat/updateChatGroup`,
			name: "更新群信息",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		setStopChatGroup:{
			url: `${config.API_URL}/set/chat/setStopChatGroup`,
			name: "禁用群",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		setOpenChatGroup:{
			url: `${config.API_URL}/set/chat/setOpenChatGroup`,
			name: "开启群",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getAllChatGroupList:{
			url: `${config.API_URL}/get/chat/getAllChatGroupList`,
			name: "获取全部群",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	user:{
		updateUserPassword:{
			url: `${config.API_URL}/set/chat/updateUserPassword`,
			name: "重置用户密码",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		getChatUserInfoByDeptId:{
			url: `${config.API_URL}/get/chat/getChatUserInfoByDeptId`,
			name: "获取全部群",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		addChatAccount:{
			url: `${config.API_URL}/set/chat/addChatAccount`,
			name: "创建即时通讯账户",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		setStopLogin:{
			url: `${config.API_URL}/set/chat/setStopLogin`,
			name: "禁用即时通讯账号",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		},
		setOpenLogin:{
			url: `${config.API_URL}/set/chat/setOpenLogin`,
			name: "开启即时通讯账户",
			post: async function (params) {
				return await http.post(this.url, params, {
					headers: {}
				});
			}
		}
	}
}
