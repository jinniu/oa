import config from "@/config"
import http from "@/utils/request"

export default {
	webEmail:{
		deleteWebMail:{
			url: `${config.API_URL}/set/email/deleteWebMail`,
			name: "删除指定的web邮件",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMyWebEmail:{
			url: `${config.API_URL}/get/email/getMyWebEmail`,
			name: "获取人个外部邮件",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	getMySendEmailCount:{
		url: `${config.API_URL}/get/email/getMySendEmailCount`,
		name: "获取个人发送邮件数量",
		get: async function () {
			return await http.get(this.url);
		}
	},
	getMyEmailAllForApp:{
		url: `${config.API_URL}/get/email/getMyEmailAllForApp`,
		name: "移动端内部邮件",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getEmailListForDesk:{
		url: `${config.API_URL}/get/email/getEmailListForDesk`,
		name: "获取消息互通的组织机构列表",
		get: async function () {
			return await http.get(this.url);
		}
	},
	getEmailForwardOrgList:{
		url: `${config.API_URL}/get/email/getEmailForwardOrgList`,
		name: "获取消息互通的组织机构列表",
		get: async function () {
			return await http.get(this.url);
		}
	},
	getEmailForwardConfig:{
		url: `${config.API_URL}/get/email/getEmailForwardConfig`,
		name: "获取内部邮件转发配置",
		get: async function () {
			return await http.get(this.url);
		}
	},
	getMyForwardRecordList:{
		url: `${config.API_URL}/get/email/getMyForwardRecordList`,
		name: "获取互通消息列表",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	sendEmailForwardRecord:{
		url: `${config.API_URL}/set/email/sendEmailForwardRecord`,
		name: "发送互通消息",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	deleteEmailForwardConfig:{
		url: `${config.API_URL}/set/email/deleteEmailForwardConfig`,
		name: "删除邮件转发人员",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	setEmailForwardConfig:{
		url: `${config.API_URL}/set/email/setEmailForwardConfig`,
		name: "设置邮件转发接收人员",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	insertEmailBox:{
		url: `${config.API_URL}/set/email/insertEmailBox`,
		name: "创建邮件文件夹",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	deleteEmailBox:{
		url: `${config.API_URL}/set/email/deleteEmailBox`,
		name: "删除邮件文件夹",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	updateEmailBox:{
		url: `${config.API_URL}/set/email/updateEmailBox`,
		name: "更新邮件文件夹",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	getEmailBoxList:{
		url: `${config.API_URL}/get/email/getEmailBoxList`,
		name: "获取邮件文件夹列表",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getAllEmailBoxList:{
		url: `${config.API_URL}/get/email/getAllEmailBoxList`,
		name: "获取邮件文件夹列表",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getEmailReadStatus:{
		url: `${config.API_URL}/get/email/getEmailReadStatus`,
		name: "获取邮件的查收情况",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getEmailCount:{
		url: `${config.API_URL}/get/email/getEmailCount`,
		name: "获取各类型邮件总数",
		get: async function () {
			return await http.get(this.url);
		}
	},
	sendEmail:{
		url: `${config.API_URL}/set/email/sendEmail`,
		name: "发送邮件",
		post: async function (data) {
			return await http.bigPost(this.url, data,
				{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
			);
		}
	},
	saveEmail:{
		url: `${config.API_URL}/set/email/saveEmail`,
		name: "保存草稿",
		post: async function (data) {
			return await http.bigPost(this.url, data,
				{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
			);
		}
	},
	getMyEmailAll: {
		url: `${config.API_URL}/get/email/getMyEmailAll`,
		name: "获取所有邮件",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getStarEmail: {
		url: `${config.API_URL}/get/email/getStarEmail`,
		name: "获取标星邮件",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getSendBoxEmail:{
		url: `${config.API_URL}/get/email/getSendBoxEmail`,
		name: "获取标星邮件",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getDraftBoxEmail:{
		url: `${config.API_URL}/get/email/getDraftBoxEmail`,
		name: "获取草稿箱内容列表",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getMyDeleteEmailAll:{
		url: `${config.API_URL}/get/email/getMyDeleteEmailAll`,
		name: "获取回收站邮件",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getMyEmailById:{
		url: `${config.API_URL}/get/email/getMyEmailById`,
		name: "获取邮件详情",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	setEmailBoxEmailByIds:{
		url: `${config.API_URL}/set/email/setEmailBoxEmailByIds`,
		name: "批量设置邮件文件夹",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	deleteEmail:{
		url: `${config.API_URL}/set/email/deleteEmail`,
		name: "删除自己接收的邮件",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	setEmailReadStatus:{
		url: `${config.API_URL}/set/email/setEmailReadStatus`,
		name: "获取内部邮件详情",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	deleteEmailByIds:{
		url: `${config.API_URL}/set/email/deleteEmailByIds`,
		name: "删除自己接收的邮件",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	updateEmail:{
		url: `${config.API_URL}/set/email/updateEmail`,
		name: "更新个人邮件",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	cancelEmailStars:{
		url: `${config.API_URL}/set/email/cancelEmailStars`,
		name: "批量取消邮件星标记",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	deleteMySendEmail:{
		url: `${config.API_URL}/set/email/deleteMySendEmail`,
		name: "删除自己发的邮件",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	deletePhysicalMySendEmail:{
		url: `${config.API_URL}/set/email/deletePhysicalMySendEmail`,
		name: "批量删除自己发的邮件",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	deleteMySendEmailByIds:{
		url: `${config.API_URL}/set/email/deleteMySendEmailByIds`,
		name: "批量自己发的邮件",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	deletePhysicalMySendEmailByIds:{
		url: `${config.API_URL}/set/email/deletePhysicalMySendEmailByIds`,
		name: "物理批量自己发的邮件",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	emailConfig: {
		getMyEmailConfig: {
			url: `${config.API_URL}/get/email/getMyEmailConfig`,
			name: "获取个人邮件配置",
			get: async function () {
				return await http.get(this.url);
			}
		},
		setEmailConfig: {
			url: `${config.API_URL}/set/email/setEmailConfig`,
			name: "设置个人邮件服务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	}
}
