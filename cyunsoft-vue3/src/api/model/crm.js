import config from "@/config"
import http from "@/utils/request"

export default {
	crmOrderMx:{
		getCrmOrderMxList:{
			url: `${config.API_URL}/get/crm/getCrmOrderMxList`,
			name: "获取订单明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	crmOrder:{
		insertCrmOrder:{
			url: `${config.API_URL}/set/crm/insertCrmOrder`,
			name: "创建订单记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmOrderAndMx:{
			url: `${config.API_URL}/set/crm/deleteCrmOrderAndMx`,
			name: "删除订单记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmOrder:{
			url: `${config.API_URL}/set/crm/updateCrmOrder`,
			name: "更新订单付款状态",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmOrderAndMx:{
			url: `${config.API_URL}/set/crm/updateCrmOrderAndMx`,
			name: "更新订单记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getCrmOrderList:{
			url: `${config.API_URL}/get/crm/getCrmOrderList`,
			name: "获取订单列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
	},
	crmCare:{
		approvedCrmCare:{
			url: `${config.API_URL}/set/crm/approvedCrmCare`,
			name: "客户关怀审批",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		insertCrmCare:{
			url: `${config.API_URL}/set/crm/insertCrmCare`,
			name: "创建客户关怀记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmCare:{
			url: `${config.API_URL}/set/crm/deleteCrmCare`,
			name: "删除客户关怀记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmCareByIds:{
			url: `${config.API_URL}/set/crm/deleteCrmCareByIds`,
			name: "批量删除客户关怀记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmCare:{
			url: `${config.API_URL}/set/crm/updateCrmCare`,
			name: "更新客户关怀记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getCrmCareList:{
			url: `${config.API_URL}/get/crm/getCrmCareList`,
			name: "获取客户关怀记录列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getApprovedCrmCareList:{
			url: `${config.API_URL}/get/crm/getApprovedCrmCareList`,
			name: "获取客户关怀记录列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	crmQuotation:{
		approvedCrmQuotation:{
			url: `${config.API_URL}/set/crm/approvedCrmQuotation`,
			name: "报价单审批",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		insertCrmQuotation:{
			url: `${config.API_URL}/set/crm/insertCrmQuotation`,
			name: "创建报价单",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmQuotation:{
			url: `${config.API_URL}/set/crm/deleteCrmQuotation`,
			name: "删除报价单",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmQuotation:{
			url: `${config.API_URL}/set/crm/updateCrmQuotation`,
			name: "更新报价单",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMyCrmQuotationList:{
			url: `${config.API_URL}/get/crm/getMyCrmQuotationList`,
			name: "获取报价列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyApprovedList:{
			url: `${config.API_URL}/get/crm/getMyApprovedList`,
			name: "获取审批列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCrmQuotationFormJsonById:{
			url: `${config.API_URL}/get/crm/getCrmQuotationFormJsonById`,
			name: "获取报价单明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCrmQuotationListForSelect:{
			url: `${config.API_URL}/get/crm/getCrmQuotationListForSelect`,
			name: "获取报价单列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	crmInquiry:{
		insertCrmInquiry: {
			url: `${config.API_URL}/set/crm/insertCrmInquiry`,
			name: "创建询价单",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmInquiry: {
			url: `${config.API_URL}/set/crm/deleteCrmInquiry`,
			name: "删除询价单",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmInquiryByIds: {
			url: `${config.API_URL}/set/crm/deleteCrmInquiryByIds`,
			name: "删除询价单",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInquiry: {
			url: `${config.API_URL}/set/crm/updateInquiry`,
			name: "更改询价单状态",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getCrmInquiryList:{
			url: `${config.API_URL}/get/crm/getCrmInquiryList`,
			name: "获限权限内的询价单列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCrmInquiryListForSelect:{
			url: `${config.API_URL}/get/crm/getCrmInquiryListForSelect`,
			name: "获取询价单列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCrmInquiryFormJsonById:{
			url: `${config.API_URL}/get/crm/getCrmInquiryFormJsonById`,
			name: "获取询价单明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	crmContract:{
		insertCrmContractRecord: {
			url: `${config.API_URL}/set/crm/insertCrmContractRecord`,
			name: "创建联系记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmContractRecord:{
			url: `${config.API_URL}/set/crm/updateCrmContractRecord`,
			name: "更新联系记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmContractRecord:{
			url: `${config.API_URL}/set/crm/deleteCrmContractRecord`,
			name: "删除客户联系记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmContractRecordByIds:{
			url: `${config.API_URL}/set/crm/deleteCrmContractRecordByIds`,
			name: "批量删除客户联系记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getRecordByCustomerId:{
			url: `${config.API_URL}/get/crm/getRecordByCustomerId`,
			name: "获取企业联系记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getContractRecordList:{
			url: `${config.API_URL}/get/crm/getContractRecordList`,
			name: "获取企业联系记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	crmLinkMan:{
		insertCrmLinkMan: {
			url: `${config.API_URL}/set/crm/insertCrmLinkMan`,
			name: "添加客户联系人",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmLinkMan: {
			url: `${config.API_URL}/set/crm/deleteCrmLinkMan`,
			name: "删除联系人",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmLinkMan: {
			url: `${config.API_URL}/set/crm/updateCrmLinkMan`,
			name: "更新联系人",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMyCrmLinkManAllList: {
			url: `${config.API_URL}/get/crm/getMyCrmLinkManAllList`,
			name: "获取个人客户联系人列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCrmLinkManAllList:{
			url: `${config.API_URL}/get/crm/getCrmLinkManAllList`,
			name: "获取CRM联系人列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCrmLinkManListForSelect:{
			url: `${config.API_URL}/get/crm/getCrmLinkManListForSelect`,
			name: "获取联系人列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	crmCustomer:{
		insertCrmCustomer: {
			url: `${config.API_URL}/set/crm/insertCrmCustomer`,
			name: "客户报备",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		insertCrmCustomerToPool:{
			url: `${config.API_URL}/set/crm/insertCrmCustomerToPool`,
			name: "创建客户",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmCustomer: {
			url: `${config.API_URL}/set/crm/deleteCrmCustomer`,
			name: "删除客户",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmCustomerByIds:{
			url: `${config.API_URL}/set/crm/deleteCrmCustomerByIds`,
			name: "批量删除客户",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmCustomer: {
			url: `${config.API_URL}/set/crm/updateCrmCustomer`,
			name: "更新客户信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getCrmCustomerList: {
			url: `${config.API_URL}/get/crm/getCrmCustomerList`,
			name: "获取客户列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyCustomerListForSelect:{
			url: `${config.API_URL}/get/crm/getMyCustomerListForSelect`,
			name: "获取个人客户列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getAllCustomerListForSelect:{
			url: `${config.API_URL}/get/crm/getAllCustomerListForSelect`,
			name: "获取所有客户列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getAllCrmCustomerList:{
			url: `${config.API_URL}/get/crm/getAllCrmCustomerList`,
			name: "获取客户池信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getCrmCustomerById:{
			url: `${config.API_URL}/get/crm/getCrmCustomerById`,
			name: "获取客户详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	crmDic:{
		getCrmDicTree: {
			url: `${config.API_URL}/get/crm/getCrmDicTree`,
			name: "获取字典树",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getCrmDicList: {
			url: `${config.API_URL}/get/crm/getCrmDicList`,
			name: "字典明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCrmDicParentList: {
			url: `${config.API_URL}/get/crm/getCrmDicParentList`,
			name: "获取字典数据",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/crm/insertCrmDic`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/crm/deleteCrmDic`,
			name: "删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		dropCrmDic: {
			url: `${config.API_URL}/set/crm/dropCrmDic`,
			name: "字典拖拽排序",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmDicByIds: {
			url: `${config.API_URL}/set/crm/deleteCrmDicByIds`,
			name: "批量删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/crm/updateCrmDic`,
			name: "更新字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getCrmDicByCode: {
			url: `${config.API_URL}/get/crm/getCrmDicByCode`,
			name: "获取字典数据",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	},
	crmProduct:{
		insertCrmProduct:{
			url: `${config.API_URL}/set/crm/insertCrmProduct`,
			name: "添加销售产品",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmProduct:{
			url: `${config.API_URL}/set/crm/deleteCrmProduct`,
			name: "删除销售产品",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmProductByIds:{
			url: `${config.API_URL}/set/crm/deleteCrmProductByIds`,
			name: "批量删除销售产品",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmProduct:{
			url: `${config.API_URL}/set/crm/updateCrmProduct`,
			name: "更新销售产品",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		importCrmProductInfo:{
			url: `${config.API_URL}/set/crm/importCrmProductInfo`,
			name: "批量导入产品",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		getCrmProductListBySortId:{
			url: `${config.API_URL}/get/crm/getCrmProductListBySortId`,
			name: "获取产品列表",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getProductListForSelect:{
			url: `${config.API_URL}/get/crm/getProductListForSelect`,
			name: "获取产品列表",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getAllProductListForSelect:{
			url: `${config.API_URL}/get/crm/getAllProductListForSelect`,
			name: "获取所有产品列表",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getCrmProductListByIdsForSelect:{
			url: `${config.API_URL}/get/crm/getCrmProductListByIdsForSelect`,
			name: "获取产品列表",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	},
	crmProductSort:{
		insertCrmProductSort:{
			url: `${config.API_URL}/set/crm/insertCrmProductSort`,
			name: "添加产品分类",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteCrmProductSort:{
			url: `${config.API_URL}/set/crm/deleteCrmProductSort`,
			name: "删除产品分类",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateCrmProductSort:{
			url: `${config.API_URL}/set/crm/updateCrmProductSort`,
			name: "更新产品分类",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getCrmProductSortTree: {
			url: `${config.API_URL}/get/crm/getCrmProductSortTree`,
			name: "获取字典数据",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	},
	crmRole:{
		setCrmRole:{
			url: `${config.API_URL}/set/crm/setCrmRole`,
			name: "设置CRM权限",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getCrmRole: {
			url: `${config.API_URL}/get/crm/getCrmRole`,
			name: "获取CRM管理权限",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getCrmApprovedUserList:{
			url: `${config.API_URL}/get/crm/getCrmApprovedUserList`,
			name: "获取审批人员列表",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	echarts:{
		getBiQuotationByDeptPie:{
			url: `${config.API_URL}/get/crm/getBiQuotationByDeptPie`,
			name: "部门报价单占比前10的占比",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiInquiryByPayTypePie:{
			url: `${config.API_URL}/get/crm/getBiInquiryByPayTypePie`,
			name: "获取支付类型分类",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiQuotationByMonthLine:{
			url: `${config.API_URL}/get/crm/getBiQuotationByMonthLine`,
			name: "按月份统计报价单",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiQuotationByAccountPie:{
			url: `${config.API_URL}/get/crm/getBiQuotationByAccountPie`,
			name: "获取报价单人员占比",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiInquiryByDeptPie:{
			url: `${config.API_URL}/get/crm/getBiInquiryByDeptPie`,
			name: "部门询价单占比前10的占比",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiInquiryByMonthLine:{
			url: `${config.API_URL}/get/crm/getBiInquiryByMonthLine`,
			name: "按月份统计工作量",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiInquiryByAccountPie:{
			url: `${config.API_URL}/get/crm/getBiInquiryByAccountPie`,
			name: "获取询价单人员占比",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiCustomerIndustryPie:{
			url: `${config.API_URL}/get/crm/getBiCustomerIndustryPie`,
			name: "获取crm的客户行业占比",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiCustomerKeepUserPie:{
			url: `${config.API_URL}/get/crm/getBiCustomerKeepUserPie`,
			name: "获取CRM销售人员的占比",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiCustomerLevelPie:{
			url: `${config.API_URL}/get/crm/getBiCustomerLevelPie`,
			name: "获取CRM客户等级占比",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getBiCustomerSourcePie:{
			url: `${config.API_URL}/get/crm/getBiCustomerSourcePie`,
			name: "获取客户来源等级占比",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}

	}
}
