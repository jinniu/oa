import config from "@/config"
import http from "@/utils/request"

export default {
	dynamics:{
		getDynamicsCommentList:{
			url: `${config.API_URL}/get/info/getDynamicsCommentList`,
			name: "获取动态评论列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDynamicsRecordList:{
			url: `${config.API_URL}/get/info/getDynamicsRecordList`,
			name: "获取动态列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDynamicsRecordForDesk:{
			url: `${config.API_URL}/get/info/getDynamicsRecordForDesk`,
			name: "获取桌面信息动态列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertDynamicsRecord:{
			url: `${config.API_URL}/set/info/insertDynamicsRecord`,
			name: "创建动态信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteDynamicsRecord:{
			url: `${config.API_URL}/set/info/deleteDynamicsRecord`,
			name: "删除动态信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateDynamicsRecord:{
			url: `${config.API_URL}/set/info/updateDynamicsRecord`,
			name: "更新动态信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertDynamicsComment:{
			url: `${config.API_URL}/set/info/insertDynamicsComment`,
			name: "发表评论",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteDynamicsComment:{
			url: `${config.API_URL}/set/info/deleteDynamicsComment`,
			name: "删除评论",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateDynamicsComment:{
			url: `${config.API_URL}/set/info/updateDynamicsComment`,
			name: "更新评论",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	discuss:{
		getDiscussUserList:{
			url: `${config.API_URL}/get/info/getDiscussUserList`,
			name: "获取讨论区参与人列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDiscussRecordListById:{
			url: `${config.API_URL}/get/info/getDiscussRecordListById`,
			name: "获取子帖",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getTopDiscussRecordList:{
			url: `${config.API_URL}/get/info/getTopDiscussRecordList`,
			name: "获取讨论主题列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		revDiscussRecord:{
			url: `${config.API_URL}/set/info/revDiscussRecord`,
			name: "回帖",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertDiscussRecord:{
			url: `${config.API_URL}/set/info/insertDiscussRecord`,
			name: "发贴",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteDiscussRecord:{
			url: `${config.API_URL}/set/info/deleteDiscussRecord`,
			name: "删帖",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateDiscussRecord:{
			url: `${config.API_URL}/set/info/updateDiscussRecord`,
			name: "更新帖子",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getDiscussNoticeForManage:{
			url: `${config.API_URL}/get/info/getDiscussNoticeForManage`,
			name: "获取通知公告管理列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertDiscussNotice:{
			url: `${config.API_URL}/set/info/insertDiscussNotice`,
			name: "发布通知公告",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteDiscussNotice:{
			url: `${config.API_URL}/set/info/deleteDiscussNotice`,
			name: "删除通知公告",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteDiscussNoticeByIds:{
			url: `${config.API_URL}/set/info/deleteDiscussNoticeByIds`,
			name: "批量删除讨论区通知公告",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateDiscussNotice:{
			url: `${config.API_URL}/set/info/updateDiscussNotice`,
			name: "更新通知公告",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getDiscussNoticeList:{
			url: `${config.API_URL}/get/info/getDiscussNoticeList`,
			name: "获取通知公告列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyDiscussList:{
			url: `${config.API_URL}/get/info/getMyDiscussList`,
			name: "获取讨论区列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getDiscussList:{
			url: `${config.API_URL}/get/info/getDiscussList`,
			name: "版块列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertDiscuss:{
			url: `${config.API_URL}/set/info/insertDiscuss`,
			name: "创建版块",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteDiscuss:{
			url: `${config.API_URL}/set/info/deleteDiscuss`,
			name: "删除讨论区",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateDiscuss:{
			url: `${config.API_URL}/set/info/updateDiscuss`,
			name: "更新讨论区",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	bigStory:{
		importBigStory:{
			url: `${config.API_URL}/set/info/importBigStory`,
			name: "导入大记事",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertBigStory:{
			url: `${config.API_URL}/set/info/insertBigStory`,
			name: "创建大记录",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		deleteBigStoryByIds:{
			url: `${config.API_URL}/set/info/deleteBigStoryByIds`,
			name: "批量删除大记事",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteBigStory:{
			url: `${config.API_URL}/set/info/deleteBigStory`,
			name: "删除大记事",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateBigStory:{
			url: `${config.API_URL}/set/info/updateBigStory`,
			name: "更新大记录",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		getBigStoryList:{
			url: `${config.API_URL}/get/info/getBigStoryList`,
			name: "获取所有大记事",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBigStoryListForManage:{
			url: `${config.API_URL}/get/info/getBigStoryListForManage`,
			name: "获取大纪事管理列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	vote:{
		getMyVoteListForApp:{
			url: `${config.API_URL}/get/info/getMyVoteListForApp`,
			name: "获取移动端个人投票列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyVoteListForVoteForDesk:{
			url: `${config.API_URL}/get/info/getMyVoteListForVoteForDesk`,
			name: "获取投票人员桌面列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		voteDetailsForUser:{
			url: `${config.API_URL}/get/info/voteDetailsForUser`,
			name: "获取投票人员列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getVoteStatus:{
			url: `${config.API_URL}/get/info/getVoteStatus`,
			name: "获取投票情况",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getVoteResult:{
			url: `${config.API_URL}/get/info/getVoteResult`,
			name: "获取投票结果",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertVoteResult:{
			url: `${config.API_URL}/set/info/insertVoteResult`,
			name: "人员投票",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getMyVoteListForVote:{
			url: `${config.API_URL}/get/info/getMyVoteListForVote`,
			name: "获取待我投票列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyOldVoteListForVote:{
			url: `${config.API_URL}/get/info/getMyOldVoteListForVote`,
			name: "获取历史投票列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertVote:{
			url: `${config.API_URL}/set/info/insertVote`,
			name: "添加投票记录",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		updateVote:{
			url: `${config.API_URL}/set/info/updateVote`,
			name: "更新投票记录",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		deleteVoteByIds:{
			url: `${config.API_URL}/set/info/deleteVoteByIds`,
			name: "批量删除投票记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVote:{
			url: `${config.API_URL}/set/info/deleteVote`,
			name: "删除投票记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getVoteListForManage:{
			url: `${config.API_URL}/get/info/getVoteListForManage`,
			name: "获取投票管理列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyVoteById:{
			url: `${config.API_URL}/get/info/getMyVoteById`,
			name: "获取投票详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	addressBook:{
		insertAddressBook:{
			url: `${config.API_URL}/set/info/insertAddressBook`,
			name: "创建通讯录人员",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteAddressBook:{
			url: `${config.API_URL}/set/info/deleteAddressBook`,
			name: "删除通讯录联系方式",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateAddressBook:{
			url: `${config.API_URL}/set/info/updateAddressBook`,
			name: "更新通讯录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		importAddressBookForExcel:{
			url: `${config.API_URL}/set/info/importAddressBookForExcel`,
			name: "导入通讯录",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		getMyShareAddressBookList:{
			url: `${config.API_URL}/get/info/getMyShareAddressBookList`,
			name: "获取他人共享的联系人",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyFriendsByLevel:{
			url: `${config.API_URL}/get/info/getMyFriendsByLevel`,
			name: "获取同事列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyAddressBookList:{
			url: `${config.API_URL}/get/info/getMyAddressBookList`,
			name: "获取个人通讯录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	uploadData:{
		getDataUploadInfoById:{
			url: `${config.API_URL}/get/info/getDataUploadInfoById`,
			name: "获取信息上报详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyDataInfoForDesk:{
			url: `${config.API_URL}/get/info/getMyDataInfoForDesk`,
			name: "获取桌面上报信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertDataUploadHandle:{
			url: `${config.API_URL}/set/info/insertDataUploadHandle`,
			name: "添加事件处理结果",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteDataUploadHandle:{
			url: `${config.API_URL}/set/info/deleteDataUploadHandle`,
			name: "删除事件处理结果",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateDataUploadHandle:{
			url: `${config.API_URL}/set/info/updateDataUploadHandle`,
			name: "更新处理结果",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getDataUploadHandleById:{
			url: `${config.API_URL}/get/info/getDataUploadHandleById`,
			name: "获取信息处理详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getToProcessInfoList:{
			url: `${config.API_URL}/get/info/getToProcessInfoList`,
			name: "获取持处理的信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDataUploadInfoList:{
			url: `${config.API_URL}/get/info/getDataUploadInfoList`,
			name: "获取上报信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertDataUploadInfo:{
			url: `${config.API_URL}/set/info/insertDataUploadInfo`,
			name: "上报信息",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		updateDataUploadInfo:{
			url: `${config.API_URL}/set/info/updateDataUploadInfo`,
			name: "更新上报信息",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		deleteDataUploadInfo:{
			url: `${config.API_URL}/set/info/deleteDataUploadInfo`,
			name: "删除上报信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		}
	},
	LeaderMailbox:{
		getMyLeaderMailBoxList:{
			url: `${config.API_URL}/get/info/getMyLeaderMailBoxList`,
			name: "获取领导信箱信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeaderMailBoxList:{
			url: `${config.API_URL}/get/info/getLeaderMailBoxList`,
			name: "获取领导信箱记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		setLeaderMailBox:{
			url: `${config.API_URL}/set/info/setLeaderMailBox`,
			name: "发送领导信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteLeaderMailbox:{
			url: `${config.API_URL}/set/info/deleteLeaderMailbox`,
			name: "删除领导信箱信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		setReadLeaderMailbox:{
			url: `${config.API_URL}/set/info/setReadLeaderMailbox`,
			name: "更新领导信箱接收状态信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateLeaderMailbox:{
			url: `${config.API_URL}/set/info/updateLeaderMailbox`,
			name: "更新领导信箱信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		setLeaderMailboxConfig:{
			url: `${config.API_URL}/set/info/setLeaderMailboxConfig`,
			name: "更新领导信息配置",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getLeaderMailboxConfig:{
			url: `${config.API_URL}/get/info/getLeaderMailboxConfig`,
			name: "领导信箱设置祥情",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	leadActivity:{
		getLeadActivityById:{
			url: `${config.API_URL}/get/info/getLeadActivityById`,
			name: "获取领导行程详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyLeadActivityForApp:{
			url: `${config.API_URL}/get/info/getMyLeadActivityForApp`,
			name: "获取移动端领导行程",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeadActivityListForDesk:{
			url: `${config.API_URL}/get/info/getLeadActivityListForDesk`,
			name: "获取桌面领导日程",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeadActivityList:{
			url: `${config.API_URL}/get/info/getLeadActivityList`,
			name: "获取领导行程列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeadActivityQueryList:{
			url: `${config.API_URL}/get/info/getLeadActivityQueryList`,
			name: "个人查询领导行程列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertLeadActivity:{
			url: `${config.API_URL}/set/info/insertLeadActivity`,
			name: "创建领导行程",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		deleteLeadActivityByIds:{
			url: `${config.API_URL}/set/info/deleteLeadActivityByIds`,
			name: "批量删除领导行程",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteLeadActivity: {
			url: `${config.API_URL}/set/info/deleteLeadActivity`,
			name: "删除领导行程",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateLeadActivity: {
			url: `${config.API_URL}/set/info/updateLeadActivity`,
			name: "更新领导行程",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
	},
	emergency:{
		getEmergencyListForManage:{
			url: `${config.API_URL}/get/info/getEmergencyListForManage`,
			name: "获取紧急事件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getEmergencyListForDesk:{
			url: `${config.API_URL}/get/info/getEmergencyListForDesk`,
			name: "获取当前紧急事件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertEmergency: {
			url: `${config.API_URL}/set/info/insertEmergency`,
			name: "发布紧急事件",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		deleteEmergency: {
			url: `${config.API_URL}/set/info/deleteEmergency`,
			name: "删除紧急事件",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteEmergencyByIds:{
			url: `${config.API_URL}/set/info/deleteEmergencyByIds`,
			name: "批量紧急事件",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateEmergency: {
			url: `${config.API_URL}/set/info/updateEmergency`,
			name: "更新紧急事件",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	news: {
		getMyNewsListForApp:{
			url: `${config.API_URL}/get/info/getMyNewsListForApp`,
			name: "获取移动端企业新闻的列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getNewsManageList: {
			url: `${config.API_URL}/get/info/getNewsManageList`,
			name: "获取新闻管理列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyNewsList: {
			url: `${config.API_URL}/get/info/getMyNewsList`,
			name: "获取个人权限内的新闻",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		update: {
			url: `${config.API_URL}/set/info/updateNews`,
			name: "更新新闻",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		insert: {
			url: `${config.API_URL}/set/info/insertNews`,
			name: "发布新闻",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		delete: {
			url: `${config.API_URL}/set/info/deleteNews`,
			name: "删除新闻",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteNewsByIds:{
			url: `${config.API_URL}/set/info/deleteNewsByIds`,
			name: "批量删除新闻",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		setNewsReadStatus: {
			url: `${config.API_URL}/set/info/setNewsReadStatus`,
			name: "设置新闻查看状态",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		getMyPicNewsListForDesk:{
			url: `${config.API_URL}/get/info/getMyPicNewsListForDesk`,
			name: "获取新闻图片",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyNewsListForDesk:{
			url: `${config.API_URL}/get/info/getMyNewsListForDesk`,
			name: "获取新闻图片",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getNewsReadStatus:{
			url: `${config.API_URL}/get/info/getNewsReadStatus`,
			name: "获取桌面新闻",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyNewsById:{
			url: `${config.API_URL}/get/info/getMyNewsById`,
			name: "获取新闻详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	notice:{
		getMyNoticeListForApp:{
			url: `${config.API_URL}/get/info/getMyNoticeListForApp`,
			name: "获取移动端通知公告",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getNoticeReadStatus:{
			url: `${config.API_URL}/get/info/getNoticeReadStatus`,
			name: "获取通知公告人员查看状态",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyNoticeListForDesk:{
			url: `${config.API_URL}/get/info/getMyNoticeListForDesk`,
			name: "获取桌面的通知消息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		updateNotice: {
			url: `${config.API_URL}/set/info/updateNotice`,
			name: "更新通知公告",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		sendNotice: {
			url: `${config.API_URL}/set/info/sendNotice`,
			name: "发布通知公告",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		setNoticeStatus:{
			url: `${config.API_URL}/set/info/setNoticeStatus`,
			name: "更新通知公告状态",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteNotice:{
			url: `${config.API_URL}/set/info/deleteNotice`,
			name: "删除通知公告",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteNoticeByIds:{
			url: `${config.API_URL}/set/info/deleteNoticeByIds`,
			name: "批量删除通知公告",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		approvalNotice:{
			url: `${config.API_URL}/set/info/approvalNotice`,
			name: "审批通知公告",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},

		getMyNoticeList:{
			url: `${config.API_URL}/get/info/getMyNoticeList`,
			name: "获取个人的通知公告",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getReadNoticeByNoticeId:{
			url: `${config.API_URL}/get/info/getReadNoticeByNoticeId`,
			name: "获取查阅的通知公告详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		setNoticeReadStatus:{
			url: `${config.API_URL}/set/info/setNoticeReadStatus`,
			name: "设置通知公告查看状态",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getNoticeListForApproval:{
			url: `${config.API_URL}/get/info/getNoticeListForApproval`,
			name: "获取审批列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getNoticeManageList:{
			url: `${config.API_URL}/get/info/getNoticeManageList`,
			name: "获取通知公告维护列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getNoticeTemplateListByType:{
			url: `${config.API_URL}/get/info/getNoticeTemplateListByType`,
			name: "按分类获取红头列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getNoticeTemplateList:{
			url: `${config.API_URL}/get/info/getNoticeTemplateList`,
			name: "获取通知公告模版",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyNoticeById:{
			url: `${config.API_URL}/get/info/getMyNoticeById`,
			name: "获取公告详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertNoticeTemplate:{
			url: `${config.API_URL}/set/info/insertNoticeTemplate`,
			name: "添加通知公告红头模版",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteNoticeTemplate:{
			url: `${config.API_URL}/set/info/deleteNoticeTemplate`,
			name: "删除红头模版",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteNoticeTemplateByIds:{
			url: `${config.API_URL}/set/info/deleteNoticeTemplateByIds`,
			name: "批量删除通知公告模版",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateNoticeTemplate:{
			url: `${config.API_URL}/set/info/updateNoticeTemplate`,
			name: "更新模版信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getNoticeConfigList:{
			url: `${config.API_URL}/get/info/getNoticeConfigList`,
				name: "获取通知公告配置列表",
				get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		updateNoticeConfig:{
			url: `${config.API_URL}/set/info/updateNoticeConfig`,
			name: "设置通知公告配置信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		}
	},
	sysWebSite:{
		getMySysWebSiteList:{
			url: `${config.API_URL}/get/info/getMySysWebSiteList`,
			name: "获取个人常用网址列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSysWebSite:{
			url: `${config.API_URL}/get/info/getSysWebSite`,
			name: "获取常用网址列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		deleteSysWebSiteBatch:{
			url: `${config.API_URL}/set/info/deleteSysWebSiteBatch`,
			name: "批量删除共享网站",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		insert:{
			url: `${config.API_URL}/set/info/insertSysWebSite`,
			name: "创建共享网站",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete:{
			url: `${config.API_URL}/set/info/deleteSysWebSite`,
			name: "删除共享网站",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update:{
			url: `${config.API_URL}/set/info/updateSysWebSite`,
			name: "更新共享网站",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	bookMark:{
		setBookMark:{
			url: `${config.API_URL}/set/info/setBookMark`,
			name: "更新工作便签信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMyBookMark:{
			url: `${config.API_URL}/get/info/getMyBookMark`,
			name: "获取工作便签信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	openInfo:{
		getOpenInfoListForManage:{
			url: `${config.API_URL}/get/info/getOpenInfoListForManage`,
			name: "获取政务公开信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyOpenInfoList:{
			url: `${config.API_URL}/get/info/getMyOpenInfoList`,
			name: "获取所有政务公开信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getOpenInfoListForDesk:{
			url: `${config.API_URL}/get/info/getOpenInfoListForDesk`,
			name: "获取桌面模块政务公开例表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getOpenInfoById:{
			url: `${config.API_URL}/get/info/getOpenInfoListForDesk`,
			name: "获取政务公开详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertOpenInfo:{
			url: `${config.API_URL}/set/info/insertOpenInfo`,
			name: "发布政务公开信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteOpenInfo:{
			url: `${config.API_URL}/set/info/deleteOpenInfo`,
			name: "删除政务公开信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteOpenInfoByIds:{
			url: `${config.API_URL}/set/info/deleteOpenInfoByIds`,
			name: "批量删除政务公开信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateOpenInfo:{
			url: `${config.API_URL}/set/info/updateOpenInfo`,
			name: "更新政务公开信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		setOpenInfoReaderUser:{
			url: `${config.API_URL}/set/info/setOpenInfoReaderUser`,
			name: "设置查阅人员",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	}
}
