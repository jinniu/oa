import config from "@/config"
import http from "@/utils/request"

export default {
	register: {
		getRegInfo: {
			url: `${config.API_URL}/get/system/getRegInfo`,
			name: "获取软件信息",
			get: async function () {
				return await http.get(this.url);
			}
		},
		regSoft: {
			url: `${config.API_URL}/set/file/regSoft`,
			name: "软件注册",
			post: async function (data, config = {}) {
				var res = await http.postFile(this.url, data, config);
				if (res.code == 200) {
					location.reload()
				}
				return res;
			}
		}
	},
	sysConfigInit: {
		getSysConfigInfo: {
			url: `${config.API_URL}/get/system/getSysConfigInit`,
			name: "获取系统配置",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSysConfigInitForReSetPassword: {
			url: `${config.API_URL}/get/system/getSysConfigInitForReSetPassword`,
			name: "获取重置密码的系统规则",
			get: async function () {
				return await http.get(this.url);
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateSysConfigInit`,
			name: "更新系统配置",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	sysConfig:{
		getSysConfig: {
			url: `${config.API_URL}/get/system/getSysConfig`,
			name: "获取系统服务配置",
			get: async function () {
				return await http.get(this.url);
			}
		},
		setSysConfig: {
			url: `${config.API_URL}/set/system/setSysConfig`,
			name: "更新系统服务配置",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	sysMsgConfig: {
		getSysMsgConfigByCode:
			{
				url: `${config.API_URL}/get/system/getSysMsgConfigByCode`,
				name: "按模块获取消息权限",
				get: async function (params) {
					return await http.get(this.url, params);
				}
			},
		getSysMsgConfigList:
			{
				url: `${config.API_URL}/get/system/getSysMsgConfigList`,
				name: "获取设置权限列表",
				get: async function (params) {
					return await http.get(this.url, params);
				}
			},
		insert: {
			url: `${config.API_URL}/set/system/insertSysMsgConfig`,
			name: "创建MSG提醒",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateSysMsgConfig`,
			name: "更新MSG提醒",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteSysMsgConfig`,
			name: "删除MSG提醒",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	shortMenu:{
		setMyShortMenu:{
			url: `${config.API_URL}/set/system/setMyShortMenu`,
			name: "设置个人的快捷菜单",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getSysMenuInRoleForShortMenu:{
			url: `${config.API_URL}/get/system/getSysMenuInRoleForShortMenu`,
			name: "获取快捷菜单备选",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyShortcutMenu:{
			url: `${config.API_URL}/get/system/getMyShortcutMenu`,
			name: "获取个人的快捷方式",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	menu: {
		myMenus: {
			url: `${config.API_URL}/get/system/getSysMenuInRole`,
			name: "获取我的菜单",
			get: async function () {
				return await http.get(this.url);
			}
		},
		list: {
			url: `${config.API_URL}/get/system/getAllSysMenuListForTree`,
			name: "获取菜单",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertSysMenu`,
			name: "创建菜单",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateSysMenu`,
			name: "更新菜单",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		position: {
			url: `${config.API_URL}/set/system/updateSysMenuPosition`,
			name: "更新菜单",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {
						//'response-status': 401
					}
				});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteSysMenu`,
			name: "删除菜单",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {
						//'response-status': 401
					}
				});
			}
		}
	},
	unit:
		{
			getUnit: {
				url: `${config.API_URL}/get/system/getUnit`,
				name: "获取单位信息",
				get: async function () {
					return await http.get(this.url);
				}
			},
			getUnitListForSelect:{
				url: `${config.API_URL}/get/system/getUnitListForSelect`,
				name: "获取组织机构列表",
				get: async function () {
					return await http.get(this.url);
				}
			},
			update: {
				url: `${config.API_URL}/set/system/updateUnit`,
				name: "更新单位信息",
				post: async function (data) {
					return await http.post(this.url, data, {
						headers: {
							//'response-status': 401
						}
					});
				}
			},
		},
	userRole: {
		insert: {
			url: `${config.API_URL}/set/system/insertUserRole`,
			name: "创建角色",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateUserRole`,
			name: "更新角色",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteUserRole`,
			name: "删除角色",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteUserRoleByIds: {
			url: `${config.API_URL}/set/system/deleteUserRoleByIds`,
			name: "批量删除角色",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		list: {
			url: `${config.API_URL}/get/system/getAllUserRole`,
			name: "获取角色列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllUserRoleForSelect: {
			url: `${config.API_URL}/get/system/getAllUserRoleForSelect`,
			name: "获取角色列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getPermissionByRoleId: {
			url: `${config.API_URL}/get/system/getPermissionByRoleId`,
			name: "获取权限",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		cloneUserRole:{
			url: `${config.API_URL}/set/system/cloneUserRole`,
			name: "克隆权限",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	unitDept: {
		getExpDeptRecord:{
			url: `${config.API_URL}/get/system/getExpDeptRecord`,
			name: "导出部门列表",
			fileDownload: async function (data) {
				return await http.fileDownload(this.url, data,{
					headers: {}
				})
			}
		},
		list: {
			url: `${config.API_URL}/get/system/getAllUnitDeptList`,
			name: "获取部门列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllUnitDeptList: {
			url: `${config.API_URL}/get/system/getAllUnitDeptList`,
			name: "获取部门列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getDeptListByDeptIdForSelect: {
			url: `${config.API_URL}/get/system/getDeptListByDeptIdForSelect`,
			name: "获取备选部门列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getDeptListByDeptIds: {
			url: `${config.API_URL}/get/system/getDeptListByDeptIds`,
			name: "按部部ID获取部门列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertUnitDept: {
			url: `${config.API_URL}/set/system/insertUnitDept`,
			name: "创建部门",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateUnitDept: {
			url: `${config.API_URL}/set/system/updateUnitDept`,
			name: "更新部门",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteUnitDept: {
			url: `${config.API_URL}/set/system/deleteUnitDept`,
			name: "删除部门",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteUnitDeptByBatch: {
			url: `${config.API_URL}/set/system/deleteUnitDeptByBatch`,
			name: "批量删除部门",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		syncDingDingUnitDept:{
			url: `${config.API_URL}/set/system/syncDingDingUnitDept`,
			name: "同步部门到钉钉后台",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		syncWeiXinUnitDept:{
			url: `${config.API_URL}/set/system/syncWeiXinUnitDept`,
			name: "同步部门到企业微信后台",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		importUnitDept:{
			url: `${config.API_URL}/set/system/importUnitDept`,
			name: "批量导入部门",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		setInitUnitDept: {
			url: `${config.API_URL}/set/system/setInitUnitDept`,
			name: "初始化当前组织下部门",
			post: async function () {
				return await http.post(this.url, null, {
					headers: {}
				});
			}
		},
	},
	userOptRole: {
		getAllUserOptRoleList: {
			url: `${config.API_URL}/get/system/getAllUserOptRoleList`,
			name: "获取所有数所操作权限列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getUserOptRoleList: {
			url: `${config.API_URL}/get/system/getUserOptRoleList`,
			name: "获取数据权限列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertUserOptRole`,
			name: "创建数据权限",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateUserOptRole`,
			name: "更新数据权限",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteUserOptRole`,
			name: "删除数据权限",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteUserOptRoleByIds: {
			url: `${config.API_URL}/set/system/deleteUserOptRoleByIds`,
			name: "批量删除数据权限",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		}
	},
	user: {
		getExpUserInfoRecord:{
			url: `${config.API_URL}/get/system/getExpUserInfoRecord`,
			name: "导出用户列表",
			fileDownload: async function (data) {
				return await http.fileDownload(this.url, data,{
					headers: {}
				})
			}
		},
		getAllUserForTree: {
			url: `${config.API_URL}/get/system/getAllUserForTree`,
			name: "获取人员列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getUserInfoById:{
			url: `${config.API_URL}/get/system/getUserInfoById`,
			name: "获取用户详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMySubordinates:{
			url: `${config.API_URL}/get/system/getMySubordinates`,
			name: "获取本人下属用户列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		list: {
			url: `${config.API_URL}/get/system/getUserListByDeptId`,
			name: "获取用户列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getUserListByAccountIds: {
			url: `${config.API_URL}/get/system/getUserListByAccountIds`,
			name: "按账号获取人员列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getUserListByDeptIdForSelect: {
			url: `${config.API_URL}/get/system/getUserListByDeptIdForSelect`,
			name: "获取用户列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		updateNewPassWord: {
			url: `${config.API_URL}/set/system/updateNewPassWord`,
			name: "到期更新密码",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		resetNewPassWord: {
			url: `${config.API_URL}/set/system/resetNewPassWord`,
			name: "到期更新密码",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updatePassWord: {
			url: `${config.API_URL}/set/system/updatePassWord`,
			name: "个人修改密码",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		logOut: {
			url: `${config.API_URL}/set/system/logOut`,
			name: "退出账号",
			post: async function () {
				return await http.post(this.url, null,{});
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertUser`,
			name: "创建账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteUser`,
			name: "管理员删除账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteByAccountIds: {
			url: `${config.API_URL}/set/system/deleteByAccountIds`,
			name: "管理员批量删除账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateUser`,
			name: "更新个人账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateUserInfo: {
			url: `${config.API_URL}/set/system/updateUserInfo`,
			name: "管理人员更新账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		setPassWordByEmail: {
			url: `${config.API_URL}/set/system/setPassWordByEmail`,
			name: "通过邮件找回密码",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		syncWeiXinAccount:{
			url: `${config.API_URL}/set/system/syncWeiXinAccount`,
			name: "同步微信账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		syncDingDingAccount:{
			url: `${config.API_URL}/set/system/syncDingDingAccount`,
			name: "同步钉钉账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		importUser:{
			url: `${config.API_URL}/set/system/importUser`,
			name: "批量导入用户账号",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		cloneAdminDesk:{
			url: `${config.API_URL}/set/system/cloneAdminDesk`,
			name: "克隆管理员桌面",
			post: async function () {
				return await http.post(this.url, null,{headers: {}});
			}
		},
		setInitUser: {
			url: `${config.API_URL}/set/system/setInitUser`,
			name: "初始化当前组织机构下的人员账号",
			post: async function () {
				return await http.post(this.url, null, {headers: {}});
			}
		},
	},
	sysTasks: {
		list: {
			url: `${config.API_URL}/get/system/getSysTasksList`,
			name: "获取定时任务",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertSysTasks`,
			name: "创建定时任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateSysTasks`,
			name: "更新定时任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		startRunSysTasks:{
			url: `${config.API_URL}/set/system/startRunSysTasks`,
			name: "立即执行定时任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteSysTasks`,
			name: "删除定时任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	userLevel: {
		getUserLevelListForManage: {
			url: `${config.API_URL}/get/system/getUserLevelListForManage`,
			name: "获取所有职务列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllUserLevelForSearch: {
			url: `${config.API_URL}/get/system/getAllUserLevelForSearch`,
			name: "获取所有职务",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getUserLevelForSelect:{
			url: `${config.API_URL}/get/system/getUserLevelForSelect`,
			name: "获取所有职务",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertUserLevel`,
			name: "创建职务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteUserLevel`,
			name: "删除职务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteUserLevelByIds: {
			url: `${config.API_URL}/set/system/deleteUserLevelByIds`,
			name: "批量删除职务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateUserLevel`,
			name: "更新职务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	sysLuceneConfig: {
			getAllSysLuceneConfig: {
				url: `${config.API_URL}/get/system/getAllSysLuceneConfig`,
				name: "获取当前全文检索列表",
				get: async function (params) {
					return await http.get(this.url, params);
				}
			},
			setSysLuceneConfig: {
				url: `${config.API_URL}/set/system/setSysLuceneConfig`,
				name: "更新全文检索配置",
				post: async function (data) {
					return await http.post(this.url, data, {headers: {}});
				}
			}
			},
	sysAttachBackConfig: {
		getAllSysAttachBackConfig: {
			url: `${config.API_URL}/get/system/getAllSysAttachBackConfig`,
			name: "获取当前全文检索列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		setSysAttachBackConfig: {
			url: `${config.API_URL}/set/system/setSysAttachBackConfig`,
			name: "更新全文检索配置",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	attendConfig: {
		getAllAttendConfigList: {
			url: `${config.API_URL}/get/system/getAllAttendConfigList`,
			name: "获取系统考勤规则",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAttendConfigById:{
			url: `${config.API_URL}/get/system/getAttendConfigById`,
			name: "获取考勤配置详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},

		getAttendConfigList: {
			url: `${config.API_URL}/get/system/getAttendConfigList`,
			name: "获取考勤规则列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertAttendConfig:{
			url: `${config.API_URL}/set/system/insertAttendConfig`,
			name: "创建考勤规则",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteAttendConfig:{
			url: `${config.API_URL}/set/system/deleteAttendConfig`,
			name: "删除考勤规则",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateAttendConfig:{
			url: `${config.API_URL}/set/system/updateAttendConfig`,
			name: "更新考勤规则",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	sysLog: {
		list: {
			url: `${config.API_URL}/get/system/getSysLogList`,
			name: "获取个人登陆日志列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllSysLogList: {
			url: `${config.API_URL}/get/system/getAllSysLogList`,
			name: "获取系统日志列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllSysLogListBySafetyAuditor:{
			url: `${config.API_URL}/get/system/getAllSysLogListBySafetyAuditor`,
			name: "审计员查看日志列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		deleteSysLog: {
			url: `${config.API_URL}/set/system/deleteSysLog`,
			name: "删除日志",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSysLogByIds: {
			url: `${config.API_URL}/set/system/deleteSysLogByIds`,
			name: "批量删除日志",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSysLogByTime: {
			url: `${config.API_URL}/set/system/deleteSysLogByTime`,
			name: "按时间段删除系统日志",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	monitor: {
		getHardwareInfo: {
			url: `${config.API_URL}/get/system/getHardwareInfo`,
			name: "获取服务器CPU信息",
			get: async function () {
				return await http.get(this.url);
			}
		},
		kickAccount: {
			url: `${config.API_URL}/set/system/kickAccount`,
			name: "踢出账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		clearSysRedis: {
			url: `${config.API_URL}/set/system/clearSysRedis`,
			name: "清空redis缓存",
			post: async function () {
				return await http.post(this.url,null);
			}
		},
		setMainMode: {
			url: `${config.API_URL}/set/system/setMainMode`,
			name: "维护",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		relieveMaintenanceMode: {
			url: `${config.API_URL}/set/system/relieveMaintenanceMode`,
			name: "启用",
			post: async function () {
				return await http.post(this.url,null);
			}
		},
		getSysRunStatus:{
			url: `${config.API_URL}/get/system/getSysRunStatus`,
			name: "获取当前系统是否在维护",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	sysDic: {
		getSysDicTree: {
			url: `${config.API_URL}/get/system/getSysDicTree`,
			name: "获取字典树",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSysDicList: {
			url: `${config.API_URL}/get/system/getSysDicList`,
			name: "字典明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getSysDicParentList: {
			url: `${config.API_URL}/get/system/getSysDicParentList`,
			name: "获取字典数据",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertSysDic`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteSysDic`,
			name: "删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		dropSysDic: {
			url: `${config.API_URL}/set/system/dropSysDic`,
			name: "字典拖拽排序",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSysDicByIds: {
			url: `${config.API_URL}/set/system/deleteSysDicByIds`,
			name: "批量删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateSysDic`,
			name: "更新字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSysDicByCode: {
			url: `${config.API_URL}/get/system/getSysDicByCode`,
			name: "获取字典数据",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
	},
	ddConfig: {
		updateDdConfig: {
			url: `${config.API_URL}/set/system/updateDdConfig`,
			name: "更新企业钉钉配置",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getDdConfig: {
			url: `${config.API_URL}/get/system/getDdConfig`,
			name: "获取企业钉钉配置",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	wxConfig: {
		updateWxConfig: {
			url: `${config.API_URL}/set/system/updateWxConfig`,
			name: "更新企业微信配置",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getWxConfig: {
			url: `${config.API_URL}/get/system/getWxConfig`,
			name: "获取企业微信配置",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	sysDbInterface:{
		insertSysDbInterface: {
			url: `${config.API_URL}/set/system/insertSysDbInterface`,
			name: "创建数据接口",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSysDbInterface: {
			url: `${config.API_URL}/set/system/updateSysDbInterface`,
			name: "更新数据接口",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSysDbInterface: {
			url: `${config.API_URL}/set/system/deleteSysDbInterface`,
			name: "删除数据接口",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSysDbInterfaceByIds: {
			url: `${config.API_URL}/set/system/deleteSysDbInterfaceByIds`,
			name: "批量删除数据接口",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSysDbInterfaceList: {
			url: `${config.API_URL}/get/system/getSysDbInterfaceList`,
			name: "获取数据接口列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSysDbInterfaceForSelect:{
			url: `${config.API_URL}/get/system/getSysDbInterfaceForSelect`,
			name: "获取下拉数据接口列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getDataBySysDbInterface:{
			url: `${config.API_URL}/get/system/getDataBySysDbInterface`,
			name: "获取接口数据",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	dbSourceConfig: {
		getDbSourceListForSelect: {
			url: `${config.API_URL}/get/system/getDbSourceListForSelect`,
			name: "获取数据源选择列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSysDbSourceList: {
			url: `${config.API_URL}/get/system/getSysDbSourceList`,
			name: "获取第三方数据源列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertSysDbSource`,
			name: "创建第三方数据源",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateSysDbSource`,
			name: "更新第三方数据源",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteSysDbSource`,
			name: "删除第三方数据源",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteDbSourceByIds: {
			url: `${config.API_URL}/set/system/deleteDbSourceByIds`,
			name: "批量第三方数据源",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	sysOrgConfig: {
		createOrgConfig: {
			url: `${config.API_URL}/set/system/createOrgConfig`,
			name: "创建新机构",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		clearSysOrg: {
			url: `${config.API_URL}/set/system/clearSysOrg`,
			name: "管理员删除指定机构",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateSysOrgConfig`,
			name: "更新机构信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSysOrgConfigList: {
			url: `${config.API_URL}/get/system/getSysOrgConfigList`,
			name: "获取机构列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
	},
	initialization: {
		doInitialization: {
			url: `${config.API_URL}/set/system/doInitialization`,
			name: "系统初始化",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	sysInterface: {
		setSysInterface: {
			url: `${config.API_URL}/set/system/setSysInterface`,
			name: "设置系统界面",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getSysInterface: {
			url: `${config.API_URL}/get/system/getSysInterface`,
			name: "获取系统界面配置",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSysInterfaceForLogin:{
			url: `${config.API_URL}/get/system/getSysInterfaceForLogin`,
			name: "获取登陆时系统界面配置",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	sysDeskConfig: {
		getAllSysDeskConfig: {
			url: `${config.API_URL}/get/system/getAllSysDeskConfig`,
			name: "获取桌面模块列表配置",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllSysDeskConfigList:{
			url: `${config.API_URL}/get/system/getAllSysDeskConfigList`,
			name: "获取可用桌面模块列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getDeskConfigList: {
			url: `${config.API_URL}/get/system/getDeskConfigList`,
			name: "获取所有桌面模块",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getAllComps: {
			url: `${config.API_URL}/get/system/getAllComps`,
			name: "获取个人桌面模块",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertSysDeskConfig`,
			name: "创建桌面模块",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateSysDeskConfig`,
			name: "更新桌面模块",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteSysDeskConfig`,
			name: "删除桌面模块",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSysDeskConfigByIds: {
			url: `${config.API_URL}/set/system/deleteSysDeskConfigByIds`,
			name: "批量删除桌面模块",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	tempOrg: {
		getTempUnitList: {
			url: `${config.API_URL}/get/system/getTempUnitList`,
			name: "获取虚拟组织列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getTempUnitListForSelect: {
			url: `${config.API_URL}/get/system/getTempUnitListForSelect`,
			name: "获取虚拟组织下拉列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getTempUnitListForSelect2: {
			url: `${config.API_URL}/get/system/getTempUnitListForSelect2`,
			name: "获取虚拟组织下拉列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getPendingTempUnitList: {
			url: `${config.API_URL}/get/system/getPendingTempUnitList`,
			name: "获取虚拟组织筛选列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/system/insertTempUnit`,
			name: "创建虚拟组织",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/system/updateTempUnit`,
			name: "更新虚拟组织",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/system/deleteTempUnit`,
			name: "删除虚拟组织",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getTempUserLevelListForManage: {
			url: `${config.API_URL}/get/system/getTempUserLevelListForManage`,
			name: "获取所有虚拟职务列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getTempUserLevelForSelect: {
			url: `${config.API_URL}/get/system/getTempUserLevelForSelect`,
			name: "获取所有虚拟职务",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllTempUserLevelForSearch:{
			url: `${config.API_URL}/get/system/getAllTempUserLevelForSearch`,
			name: "按要求查询虚拟职务列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertTempUserLevel: {
			url: `${config.API_URL}/set/system/insertTempUserLevel`,
			name: "创建虚拟职务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteTempUserLevel: {
			url: `${config.API_URL}/set/system/deleteTempUserLevel`,
			name: "删除虚拟职务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteTempUserLevelByIds: {
			url: `${config.API_URL}/set/system/deleteTempUserLevelByIds`,
			name: "批量虚拟删除职务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateTempUserLevel: {
			url: `${config.API_URL}/set/system/updateTempUserLevel`,
			name: "更新虚拟职务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getTempDeptList: {
			url: `${config.API_URL}/get/system/getTempDeptList`,
			name: "获取所有虚拟部门",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getDeptListByTempDeptIdForSelect:{
			url: `${config.API_URL}/get/system/getDeptListByTempDeptIdForSelect`,
			name: "按条件获取虚拟部门列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getDeptListByTempDeptIds:{
			url: `${config.API_URL}/get/system/getDeptListByTempDeptIds`,
			name: "按部门ID字符串获取虚拟部门列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllTempUnitDeptForTree: {
			url: `${config.API_URL}/get/system/getAllTempUnitDeptForTree`,
			name: "获取所有虚拟部门树",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertTempUnitDept: {
			url: `${config.API_URL}/set/system/insertTempUnitDept`,
			name: "创建虚拟部门",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateTempUnitDept: {
			url: `${config.API_URL}/set/system/updateTempUnitDept`,
			name: "更新虚拟部门",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTempUnitDept: {
			url: `${config.API_URL}/set/system/deleteTempUnitDept`,
			name: "删除虚拟部门",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTempUnitDeptByBatch: {
			url: `${config.API_URL}/set/system/deleteTempUnitDeptByBatch`,
			name: "批量删除部门",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getTempUserByDeptId: {
			url: `${config.API_URL}/get/system/getTempUserByDeptId`,
			name: "获取所有虚拟部门下人员",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getUserListByTempDeptIdForSelect:{
			url: `${config.API_URL}/get/system/getUserListByTempDeptIdForSelect`,
			name: "获取所有虚拟部门下人员",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertTempUser: {
			url: `${config.API_URL}/set/system/insertTempUser`,
			name: "创建组织人员",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateTempUser: {
			url: `${config.API_URL}/set/system/updateTempUser`,
			name: "更新虚拟组织人员",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTempUser: {
			url: `${config.API_URL}/set/system/deleteTempUser`,
			name: "删除虚拟组织人员",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTempUserByIds: {
			url: `${config.API_URL}/set/system/deleteTempUserByIds`,
			name: "管理员批量虚拟人员账号",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	sysSecurity: {
		setSysSecurityApproval:{
			url: `${config.API_URL}/set/system/setSysSecurityApproval`,
			name: "执行三员事件审批",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSysSecurityEventColumnComment:{
			url: `${config.API_URL}/get/system/getSysSecurityEventColumnComment`,
			name: "获取系统安全日志表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getSysSecurityApprovalList: {
			url: `${config.API_URL}/get/system/getSysSecurityApprovalList`,
			name: "获取系统安全日志表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getSysSecurityEventList:{
			url: `${config.API_URL}/get/system/getSysSecurityEventList`,
			name: "获取所有安全事件审批列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getThreeEventLogList: {
			url: `${config.API_URL}/get/system/getSysSecurityEventLogList`,
			name: "获取系统安全日志表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		deleteSysSecurityEventByIds: {
			url: `${config.API_URL}/set/system/deleteSysSecurityEventByIds`,
			name: "批量删除系统安全日志",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSysSecurityEvent: {
			url: `${config.API_URL}/set/system/deleteSysSecurityEvent`,
			name: "删除系统安全日志",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSysSecurityUser: {
			url: `${config.API_URL}/get/system/getSysSecurityUser`,
			name: "获取三员管理人员设置",
			get: async function () {
				return await http.get(this.url);
			}
		},
		setSysSecurityUser: {
			url: `${config.API_URL}/set/system/setSysSecurityUser`,
			name: "设置三员管理权限",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		insertSysSecurityConfig: {
			url: `${config.API_URL}/set/system/insertSysSecurityConfig`,
			name: "创建三员管理模块权限",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSysSecurityConfig: {
			url: `${config.API_URL}/set/system/updateSysSecurityConfig`,
			name: "更新三员管理模块权限",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSysSecurityConfig: {
			url: `${config.API_URL}/set/system/deleteSysSecurityConfig`,
			name: "删除三员管理模块权限",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSysSecurityConfigList: {
			url: `${config.API_URL}/get/system/getSysSecurityConfigList`,
			name: "获取三员管理模块权限列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getSystemLogList: {
			url: `${config.API_URL}/get/system/getSystemLogList`,
			name: "获取系统运行日志列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	sms: {
		getNoReadSms: {
			url: `${config.API_URL}/get/system/getNoReadSms`,
			name: "获取未读消息列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		setReadStatusSmsByIds: {
			url: `${config.API_URL}/set/system/setReadStatusSmsByIds`,
			name: "设置消息已读",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSmsByIds:{
			url: `${config.API_URL}/set/system/deleteSmsByIds`,
			name: "批量删除消息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		sendSms:{
			url: `${config.API_URL}/set/system/sendSms`,
			name: "发送内部短信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSms:{
			url: `${config.API_URL}/set/system/updateSms`,
			name: "更新消息状态",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSms:{
			url: `${config.API_URL}/set/system/deleteSms`,
			name: "删除消息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMySmsList:{
			url: `${config.API_URL}/get/system/getMySmsList`,
			name: "获取个人所有消息列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	mobilesms:{
		sendMobileSms:{
			url: `${config.API_URL}/set/system/sendMobileSms`,
			name: "添加短信发送计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMySendMobileSms:{
			url: `${config.API_URL}/get/system/getMySendMobileSms`,
			name: "获取手机短信息列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		deleteMobileSms:{
			url: `${config.API_URL}/set/system/deleteMobileSms`,
			name: "删除手机短信",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteMobileSmsByIds:{
			url: `${config.API_URL}/set/system/deleteMobileSmsByIds`,
			name: "批量删除消息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	app: {
		list: {
			url: `${config.API_URL}/system/app/list`,
			name: "应用列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	log: {
		list: {
			url: `${config.API_URL}/system/log/list`,
			name: "日志列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	table: {
		list: {
			url: `${config.API_URL}/system/table/list`,
			name: "表格列管理列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		info: {
			url: `${config.API_URL}/system/table/info`,
			name: "表格列管理详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	mobileNet:{
		getLocation:{
			url: `${config.API_URL}/get/system/getLocation`,
			name: "获取地理位置",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	sysMessageType:{
		getSysMessageTypeForSelect:{
			url: `${config.API_URL}/get/system/getSysMessageTypeForSelect`,
			name: "获取系统消息类型",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	getMyPassword:{
		url: `${config.API_URL}/set/system/getMyPassword`,
		name: "忘记密码发送邮件",
		post: async function (data) {
			return await http.post(this.url, data, {headers: {}});
		}
	},
	sysCodeRule:{
		createRuleCodeStr:{
			url: `${config.API_URL}/get/system/createRuleCodeStr`,
			name: "按规则生成编号",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSysCodeRuleListForSelect:{
			url: `${config.API_URL}/get/system/getSysCodeRuleListForSelect`,
			name: "获取下接编号规则列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSysCodeRuleList: {
			url: `${config.API_URL}/get/system/getSysCodeRuleList`,
			name: "获取编号规则列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertSysCodeRule: {
			url: `${config.API_URL}/set/system/insertSysCodeRule`,
			name: "添加编号规则",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateSysCodeRule: {
			url: `${config.API_URL}/set/system/updateSysCodeRule`,
			name: "更新编号规则",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteSysCodeRule: {
			url: `${config.API_URL}/set/system/deleteSysCodeRule`,
			name: "删除编号规则",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteSysCodeRuleByIds: {
			url: `${config.API_URL}/set/system/deleteSysCodeRuleByIds`,
			name: "批量删编号规则",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	getAddressByClient:{
		url: `${config.API_URL}/get/system/getAddressByClient`,
		name: "获取当前用户所在地理信息",
		get: async function () {
			return await http.get(this.url);
		}
	},
	getCityCodeByClient:{
		url: `${config.API_URL}/get/system/getCityCodeByClient`,
		name: "按地址获取当前城市的编号",
		get: async function () {
			return await http.get(this.url);
		}
	},
	getCitWeatherByClient:{
		url: `${config.API_URL}/get/system/getCitWeatherByClient`,
		name: "获取当前城市的天气",
		get: async function () {
			return await http.get(this.url);
		}
	},
	getCityCodeList:{
		url: `${config.API_URL}/get/system/getCityCodeList`,
		name: "获取城市编码列表",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	userGroup:{
		getUserListByGroup:{
			url: `${config.API_URL}/get/system/getUserListByGroup`,
			name: "按用户分组获取人员列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getUserGroupListForManage:{
			url: `${config.API_URL}/get/system/getUserGroupListForManage`,
			name: "获取个人用户分组",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getUserGroupListByAccountId:{
			url: `${config.API_URL}/get/system/getUserGroupListByAccountId`,
			name: "获取个人用户分组",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertUserGroup: {
			url: `${config.API_URL}/set/system/insertUserGroup`,
			name: "添加用户分组",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateUserGroup: {
			url: `${config.API_URL}/set/system/updateUserGroup`,
			name: "更新用户分组",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteUserGroup: {
			url: `${config.API_URL}/set/system/deleteUserGroup`,
			name: "删除用户分组",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteUserGroupByIds: {
			url: `${config.API_URL}/set/system/deleteUserGroupByIds`,
			name: "批量删除用户分组",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	sysBannerConfig:{
		getSysBannerConfig:{
			url: `${config.API_URL}/get/system/getSysBannerConfig`,
			name: "获取系统Banner图配置信息",
			get: async function () {
				return await http.get(this.url);
			}
		},
		updateSysBannerConfig:{
			url: `${config.API_URL}/set/system/updateSysBannerConfig`,
			name: "配置系统Banner图",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	getTodayKanbanInfo:{
		url: `${config.API_URL}/get/system/getTodayKanbanInfo`,
		name: "获取今日看板数据",
		get: async function () {
			return await http.get(this.url);
		}
	},
	sysEvent:{
		getSysEventTree:{
			url: `${config.API_URL}/get/system/getSysEventTree`,
			name: "获取系统事件列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	verification:{
		getUserInfoVerificationList:{
			url: `${config.API_URL}/get/system/getUserInfoVerificationList`,
			name: "获取用户校验信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getUnitDeptVerificationList:{
			url: `${config.API_URL}/get/system/getUnitDeptVerificationList`,
			name: "获取部门信息校验信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getUserRoleVerificationList:{
			url: `${config.API_URL}/get/system/getUserRoleVerificationList`,
			name: "获取权限校验信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getUserLevelVerificationList:{
			url: `${config.API_URL}/get/system/getUserLevelVerificationList`,
			name: "获取行政级别校验信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		setUserInfoSignature:{
			url: `${config.API_URL}/set/system/setUserInfoSignature`,
			name: "用户信息签名",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		setUnitDeptSignature:{
			url: `${config.API_URL}/set/system/setUnitDeptSignature`,
			name: "部门信息签名",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		setUserRoleSignature:{
			url: `${config.API_URL}/set/system/setUserRoleSignature`,
			name: "权限信息签名",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		setUserLevelSignature:{
			url: `${config.API_URL}/set/system/setUserLevelSignature`,
			name: "设置行政级别记录签名",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	sysPortal:{
		insertSysPortal: {
			url: `${config.API_URL}/set/system/insertSysPortal`,
			name: "创建自定义门户",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateSysPortal: {
			url: `${config.API_URL}/set/system/updateSysPortal`,
			name: "更新自定义门户",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteSysPortal: {
			url: `${config.API_URL}/set/system/deleteSysPortal`,
			name: "删除自定义门户",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteSysPortalByIds: {
			url: `${config.API_URL}/set/system/deleteSysPortalByIds`,
			name: "批量删除自定义门户",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getAllSysPortalList:{
			url: `${config.API_URL}/get/system/getAllSysPortalList`,
			name: "获取全部门户列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSysPortalByPortalId:{
			url: `${config.API_URL}/get/system/getSysPortalByPortalId`,
			name: "获取自定义门户详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMySysPortalForSelect:{
			url: `${config.API_URL}/get/system/getMySysPortalForSelect`,
			name: "获取个人权限内的门户列表",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	sysPortalItem:{
		insertSysPortalItem: {
			url: `${config.API_URL}/set/system/insertSysPortalItem`,
			name: "创建自定义门户组件",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateSysPortalItem: {
			url: `${config.API_URL}/set/system/updateSysPortalItem`,
			name: "更新自定义门户组件",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteSysPortalItem: {
			url: `${config.API_URL}/set/system/deleteSysPortalItem`,
			name: "删除自定义门户组件",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteSysPortalItemByIds: {
			url: `${config.API_URL}/set/system/deleteSysPortalItemByIds`,
			name: "批量删除自定义门户组件",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getAllSysPortalItemList:{
			url: `${config.API_URL}/get/system/getAllSysPortalItemList`,
			name: "获取自定义门户组件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	sysResource:{
		getResourceFileList:{
			url: `${config.API_URL}/get/system/getResourceFileList`,
			name: "获取系统资源文件列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		uploadSysResource:{
			url: `${config.API_URL}/set/system/uploadSysResource`,
			name: "上传静态资源文件",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		deleteSysResource:{
			url: `${config.API_URL}/set/system/deleteSysResource`,
			name: "删除系统资源文件",
			post: async function (data) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteSysResourceByFileNames:{
			url: `${config.API_URL}/set/system/deleteSysResourceByFileNames`,
			name: "批量删除系统资源文件",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		}
	},
	getEncryptionKey:{
		url: `${config.API_URL}/get/system/getEncryptionKey`,
		name: "生成密钥",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	dbUnit: {
		getDbSqlFile: {
			url: `${config.API_URL}/get/system/getDbSqlFile`,
			name: "生成数据结构",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	pending: {
		getMyPendingInfo: {
			url: `${config.API_URL}/get/system/getMyPendingInfo`,
			name: "获取个人待办信息",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	userOut:{
		getAllUserOutForWeek: {
			url: `${config.API_URL}/get/system/getAllUserOutForWeek`,
			name: "获取近期外出人员列表",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	sysDataBase:{
		getSystemDBTableInfo: {
			url: `${config.API_URL}/get/system/getSystemDBTableInfo`,
			name: "获取系统表信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	}

}
