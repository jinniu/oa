import config from "@/config"
import http from "@/utils/request"

export default {
	knowledge:{
		getKnowledgeCommentList:{
			url: `${config.API_URL}/get/file/getKnowledgeCommentList`,
			name: "获取评论列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		setKnowledgeLearn:{
			url: `${config.API_URL}/set/file/setKnowledgeLearn`,
			name: "创建学习记录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		insertKnowledgeComment:{
			url: `${config.API_URL}/set/file/insertKnowledgeComment`,
			name: "添加评论",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteKnowledgeComment:{
			url: `${config.API_URL}/set/file/deleteKnowledgeComment`,
			name: "删除评论",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updateKnowledgeComment:{
			url: `${config.API_URL}/set/file/updateKnowledgeComment`,
			name: "更新评论",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		searchIndex:{
			url: `${config.API_URL}/get/file/searchIndex`,
			name: "获取所有知识分类",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getKnowledgeLearnList:{
			url: `${config.API_URL}/get/file/getKnowledgeLearnList`,
			name: "获取所有学习记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getAllKnowledgeSortMap:{
			url: `${config.API_URL}/get/file/getAllKnowledgeSortMap`,
			name: "获取所有知识分类",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getHostKeywords:{
			url: `${config.API_URL}/get/file/getHostKeywords`,
			name: "获取热门关键字",
			get: async function () {
				return await http.get(this.url);
			}
		},
		deleteKnowledgeSearch:{
			url: `${config.API_URL}/set/file/deleteKnowledgeSearch`,
			name: "删除关键词",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteKnowledgeSearchByIds:{
			url: `${config.API_URL}/set/file/deleteKnowledgeSearchByIds`,
			name: "批量删除关键词",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		getKnowledgeSearchList:{
			url: `${config.API_URL}/get/file/getKnowledgeSearchList`,
			name: "关键词管理",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		resetKnowledgeIndex:{
			url: `${config.API_URL}/set/file/resetKnowledgeIndex`,
			name: "重建知识文档索引",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteKnowledgeIndex:{
			url: `${config.API_URL}/set/file/deleteKnowledgeIndex`,
			name: "删除知识文档的索引",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		insertKnowledge:{
			url: `${config.API_URL}/set/file/insertKnowledge`,
			name: "添加知识",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		updateKnowledge:{
			url: `${config.API_URL}/set/file/updateKnowledge`,
			name: "更新知识",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			}
		},
		deleteKnowledgeByIds:{
			url: `${config.API_URL}/set/file/deleteKnowledgeByIds`,
			name: "批量删除知识文档",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteKnowledge:{
			url: `${config.API_URL}/set/file/deleteKnowledge`,
			name: "删除知识文档",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		getMyCreateKnowledgeList:{
			url: `${config.API_URL}/get/file/getMyCreateKnowledgeList`,
			name: "获取我可管理的知识列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getKnowledgeSortTree: {
			url: `${config.API_URL}/get/file/getKnowledgeSortTree`,
			name: "获取知识分类树",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyKnowledgeById:{
			url: `${config.API_URL}/get/file/getMyKnowledgeById`,
			name: "获取知识文档详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertKnowledgeSort:{
			url: `${config.API_URL}/set/file/insertKnowledgeSort`,
			name: "添加知识分类",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteKnowledgeSort:{
			url: `${config.API_URL}/set/file/deleteKnowledgeSort`,
			name: "删除知识分类",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updateKnowledgeSort:{
			url: `${config.API_URL}/set/file/updateKnowledgeSort`,
			name: "更新知识分类",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
	},
    uploadLogo: {
        url: `${config.API_URL}/set/file/uploadLogo`,
        name: "上传系统Logo",
        post: async function (data, config = {}) {
            return await http.postFile(this.url, data, config);
        }
    },
	uploadSysBanner:{
		url: `${config.API_URL}/set/file/uploadSysBanner`,
		name: "上传系统系统Banner",
		post: async function (data, config = {}) {
			return await http.postFile(this.url, data, config);
		}
	},
	uploadSysLogo:{
		url: `${config.API_URL}/set/file/uploadSysLogo`,
		name: "上传系统LOGO图片",
		post: async function (data, config = {}) {
			return await http.postFile(this.url, data, config);
		}
	},
    uploadImg: {
        url: `${config.API_URL}/set/file/uploadImg`,
        name: "上传图片",
        post: async function (data, config = {}) {
            return await http.postFile(this.url, data, config);
        }
    },
	uploadBackGroundImg: {
		url: `${config.API_URL}/set/file/uploadBackGroundImg`,
		name: "上传背景图片",
		post: async function (data, config = {}) {
			return await http.postFile(this.url, data, config);
		}
	},
	uploadHrPhoto: {
		url: `${config.API_URL}/set/file/uploadHrPhoto`,
		name: "上传图片",
		post: async function (data, config = {}) {
			return await http.postFile(this.url, data, config);
		}
	},
	uploadBpmSign:{
		url: `${config.API_URL}/set/file/uploadBpmSign`,
		name: "上传BPM印章",
		post: async function (data, config = {}) {
			return await http.postFile(this.url, data, config);
		}
	},
    uploadAvatar: {
        url: `${config.API_URL}/set/file/uploadAvatar`,
        name: "上传用户头像",
        post: async function (data, config = {}) {
            return await http.postFile(this.url, data, config);
        }
    },
	onLinePreview: {
		url: `${config.API_URL}/get/file/onLinePreview`,
		name: "获取文件预览结果",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
    attach: {
		downLoadFile:{
			url: `${config.API_URL}/get/file/downLoadFile`,
			name: "下载文件",
			fileDownload: async function (data) {
				return await http.fileDownload(this.url, data,{
					headers: {}
				})
			}
		},
		getMyAttachCount:{
			url: `${config.API_URL}/get/file/getMyAttachCount`,
			name: "获取个人上传附件的总数",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAttachVersionList:{
			url: `${config.API_URL}/get/file/getAttachVersionList`,
			name: "获取历史文件版本",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
        getAttachList: {
            url: `${config.API_URL}/get/file/getAttachList`,
            name: "按附件ID获取附件列表",
            get: async function (params) {
                return await http.get(this.url, params);
            }
        },
        uploadFile: {
            url: `${config.API_URL}/set/file/uploadFile`,
            name: "附件上传",
            post: async function (params, config = {}) {
                return await http.postFile(this.url, params, config);
            }
        },
        getAttachManageList: {
            url: `${config.API_URL}/get/file/getAttachManageList`,
            name: "系统附件列表",
            get: async function (params) {
                return await http.get(this.url, params);
            }
        },
        deleteAttach: {
            url: `${config.API_URL}/set/file/deleteAttach`,
            name: "删除指定的附件",
            post: async function (params, config = {}) {
                return await http.post(this.url, params, config);
            }
        },
    },
	netDisk:{
		getNetDiskFolder:{
			url: `${config.API_URL}/get/file/getNetDiskFolder`,
			name: "获取指定目录下的目录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getNetDiskFiles:{
			url: `${config.API_URL}/get/file/getNetDiskFiles`,
			name: "获取指定目录下的文件",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getNetDiskRole:{
			url: `${config.API_URL}/get/file/getNetDiskRole`,
			name: "获取当前用户在本网络硬盘下的权限",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyNetDiskList:{
			url: `${config.API_URL}/get/file/getMyNetDiskList`,
			name: "获取权限内的网络硬盘目录",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getNetDiskList:{
			url: `${config.API_URL}/get/file/getNetDiskList`,
			name: "获取网络云盘列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		renameFileFolderForNetDisk:{
			url: `${config.API_URL}/set/file/renameFileFolderForNetDisk`,
			name: "网络硬盘文件夹命名",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteNetDiskFile:{
			url: `${config.API_URL}/set/file/deleteNetDiskFile`,
			name: "删除网络硬盘文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		shearNetDiskFile:{
			url: `${config.API_URL}/set/file/shearNetDiskFile`,
			name: "网络硬盘文件剪切",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		copyNetDiskFile:{
			url: `${config.API_URL}/set/file/copyNetDiskFile`,
			name: "网络硬盘文件复制",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		createNetDiskFolder:{
			url: `${config.API_URL}/set/file/createNetDiskFolder`,
			name: "网络硬盘创建文件夹",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileFolderForNetDisk:{
			url: `${config.API_URL}/set/file/deleteFileFolderForNetDisk`,
			name: "删除网络硬盘文件夹",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		renameNetDiskFileName:{
			url: `${config.API_URL}/set/file/renameNetDiskFileName`,
			name: "网络硬盘文件重命名",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		createNetDiskFile:{
			url: `${config.API_URL}/set/file/createNetDiskFile`,
			name: "在网络硬盘中创建文件",
			post: async function (params, config = {}) {
				return await http.postFile(this.url, params,config);
			}
		},
		insert: {
			url: `${config.API_URL}/set/file/insertNetDisk`,
			name: "创建网络云盘",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		delete: {
			url: `${config.API_URL}/set/file/deleteNetDisk`,
			name: "删除网络云盘",
			post: async function (params, config = {}) {
				return await http.post(this.url, params, config);
			}
		},
		update: {
			url: `${config.API_URL}/set/file/updateNetDisk`,
			name: "更新网络云盘",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		setNetDiskRole:{
			url: `${config.API_URL}/set/file/setNetDiskRole`,
			name: " 批量更新权限",
			post: async function (params, config = {}) {
				console.log(params);
				return await http.post(this.url, params,config);
			}
		},
		removeNetDiskRole:{
			url: `${config.API_URL}/set/file/removeNetDiskRole`,
			name: " 批量移除权限",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		}
	},
	personalFile:{
		getSharePersonalFileFolder:{
			url: `${config.API_URL}/get/file/getSharePersonalFileFolder`,
			name: "获取个人文件柜共享目录",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertPersonalFile:{
			url: `${config.API_URL}/set/file/insertPersonalFile`,
			name: "创建个人文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deletePersonalFile:{
			url: `${config.API_URL}/set/file/deletePersonalFile`,
			name: "删除个人文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updatePersonalFile:{
			url: `${config.API_URL}/set/file/updatePersonalFile`,
			name: "更新个人文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		getSharePersonalFileList:{
			url: `${config.API_URL}/get/file/getSharePersonalFileList`,
			name: "获取共享文件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getPersonalFileList:{
			url: `${config.API_URL}/get/file/getPersonalFileList`,
			name: "获取个人文件夹下面的文件",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getPersonalFolderTree: {
			url: `${config.API_URL}/get/file/getPersonalFolderTree`,
			name: "获取个人文件柜目录",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getPersonalFolderForDesk:{
			url: `${config.API_URL}/get/file/getPersonalFolderForDesk`,
			name: "获取桌面个人文件柜列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getShareFileFolderForDesk:{
			url: `${config.API_URL}/get/file/getShareFileFolderForDesk`,
			name: "获取桌面个人共享文件柜列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertPersonalFileFolder: {
			url: `${config.API_URL}/set/file/insertPersonalFileFolder`,
			name: "创建文件夹",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deletePersonalFileFolder:{
			url: `${config.API_URL}/set/file/deletePersonalFileFolder`,
			name: "删除文件夹",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updatePersonalFileFolder: {
			url: `${config.API_URL}/set/file/updatePersonalFileFolder`,
			name: "更新文件夹",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		pastePersonalFile:{
			url: `${config.API_URL}/set/file/pastePersonalFile`,
			name: "复制粘贴",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		shearPersonalFile:{
			url: `${config.API_URL}/set/file/shearPersonalFile`,
			name: "公共文件柜剪切",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		}
	},
	publicFile:{
		getMyPublicFolderInRoleForDesk:{
			url: `${config.API_URL}/get/file/getMyPublicFolderInRoleForDesk`,
			name: "获取桌面公共文件柜",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getPublicFolderRoleInfo:{
			url: `${config.API_URL}/get/file/getPublicFolderRoleInfo`,
			name: "获取当前文件柜的操作权限",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getPublicFileList:{
			url: `${config.API_URL}/get/file/getPublicFileList`,
			name: "获取公共文件夹下面的文件",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getPublicFileFolderById:{
			url: `${config.API_URL}/get/file/getPublicFileFolderById`,
			name: "获取当前文件夹的信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getPublicFileFolderTree: {
			url: `${config.API_URL}/get/file/getPublicFileFolderTree`,
			name: "获取文件柜目录",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyPublicFolderInRole:{
			url: `${config.API_URL}/get/file/getMyPublicFolderInRole`,
			name: "获取权限内的文件框目录",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertPublicFileFolder: {
			url: `${config.API_URL}/set/file/insertPublicFileFolder`,
			name: "创建文件夹",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		insertPublicFile:{
			url: `${config.API_URL}/set/file/insertPublicFile`,
			name: "创建文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deletePublicFileFolder: {
			url: `${config.API_URL}/set/file/deletePublicFileFolder`,
			name: "删除文件夹",
			post: async function (params, config = {}) {
				return await http.post(this.url, params, config);
			}
		},
		updatePublicFileFolder: {
			url: `${config.API_URL}/set/file/updatePublicFileFolder`,
			name: "更新文件夹",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updatePublicFile:{
			url: `${config.API_URL}/set/file/updatePublicFile`,
			name: "更新文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deletePublicFile:{
			url: `${config.API_URL}/set/file/deletePublicFile`,
			name: "删除公共文件柜文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deletePublicFileFolderAndFile:{
			url: `${config.API_URL}/set/file/deletePublicFileFolderAndFile`,
			name: "删除公共文件柜文件夹与文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		addPublicFolderRole:{
			url: `${config.API_URL}/set/file/addPublicFolderRole`,
			name: "批量添加公共文件柜权限",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		removePublicFolderRole:{
			url: `${config.API_URL}/set/file/removePublicFolderRole`,
			name: "批量删除公共文件柜权限",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		shearPublicFile:{
			url: `${config.API_URL}/set/file/shearPublicFile`,
			name: "公共文件柜剪切",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		pastePublicFile:{
			url: `${config.API_URL}/set/file/pastePublicFile`,
			name: "复制粘贴",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
	},
	photo:{
		getPhotoRole:{
			url: `${config.API_URL}/get/file/getPhotoRole`,
			name: "获取个人相册权限",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createPhotoFile:{
			url: `${config.API_URL}/set/file/createPhotoFile`,
			name: "在相册中创建图片",
			post: async function (params, config = {}) {
				return await http.postFile(this.url, params,config);
			}
		},
		getPhotoList: {
			url: `${config.API_URL}/get/file/getPhotoList`,
			name: "获取相册目录列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getPhotoFileList: {
			url: `${config.API_URL}/get/file/getPhotoFileList`,
			name: "获取批定相册目录下的图片",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyPhotoList: {
			url: `${config.API_URL}/get/file/getMyPhotoList`,
			name: "获取个人权限内的相册",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyPhotoFileList: {
			url: `${config.API_URL}/get/file/getMyPhotoFileList`,
			name: "获取相册目录中的图片",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insert: {
			url: `${config.API_URL}/set/file/insertPhoto`,
			name: "创建相册",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		delete: {
			url: `${config.API_URL}/set/file/deletePhoto`,
			name: "删除相册",
			post: async function (params, config = {}) {
				return await http.post(this.url, params, config);
			}
		},
		update: {
			url: `${config.API_URL}/set/file/updatePhoto`,
			name: "更新相册",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deletePhotoFile:{
			url: `${config.API_URL}/set/file/deletePhotoFile`,
			name: "删除相册图片",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		}
	},
	fileMap:{
		getMyFileMapForDesk:{
			url: `${config.API_URL}/get/file/getMyFileMapForDesk`,
			name: "获取桌面个人内网文件",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyMangeFileItem:{
			url: `${config.API_URL}/get/file/getMyMangeFileItem`,
			name: "获取文档管理地图文件目录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getFileRecordListForMange:{
			url: `${config.API_URL}/get/file/getFileRecordListForMange`,
			name: "获取文档管理文件目录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		searchMyFileMapFileList:{
			url: `${config.API_URL}/get/file/searchMyFileMapFileList`,
			name: "查询获取权限内的文档",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyFileItemList:{
			url: `${config.API_URL}/get/file/getMyFileItemList`,
			name: "获取个人文件",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		setFileMapReadStatus:{
			url: `${config.API_URL}/set/file/setFileMapReadStatus`,
			name: "更新查阅状态",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileMapRecord:{
			url: `${config.API_URL}/set/file/deleteFileMapRecord`,
			name: "删除文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileMapRecordByIds:{
			url: `${config.API_URL}/set/file/deleteFileMapRecordByIds`,
			name: "批量删除文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updateFileMapRecord:{
			url: `${config.API_URL}/set/file/updateFileMapRecord`,
			name: "更新文件记录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		insertFileMapRecord:{
			url: `${config.API_URL}/set/file/insertFileMapRecord`,
			name: "添加文件记录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		insertFileMapItem:{
			url: `${config.API_URL}/set/file/insertFileMapItem`,
			name: "创建目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileMapItem:{
			url: `${config.API_URL}/set/file/deleteFileMapItem`,
			name: "删除目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileMapItemIds:{
			url: `${config.API_URL}/set/file/deleteFileMapItemIds`,
			name: "批量删除目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updateFileMapItem:{
			url: `${config.API_URL}/set/file/updateFileMapItem`,
			name: "更新目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		getFileItemListForManage:{
			url: `${config.API_URL}/get/file/getFileItemListForManage`,
			name: "获取目录结构",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getFileMapSortTree:{
			url: `${config.API_URL}/get/file/getFileMapSortTree`,
			name: "获取目录结构",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertFileMapSort:{
			url: `${config.API_URL}/set/file/insertFileMapSort`,
			name: "添加文档地图目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileMapSort:{
			url: `${config.API_URL}/set/file/deleteFileMapSort`,
			name: "删除文档地图目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updateFileMapSort:{
			url: `${config.API_URL}/set/file/updateFileMapSort`,
			name: "更新文档地图目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		}
	},
	fileLaw:{
		getFileLawSortTree:{
			url: `${config.API_URL}/get/file/getFileLawSortTree`,
			name: "获取制度目录结构",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertFileLawSort:{
			url: `${config.API_URL}/set/file/insertFileLawSort`,
			name: "添加制度目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileLawSort:{
			url: `${config.API_URL}/set/file/deleteFileLawSort`,
			name: "删除制度目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updateFileLawSort:{
			url: `${config.API_URL}/set/file/updateFileLawSort`,
			name: "更新制度目录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		insertFileLawRecord:{
			url: `${config.API_URL}/set/file/insertFileLawRecord`,
			name: "创建制度文件记录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileLawRecord:{
			url: `${config.API_URL}/set/file/deleteFileLawRecord`,
			name: "删除制度文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileLawRecordByIds:{
			url: `${config.API_URL}/set/file/deleteFileLawRecordByIds`,
			name: "批量删除文档",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		setFileLawReadStatus:{
			url: `${config.API_URL}/set/file/setFileLawReadStatus`,
			name: "更新查阅状态",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		updateFileLawRecord:{
			url: `${config.API_URL}/set/file/updateFileLawRecord`,
			name: "更新制度文件",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		resetFileLawIndex:{
			url: `${config.API_URL}/set/file/resetFileLawIndex`,
			name: "重建制度文件索引",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		deleteFileLawIndex:{
			url: `${config.API_URL}/set/file/deleteFileLawIndex`,
			name: "删除制度文件索引",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		setFileLawRecordReader:{
			url: `${config.API_URL}/set/file/setFileLawRecordReader`,
			name: "设置查看人员",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		setFileLawRecordStatus:{
			url: `${config.API_URL}/set/file/setFileLawRecordStatus`,
			name: "审批制度文件记录",
			post: async function (params, config = {}) {
				return await http.post(this.url, params,config);
			}
		},
		getFileLawRecordListForMange:{
			url: `${config.API_URL}/get/file/getFileLawRecordListForMange`,
			name: "获取制度文件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getFileLawRecordListForApproval:{
			url: `${config.API_URL}/get/file/getFileLawRecordListForApproval`,
			name: "获取审批制度文件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getFileLawRecordListForSearch:{
			url: `${config.API_URL}/get/file/getFileLawRecordListForSearch`,
			name: "查询审批制度文件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getFileLawSortTypePie:{
			url: `${config.API_URL}/get/file/getFileLawSortTypePie`,
			name: "制度文件分类占比",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getFileLawByMonthLine:{
			url: `${config.API_URL}/get/file/getFileLawByMonthLine`,
			name: "制度文件创建趋势",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getFileLawLogByMonthLine:{
			url: `${config.API_URL}/get/file/getFileLawLogByMonthLine`,
			name: "制度文件查看趋势",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getFileLawClickBar:{
			url: `${config.API_URL}/get/file/getFileLawClickBar`,
			name: "制度查看次数排行",
			get: async function () {
				return await http.get(this.url);
			}
		},
	}
}
