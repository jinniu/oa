import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/display.css'
import cyui from './cyui'
import {registerIcon} from '@/utils/el-icons'
/*由于钉钉APP的浏览器的版本不支持globalthis全句变量,在此手动引入即可。高版本版浏览器则无此问题*/
import 'globalthis/auto'
import i18n from './locales'
import router from './router'
import store from './store'
import { createApp } from 'vue'
import App from './App.vue'
import Draggable from 'vuedraggable'
import moment from 'moment'
const app = createApp(App);
app.use(store);
app.use(router);
app.use(registerIcon);
app.component('draggable', Draggable);
app.use(ElementPlus);
/*app.use(ContainerWidgets);
app.use(ContainerItems);*/
app.use(i18n);
app.use(moment);
app.use(cyui);
app.mount('#app');

const debounce = (fn, delay) => {
	let timer = null;
	return function () {
		let context = this;
		let args = arguments;
		clearTimeout(timer);
		timer = setTimeout(function () {
			fn.apply(context, args);
		}, delay);
	}
}

const _ResizeObserver = window.ResizeObserver;
window.ResizeObserver = class ResizeObserver extends _ResizeObserver {
	constructor(callback) {
		callback = debounce(callback, 16);
		super(callback);
	}
}

