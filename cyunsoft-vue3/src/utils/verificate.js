
//验证手机号
import eleme_default from "@element-plus/icons-vue/dist/global";

export function verifyPhone(rule, value, callback) {
	if(value)
	{
		let reg = /^[1][3, 4, 5, 6, 7, 8, 9][0-9]{9}$/
		if(!reg.test(value)){
			return callback(new Error('请输入正确的手机号码'))
		}
		callback()
	}else
	{
		callback()
	}
}
//电子邮件
export function verifyEmail(rule, value, callback) {
	if(value)
	{
		let reg = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/
		if(!reg.test(value)){
			return callback(new Error('请输入正确的电子邮件'))
		}
		callback()
	}else {
		callback()
	}
}

//车牌号码
export function verifyCars(rule, value, callback) {
	let reg = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-HJ-NP-Z][A-HJ-NP-Z0-9]{4,5}[A-HJ-NP-Z0-9挂学警港澳]$/
	if(!reg.test(value)){
		return callback(new Error('请输入正确的车牌号码'))
	}
	callback()
}
//身份证
export function validateIdNo(rule, value,callback) {
	const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
	if(value==''||value==undefined||value==null){
		callback();
	}else {
		if ((!reg.test(value)) && value != '') {
			callback(new Error('请输入正确的身份证号码'));
		} else {
			callback();
		}
	}
}
//url校验
export function validateURL(rule, value, callback) {
	const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
	if(!reg.test(value)){
		return callback(new Error('Url地址不正确'))
	}
	callback()
}
//Ip地址校验
export function validateIP(rule, value,callback) {
	if(value==''||value==undefined||value==null){
		callback();
	}else {
		const reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
		if ((!reg.test(value)) && value != '') {
			callback(new Error('请输入正确的IP地址'));
		} else {
			callback();
		}
	}
}
//是否邮箱
export function validatePhone(rule, value,callback) {
	const reg =/^[1][3-9][0-9]{9}$/;
	if(value==''||value==undefined||value==null){
		callback();
	}else {
		if ((!reg.test(value)) && value != '') {
			callback(new Error('请输入正确的电话号码'));
		} else {
			callback();
		}
	}
}

export function pwdCheck(rule, value, callback) {
	const reg = /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{6,16}$/;
	if (value !== '') {
		if (value.length < 6) {
			return callback(new Error('新密码至少输入6位'))
		} else if (value.length > 16) {
			return callback(new Error('密码最长不能超过16位'))
		} else if (!reg.test(value)) {
			return callback(new Error('密码输入有误，请检查格式是否正确！'))
		} else {
			callback()
		}
	}else{
		return callback(new Error('新密码不能为空'))
	}
}
// 重复密码验证
export function pwdAgainCheck(rule, value, callback) {
	if (value.length < 1) {
		return callback(new Error('重复密码不能为空！'));
	} else if (this.form.password !== value) {
		return callback(new Error('两次输入密码不一致！'));
	} else {
		callback()
	}
}

