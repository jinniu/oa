import sysConfig from "@/config";
import tool from "@/utils/tool";

export default {
	methods: {
		openFileOnLine(extName, netDiskId, role, path) {
			if (this.$TOOL.data.get("OFFICE_TYPE") == '0') {
				if (sysConfig.DOC_FILE.includes(extName)) {
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")  + '/office/openword?netDiskId=' + tool.crypto.createParam(netDiskId) + '&path=' + tool.crypto.createParam(path) + '&extName=' + extName + '&openModeType=' + role, 'width=1200px;height=800px;scroll=no;');
					}else{
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")  + '/office/openword?netDiskId=' + netDiskId + '&path=' + encodeURIComponent(path) + '&extName=' + extName + '&openModeType=' + role, 'width=1200px;height=800px;scroll=no;');
					}
				} else if (sysConfig.EXCEL_FILE.includes(extName)) {
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")  + '/office/openexcel?netDiskId=' + tool.crypto.createParam(netDiskId) + '&path=' + tool.crypto.createParam(path) + '&extName=' + extName + '&openModeType=' + role, 'width=1200px;height=800px;scroll=no;');
					}else{
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")  + '/office/openexcel?netDiskId=' + netDiskId + '&path=' + encodeURIComponent(path) + '&extName=' + extName + '&openModeType=' + role, 'width=1200px;height=800px;scroll=no;');
					}
				} else if (sysConfig.PPT_FILE.includes(extName)) {
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")  + '/office/openppt?netDiskId=' + tool.crypto.createParam(netDiskId) + '&path=' + tool.crypto.createParam(path) + '&extName=' + extName + '&openModeType=' + role, 'width=1200px;height=800px;scroll=no;');
					}else{
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")  + '/office/openppt?netDiskId=' + netDiskId + '&path=' + encodeURIComponent(path) + '&extName=' + extName + '&openModeType=' + role, 'width=1200px;height=800px;scroll=no;');
					}
				} else if (sysConfig.PDF_FILE.includes(extName)) {
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")  + '/office/openpdf?netDiskId=' +tool.crypto.createParam(netDiskId) + '&path=' + tool.crypto.createParam(path) + '&extName=' + extName + '&openModeType=' + role, 'width=1200px;height=800px;scroll=no;');
					}else{
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")  + '/office/openpdf?netDiskId=' + netDiskId + '&path=' + encodeURIComponent(path) + '&extName=' + extName + '&openModeType=' + role, 'width=1200px;height=800px;scroll=no;');
					}
				} else if (sysConfig.IMG_FILE.includes(extName)) {
					const image = new Image();
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						image.src = sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + tool.crypto.createParam(netDiskId) + "&fileName=" + tool.crypto.createParam(path)+"&ddtab=true"
					}else{
						image.src = sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + netDiskId + "&fileName=" + encodeURIComponent(path)+"&ddtab=true"
					}
						image.onload = () => {
							image.style.margin = "0 auto"
							image.style.display = "block"
							const newWin = window.open("", "_blank")
							newWin.document.write(image.outerHTML)
						}
				}
			} else if (this.$TOOL.data.get("OFFICE_TYPE") == '2') {
				if(sysConfig.DOC_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openword?netDiskId=' + tool.crypto.createParam(netDiskId) + '&path=' + tool.crypto.createParam(path) + '&extName=' + extName + '&openModeType=' + role+'&token='+this.$TOOL.cookie.get("TOKEN")+"&ddtab=true");
					}else{
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openword?netDiskId=' + netDiskId + '&path=' + encodeURIComponent(path) + '&extName=' + extName + '&openModeType=' + role+'&token='+this.$TOOL.cookie.get("TOKEN")+"&ddtab=true");
					}
				}else if(sysConfig.EXCEL_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openexcel?netDiskId=' + tool.crypto.createParam(netDiskId) + '&path=' + tool.crypto.createParam(path) + '&extName=' + extName + '&openModeType=' + role+'&token='+this.$TOOL.cookie.get("TOKEN")+"&ddtab=true");
					}else{
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openexcel?netDiskId=' + netDiskId + '&path=' + encodeURIComponent(path) + '&extName=' + extName + '&openModeType=' + role+'&token='+this.$TOOL.cookie.get("TOKEN")+"&ddtab=true");
					}
				}else if(sysConfig.PPT_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openppt?netDiskId=' + tool.crypto.createParam(netDiskId) + '&path=' + tool.crypto.createParam(path) + '&extName=' + extName + '&openModeType=' + role+'&token='+this.$TOOL.cookie.get("TOKEN")+"&ddtab=true");
					}else{
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openppt?netDiskId=' + netDiskId + '&path=' + encodeURIComponent(path) + '&extName=' + extName + '&openModeType=' + role+'&token='+this.$TOOL.cookie.get("TOKEN")+"&ddtab=true");
					}
				}else if(sysConfig.PDF_FILE.includes(extName))
				{
					if(extName=='.ofd')
					{
						this.$nextTick(() => {
							window.open('/#/file/preview/ofd?netDiskId=' + netDiskId + "&extName=" + extName + "&isNetDisk=true&attachId=" +encodeURIComponent(path)+"&ddtab=true", "_blank");
						})
					}else{
						this.$nextTick(() => {
							window.open('/#/file/preview/pdf?netDiskId=' + netDiskId + "&extName=" + extName + "&isNetDisk=true&attachId=" +encodeURIComponent(path)+"&ddtab=true", "_blank");
						})
					}
				}else if (sysConfig.IMG_FILE.includes(extName)) {
					const image = new Image();
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						image.src = sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + tool.crypto.createParam(netDiskId) + "&fileName=" + tool.crypto.createParam(path)+"&ddtab=true"
					}else{
						image.src = sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + netDiskId + "&fileName=" + encodeURIComponent(path)+"&ddtab=true"
					}
						image.onload = () => {
							image.style.margin = "0 auto"
							image.style.display = "block"
							const newWin = window.open("", "_blank")
							newWin.document.write(image.outerHTML)
						}
				}
			}else if (this.$TOOL.data.get("OFFICE_TYPE") == '3') {
				if(sysConfig.DOC_FILE.includes(extName)||sysConfig.EXCEL_FILE.includes(extName)||sysConfig.PPT_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/v3/weboffice/index?attachId='+tool.crypto.createParam(path)+'&netDiskId=' + tool.crypto.createParam(netDiskId)+'&extName='+extName+'&openModeType=' + role+'&isNetDisk=1&token='+this.$TOOL.cookie.get("TOKEN")+"&ddtab=true");
					}else{
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/v3/weboffice/index?attachId='+encodeURIComponent(path)+'&netDiskId=' + netDiskId+'&extName='+extName+'&openModeType=' + role+'&isNetDisk=1&token='+this.$TOOL.cookie.get("TOKEN")+"&ddtab=true");
					}
				}else if(sysConfig.PDF_FILE.includes(extName))
				{
					this.openPdfFile(extName, netDiskId, role, path)
				}else if (sysConfig.IMG_FILE.includes(extName)) {
					const image = new Image();
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						image.src = sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + tool.crypto.createParam(netDiskId) + "&fileName=" + tool.crypto.createParam(path)+"&ddtab=true"
					}else{
						image.src = sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + netDiskId + "&fileName=" + encodeURIComponent(path)+"&ddtab=true"
					}
					image.onload = () => {
						image.style.margin = "0 auto"
						image.style.display = "block"
						const newWin = window.open("", "_blank")
						newWin.document.write(image.outerHTML)
					}
				}
			}
		},
		openPdfFile(extName, netDiskId, role, path) {
			if (this.$TOOL.data.get("OFD_TYPE") === '0') {
				if(extName=='.ofd')
				{
					this.$nextTick(() => {
						window.open('/#/file/preview/ofd?attachId=' + path + "&extName=" + extName+ "&netDiskId=" + netDiskId+ "&isNetDisk=true"+"&ddtab=true", "_blank");
					})
				}else {
					this.$nextTick(() => {
						window.open('/#/file/preview/pdf?attachId=' + path + "&extName=" + extName+ "&netDiskId=" + netDiskId+ "&isNetDisk=true"+"&ddtab=true", "_blank");
					})
				}
			} else if (this.$TOOL.data.get("OFD_TYPE") === '1') {
				if (sysConfig.SM4_PRIVATE_KEY != '') {
					window.open(this.$TOOL.data.get("ATTACH_HOST") + '/office/ofd/openfxofd?path=' + tool.crypto.createParam(path) + "&extName=" + extName+ "&netDiskId=" + tool.crypto.createParam(netDiskId)+ "&isNetDisk=true&openModeType="+role+"&ddtab=true", "_blank");
				}else{
					window.open(this.$TOOL.data.get("ATTACH_HOST") + '/office/ofd/openfxofd?path=' + path + "&extName=" + extName+ "&netDiskId=" + netDiskId+ "&isNetDisk=true&openModeType="+role+"&ddtab=true", "_blank");
				}
			} else if (this.$TOOL.data.get("OFD_TYPE") === '2') {
				if (sysConfig.SM4_PRIVATE_KEY != '') {
					window.open(this.$TOOL.data.get("ATTACH_HOST") + '/office/ofd/openswofd?path=' + tool.crypto.createParam(path) + "&extName=" + extName+ "&netDiskId=" + tool.crypto.createParam(netDiskId)+ "&isNetDisk=true&openModeType="+role+"&ddtab=true", "_blank");
				}else{
					window.open(this.$TOOL.data.get("ATTACH_HOST") + '/office/ofd/openswofd?path=' + path + "&extName=" + extName+ "&netDiskId=" + netDiskId+ "&isNetDisk=true&openModeType="+role+"&ddtab=true", "_blank");
				}
			}
		},
	}
}
