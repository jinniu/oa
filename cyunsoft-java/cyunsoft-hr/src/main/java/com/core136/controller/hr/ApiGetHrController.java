package com.core136.controller.hr;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.UserInfo;
import com.core136.bean.system.PageParam;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.hr.*;
import com.core136.unit.PasswordEncryptionTools;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/get/hr")
@CrossOrigin
@Api(value="人力GetController",tags={"人力获取数据接口"})
public class ApiGetHrController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
    private UserInfoService userInfoService;
    private HrDicService hrDicService;
	private HrDepartmentService hrDepartmentService;
	private HrUserService hrUserService;
	private HrContractService hrContractService;
	private HrLicenceService hrLicenceService;
	private HrLearnRecordService hrLearnRecordService;
	private HrWorkRecordService hrWorkRecordService;
	private HrIncentiveService hrIncentiveService;
	private HrWorkSkillsService hrWorkSkillsService;
	private HrTrainRecordService hrTrainRecordService;
	private HrPersonnelTransferService hrPersonnelTransferService;
	private HrLeaveRecordService hrLeaveRecordService;
	private HrReinstatementService hrReinstatementService;
	private HrTitleEvaluationService hrTitleEvaluationService;
	private HrCareRecordService hrCareRecordService;
	private HrRecruitNeedsService hrRecruitNeedsService;
	private HrRecruitPlanService hrRecruitPlanService;
	private HrRecruitTaskService hrRecruitTaskService;
	private HrKpiPlanService hrKpiPlanService;
	private HrSalaryRecordService hrSalaryRecordService;
	private HrKpiPlanRecordService hrKpiPlanRecordService;
	private HrKpiItemService hrKpiItemService;
	private HrWelfareRecordService hrWelfareRecordService;
	private HrEvaluateService hrEvaluateService;
	private HrEchartsService hrEchartsService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}
	@Autowired
	public void setHrDepartmentService(HrDepartmentService hrDepartmentService) {
		this.hrDepartmentService = hrDepartmentService;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}
	@Autowired
	public void setHrContractService(HrContractService hrContractService) {
		this.hrContractService = hrContractService;
	}
	@Autowired
	public void setHrLicenceService(HrLicenceService hrLicenceService) {
		this.hrLicenceService = hrLicenceService;
	}
	@Autowired
	public void setHrLearnRecordService(HrLearnRecordService hrLearnRecordService) {
		this.hrLearnRecordService = hrLearnRecordService;
	}
	@Autowired
	public void setHrWorkRecordService(HrWorkRecordService hrWorkRecordService) {
		this.hrWorkRecordService = hrWorkRecordService;
	}
	@Autowired
	public void setHrIncentiveService(HrIncentiveService hrIncentiveService) {
		this.hrIncentiveService = hrIncentiveService;
	}
	@Autowired
	public void setHrWorkSkillsService(HrWorkSkillsService hrWorkSkillsService) {
		this.hrWorkSkillsService = hrWorkSkillsService;
	}
	@Autowired
	public void setHrTrainRecordService(HrTrainRecordService hrTrainRecordService) {
		this.hrTrainRecordService = hrTrainRecordService;
	}
	@Autowired
	public void setHrPersonnelTransferService(HrPersonnelTransferService hrPersonnelTransferService) {
		this.hrPersonnelTransferService = hrPersonnelTransferService;
	}
	@Autowired
	public void setHrLeaveRecordService(HrLeaveRecordService hrLeaveRecordService) {
		this.hrLeaveRecordService = hrLeaveRecordService;
	}
	@Autowired
	public void setHrReinstatementService(HrReinstatementService hrReinstatementService) {
		this.hrReinstatementService = hrReinstatementService;
	}
	@Autowired
	public void setHrTitleEvaluationService(HrTitleEvaluationService hrTitleEvaluationService) {
		this.hrTitleEvaluationService = hrTitleEvaluationService;
	}
	@Autowired
	public void setHrCareRecordService(HrCareRecordService hrCareRecordService) {
		this.hrCareRecordService = hrCareRecordService;
	}
	@Autowired
	public void setHrRecruitNeedsService(HrRecruitNeedsService hrRecruitNeedsService) {
		this.hrRecruitNeedsService = hrRecruitNeedsService;
	}
	@Autowired
	public void setHrRecruitPlanService(HrRecruitPlanService hrRecruitPlanService) {
		this.hrRecruitPlanService = hrRecruitPlanService;
	}
	@Autowired
	public void setHrRecruitTaskService(HrRecruitTaskService hrRecruitTaskService) {
		this.hrRecruitTaskService = hrRecruitTaskService;
	}
	@Autowired
	public void setHrKpiPlanService(HrKpiPlanService hrKpiPlanService) {
		this.hrKpiPlanService = hrKpiPlanService;
	}
	@Autowired
	public void setHrSalaryRecordService(HrSalaryRecordService hrSalaryRecordService) {
		this.hrSalaryRecordService = hrSalaryRecordService;
	}
	@Autowired
	public void setHrKpiPlanRecordService(HrKpiPlanRecordService hrKpiPlanRecordService) {
		this.hrKpiPlanRecordService = hrKpiPlanRecordService;
	}
	@Autowired
	public void setHrKpiItemService(HrKpiItemService hrKpiItemService) {
		this.hrKpiItemService = hrKpiItemService;
	}
	@Autowired
	public void setHrWelfareRecordService(HrWelfareRecordService hrWelfareRecordService) {
		this.hrWelfareRecordService = hrWelfareRecordService;
	}
	@Autowired
	public void setHrEvaluateService(HrEvaluateService hrEvaluateService) {
		this.hrEvaluateService = hrEvaluateService;
	}
	@Autowired
	public void setHrEchartsService(HrEchartsService hrEchartsService) {
		this.hrEchartsService = hrEchartsService;
	}

	/**
	 * 人员关怀复职分析tabale
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCareTableForAnalysis")
	public RetDataBean getCareTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getCareTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 人员关怀复职柱状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCareBarForAnalysis")
	public RetDataBean getCareBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getCareBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 人员关怀复饼状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCarePieForAnalysis")
	public RetDataBean getCarePieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getCarePieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 职称评定分析tabale
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getEvaluationTableForAnalysis")
	public RetDataBean getEvaluationTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getEvaluationTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 职称评定柱状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getEvaluationBarForAnalysis")
	public RetDataBean getEvaluationBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getEvaluationBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 职称评定饼状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getEvaluationPieForAnalysis")
	public RetDataBean getEvaluationPieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getEvaluationPieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 人员复职分析tabale
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getReinstatTableForAnalysis")
	public RetDataBean getReinstatTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getReinstatTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 人员复职柱状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getReinstatBarForAnalysis")
	public RetDataBean getReinstatBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getReinstatBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 人员复饼状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getReinstatPieForAnalysis")
	public RetDataBean getReinstatPieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig =hrEchartsService.getReinstatPieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 *  人员离职分析tabale
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeaveTableForAnalysis")
	public RetDataBean getLeaveTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getLeaveTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 人员离职柱状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeaveBarForAnalysis")
	public RetDataBean getLeaveBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig =hrEchartsService.getLeaveBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 人员离职饼状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeavePieForAnalysis")
	public RetDataBean getLeavePieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getLeavePieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 调动类型分析tabale
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTransferTableForAnalysis")
	public RetDataBean getTransferTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getTransferTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 调动类型柱状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTransferBarForAnalysis")
	public RetDataBean getTransferBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getTransferBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 调动类型饼状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTransferPieForAnalysis")
	public RetDataBean getTransferPieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getTransferPieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 劳动技能分析tabale
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSkillsTableForAnalysis")
	public RetDataBean getSkillsTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getSkillsTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 劳动技能柱状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSkillsBarForAnalysis")
	public RetDataBean getSkillsBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getSkillsBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 劳动技能饼状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSkillsPieForAnalysis")
	public RetDataBean getSkillsPieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getSkillsPieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 学习经功分析tabale
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLearnTableForAnalysis")
	public RetDataBean getLearnTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getLearnTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 学习经历饼状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLearnPieForAnalysis")
	public RetDataBean getLearnPieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getLearnPieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 学习经历柱状图分析
	 * @param deptId 部门Id
	 * @param dataType 查询数据类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLearnBarForAnalysis")
	public RetDataBean getLearnBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getLearnBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 证照分析tabale
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getLicenceTableForAnalysis")
	public RetDataBean getLicenceTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getLicenceTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 证照柱状图分析
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getLicenceBarForAnalysis")
	public RetDataBean getLicenceBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getLicenceBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 证照饼状图分析
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getLicencePieForAnalysis")
	public RetDataBean getLicencePieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getLicencePieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 奖惩分析tabale
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getIncentiveTableForAnalysis")
	public RetDataBean getIncentiveTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getIncentiveTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 奖惩柱状图分析
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getIncentiveBarForAnalysis")
	public RetDataBean getIncentiveBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getIncentiveBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 奖惩饼状图分析
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getIncentivePieForAnalysis")
	public RetDataBean getIncentivePieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getIncentivePieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 合同分析tabale
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getContractTableForAnalysis")
	public RetDataBean getContractTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getContractTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 合同柱状图分析
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getContractBarForAnalysis")
	public RetDataBean getContractBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getContractBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 合同饼状图分析
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getContractPieForAnalysis")
	public RetDataBean getContractPieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getContractPieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 人事档案分析Table
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getBaseInfoTableForAnalysis")
	public RetDataBean getBaseInfoTableForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEchartsService.getBaseInfoTableForAnalysis(userInfo.getOrgId(), deptId, dataType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 人事档案分析饼状图
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getBaseInfoPieForAnalysis")
	public RetDataBean getBaseInfoPieForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getBaseInfoPieForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 人事档案分析柱状图
	 * @param deptId
	 * @param dataType
	 * @return
	 */
	@GetMapping(value = "/getBaseInfoBarForAnalysis")
	public RetDataBean getBaseInfoBarForAnalysis(String deptId, String dataType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = hrEchartsService.getBaseInfoBarForAnalysis(userInfo.getOrgId(), deptId, dataType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 按条件获取部门列表
	 * @param parentId
	 * @return
	 */
	@GetMapping(value = "/getDeptListByDeptIdForSelect")
	public RetDataBean getDeptListByDeptIdForSelect(String parentId,String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isBlank(keyword))
			{
				keyword="";
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDepartmentService.getDeptListByDeptIdForSelect(userInfo.getOrgId(),parentId,keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取部门列表
	 * @param deptIds
	 * @return
	 */
	@GetMapping(value = "/getHrDeptByDeptIds")
	public RetDataBean getHrDeptByDeptIds(String deptIds) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(deptIds)) {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, new JSONArray());
			} else {
				if (deptIds.equals("@all")) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("deptId", "@all");
					jsonObject.put("deptName", "全体部门");
					JSONArray jsonArray = new JSONArray();
					jsonArray.add(jsonObject);
					return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, jsonArray);
				} else {
					String[] aArr = deptIds.split(",");
					List<String> list = Arrays.asList(aArr);
					return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDepartmentService.getHrDeptByDeptIds(userInfo.getOrgId(), list));
				}
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人员评价列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrEvaluateByUserIdList")
	public RetDataBean getHrEvaluateByUserIdList(PageParam pageParam, String userId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("e.create_time");
			} else {
				pageParam.setProp("e."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrEvaluateService.getHrEvaluateByUserIdList(pageParam, userId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 查询领导评价
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param status
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrEvaluateQueryList")
	public RetDataBean getHrEvaluateQueryList(PageParam pageParam, String userId, String date, String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("e.create_time");
			} else {
				pageParam.setProp("e."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrEvaluateService.getHrEvaluateQueryList(pageParam, userId, date,timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取福利列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param welfareType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrWelfareRecordList")
	public RetDataBean getHrWelfareRecordList(PageParam pageParam, String userId, String date, String welfareType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = hrWelfareRecordService.getHrWelfareRecordList(pageParam,date, timeArr[0], timeArr[1], userId, welfareType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取个人考核列表
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyKpiPlanForList")
	public RetDataBean getMyKpiPlanForList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.create_time");
			} else {
				pageParam.setProp("l." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getSortOrder())) {
				pageParam.setSortOrder("desc");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrKpiPlanService.getMyKpiPlanForList(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 查询获取考核列表
	 * @param pageParam 分页参数
	 * @param planId
	 * @param chargeUser
	 * @param accountId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getKpiPlanForUserQueryList")
	public RetDataBean getKpiPlanForUserQueryList(PageParam pageParam,
												  String planId, String chargeUser, String accountId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("createTime");
			} else {
				pageParam.setProp(StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getSortOrder())) {
				pageParam.setSortOrder("desc");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(accountId);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrKpiPlanService.getKpiPlanForUserQueryList(pageParam, planId, chargeUser);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取考核管理列表
	 * @param pageParam 分页参数
	 * @param status
	 * @param kpiRule
	 * @param date
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrKpiPlanList")
	public RetDataBean getMyHrKpiPlanList(PageParam pageParam, String status, String kpiRule,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.sort_no");
			} else {
				pageParam.setProp("l."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrKpiPlanService.getMyHrKpiPlanList(pageParam, status, kpiRule,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取考核指标列表
	 * @param pageParam 分页参数
	 * @param createUser
	 * @param kpiType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrKpiItemList")
	public RetDataBean getHrKpiItemList(PageParam pageParam, String createUser, String kpiType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getSortOrder())) {
				pageParam.setSortOrder("desc");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrKpiItemService.getHrKpiItemList(pageParam, createUser, kpiType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  获取考核指标集
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrKpiItemListForSelect")
	public RetDataBean getHrKpiItemListForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiItemService.getHrKpiItemListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取个人考核分数明细
	 * @param planId
	 * @param accountId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrKpiScoreList")
	public RetDataBean getMyHrKpiScoreList(String planId, String accountId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanRecordService.getMyHrKpiScoreList(userInfo.getOrgId(), planId, accountId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人员考核
	 * @param planId
	 * @param accountId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrKpiRecordForUser")
	public RetDataBean getHrKpiRecordForUser(String planId, String accountId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanRecordService.getHrKpiRecordForUser(userInfo.getOrgId(), planId, accountId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取考核详情
	 * @param planId
	 * @param accountId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrKpiRecordForDetails")
	public RetDataBean getHrKpiRecordForDetails(String planId, String accountId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanRecordService.getHrKpiRecordForDetails(userInfo.getOrgId(), planId, accountId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人员薪资列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param year
	 * @param month
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrSalaryRecordList")
	public RetDataBean getHrSalaryRecordList(PageParam pageParam, String userId, String year, String month
	) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getSortOrder())) {
				pageParam.setSortOrder("desc");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrSalaryRecordService.getHrSalaryRecordList(pageParam, userId, year, month);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人员薪资列表
	 * @param pageParam 分页参数
	 * @param password 登陆登陆
	 * @return 消息结构 消息结构
	 */
	@GetMapping(value = "/getMyHrSalaryRecordList")
	public RetDataBean getMyHrSalaryRecordList(PageParam pageParam,String password) {
		UserInfo userInfo = userInfoService.getRedisUser();
		if(!PasswordEncryptionTools.getEnPassWord(userInfo.getAccountId() + password).equals(userInfo.getPassWord()))
		{
			return RetDataTools.Ok(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS, new PageInfo<Map<String, String>>());
		}
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrSalaryRecordService.getMyHrSalaryRecordList(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取考核计划列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getKpiPlanForListForSelect")
	public RetDataBean getKpiPlanForListForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanService.getKpiPlanForListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人员考核列表
	 * @param pageParam 分页参数
	 * @return 消息结构 考核列表
	 */
	@GetMapping(value = "/getKpiPlanForUserList")
	public RetDataBean getKpiPlanForUserList(PageParam pageParam,String userId,String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.sort_no");
			} else {
				pageParam.setProp("l."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrKpiPlanService.getKpiPlanForUserList(pageParam,userId,status,date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  获取培训列表
	 * @param pageParam 分页参数
	 * @param channel
	 * @param courseType
	 * @param status
	 * @param date
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrTrainRecordList")
	public RetDataBean getHrTrainRecordList(PageParam pageParam,String channel, String courseType, String status,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrTrainRecordService.getHrTrainRecordList(pageParam, channel, courseType, status, date,timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取培训审批记录
	 * @param pageParam 分页参数
	 * @param channel
	 * @param courseType
	 * @param date
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrTrainRecordApprovedList")
	public RetDataBean getHrTrainRecordApprovedList(PageParam pageParam,String channel, String courseType, String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("create_time");
			} else {
				pageParam.setProp(StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrTrainRecordService.getHrTrainRecordApprovedList(pageParam, channel, courseType,status,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取招聘任务列表
	 * @param pageParam 分页参数
	 * @param date
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrRecruitTaskList")
	public RetDataBean getHrRecruitTaskList(PageParam pageParam, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.create_time");
			} else {
				pageParam.setProp("t."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrRecruitTaskService.getHrRecruitTaskList(pageParam, date,timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取待审批需求列表
	 * @param pageParam 分页参数
	 * @param highsetShool
	 * @param occupation
	 * @param date
	 * @return 消息结构
	 */
	@GetMapping(value = "/getApprovedHrRecruitNeedsList")
	public RetDataBean getApprovedHrRecruitNeedsList(PageParam pageParam, String highsetShool, String occupation, String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrRecruitNeedsService.getApprovedHrRecruitNeedsList(pageParam, occupation, highsetShool,date, timeArr[0], timeArr[1],status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取招聘计划列表
	 * @param pageParam 分页参数
	 * @param date
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrRecruitPlanList")
	public RetDataBean getHrRecruitPlanList(PageParam pageParam, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrRecruitPlanService.getHrRecruitPlanList(pageParam, date,timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取当前可填报的招聘计划
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrRecruitPlanForSelect")
	public RetDataBean getHrRecruitPlanForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrRecruitPlanService.getHrRecruitPlanForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取招聘需求列表
	 * @param pageParam 分页参数
	 * @param education
	 * @param occupation
	 * @param status
	 * @param date
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrRecruitNeedsList")
	public RetDataBean getHrRecruitNeedsList(PageParam pageParam,String education, String occupation, String status, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrRecruitNeedsService.getHrRecruitNeedsList(pageParam, occupation, education, status,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取员工关怀记录列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param careType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrCareRecordList")
	public RetDataBean getHrCareRecordList(PageParam pageParam, String userId, String date,String careType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrCareRecordService.getHrCareRecordList(pageParam, userId, date,timeArr[0], timeArr[1], careType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取人员职称评定列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param getType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrTitleEvaluationList")
	public RetDataBean getHrTitleEvaluationList(PageParam pageParam, String userId, String date,String getType
	) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrTitleEvaluationService.getHrTitleEvaluationList(pageParam, userId, date,timeArr[0], timeArr[1], getType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取复职记录列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param reinstatementType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrReinstatementList")
	public RetDataBean getHrReinstatementList(PageParam pageParam, String userId, String date,String reinstatementType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrReinstatementService.getHrReinstatementList(pageParam, userId, date,timeArr[0], timeArr[1], reinstatementType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取离职人员列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param leaveType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrLeaveRecordList")
	public RetDataBean getHrLeaveRecordList(PageParam pageParam, String userId, String date, String leaveType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrLeaveRecordService.getHrLeaveRecordList(pageParam, userId, date,timeArr[0], timeArr[1], leaveType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 *  获取人员调动列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param transferType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrPersonnelTransferList")
	public RetDataBean getHrPersonnelTransferList(PageParam pageParam, String userId, String date,String transferType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrPersonnelTransferService.getHrPersonnelTransferList(pageParam, userId,date, timeArr[0], timeArr[1], transferType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 工作特长列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param skillsLevel
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrWorkSkillsList")
	public RetDataBean getHrWorkSkillsList(PageParam pageParam, String userId, String date,String skillsLevel) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrWorkSkillsService.getHrWorkSkillsList(pageParam, userId, date,timeArr[0], timeArr[1], skillsLevel);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取奖惩记录列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param incentiveType
	 * @param incentiveItem
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrIncentiveList")
	public RetDataBean getHrIncentiveList(PageParam pageParam, String userId, String date, String incentiveType, String incentiveItem) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrIncentiveService.getHrIncentiveList(pageParam, userId, date,timeArr[0], timeArr[1], incentiveType, incentiveItem);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取工作记录
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param nature
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrWorkRecordList")
	public RetDataBean getHrWorkRecordList(PageParam pageParam, String userId, String date, String nature) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrWorkRecordService.getHrWorkRecordList(pageParam, userId, date,timeArr[0], timeArr[1], nature);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取教育经历列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrLearnRecordList")
	public RetDataBean getHrLearnRecordList(PageParam pageParam, String userId,String date,String highsetDegree) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrLearnRecordService.getHrLearnRecordList(pageParam, userId, date,timeArr[0], timeArr[1],highsetDegree);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取证照列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param licenceType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrLicenceList")
	public RetDataBean getHrLicenceList(PageParam pageParam, String userId, String date,String licenceType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sort_no");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDayAfter(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrLicenceService.getHrLicenceList(pageParam, userId,date, timeArr[0], timeArr[1], licenceType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 按部门获取账户信息
	 *
	 * @param deptId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrUserListByDeptIdForSelect")
	public RetDataBean getHrUserListByDeptIdForSelect(String deptId, String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(keyword)) {
				keyword = "";
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserService.getHrUserListByDeptIdForSelect(userInfo.getOrgId(), deptId, keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按账号获取人员列表
	 *
	 * @param userIds
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrUserListByUserIds")
	public RetDataBean getHrUserListByUserIds(String userIds) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(userIds)) {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, new JSONArray());
			} else {
				if (userIds.equals("@all")) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("userId", "@all");
					jsonObject.put("userName", "全体人员");
					jsonObject.put("sex", "0");
					JSONArray jsonArray = new JSONArray();
					jsonArray.add(jsonObject);
					return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, jsonArray);
				} else {
					String[] aArr = userIds.split(",");
					List<String> list = Arrays.asList(aArr);
					return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserService.getHrUserListByUserIds(userInfo.getOrgId(), list));
				}
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取合同列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param date
	 * @param contractType
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrContractList")
	public RetDataBean getHrContractList(PageParam pageParam, String userId, String date,String contractType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.sign_time");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrContractService.getHrContractList(pageParam, userId, date,timeArr[0],timeArr[1],contractType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人事合同详情
	 * @param contractId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrContractById")
	public RetDataBean getHrContractById(String contractId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrContractService.getHrContractById(userInfo.getOrgId(),contractId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取个人人事合同列表
	 * @param page
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrContractListForApp")
	public RetDataBean getMyHrContractListForApp(Integer page) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrContractService.getMyHrContractListForApp(userInfo.getOrgId(), userInfo.getAccountId(), page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取移动端个人薪资查询
	 * @param page
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrSalaryRecordListForApp")
	public RetDataBean getMyHrSalaryRecordListForApp(Integer page) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrSalaryRecordService.getMyHrSalaryRecordListForApp(userInfo.getOrgId(), userInfo.getAccountId(), page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 查询自己的合同列表
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrContractList")
	public RetDataBean getMyHrContractList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrContractService.getMyHrContractList(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取快到期的合同列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDeskHrContractList")
	public RetDataBean getDeskHrContractList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrContractService.getDeskHrContractList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取部门下的人员列表
	 * @param pageParam 分页参数
	 * @param deptId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrUserListByDeptId")
	public RetDataBean getHrUserListByDeptId(PageParam pageParam, String deptId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("u.sort_no");
			} else {
				pageParam.setProp("u."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setDeptId(deptId);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrUserService.getHrUserListByDeptId(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取部门列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrDepartmentTree")
	public RetDataBean getHrDepartmentTree(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDepartmentService.getHrDepartmentTree(userInfo.getOrgId(), keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
    /**
     * 获取字典分类列表
     *
     * @return 消息结构
     */
    @GetMapping(value = "/getHrDicParentList")
    public RetDataBean getHrDicParentList() {
        try {
            UserInfo userInfo = userInfoService.getRedisUser();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDicService.getHrDicParentList(userInfo.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取字典数
     *
     * @return 消息结构
     */
    @GetMapping(value = "/getHrDicTree")
    public RetDataBean getHrDicTree() {
        try {
            UserInfo userInfo = userInfoService.getRedisUser();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDicService.getHrDicTree(userInfo.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 按模块获取分类列表
     *
     * @param code
     * @return 消息结构
     */
    @GetMapping(value = "/getHrDicByCode")
    public RetDataBean getHrDicByCode(String code) {
        try {
            UserInfo userInfo = userInfoService.getRedisUser();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDicService.getHrDicByCode(userInfo.getOrgId(), code));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取字典列表
     *
     * @param pageParam 分页参数
     * @param parentId
     * @return 消息结构
     */
    @GetMapping(value = "/getHrDicList")
    public RetDataBean getHrDicList(PageParam pageParam, String parentId) {
        try {
            if (StringUtils.isBlank(pageParam.getProp())) {
                pageParam.setProp("d.sort_no");
            } else {
                pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
            }
            if (StringUtils.isBlank(pageParam.getOrder())) {
                pageParam.setSortOrder("asc");
            } else {
                if (pageParam.getOrder().equals("descending")) {
                    pageParam.setSortOrder("desc");
                } else if (pageParam.getOrder().equals("ascending")) {
                    pageParam.setSortOrder("asc");
                }
            }
            pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
            UserInfo userInfo = userInfoService.getRedisUser();
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getUserLevel());
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOpFlag(userInfo.getOpFlag());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDicService.getHrDicList(pageParam, parentId));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 个人查询奖惩记录
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrIncentiveList")
	public RetDataBean getMyHrIncentiveList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrIncentiveService.getMyHrIncentiveList(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 查询个人证照信息
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrLicenceList")
	public RetDataBean getMyHrLicenceList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrLicenceService.getMyHrLicenceList(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人证照列表
	 * @param page
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrLicenceListForApp")
	public RetDataBean getHrLicenceListForApp(Integer page) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrLicenceService.getHrLicenceListForApp(userInfo.getOrgId(), userInfo.getAccountId(), page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取证照详情
	 * @param licenceId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrLicenceById")
	public RetDataBean getHrLicenceById(String licenceId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrLicenceService.getHrLicenceById(userInfo.getOrgId(), licenceId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 个人查询学习记录
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrLearnRecordList")
	public RetDataBean getMyHrLearnRecordList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = hrLearnRecordService.getMyHrLearnRecordList(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 查询个人工作经历
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrWorkRecordList")
	public RetDataBean getMyHrWorkRecordList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrWorkRecordService.getMyHrWorkRecordList(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 人个工作调动记录
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrPersonnelTransferList")
	public RetDataBean getMyHrPersonnelTransferList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("h.create_time");
			} else {
				pageParam.setProp("h." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = hrPersonnelTransferService.getMyHrPersonnelTransferList(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人事档案
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrUserByAccountId")
	public RetDataBean getHrUserByAccountId(String userId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isNotBlank(userId))
			{
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserService.getHrUserByAccountId(userInfo.getOrgId(),userId));
			}else
			{
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserService.getHrUserByAccountId(userInfo.getOrgId(),userInfo.getAccountId()));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取考核记录详情
	 * @param planId 计划Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyHrKpiPlanById")
	public RetDataBean getMyHrKpiPlanById(String planId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(planId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanService.getMyHrKpiPlanById(userInfo.getOrgId(),planId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取薪资记录详情
	 * @param recordId
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHrSalaryRecordById")
	public RetDataBean getHrSalaryRecordById(String recordId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(recordId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrSalaryRecordService.getHrSalaryRecordById(userInfo.getOrgId(),recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 导出人员信息
	 * @param response HttpServletResponse
	 */
	@PostMapping(value = "/getExpUserRecord")
	public void getExpUserRecord(HttpServletResponse response) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrUserService.getExpUserRecord(response, userInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
