package com.core136.bean.hr;

import java.io.Serializable;

/**
 * 奖惩记录
 * @author lsq
 */
public class HrIncentive implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String incentiveId;
    private Integer sortNo;
    private String incentiveType;
    private String incentiveItem;
    private String userId;
    private Double incentiveAmount;
    private String attachId;
    private String incentiveTime;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getIncentiveId() {
        return incentiveId;
    }

    public void setIncentiveId(String incentiveId) {
        this.incentiveId = incentiveId;
    }

    public String getIncentiveType() {
        return incentiveType;
    }

    public void setIncentiveType(String incentiveType) {
        this.incentiveType = incentiveType;
    }

    public String getIncentiveItem() {
        return incentiveItem;
    }

    public void setIncentiveItem(String incentiveItem) {
        this.incentiveItem = incentiveItem;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public Double getIncentiveAmount() {
        return incentiveAmount;
    }

    public void setIncentiveAmount(Double incentiveAmount) {
        this.incentiveAmount = incentiveAmount;
    }

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getIncentiveTime() {
        return incentiveTime;
    }

    public void setIncentiveTime(String incentiveTime) {
        this.incentiveTime = incentiveTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }


}
