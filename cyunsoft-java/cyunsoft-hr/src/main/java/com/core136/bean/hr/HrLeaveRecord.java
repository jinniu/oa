package com.core136.bean.hr;

import java.io.Serializable;

public class HrLeaveRecord implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String userId;
    private Integer sortNo;
    private String leaveType;
    private String applyTime;
    private String planTime;
    private String factTime;
    private String lastSalaryTime;
    private String salary;
    private String trace;
    private String leaveCondition;
    private String remark;
    private String attachId;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getPlanTime() {
        return planTime;
    }

    public void setPlanTime(String planTime) {
        this.planTime = planTime;
    }

    public String getFactTime() {
        return factTime;
    }

    public void setFactTime(String factTime) {
        this.factTime = factTime;
    }

    public String getLastSalaryTime() {
        return lastSalaryTime;
    }

    public void setLastSalaryTime(String lastSalaryTime) {
        this.lastSalaryTime = lastSalaryTime;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }

	public String getLeaveCondition() {
		return leaveCondition;
	}

	public void setLeaveCondition(String leaveCondition) {
		this.leaveCondition = leaveCondition;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
