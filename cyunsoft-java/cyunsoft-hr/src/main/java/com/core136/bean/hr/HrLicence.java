package com.core136.bean.hr;

import java.io.Serializable;

/**
 * @author lsq
 * HR人员证照管理
 */
public class HrLicence implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String licenceId;
    private Integer sortNo;
    private String licenceCode;
    private String name;
    private String userId;
    private String beginTime;
    private String endTime;
    private String licenceType;
    private String attachId;
    private String remark;
    private String notifiedBody;
    private String sendToUser;
    private String msgType;
    private String reminder;
    private String createTime;
    private String createUser;
    private String orgId;

	public String getLicenceId() {
		return licenceId;
	}

	public void setLicenceId(String licenceId) {
		this.licenceId = licenceId;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getLicenceCode() {
		return licenceCode;
	}

	public void setLicenceCode(String licenceCode) {
		this.licenceCode = licenceCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLicenceType() {
		return licenceType;
	}

	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getNotifiedBody() {
		return notifiedBody;
	}

	public void setNotifiedBody(String notifiedBody) {
		this.notifiedBody = notifiedBody;
	}

	public String getSendToUser() {
		return sendToUser;
	}

	public void setSendToUser(String sendToUser) {
		this.sendToUser = sendToUser;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getReminder() {
		return reminder;
	}

	public void setReminder(String reminder) {
		this.reminder = reminder;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
