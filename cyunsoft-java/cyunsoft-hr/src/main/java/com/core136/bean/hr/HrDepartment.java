package com.core136.bean.hr;

import java.io.Serializable;
import java.util.List;

/**
 * @author HR模块部门
 *
 */
public class HrDepartment implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String deptId;
    private Integer sortNo;
    private String deptName;
    private String levelId;
    private String parentId;
    private String deptTel;
    private String deptEmail;
    private String deptFax;
    private String deptLead;
    private String remark;
    private String status;
	private transient String deptLeadName;
	private transient List<HrDepartment> children;
	private String createTime;
	private String createUser;
    private String orgId;

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getLevelId() {
		return levelId;
	}

	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getDeptTel() {
		return deptTel;
	}

	public void setDeptTel(String deptTel) {
		this.deptTel = deptTel;
	}

	public String getDeptEmail() {
		return deptEmail;
	}

	public void setDeptEmail(String deptEmail) {
		this.deptEmail = deptEmail;
	}

	public String getDeptFax() {
		return deptFax;
	}

	public void setDeptFax(String deptFax) {
		this.deptFax = deptFax;
	}

	public String getDeptLead() {
		return deptLead;
	}

	public void setDeptLead(String deptLead) {
		this.deptLead = deptLead;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<HrDepartment> getChildren() {
		return children;
	}

	public void setChildren(List<HrDepartment> children) {
		this.children = children;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getDeptLeadName() {
		return deptLeadName;
	}

	public void setDeptLeadName(String deptLeadName) {
		this.deptLeadName = deptLeadName;
	}
}
