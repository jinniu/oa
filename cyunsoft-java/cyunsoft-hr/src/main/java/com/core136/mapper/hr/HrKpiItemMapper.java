package com.core136.mapper.hr;

import com.core136.bean.hr.HrKpiItem;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrKpiItemMapper extends MyMapper<HrKpiItem> {

	/**
	 * 获取考核指标列表
	 * @param orgId 机构码
	 * @param createUser
	 * @param kpiType
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrKpiItemList(@Param(value = "orgId") String orgId, @Param(value = "createUser") String createUser, @Param(value = "kpiType") String kpiType, @Param(value = "keyword") String keyword);

	/**
	 * 获取考核指标集
	 * @param orgId 机构码
	 * @return
	 */
    List<Map<String, String>> getHrKpiItemListForSelect(@Param(value = "orgId") String orgId);
}
