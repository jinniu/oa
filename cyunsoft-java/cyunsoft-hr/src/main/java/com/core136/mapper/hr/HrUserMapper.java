package com.core136.mapper.hr;

import com.core136.bean.hr.HrUser;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface HrUserMapper extends MyMapper<HrUser> {
	Map<String,String>getHrUserByAccountId(@Param(value="orgId")String orgId,@Param(value="accountId")String accountId);

	List<Map<String,Object>> getHrUserListByDeptIdForSelect(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId, @Param(value = "keyword") String keyword);
	/**
	 * 按账号获取人员列表
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	List<Map<String,String>> getHrUserListByUserIds(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);

	/**
	 * 获取部门下的人员列表
	 * @param orgId 机构码
	 * @param deptId
	 * @return
	 */
    List<Map<String, String>> getHrUserByDeptId(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId);

	/**
	 * 获取部门下的人员
	 * @param orgId 机构码
	 * @param deptId
	 * @param workStatus
	 * @param employedTime
	 * @param staffCardNo
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrUserByDeptIdInWorkList(@Param(value = "orgId") String orgId,
                                                              @Param(value = "deptId") String deptId, @Param(value = "workStatus") String workStatus,
                                                              @Param(value = "employedTime") String employedTime, @Param(value = "staffCardNo") String staffCardNo,
                                                              @Param(value = "keyword") String keyword);

	/**
	 * 按部门获取人员列表
	 * @param orgId 机构码
	 * @param deptId
	 * @return
	 */
    List<Map<String, String>> getHrUserForTree(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId);

	/**
	 * 获取HR人员列表
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	List<Map<String, String>> getUserNamesByUserIds(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);

	/**
	 * 查询HR人员
	 * @param orgId 机构码
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getHrUserBySearchUser(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

	/**
	 * 获取部门下的人员列表
	 * @param orgId 机构码
	 * @param deptId
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrUserListByDeptId(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId, @Param(value = "keyword") String keyword);

	/**
	 * 获取人力资源门户人员信息
	 * @param orgId 机构码
	 * @return
	 */
	Map<String, String> getDeskHrUser(@Param(value = "orgId") String orgId);

	/**
	 * 获取导出用户列表
	 * @param orgId 机构码
	 * @return 用户列表
	 */
	List<Map<String,Object>>getExpUserRecord(@Param(value = "orgId") String orgId);
}
