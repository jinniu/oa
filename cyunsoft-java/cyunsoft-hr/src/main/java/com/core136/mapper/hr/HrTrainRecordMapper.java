package com.core136.mapper.hr;

import com.core136.bean.hr.HrTrainRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 人员培训记录
 * @author lsq
 *
 */
public interface HrTrainRecordMapper extends MyMapper<HrTrainRecord> {

	/**
	 * 获取培训列表
	 * @param orgId 机构码
	 * @param createUser
	 * @param channel
	 * @param courseType
	 * @param status
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrTrainRecordList(@Param(value = "orgId") String orgId, @Param(value = "createUser") String createUser,
                                                   @Param(value = "channel") String channel, @Param(value = "courseType") String courseType, @Param(value = "status") String status,
												   @Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword
    );

	/**
	 * 获取待审批记录
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param channel
	 * @param courseType
	 * @param status
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrTrainRecordApprovedList(@Param(value = "orgId") String orgId,@Param(value = "opFlag") String opFlag,@Param(value = "accountId") String accountId,
                                                           @Param(value = "channel") String channel, @Param(value = "courseType") String courseType, @Param(value = "status") String status,
														   @Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword
    );


}
