package com.core136.mapper.hr;

import com.core136.bean.hr.HrDepartment;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface HrDepartmentMapper extends MyMapper<HrDepartment> {

	/**
	 * 按部门ID字符串获取部门列表
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	List<Map<String, String>> getHrDeptByDeptIds(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);

	List<HrDepartment>getAllHrDepartmentList(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

	/**
	 * 查询部门
	 * @param orgId 机构码
	 * @param parentId
	 * @param keyword 查询关键词
	 * @return
	 */
	List<Map<String, Object>> getDeptListByDeptIdForSelect(@Param(value = "orgId") String orgId, @Param(value = "parentId") String parentId, @Param(value = "keyword") String keyword);

}
