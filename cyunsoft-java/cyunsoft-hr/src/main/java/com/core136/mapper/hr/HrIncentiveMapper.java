package com.core136.mapper.hr;

import com.core136.bean.hr.HrIncentive;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrIncentiveMapper extends MyMapper<HrIncentive> {

	/**
	 * 获取奖惩记录列表
	 * @param orgId 机构码
	 * @param userId 用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param incentiveType 奖惩类型
	 * @param incentiveItem 奖惩内容
	 * @return 奖惩记录列表
	 */
    List<Map<String, String>> getHrIncentiveList(@Param(value = "orgId") String orgId,@Param(value = "userId") String userId,
												 @Param(value = "dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                 @Param(value = "endTime") String endTime, @Param(value = "incentiveType") String incentiveType,
                                                 @Param(value = "incentiveItem") String incentiveItem
    );

	/**
	 * 个人查询奖惩记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 奖惩记录列表
	 */
    List<Map<String, String>> getMyHrIncentiveList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);
}
