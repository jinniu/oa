package com.core136.mapper.hr;

import com.core136.bean.hr.HrWelfareRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrWelfareRecordMapper extends MyMapper<HrWelfareRecord> {

	/**
	 * 获取福利列表
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param welfareType
	 * @param userId
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrWelfareRecordList(@Param(value = "orgId") String orgId,@Param(value="dateQueryType")String dateQueryType, @Param(value = "beginTime") String beginTime,
                                                     @Param(value = "endTime") String endTime, @Param(value = "welfareType") String welfareType,
                                                     @Param(value = "userId") String userId, @Param(value = "keyword") String keyword);

}
