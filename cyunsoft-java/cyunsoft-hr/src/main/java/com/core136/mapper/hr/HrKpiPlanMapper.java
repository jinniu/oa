package com.core136.mapper.hr;

import com.core136.bean.hr.HrKpiPlan;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrKpiPlanMapper extends MyMapper<HrKpiPlan> {
	/**
	 * 获取考核记录详情
	 * @param orgId 机构码
	 * @param planId 考核计划
	 * @return 考核记录详情
	 */
	Map<String,String>getMyHrKpiPlanById(@Param(value="orgId")String orgId,@Param(value = "planId")String planId);

	/**
	 * 获取考核管理列表
	 * @param orgId 机构码
	 * @param status 状态
	 * @param kpiRule 考核规则
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 考核管理列表
	 */
    List<Map<String, String>> getMyHrKpiPlanList(@Param(value = "orgId") String orgId, @Param(value = "status") String status,@Param(value = "kpiRule") String kpiRule,
												 @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                 @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);

	/**
	 * 获取人员考核列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param nowTime
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getKpiPlanForUserList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value="userId")String userId,@Param(value="status")String status,
                                                    @Param(value = "nowTime") String nowTime,@Param(value="dateQueryType")String dateQueryType,@Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
													@Param(value = "keyword") String keyword);


	/**
	 * 查询获取考核列表
	 * @param orgId 机构码
	 * @param planId
	 * @param chargeUser
	 * @param accountId 用户账号
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getKpiPlanForUserQueryList(@Param(value = "orgId") String orgId,
                                                         @Param(value = "planId") String planId, @Param(value = "chargeUser") String chargeUser,
                                                         @Param(value = "accountId") String accountId, @Param(value = "keyword") String keyword);

	/**
	 * 获取个人考核列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 个人考核列表
	 */
    List<Map<String, String>> getMyKpiPlanForList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);


	/**
	 * 获取考核计划列表
	 * @param orgId 机构码
	 * @return 考核计划列表
	 */
	List<Map<String, String>> getKpiPlanForListForSelect(@Param(value = "orgId") String orgId);

}
