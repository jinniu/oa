package com.core136.mapper.hr;

import com.core136.bean.hr.HrRecruitTask;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface HrRecruitTaskMapper extends MyMapper<HrRecruitTask> {

	/**
	 * 获取招聘任务列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrRecruitTaskList(@Param(value = "orgId") String orgId,@Param(value = "dateQueryType") String dateQueryType, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                   @Param(value = "keyword") String keyword);

}
