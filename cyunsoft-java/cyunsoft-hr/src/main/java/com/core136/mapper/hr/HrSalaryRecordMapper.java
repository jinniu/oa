package com.core136.mapper.hr;

import com.core136.bean.hr.HrSalaryRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrSalaryRecordMapper extends MyMapper<HrSalaryRecord> {
	/**
	 * 获取薪资记录详情
	 * @param orgId 机构码
	 * @param recordId
	 * @return
	 */
	Map<String,String>getHrSalaryRecordById(@Param(value = "orgId")String orgId,@Param(value="recordId")String recordId);
	/**
	 * 获取人员薪资列表
	 * @param orgId 机构码
	 * @param userId
	 * @param year
	 * @param month
	 * @return
	 */
    List<Map<String, String>> getHrSalaryRecordList(
            @Param(value = "orgId") String orgId,
            @Param(value = "userId") String userId,
            @Param(value = "year") String year,
            @Param(value = "month") String month
    );

	/**
	 * 个人薪资查询
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 个人薪资
	 */
    List<Map<String, String>> getMyHrSalaryRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 * 获取移动端个人薪资查询
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param page 当前页码
	 * @return 个人薪资
	 */
	List<Map<String, String>> getMyHrSalaryRecordListForApp(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value = "page") Integer page);

}
