package com.core136.mapper.hr;

import com.core136.bean.hr.HrLicence;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface HrLicenceMapper extends MyMapper<HrLicence> {

	/**
	 * 获取个人证照列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param page 当前页码
	 * @return 个人证照列表
	 */
	List<Map<String, String>> getHrLicenceListForApp(@Param(value = "orgId") String orgId,
												  @Param(value = "accountId") String accountId,
												  @Param(value = "page") Integer page);
	/**
	 * 获取证照详情
	 * @param orgId 机构码
	 * @param licenceId
	 * @return
	 */
	Map<String,String>getHrLicenceById(@Param(value="orgId")String orgId,@Param(value="licenceId")String licenceId);

	/**
	 * 获取证照列表
	 * @param orgId 机构码
	 * @param userId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param licenceType 证照类型
	 * @param keyword 查询关键词
	 * @return  证照列表
	 */
    List<Map<String, String>> getHrLicenceList(@Param(value = "orgId") String orgId,@Param(value = "userId") String userId, @Param(value = "dateQueryType") String dateQueryType,
											   @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                               @Param(value = "licenceType") String licenceType, @Param(value = "keyword") String keyword
    );

	/**
	 * 查询个人证照信息
	 * @param orgId 机构码
	 * @param accountId
	 * @return
	 */
    List<Map<String, String>> getMyHrLicenceList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
