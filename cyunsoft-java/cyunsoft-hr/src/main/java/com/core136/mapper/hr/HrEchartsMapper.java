package com.core136.mapper.hr;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrEchartsMapper {

	/**
	 * 获取HR学历占比
	 * @param orgId 机构码
	 * @return HR学历占比
	 */
	List<Map<String, Object>> getHighsetShoolPie(@Param(value = "orgId") String orgId);

	/**
	 * 获取HR人员的工种占比
	 * @param orgId 机构码
	 * @return 人员的工种占比
	 */
	List<Map<String, Object>> getWorkTypeBar(@Param(value = "orgId") String orgId);

	/**
	 * HR人员籍贯占比
	 * @param orgId 机构码
	 * @return 人员籍贯占比
	 */
	List<Map<String, String>> getNativePlacePie(@Param(value = "orgId") String orgId);

	/**
	 * 人事档案分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return 人事档案信息
	 */
    List<Map<String, String>> getBaseInfoAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy
    );

	/**
	 * 人事合同分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return 人事合同信息
	 */
    List<Map<String, String>> getContractAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 合同奖惩分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getIncentiveAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 证照分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getLicenceAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 学习经历分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getLearnAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 劳动技能分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getSkillsAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 人事调动分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getTransferAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 离职分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getLeaveAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 人员复职分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getReinstatAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 职称评定分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getEvaluationAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

	/**
	 * 人员关怀分析
	 * @param orgId 机构码
	 * @param deptId
	 * @param module
	 * @param groupBy
	 * @return
	 */
    List<Map<String, String>> getCareAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);


}
