package com.core136.mapper.hr;

import com.core136.bean.hr.HrWorkSkills;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrWorkSkillsMapper extends MyMapper<HrWorkSkills> {

	/**
	 * 工作特长列表
	 * @param orgId 机构码
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param skillsLevel
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrWorkSkillsList(@Param(value = "orgId") String orgId,
                                                  @Param(value = "userId") String userId,
                                                  @Param(value="dateQueryType")String dateQueryType,
                                                  @Param(value = "beginTime") String beginTime,
                                                  @Param(value = "endTime") String endTime,
                                                  @Param(value = "skillsLevel") String skillsLevel,
                                                  @Param(value = "keyword") String keyword
    );

}
