package com.core136.service.hr;

import com.core136.bean.hr.HrEvaluate;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrEvaluateMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrEvaluateService {
    private HrEvaluateMapper hrEvaluateMapper;
	@Autowired
	public void setHrEvaluateMapper(HrEvaluateMapper hrEvaluateMapper) {
		this.hrEvaluateMapper = hrEvaluateMapper;
	}

	public int insertHrEvaluate(HrEvaluate hrEvaluate) {
        return hrEvaluateMapper.insert(hrEvaluate);
    }

    public int deleteHrEvaluate(HrEvaluate hrEvaluate) {
        return hrEvaluateMapper.delete(hrEvaluate);
    }

	/**
	 * 批量删除领导评价
	 * @param orgId 机构码
	 * @param list 领导评价Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteHrEvaluateByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrEvaluate.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrEvaluateMapper.deleteByExample(example));
		}
	}

    public int updateHrEvaluate(Example example, HrEvaluate hrEvaluate) {
        return hrEvaluateMapper.updateByExampleSelective(hrEvaluate, example);
    }

    public HrEvaluate selectOneHrEvaluate(HrEvaluate hrEvaluate) {
        return hrEvaluateMapper.selectOne(hrEvaluate);
    }

	/**
	 * 获取人员评价列表
	 * @param orgId 机构码
	 * @param userId Hr用户Id
	 * @return 评价列表
	 */
	public List<Map<String, String>> getHrEvaluateByUserIdList(String orgId, String userId) {
        return hrEvaluateMapper.getHrEvaluateByUserIdList(orgId, userId);
    }

	/**
	 * 获取人员评价列表
	 * @param pageParam 分页参数
	 * @param userId Hr用户Id
	 * @return 评价列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getHrEvaluateByUserIdList(PageParam pageParam, String userId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrEvaluateByUserIdList(pageParam.getOrgId(), userId);
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取查询人员评价列表
	 * @param orgId 机构码
	 * @param userId Hr用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return Hr用户Id
	 */
    public List<Map<String, String>> getHrEvaluateQueryList(String orgId, String userId,String dateQueryType, String beginTime, String endTime, String status, String keyword) {
        return hrEvaluateMapper.getHrEvaluateQueryList(orgId, userId, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

	/**
	 * 获取查询人员评价列表
	 * @param pageParam 分页参数
	 * @param userId Hr用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @return Hr用户Id
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrEvaluateQueryList(PageParam pageParam, String userId, String dateQueryType, String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrEvaluateQueryList(pageParam.getOrgId(), userId,dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
