package com.core136.service.hr;

import com.core136.bean.hr.HrLeaveRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrLeaveRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrLeaveRecordService {
    private HrLeaveRecordMapper hrLeaveRecordMapper;
	@Autowired
	public void setHrLeaveRecordMapper(HrLeaveRecordMapper hrLeaveRecordMapper) {
		this.hrLeaveRecordMapper = hrLeaveRecordMapper;
	}

	public int insertHrLeaveRecord(HrLeaveRecord hrLevelRecord) {
        return hrLeaveRecordMapper.insert(hrLevelRecord);
    }

    public int deleteHrLeaveRecord(HrLeaveRecord hrLevelRecord) {
        return hrLeaveRecordMapper.delete(hrLevelRecord);
    }
	/**
	 * 批量删除人员离职记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrLeaveRecordByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrLeaveRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrLeaveRecordMapper.deleteByExample(example));
		}
	}
    public int updateHrLeaveRecord(Example example, HrLeaveRecord hrLevelRecord) {
        return hrLeaveRecordMapper.updateByExample(hrLevelRecord, example);
    }

    public HrLeaveRecord selectOneHrLeaveRecord(HrLeaveRecord hrLevelRecord) {
        return hrLeaveRecordMapper.selectOne(hrLevelRecord);
    }

	/**
	 * 获取离职人员列表
	 * @param orgId 机构码
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param leaveType
	 * @return
	 */
    public List<Map<String, String>> getHrLeaveRecordList(String orgId, String userId,String dateQueryType, String beginTime, String endTime, String leaveType) {
        return hrLeaveRecordMapper.getHrLeaveRecordList(orgId, userId, dateQueryType,beginTime, endTime, leaveType);
    }

	/**
	 * 获取离职人员列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param leaveType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrLeaveRecordList(PageParam pageParam, String userId, String dateQueryType,String beginTime, String endTime, String leaveType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrLeaveRecordList(pageParam.getOrgId(), userId, dateQueryType,beginTime, endTime, leaveType);
		return new PageInfo<>(datalist);
    }

}
