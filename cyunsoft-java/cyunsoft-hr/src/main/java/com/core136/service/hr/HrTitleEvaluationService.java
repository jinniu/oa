package com.core136.service.hr;

import com.core136.bean.hr.HrTitleEvaluation;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrTitleEvaluationMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrTitleEvaluationService {
    private HrTitleEvaluationMapper hrTitleEvaluationMapper;
	@Autowired
	public void setHrTitleEvaluationMapper(HrTitleEvaluationMapper hrTitleEvaluationMapper) {
		this.hrTitleEvaluationMapper = hrTitleEvaluationMapper;
	}

	public int insertHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        return hrTitleEvaluationMapper.insert(hrTitleEvaluation);
    }

    public int deleteHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        return hrTitleEvaluationMapper.delete(hrTitleEvaluation);
    }
	/**
	 * 批量删除职称评定记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrTitleEvaluationByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrTitleEvaluation.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrTitleEvaluationMapper.deleteByExample(example));
		}
	}
    public int updateHrTitleEvaluation(Example example, HrTitleEvaluation hrTitleEvaluation) {
        return hrTitleEvaluationMapper.updateByExampleSelective(hrTitleEvaluation, example);
    }

    public HrTitleEvaluation selectOneHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        return hrTitleEvaluationMapper.selectOne(hrTitleEvaluation);
    }

	/**
	 * 获取人员职称评定列表
	 * @param orgId 机构码
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param getType
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getHrTitleEvaluationList(String orgId, String userId,String dateQueryType, String beginTime, String endTime, String getType, String keyword) {
        return hrTitleEvaluationMapper.getHrTitleEvaluationList(orgId, userId, dateQueryType,beginTime, endTime, getType, "%" + keyword + "%");
    }

	/**
	 * 获取人员职称评定列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param getType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrTitleEvaluationList(PageParam pageParam, String userId, String dateQueryType,String beginTime, String endTime, String getType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrTitleEvaluationList(pageParam.getOrgId(), userId, dateQueryType,beginTime, endTime, getType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
