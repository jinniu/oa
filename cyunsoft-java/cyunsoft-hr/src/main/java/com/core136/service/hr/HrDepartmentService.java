package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrDepartment;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrDepartmentMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class HrDepartmentService {
    private HrDepartmentMapper hrDepartmentMapper;
	private UserInfoService userInfoService;
	@Autowired
	public void setHrDepartmentMapper(HrDepartmentMapper hrDepartmentMapper) {
		this.hrDepartmentMapper = hrDepartmentMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertHrDepartment(HrDepartment hrDepartment) {
        return hrDepartmentMapper.insert(hrDepartment);
    }

    public int deleteHrDepartment(HrDepartment hrDepartment) {
        return hrDepartmentMapper.delete(hrDepartment);
    }

    public int updateHrDepartment(Example example, HrDepartment hrDepartment) {
        return hrDepartmentMapper.updateByExampleSelective(hrDepartment, example);
    }

    public HrDepartment selectOneHrDepartment(HrDepartment hrDepartment) {
        return hrDepartmentMapper.selectOne(hrDepartment);
    }

	/**
	 * 判断是否有子项
	 * @param orgId 机构码
	 * @param parentId 父级Id
	 * @return 是否有子项
	 */
	public boolean isExistChild(String orgId,String parentId) {
		HrDepartment hrDepartment = new HrDepartment();
		hrDepartment.setOrgId(orgId);
		hrDepartment.setParentId(parentId);
		return hrDepartmentMapper.selectCount(hrDepartment) > 0;
	}

	/**
	 * 获取部门列表
	 * @param orgId 机构Id
	 * @return 部门列表
	 */
	public List<HrDepartment> getHrDepartmentTree(String orgId, String keyword) {
		if(StringUtils.isNotBlank(keyword)) {

			keyword = "%"+keyword+"%";
			return hrDepartmentMapper.getAllHrDepartmentList(orgId,keyword);
		}else {
			keyword = "%%";
		}
		List<HrDepartment> list = hrDepartmentMapper.getAllHrDepartmentList(orgId,keyword);
		List<HrDepartment> deptList = new ArrayList<>();
		for (HrDepartment hrDepartment : list) {
			if (hrDepartment.getParentId().equals("0")) {
				deptList.add(hrDepartment);
			}
		}
		for (HrDepartment dept : deptList) {
			dept.setChildren(getChildUnitDeptList(dept.getDeptId(), list));
		}
		return deptList;
	}


	/**
	 * 获取子部门列表
	 * @param deptId 部门Id
	 * @param rootUnitDept 根部门列表
	 * @return 部门列表
	 */
	public List<HrDepartment> getChildUnitDeptList(String deptId, List<HrDepartment> rootUnitDept) {
		List<HrDepartment> childList = new ArrayList<>();
		for (HrDepartment unitDept : rootUnitDept) {
			if (unitDept.getParentId().equals(deptId)) {
				childList.add(unitDept);
			}
		}
		for (HrDepartment unitDept : childList) {
			unitDept.setChildren(getChildUnitDeptList(unitDept.getDeptId(), rootUnitDept));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}


	/**
	 * 获取子部门列表
	 * @param example 查询条件
	 * @return 部门列表
	 */
	public List<HrDepartment> getHrDeptList(Example example) {
        return hrDepartmentMapper.selectByExample(example);
    }

	/**
	 * 查询HR部门
	 * @param orgId 机构码
	 * @param parentId 父级Id
	 * @param keyword 查询关键词
	 * @return Hr部门列表
	 */
	public List<Map<String, Object>> getDeptListByDeptIdForSelect(String orgId,String parentId,String keyword) {
		return hrDepartmentMapper.getDeptListByDeptIdForSelect(orgId,parentId,"%"+keyword+"%");
	}


	/**
	 * 按部门ID字符串获取部门列表
	 */
	public List<Map<String, String>> getHrDeptByDeptIds(String orgId,List<String> list) {
		return hrDepartmentMapper.getHrDeptByDeptIds(orgId,list);
	}

    /**
     * 导入工会部门
     *
     * @param user 用户对象
     * @param file 文件导入模版
     * @return 消息结构
     * @throws IOException 服务器内部异常
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrDepartment(UserInfo user, MultipartFile file) throws IOException {
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        List<String> titleList = new ArrayList<>();
		List<String> resList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("部门名称");
		titleList.add("上级部门");
		titleList.add("电话");
		titleList.add("电子邮件");
		titleList.add("传真");
		titleList.add("部门领导");
		titleList.add("部门职责");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		List<HrDepartment> hrDepartmentList = new ArrayList<>();
        List<Map<String, String>> oneList = getOneDeptListMap(recordList);
        if (!oneList.isEmpty()) {
			for (Map<String, String> tempMap : oneList) {
				boolean insertFlag = true;
				HrDepartment hrDepartment = new HrDepartment();
				hrDepartment.setDeptId(SysTools.getGUID());
				for (String s : titleList) {
					if (s.equals("排序号")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							hrDepartment.setSortNo(Integer.parseInt(tempMap.get(s)));
						}else{
							resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
					if (s.equals("部门名称")) {
						hrDepartment.setDeptName(tempMap.get(s));
					}
					if (s.equals("上级部门")) {
						hrDepartment.setParentId("0");
					}
					if (s.equals("电话")) {
						hrDepartment.setDeptTel(tempMap.get(s));
					}
					if (s.equals("电子邮件")) {
						hrDepartment.setDeptEmail(tempMap.get(s));
					}
					if (s.equals("传真")) {
						hrDepartment.setDeptFax(tempMap.get(s));
					}
					if (s.equals("部门领导")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							UserInfo leadUserInfo = new UserInfo();
							leadUserInfo.setStatus("0");
							leadUserInfo.setUserName(tempMap.get(s));
							try{
								leadUserInfo = userInfoService.selectOneUser(leadUserInfo);
								if(leadUserInfo!=null) {
									hrDepartment.setDeptLead(leadUserInfo.getAccountId());
								}else{
									hrDepartment.setDeptLead("");
								}
							}catch (Exception e) {
								resList.add("部门领导查询到多个账号!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}else{
							hrDepartment.setDeptLead("");
						}
					}
					if (s.equals("部门职责")) {
						hrDepartment.setRemark(tempMap.get(s));
					}
				}
				hrDepartment.setLevelId("/");
				hrDepartment.setStatus("0");
				hrDepartment.setCreateTime(createTime);
				hrDepartment.setOrgId(user.getOrgId());
				if(insertFlag) {
					hrDepartmentList.add(hrDepartment);
				}
			}
			for(HrDepartment hrDepartment:hrDepartmentList) {
				insertHrDepartment(hrDepartment);
			}
			hrDepartmentList.clear();
        }
        List<Map<String, String>> twoList = getTwoDeptListMap(recordList);
        if (!twoList.isEmpty()) {
			for (Map<String, String> tempMap : twoList) {
				boolean insertFlag = true;
				HrDepartment hrDepartment = new HrDepartment();
				hrDepartment.setDeptId(SysTools.getGUID());
				for (String s : titleList) {
					if (s.equals("排序号")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							hrDepartment.setSortNo(Integer.parseInt(tempMap.get(s)));
						}else{
							resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
					if (s.equals("部门名称")) {
						hrDepartment.setDeptName(tempMap.get(s));
					}
					if (s.equals("上级部门")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							HrDepartment parentHrDepartment = new HrDepartment();
							parentHrDepartment.setOrgId(user.getOrgId());
							parentHrDepartment.setDeptName(tempMap.get(s));
							try{
								parentHrDepartment = selectOneHrDepartment(parentHrDepartment);
								if(parentHrDepartment!=null) {
									hrDepartment.setParentId(parentHrDepartment.getDeptId());
									hrDepartment.setLevelId(parentHrDepartment.getLevelId()+parentHrDepartment.getDeptId());
								}else {
									resList.add("上级部门未能查询到!-->" + tempMap.get(s) + "--");
									insertFlag = false;
									continue;
								}
							}catch (Exception e) {
								resList.add("查询到多个上级部门!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}else {
							resList.add("上级部门未能查询到!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
					if (s.equals("电话")) {
						hrDepartment.setDeptTel(tempMap.get(s));
					}
					if (s.equals("电子邮件")) {
						hrDepartment.setDeptEmail(tempMap.get(s));
					}
					if (s.equals("传真")) {
						hrDepartment.setDeptFax(tempMap.get(s));
					}
					if (s.equals("部门领导")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							UserInfo leadUserInfo = new UserInfo();
							leadUserInfo.setStatus("0");
							leadUserInfo.setUserName(tempMap.get(s));
							try{
								leadUserInfo = userInfoService.selectOneUser(leadUserInfo);
								if(leadUserInfo!=null) {
									hrDepartment.setDeptLead(leadUserInfo.getAccountId());
								}else{
									hrDepartment.setDeptLead("");
								}
							}catch (Exception e) {
								resList.add("部门领导查询到多个账号!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}else{
							hrDepartment.setDeptLead("");
						}
					}
					if (s.equals("部门职责")) {
						hrDepartment.setRemark(tempMap.get(s));
					}
				}
				hrDepartment.setStatus("0");
				hrDepartment.setCreateTime(createTime);
				hrDepartment.setOrgId(user.getOrgId());
				if(insertFlag) {
					hrDepartmentList.add(hrDepartment);
				}
			}
			for(HrDepartment hrDepartment:hrDepartmentList) {
				insertHrDepartment(hrDepartment);
			}
			hrDepartmentList.clear();
        }

        List<Map<String, String>> threeList = getThreeDeptListMap(recordList);
        if (!threeList.isEmpty()) {
			for (Map<String, String> tempMap : threeList) {
				boolean insertFlag = true;
				HrDepartment hrDepartment = new HrDepartment();
				hrDepartment.setDeptId(SysTools.getGUID());
				for (String s : titleList) {
					if (s.equals("排序号")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							hrDepartment.setSortNo(Integer.parseInt(tempMap.get(s)));
						}else{
							resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
					if (s.equals("部门名称")) {
						hrDepartment.setDeptName(tempMap.get(s));
					}
					if (s.equals("上级部门")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							HrDepartment parentHrDepartment = new HrDepartment();
							parentHrDepartment.setOrgId(user.getOrgId());
							parentHrDepartment.setDeptName(tempMap.get(s));
							try{
								parentHrDepartment = selectOneHrDepartment(parentHrDepartment);
								if(parentHrDepartment!=null) {
									hrDepartment.setParentId(parentHrDepartment.getDeptId());
									hrDepartment.setLevelId(parentHrDepartment.getLevelId()+"/"+parentHrDepartment.getDeptId());
								}else {
									resList.add("上级部门未能查询到!-->" + tempMap.get(s) + "--");
									insertFlag = false;
									continue;
								}
							}catch (Exception e) {
								resList.add("查询到多个上级部门!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}else {
							resList.add("上级部门未能查询到!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
					if (s.equals("电话")) {
						hrDepartment.setDeptTel(tempMap.get(s));
					}
					if (s.equals("电子邮件")) {
						hrDepartment.setDeptEmail(tempMap.get(s));
					}
					if (s.equals("传真")) {
						hrDepartment.setDeptFax(tempMap.get(s));
					}
					if (s.equals("部门领导")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							UserInfo leadUserInfo = new UserInfo();
							leadUserInfo.setStatus("0");
							leadUserInfo.setUserName(tempMap.get(s));
							try{
								leadUserInfo = userInfoService.selectOneUser(leadUserInfo);
								if(leadUserInfo!=null) {
									hrDepartment.setDeptLead(leadUserInfo.getAccountId());
								}else{
									hrDepartment.setDeptLead("");
								}
							}catch (Exception e) {
								resList.add("部门领导查询到多个账号!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}else{
							hrDepartment.setDeptLead("");
						}
					}
					if (s.equals("部门职责")) {
						hrDepartment.setRemark(tempMap.get(s));
					}
				}
				hrDepartment.setStatus("0");
				hrDepartment.setCreateTime(createTime);
				hrDepartment.setOrgId(user.getOrgId());
				if(insertFlag) {
					hrDepartmentList.add(hrDepartment);
				}
			}
			for(HrDepartment hrDepartment:hrDepartmentList) {
				insertHrDepartment(hrDepartment);
			}
			hrDepartmentList.clear();
        }

        List<Map<String, String>> fourList = getFourDeptListMap(recordList);
        if (!fourList.isEmpty()) {
			for (Map<String, String> tempMap : fourList) {
				boolean insertFlag = true;
				HrDepartment hrDepartment = new HrDepartment();
				hrDepartment.setDeptId(SysTools.getGUID());
				for (String s : titleList) {
					if (s.equals("排序号")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							hrDepartment.setSortNo(Integer.parseInt(tempMap.get(s)));
						}else{
							resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
					if (s.equals("部门名称")) {
						hrDepartment.setDeptName(tempMap.get(s));
					}
					if (s.equals("上级部门")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							HrDepartment parentHrDepartment = new HrDepartment();
							parentHrDepartment.setOrgId(user.getOrgId());
							parentHrDepartment.setDeptName(tempMap.get(s));
							try{
								parentHrDepartment = selectOneHrDepartment(parentHrDepartment);
								if(parentHrDepartment!=null) {
									hrDepartment.setParentId(parentHrDepartment.getDeptId());
									hrDepartment.setLevelId(parentHrDepartment.getLevelId()+"/"+parentHrDepartment.getDeptId());
								}else {
									resList.add("上级部门未能查询到!-->" + tempMap.get(s) + "--");
									insertFlag = false;
									continue;
								}
							}catch (Exception e) {
								resList.add("查询到多个上级部门!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}else {
							resList.add("上级部门未能查询到!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
					if (s.equals("电话")) {
						hrDepartment.setDeptTel(tempMap.get(s));
					}
					if (s.equals("电子邮件")) {
						hrDepartment.setDeptEmail(tempMap.get(s));
					}
					if (s.equals("传真")) {
						hrDepartment.setDeptFax(tempMap.get(s));
					}
					if (s.equals("部门领导")) {
						if(StringUtils.isNotBlank(tempMap.get(s))) {
							UserInfo leadUserInfo = new UserInfo();
							leadUserInfo.setStatus("0");
							leadUserInfo.setUserName(tempMap.get(s));
							try{
								leadUserInfo = userInfoService.selectOneUser(leadUserInfo);
								if(leadUserInfo!=null) {
									hrDepartment.setDeptLead(leadUserInfo.getAccountId());
								}else{
									hrDepartment.setDeptLead("");
								}
							}catch (Exception e) {
								resList.add("部门领导查询到多个账号!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}else{
							hrDepartment.setDeptLead("");
						}
					}
					if (s.equals("部门职责")) {
						hrDepartment.setRemark(tempMap.get(s));
					}
				}
				hrDepartment.setStatus("0");
				hrDepartment.setCreateTime(createTime);
				hrDepartment.setOrgId(user.getOrgId());
				if(insertFlag) {
					hrDepartmentList.add(hrDepartment);
				}
			}
			for(HrDepartment hrDepartment:hrDepartmentList) {
				insertHrDepartment(hrDepartment);
			}
			hrDepartmentList.clear();
        }
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }

    /**
     * 获取第一层部门
     *
     * @param recordList 部门列表
     * @return 部门列表
     */
    public List<Map<String, String>> getOneDeptListMap(List<Map<String, String>> recordList) {
        List<Map<String, String>> retList = new ArrayList<>();
		for (Map<String, String> tempMap : recordList) {
			String tempStr = tempMap.get("上级部门");
			if (StringUtils.isBlank(tempStr)) {
				retList.add(tempMap);
			}
		}
        return retList;
    }

    /**
     * 获取第二层部门列表
     *
     * @param recordList 部门列表
     * @return 部门列表
     */
    public List<Map<String, String>> getTwoDeptListMap(List<Map<String, String>> recordList) {
        List<Map<String, String>> retList = new ArrayList<>();
		for (Map<String, String> tempMap : recordList) {
			String tempStr = tempMap.get("上级部门");
			if (StringUtils.isNotBlank(tempStr) && (SysTools.countStr(tempStr, "/") == 0)) {
					retList.add(tempMap);
			}
		}
        return retList;
    }

    /**
     * 获取第三层部门列表
     *
     * @param recordList 部门列表
     * @return 部门列表
     */
    public List<Map<String, String>> getThreeDeptListMap(List<Map<String, String>> recordList) {
        List<Map<String, String>> retList = new ArrayList<>();
		for (Map<String, String> tempMap : recordList) {
			String tempStr = tempMap.get("上级部门");
			if (StringUtils.isNotBlank(tempStr) && (SysTools.countStr(tempStr, "/") == 1)) {
					retList.add(tempMap);

			}
		}
        return retList;
    }

    /**
     * 第四级部门列表
     *
     * @param recordList 部门列表
     * @return 部门列表
     */
    public List<Map<String, String>> getFourDeptListMap(List<Map<String, String>> recordList) {
        List<Map<String, String>> retList = new ArrayList<>();
		for (Map<String, String> tempMap : recordList) {
			String tempStr = tempMap.get("上级部门");
			if (StringUtils.isNotBlank(tempStr) && (SysTools.countStr(tempStr, "/") == 2)) {
				retList.add(tempMap);
			}
		}
        return retList;
    }

}
