package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrKpiItem;
import com.core136.bean.hr.HrKpiPlanRecord;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.hr.HrKpiPlanRecordMapper;
import com.core136.service.account.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HrKpiPlanRecordService {
    private UserInfoService userInfoService;
    private HrKpiPlanRecordMapper hrKpiPlanRecordMapper;
    private HrKpiItemService hrKpiItemService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setHrKpiPlanRecordMapper(HrKpiPlanRecordMapper hrKpiPlanRecordMapper) {
		this.hrKpiPlanRecordMapper = hrKpiPlanRecordMapper;
	}
	@Autowired
	public void setHrKpiItemService(HrKpiItemService hrKpiItemService) {
		this.hrKpiItemService = hrKpiItemService;
	}

	public int insertHrKpiPlanRecord(HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.insert(hrKpiPlanRecord);
    }

    public int deleteHrKpiPlanRecord(HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.delete(hrKpiPlanRecord);
    }

    public int updateHrKpiPlanRecord(Example example, HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.updateByExampleSelective(hrKpiPlanRecord, example);
    }

    public HrKpiPlanRecord selectOneHrKpiPlanRecord(HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.selectOne(hrKpiPlanRecord);
    }

	/**
	 * 获取个人考核分数明细
	 * @param orgId 机构码
	 * @param planId 考核计划Id
	 * @param accountId 用户账号
	 * @return 个人考核分数明细
	 */
	public List<Map<String, String>> getMyHrKpiScoreList(String orgId, String planId, String accountId) {
        return hrKpiPlanRecordMapper.getMyHrKpiScoreList(orgId, planId, accountId);
    }

	public HrKpiItem getHrKpiRecordForUser(String orgId, String planId, String accountId) {
    	HrKpiPlanRecord hrKpiPlanRecord = new HrKpiPlanRecord();
    	hrKpiPlanRecord.setOrgId(orgId);
    	hrKpiPlanRecord.setPlanId(planId);
    	hrKpiPlanRecord.setAccountId(accountId);
    	hrKpiPlanRecord = hrKpiPlanRecordMapper.selectOne(hrKpiPlanRecord);
		HrKpiItem hrKpiItem = new HrKpiItem();
		hrKpiItem.setItemId(hrKpiPlanRecord.getItemId());
		hrKpiItem.setOrgId(hrKpiPlanRecord.getOrgId());
		return hrKpiItemService.selectOneHrKpiItem(hrKpiItem);
	}

    public Map<String,Object> getHrKpiRecordForDetails(String orgId, String planId, String accountId) {
        HrKpiPlanRecord hrKpiPlanRecord = new HrKpiPlanRecord();
        hrKpiPlanRecord.setOrgId(orgId);
        hrKpiPlanRecord.setPlanId(planId);
        hrKpiPlanRecord.setAccountId(accountId);
        hrKpiPlanRecord = hrKpiPlanRecordMapper.selectOne(hrKpiPlanRecord);
        HrKpiItem hrKpiItem = new HrKpiItem();
        hrKpiItem.setItemId(hrKpiPlanRecord.getItemId());
        hrKpiItem.setOrgId(hrKpiPlanRecord.getOrgId());
        hrKpiItem=hrKpiItemService.selectOneHrKpiItem(hrKpiItem);
        Map<String,Object> map = new HashMap<>();
        map.put("userInfo", userInfoService.getUserByAccountId(hrKpiPlanRecord.getAccountId(),orgId));
        map.put("hrKpiPlanRecord",hrKpiPlanRecord);
        map.put("hrKpiItem",hrKpiItem);
        return map;
    }

	/**
	 * 获取总数
	 * @param hrKpiPlanRecord
	 * @return
	 */
	public int getHrKpiPlanRecordCount(HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.selectCount(hrKpiPlanRecord);
    }

	/**
	 * 设置考核分
	 * @param user
	 * @param planId
	 * @param accountId 用户账号
	 * @param scoreList
	 * @param score
	 * @return
	 */
    public RetDataBean setScoreToUser(UserInfo user, String planId, String accountId, String scoreList, Double score) {
        try {
            HrKpiPlanRecord hrKpiPlanRecord = new HrKpiPlanRecord();
			hrKpiPlanRecord.setStatus("1");
			hrKpiPlanRecord.setRemark(scoreList);
			hrKpiPlanRecord.setScore(score);
            Example example1 = new Example(HrKpiPlanRecord.class);
            example1.createCriteria().andEqualTo("orgId", user.getOrgId()).andEqualTo("planId", planId)
                    .andEqualTo("accountId", accountId);
            updateHrKpiPlanRecord(example1, hrKpiPlanRecord);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }

    }


}
