package com.core136.service.hr;

import com.core136.bean.hr.HrRecruitNeeds;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrRecruitNeedsMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrRecruitNeedsService {
    private HrRecruitNeedsMapper hrRecruitNeedsMapper;
	@Autowired
	public void setHrRecruitNeedsMapper(HrRecruitNeedsMapper hrRecruitNeedsMapper) {
		this.hrRecruitNeedsMapper = hrRecruitNeedsMapper;
	}

	public int insertHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
        return hrRecruitNeedsMapper.insert(hrRecruitNeeds);
    }

    public int deleteHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
        return hrRecruitNeedsMapper.delete(hrRecruitNeeds);
    }

	/**
	 * 批量删除招聘需求
	 * @param orgId 机构码
	 * @param list 招聘需求Id 数组
	 * @return 消息结构
	 */
	public RetDataBean deleteHrRecruitNeedsByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrRecruitNeeds.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrRecruitNeedsMapper.deleteByExample(example));
		}
	}

    public int updateHrRecruitNeeds(Example example, HrRecruitNeeds hrRecruitNeeds) {
        return hrRecruitNeedsMapper.updateByExampleSelective(hrRecruitNeeds, example);
    }

    public HrRecruitNeeds selectOneHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
        return hrRecruitNeedsMapper.selectOne(hrRecruitNeeds);
    }

	/**
	 * 获取招聘需求列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param occupation
	 * @param education
	 * @param status 状态
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 招聘需求列表
	 */
    public List<Map<String, String>> getHrRecruitNeedsList(String orgId, String accountId, String occupation, String education, String status,String dateQueryType, String beginTime, String endTime, String keyword) {
        return hrRecruitNeedsMapper.getHrRecruitNeedsList(orgId, accountId, occupation, education, status, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取需求列表
	 * @param pageParam 分页参数
	 * @param occupation
	 * @param education
	 * @param status 状态
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 需求列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrRecruitNeedsList(PageParam pageParam, String occupation, String education, String status,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrRecruitNeedsList(pageParam.getOrgId(), pageParam.getAccountId(), occupation, education, status, dateQueryType,beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取待审批需求列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param occupation
	 * @param highsetShool
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getApprovedHrRecruitNeedsList(String orgId, String accountId, String occupation, String highsetShool,String dateQueryType, String beginTime, String endTime,String status, String keyword) {
        return hrRecruitNeedsMapper.getApprovedHrRecruitNeedsList(orgId, accountId, occupation, highsetShool, dateQueryType,beginTime, endTime, status ,"%" + keyword + "%");
    }

	/**
	 * 获取待审批需求列表
	 * @param pageParam 分页参数
	 * @param occupation
	 * @param education
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 待审批需求列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getApprovedHrRecruitNeedsList(PageParam pageParam, String occupation, String education,String dateQueryType, String beginTime, String endTime,String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getApprovedHrRecruitNeedsList(pageParam.getOrgId(), pageParam.getAccountId(), occupation, education,dateQueryType, beginTime, endTime, status,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
