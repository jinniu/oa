package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrDic;
import com.core136.bean.hr.HrIncentive;
import com.core136.bean.hr.HrUser;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrIncentiveMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HrIncentiveService {
    private HrIncentiveMapper hrIncentiveMapper;
	private HrDicService hrDicService;
	private HrUserService hrUserService;
	@Autowired
	public void setHrIncentiveMapper(HrIncentiveMapper hrIncentiveMapper) {
		this.hrIncentiveMapper = hrIncentiveMapper;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}

	public int insertHrIncentive(HrIncentive hrIncentive) {
        return hrIncentiveMapper.insert(hrIncentive);
    }

    public int deleteHrIncentive(HrIncentive hrIncentive) {
        return hrIncentiveMapper.delete(hrIncentive);
    }

	/**
	 * 批量删除奖惩记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrIncentiveByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrIncentive.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("incentiveId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrIncentiveMapper.deleteByExample(example));
		}
	}

    public int updateHrIncentive(Example example, HrIncentive hrIncentive) {
        return hrIncentiveMapper.updateByExampleSelective(hrIncentive, example);
    }

    public HrIncentive selectOneHrIncentive(HrIncentive hrIncentive) {
        return hrIncentiveMapper.selectOne(hrIncentive);
    }

	/**
	 * 获取奖惩记录列表
	 * @param orgId 机构码
	 * @param userId
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param incentiveType
	 * @param incentiveItem
	 * @return
	 */
    public List<Map<String, String>> getHrIncentiveList(String orgId, String userId, String dateQueryType,String beginTime, String endTime, String incentiveType, String incentiveItem) {
        return hrIncentiveMapper.getHrIncentiveList(orgId, userId, dateQueryType,beginTime, endTime, incentiveType, incentiveItem);
    }

	/**
	 * 个人查询奖惩记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 奖惩记录
	 */
    public List<Map<String, String>> getMyHrIncentiveList(String orgId, String accountId) {
        return hrIncentiveMapper.getMyHrIncentiveList(orgId, accountId);
    }

	/**
	 * 获取奖惩记录列表
	 * @param pageParam 分页参数
	 * @param userId 用户Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param incentiveType
	 * @param incentiveItem
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrIncentiveList(PageParam pageParam, String userId,String dateQueryType, String beginTime, String endTime, String incentiveType, String incentiveItem) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrIncentiveList(pageParam.getOrgId(), userId,dateQueryType, beginTime, endTime, incentiveType, incentiveItem);
        return new PageInfo<>(datalist);
    }

	/**
	 * 个人查询奖惩记录
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyHrIncentiveList(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrIncentiveList(pageParam.getOrgId(), pageParam.getAccountId());
		return new PageInfo<>(datalist);
    }


    /**
     * 奖惩记录导入
     * @param user
     * @param file
     * @return
     * @throws IOException
     */
    @Transactional(value = "generalTM")
	public RetDataBean importHrIncentive(UserInfo user, MultipartFile file) throws IOException {
		List<String> resList = new ArrayList<>();
		List<HrIncentive> hrIncentiveList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("奖惩类型");
		titleList.add("涉及人员");
		titleList.add("处理事项");
		titleList.add("处理日期");
		titleList.add("奖惩金额");
		titleList.add("工资月份");
		titleList.add("备注");
		List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrIncentive hrIncentive = new HrIncentive();
			hrIncentive.setIncentiveId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						hrIncentive.setSortNo(Integer.parseInt(tempMap.get(s)));
					}else{
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("奖惩类型")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic hrDic = new HrDic();
						hrDic.setOrgId(user.getOrgId());
						hrDic.setName(tempMap.get(s));
						hrDic.setCode("incentiveType");
						try{
							hrDic = hrDicService.selectOneHrDic(hrDic);
							if(hrDic!=null){
								hrIncentive.setIncentiveType(hrDic.getKeyValue());
							}else{
								resList.add("奖惩类型不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("奖惩类型查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("奖惩类型不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("涉及人员")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrUser hrUser = new HrUser();
						hrUser.setOrgId(user.getOrgId());
						hrUser.setUserName(tempMap.get(s));
						try{
							hrUser = hrUserService.selectOneHrUser(hrUser);
							if(hrUser!=null){
								hrIncentive.setUserId(hrUser.getUserId());
							}else{
								resList.add("涉及人员不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("涉及人员查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("涉及人员查询出错!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("处理事项")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic incentiveItem = new HrDic();
						incentiveItem.setOrgId(user.getOrgId());
						incentiveItem.setName(tempMap.get(s));
						incentiveItem.setCode("incentiveItem");
						try{
							incentiveItem = hrDicService.selectOneHrDic(incentiveItem);
							if(incentiveItem!=null){
								hrIncentive.setIncentiveItem(incentiveItem.getKeyValue());
							}else{
								resList.add("处理事项不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("处理事项查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("处理事项不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("处理日期")) {
					hrIncentive.setIncentiveTime(tempMap.get(s));
				}
				if (s.equals("奖惩金额")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						try {
							hrIncentive.setIncentiveAmount(Double.parseDouble(tempMap.get(s)));
						}catch (Exception e){
							resList.add("奖惩金额格式不正确!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						hrIncentive.setIncentiveAmount(0.0);
					}
				}
				if (s.equals("工资月份")) {
					hrIncentive.setIncentiveTime(tempMap.get(s));
				}
				if (s.equals("备注")) {
					hrIncentive.setIncentiveTime(tempMap.get(s));
				}
			}
			hrIncentive.setCreateTime(createTime);
			hrIncentive.setCreateUser(user.getAccountId());
			hrIncentive.setOrgId(user.getOrgId());
			if(insertFlag) {
				hrIncentiveList.add(hrIncentive);
			}
		}
		for(HrIncentive hrIncentive:hrIncentiveList){
			insertHrIncentive(hrIncentive);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
	}

}
