package com.core136.service.hr;

import com.core136.bean.hr.HrRecruitPlan;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrRecruitPlanMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrRecruitPlanService {
    private HrRecruitPlanMapper hrRecruitPlanMapper;
	@Autowired
	public void setHrRecruitPlanMapper(HrRecruitPlanMapper hrRecruitPlanMapper) {
		this.hrRecruitPlanMapper = hrRecruitPlanMapper;
	}

	public int insertHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
        return hrRecruitPlanMapper.insert(hrRecruitPlan);
    }

    public int deleteHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
        return hrRecruitPlanMapper.delete(hrRecruitPlan);
    }

	/**
	 * 批量删除招聘计划
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrRecruitPlanByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrRecruitPlan.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("planId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrRecruitPlanMapper.deleteByExample(example));
		}
	}

    public int updateHrRecruitPlan(Example example, HrRecruitPlan hrRecruitPlan) {
        return hrRecruitPlanMapper.updateByExampleSelective(hrRecruitPlan, example);
    }

    public HrRecruitPlan selectOneHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
        return hrRecruitPlanMapper.selectOne(hrRecruitPlan);
    }

	/**
	 * 获取招聘计划列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getHrRecruitPlanList(String orgId,String accountId, String dateQueryType,String beginTime, String endTime, String keyword) {
        return hrRecruitPlanMapper.getHrRecruitPlanList(orgId, accountId,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取招聘计划列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrRecruitPlanList(PageParam pageParam,  String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrRecruitPlanList(pageParam.getOrgId(), pageParam.getAccountId(), dateQueryType,beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取当前可填报的招聘计划
	 * @param orgId 机构码
	 * @return 招聘计划
	 */
	public List<Map<String, String>> getHrRecruitPlanForSelect(String orgId) {
        String endTime = SysTools.getTime("yyyy-MM-dd");
        return hrRecruitPlanMapper.getHrRecruitPlanForSelect(orgId, endTime);
    }
}
