package com.core136.service.hr;

import com.core136.bean.hr.HrReinstatement;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrReinstatementMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrReinstatementService {
    private HrReinstatementMapper hrReinstatementMapper;
	@Autowired
	public void setHrReinstatementMapper(HrReinstatementMapper hrReinstatementMapper) {
		this.hrReinstatementMapper = hrReinstatementMapper;
	}

	public int insertHrReinstatement(HrReinstatement hrReinstatement) {
        return hrReinstatementMapper.insert(hrReinstatement);
    }

    public int deleteHrReinstatement(HrReinstatement hrReinstatement) {
        return hrReinstatementMapper.delete(hrReinstatement);
    }

	/**
	 * 批量删除人员复职记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrReinstatementByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrReinstatement.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrReinstatementMapper.deleteByExample(example));
		}
	}

    public int updateHrReinstatement(Example example, HrReinstatement hrReinstatement) {
        return hrReinstatementMapper.updateByExampleSelective(hrReinstatement, example);
    }

    public HrReinstatement selectOneHrReinstatement(HrReinstatement hrReinstatement) {
        return hrReinstatementMapper.selectOne(hrReinstatement);
    }

	/**
	 * 获取复值列表
	 * @param orgId 机构码
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param reinstatementType
	 * @return
	 */
    public List<Map<String, String>> getHrReinstatementList(String orgId, String userId,String dateQueryType, String beginTime, String endTime, String reinstatementType) {
        return hrReinstatementMapper.getHrReinstatementList(orgId, userId, dateQueryType,beginTime, endTime, reinstatementType);
    }

	/**
	 * 获取复值列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param reinstatementType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrReinstatementList(PageParam pageParam, String userId, String dateQueryType,String beginTime, String endTime, String reinstatementType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrReinstatementList(pageParam.getOrgId(), userId, dateQueryType,beginTime, endTime, reinstatementType);
		return new PageInfo<>(datalist);
    }

}
