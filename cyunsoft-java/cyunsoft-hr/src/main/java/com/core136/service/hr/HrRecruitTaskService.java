package com.core136.service.hr;

import com.core136.bean.hr.HrRecruitTask;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrRecruitTaskMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrRecruitTaskService {
    private HrRecruitTaskMapper hrRecruitTaskMapper;
	@Autowired
	public void setHrRecruitTaskMapper(HrRecruitTaskMapper hrRecruitTaskMapper) {
		this.hrRecruitTaskMapper = hrRecruitTaskMapper;
	}

	public int insertHrRecruitTask(HrRecruitTask hrRecruitTask) {
        return hrRecruitTaskMapper.insert(hrRecruitTask);
    }

    public int deleteHrRecruitTask(HrRecruitTask hrRecruitTask) {
        return hrRecruitTaskMapper.delete(hrRecruitTask);
    }

    public int updateHrRecruitTask(Example example, HrRecruitTask hrRecruitTask) {
        return hrRecruitTaskMapper.updateByExampleSelective(hrRecruitTask, example);
    }

    public HrRecruitTask selectOneHrRecruitTask(HrRecruitTask hrRecruitTask) {
        return hrRecruitTaskMapper.selectOne(hrRecruitTask);
    }

	/**
	 * 批量删除招聘任务
	 * @param orgId 机构码
	 * @param list 招聘任务Id 数组
	 * @return 消息结构
	 */
	public RetDataBean deleteHrRecruitTaskByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrRecruitTask.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("taskId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrRecruitTaskMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取招聘任务列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 招聘任务列表
	 */
    public List<Map<String, String>> getHrRecruitTaskList(String orgId, String dateQueryType, String beginTime, String endTime, String keyword) {
        return hrRecruitTaskMapper.getHrRecruitTaskList(orgId, dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取招聘任务列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 招聘任务列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrRecruitTaskList(PageParam pageParam,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrRecruitTaskList(pageParam.getOrgId(), dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
