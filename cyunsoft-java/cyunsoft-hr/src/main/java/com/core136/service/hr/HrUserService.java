package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrDepartment;
import com.core136.bean.hr.HrDic;
import com.core136.bean.hr.HrUser;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelExportData;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrUserMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */

@Service
public class HrUserService {
    private HrUserMapper hrUserMapper;
	private HrDepartmentService hrDepartmentService;
	private HrDicService hrDicService;
	@Autowired
	public void setHrUserMapper(HrUserMapper hrUserMapper) {
		this.hrUserMapper = hrUserMapper;
	}
	@Autowired
	public void setHrDepartmentService(HrDepartmentService hrDepartmentService) {
		this.hrDepartmentService = hrDepartmentService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}

	public int insertHrUser(HrUser hrUser) {
        return hrUserMapper.insert(hrUser);
    }

    public int deleteHrUser(HrUser hrUser) {
        return hrUserMapper.delete(hrUser);
    }

	/**
	 * 批量删除人员信息
	 * @param orgId 机构码
	 * @param list 人员Id 列表
	 * @return 人员信息
	 */
	public RetDataBean deleteByHrUserIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrUser.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("userId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrUserMapper.deleteByExample(example));
		}
	}

    public int updateHrUser(Example example, HrUser hrUser) {
        return hrUserMapper.updateByExampleSelective(hrUser, example);
    }

    public HrUser selectOneHrUser(HrUser hrUser) {
        return hrUserMapper.selectOne(hrUser);
    }

	/**
	 * 获取个人事档案
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 个人事档案
	 */
    public Map<String,String>getHrUserByAccountId(String orgId,String accountId) {
		return hrUserMapper.getHrUserByAccountId(orgId,accountId);
	}

	/**
	 * 按部门获取账户信息
	 *
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @return 账户信息
	 */
	public List<Map<String, Object>> getHrUserListByDeptIdForSelect(String orgId, String deptId, String keyword) {
		return hrUserMapper.getHrUserListByDeptIdForSelect(orgId, deptId, "%" + keyword + "%");
	}

	/**
	 * 按账号获取人员列表
	 *
	 * @param orgId 机构码
	 * @param list 人员Id
	 * @return 人员列表
	 */
	public List<Map<String, String>> getHrUserListByUserIds(String orgId, List<String> list) {
		return hrUserMapper.getHrUserListByUserIds(orgId, list);
	}

	/**
	 * 获取部门下的人员列表
	* @param orgId 机构码
	 * @param deptId 部门Id
	 * @return 部门下的人员列表
	 */
	public List<Map<String, String>> getHrUserByDeptId(String orgId, String deptId) {
        return hrUserMapper.getHrUserByDeptId(orgId, deptId);
    }

	/**
	 * 获取部门下的人员列表
	* @param orgId 机构码
	 * @param deptId
	 * @param workStatus
	 * @param employedTime
	 * @param staffCardNo
	 * @param keyword 查询关键词
	 * @return 部门下的人员列表
	 */
    public List<Map<String, String>> getHrUserByDeptIdInWorkList(String orgId, String deptId, String workStatus, String employedTime, String staffCardNo, String keyword) {
        return hrUserMapper.getHrUserByDeptIdInWorkList(orgId, deptId, workStatus, employedTime, staffCardNo, "%" + keyword + "%");
    }


	/**
	 * 获取部门下的人员列表
	 * @param pageParam 分页参数
	 * @param workStatus 工作状态
	 * @param employedTime 雇佣时间
	 * @param staffCardNo
	 * @return 部门下的人员列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrUserByDeptIdInWorkList(PageParam pageParam, String workStatus, String employedTime, String staffCardNo) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrUserByDeptIdInWorkList(pageParam.getOrgId(), pageParam.getDeptId(), workStatus, employedTime, staffCardNo, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取部门下的人员列表
	* @param orgId 机构码
	 * @param deptId
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getHrUserListByDeptId(String orgId, String deptId, String keyword) {
        return hrUserMapper.getHrUserListByDeptId(orgId, deptId, "%" + keyword + "%");
    }

	/**
	 *  获取部门下的人员列表
	 * @param pageParam 分页参数
	 * @return 部门下的人员列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrUserListByDeptId(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrUserListByDeptId(pageParam.getOrgId(), pageParam.getDeptId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 按部门获取人员列表
	* @param orgId 机构码
	 * @param deptId 部门Id
	 * @return 人员列表
	 */
	public List<Map<String, String>> getHrUserForTree(String orgId, String deptId) {
        return hrUserMapper.getHrUserForTree(orgId, deptId);
    }

	/**
	 * 获取HR人员列表
	* @param orgId 机构码
	 * @param userIds 用户Ids
	 * @return HR人员列表
	 */
	public List<Map<String, String>> getUserNamesByUserIds(String orgId, String userIds) {
        if (StringUtils.isNotBlank(userIds)) {
            String[] userIdArr = userIds.split(",");
            List<String> list = Arrays.asList(userIdArr);
            return hrUserMapper.getUserNamesByUserIds(orgId, list);
        } else {
            return null;
        }
    }

	/**
	 * 查询HR人员
	* @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return Hr人员列表
	 */
	public List<Map<String, String>> getHrUserBySearchUser(String orgId, String keyword) {
        return hrUserMapper.getHrUserBySearchUser(orgId, "%" + keyword + "%");
    }

	/**
	 * 获取人力资源门户人员信息
	 * @param orgId 机构码
	 * @return 门户人员信息
	 */
	public Map<String, String> getDeskHrUser(String orgId) {
        return hrUserMapper.getDeskHrUser(orgId);
    }


	/**
	 * 人事档案信息导入
	 * @param userInfo 用户对象
	 * @param file 导入文件模版
	 * @return 消息结构
	 * @throws IOException 服务器内部异常
	 */
	@Transactional(value = "generalTM")
    public RetDataBean importHrUser(UserInfo userInfo, MultipartFile file) throws IOException {
        List<String> titleList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		titleList.add("排序号");
		titleList.add("姓名");
		titleList.add("OA用户名");
		titleList.add("部门");
		titleList.add("职务");
		titleList.add("英文名");
		titleList.add("曾用名");
		titleList.add("性别");
		titleList.add("工号");
		titleList.add("编号");
		titleList.add("在职状态");
		titleList.add("身份证号");
		titleList.add("出生日期");
		titleList.add("籍贯");
		titleList.add("民族");
		titleList.add("婚姻状况");
		titleList.add("政治面貌");
		titleList.add("入党时间");
		titleList.add("岗位");
		titleList.add("入职时间");
		titleList.add("手机号码");
		titleList.add("学历");
		titleList.add("毕业时间");
		titleList.add("毕业学校");
		titleList.add("专业");
		List<String> resList = new ArrayList<>();
		List<HrUser> hrUserList = new ArrayList<>();
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrUser hrUser = new HrUser();
			hrUser.setUserId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrUser.setSortNo(Integer.parseInt(tempMap.get(s)));
					}else{
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("姓名")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrUser.setUserName(tempMap.get(s));
					}else{
						resList.add("人员姓名不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("OA用户名")) {
					hrUser.setAccountId(tempMap.get(s));
				}
				if (s.equals("部门")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDepartment hrDepartment = new HrDepartment();
						hrDepartment.setOrgId(userInfo.getOrgId());
						hrDepartment.setDeptName(tempMap.get(s));
						try{
							hrDepartment = hrDepartmentService.selectOneHrDepartment(hrDepartment);
							if(hrDepartment!=null) {
								hrUser.setDeptId(hrDepartment.getDeptId());
							}else{
								resList.add("未能查询到人员部门!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员部门出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("部门不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("职务")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic hrUserLevel = new HrDic();
						hrUserLevel.setOrgId(userInfo.getOrgId());
						hrUserLevel.setName(tempMap.get(s));
						hrUserLevel.setCode("userLevel");
						try{
							hrUserLevel = hrDicService.selectOneHrDic(hrUserLevel);
							if(hrUserLevel!=null) {
								hrUser.setUserLevel(hrUserLevel.getKeyValue());
							}else{
								resList.add("未能查询到人员职务!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员职务出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("人员职务不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("英文名")) {
					hrUser.setUserNameEn(tempMap.get(s));
				}
				if (s.equals("曾用名")) {
					hrUser.setBeforeUserName(tempMap.get(s));
				}
				if (s.equals("性别")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						if(tempMap.get(s).equals("男")) {
							hrUser.setSex("1");
						}else if(tempMap.get(s).equals("女")) {
							hrUser.setSex("0");
						}else{
							hrUser.setSex("3");
						}
					}else{
						hrUser.setSex("3");
					}
					hrUser.setBeforeUserName(tempMap.get(s));
				}
				if (s.equals("工号")) {
					hrUser.setWorkNo(tempMap.get(s));
				}
				if (s.equals("编号")) {
					hrUser.setStaffNo(tempMap.get(s));
				}
				if (s.equals("在职状态")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic workStatus = new HrDic();
						workStatus.setOrgId(userInfo.getOrgId());
						workStatus.setName(tempMap.get(s));
						workStatus.setCode("workStatus");
						try{
							workStatus = hrDicService.selectOneHrDic(workStatus);
							if(workStatus!=null) {
								hrUser.setWorkStatus(workStatus.getKeyValue());
							}else{
								resList.add("未能查询到人员在职状态!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员在职状态出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("人员在职状态不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("身份证号")) {
					hrUser.setStaffCardNo(tempMap.get(s));
				}
				if (s.equals("出生日期")) {
					hrUser.setBirthDay(tempMap.get(s));
				}
				if (s.equals("籍贯")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic nativePlace = new HrDic();
						nativePlace.setOrgId(userInfo.getOrgId());
						nativePlace.setName(tempMap.get(s));
						nativePlace.setCode("nativePlace");
						try{
							nativePlace = hrDicService.selectOneHrDic(nativePlace);
							if(nativePlace!=null) {
								hrUser.setNativePlace(nativePlace.getKeyValue());
							}else{
								resList.add("未能查询到人员籍贯!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员籍贯出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("人员籍贯不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("民族")) {
					hrUser.setNationalty(tempMap.get(s));
				}
				if (s.equals("婚姻状况")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic maritalStatus = new HrDic();
						maritalStatus.setOrgId(userInfo.getOrgId());
						maritalStatus.setName(tempMap.get(s));
						maritalStatus.setCode("maritalStatus");
						try{
							maritalStatus = hrDicService.selectOneHrDic(maritalStatus);
							if(maritalStatus!=null) {
								hrUser.setMaritalStatus(maritalStatus.getKeyValue());
							}else{
								resList.add("未能查询到人员婚姻状况!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员婚姻状况出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("人员婚姻状况不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("政治面貌")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic politicalStatus = new HrDic();
						politicalStatus.setOrgId(userInfo.getOrgId());
						politicalStatus.setName(tempMap.get(s));
						politicalStatus.setCode("politicalStatus");
						try{
							politicalStatus = hrDicService.selectOneHrDic(politicalStatus);
							if(politicalStatus!=null) {
								hrUser.setPoliticalStatus(politicalStatus.getKeyValue());
							}else{
								resList.add("未能查询到人员政治面貌!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员政治面貌出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("人员政治面貌不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("入党时间")) {
					hrUser.setJoinPartyTime(tempMap.get(s));
				}
				if (s.equals("岗位")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic workJob = new HrDic();
						workJob.setOrgId(userInfo.getOrgId());
						workJob.setName(tempMap.get(s));
						workJob.setCode("workJob");
						try{
							workJob = hrDicService.selectOneHrDic(workJob);
							if(workJob!=null) {
								hrUser.setWorkJob(workJob.getKeyValue());
							}else{
								resList.add("未能查询到人员岗位!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员岗位出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("人员岗位不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("入职时间")) {
					hrUser.setJoinPartyTime(tempMap.get(s));
				}
				if (s.equals("手机号码")) {
					hrUser.setMobileNo(tempMap.get(s));
				}
				if (s.equals("学历")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic highsetShool = new HrDic();
						highsetShool.setOrgId(userInfo.getOrgId());
						highsetShool.setName(tempMap.get(s));
						highsetShool.setCode("highsetShool");
						try{
							highsetShool = hrDicService.selectOneHrDic(highsetShool);
							if(highsetShool!=null) {
								hrUser.setHighsetShool(highsetShool.getKeyValue());
							}else{
								resList.add("未能查询到学历类别!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员学历类别出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
				}
				if (s.equals("毕业时间")) {
					hrUser.setGraduationTime(tempMap.get(s));
				}
				if (s.equals("毕业学校")) {
					hrUser.setGraduationShool(tempMap.get(s));
				}
				if (s.equals("专业")) {
					hrUser.setMajor(tempMap.get(s));
				}
			}
			hrUser.setAnnualLeave(0.0);
			hrUser.setAnimal("0");
			hrUser.setBloodType("");
			hrUser.setCreateTime(createTime);
			hrUser.setCreateUser(userInfo.getAccountId());
			hrUser.setOrgId(userInfo.getOrgId());
			if(insertFlag) {
				hrUserList.add(hrUser);
			}
		}
		for(HrUser hrUser:hrUserList) {
			insertHrUser(hrUser);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }

	public void getExpUserRecord(HttpServletResponse response, UserInfo user) {
		List<Map<String, Object>> lists = getExpUserRecord(user.getOrgId());
		ExcelExportData excelExportData = new ExcelExportData();
		List<String> headers = new ArrayList<>();
		headers.add("账号");
		headers.add("姓名");
		headers.add("性别");
		headers.add("工号");
		headers.add("在职状态");
		headers.add("手机号");
		headers.add("所在部门");
		headers.add("学历");
		headers.add("职务");
		try {
			excelExportData.exportFileForVue(response, "人力资源_" + System.currentTimeMillis() + ".xlsx", "花名册", headers, lists);
		} catch (IOException e) {
		}
	}

	/**
	 * 导出人员列表
	 * @param orgId 机构码
	 * @return 人员列表
	 */
	public List<Map<String, Object>> getExpUserRecord(String orgId) {
		return hrUserMapper.getExpUserRecord(orgId);
	}

}
