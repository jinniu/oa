package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrDic;
import com.core136.bean.hr.HrLicence;
import com.core136.bean.hr.HrUser;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrLicenceMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class HrLicenceService {
	private HrLicenceMapper hrLicenceMapper;
	private HrUserService hrUserService;
	private HrDicService hrDicService;
	@Autowired
	public void setHrLicenceMapper(HrLicenceMapper hrLicenceMapper) {
		this.hrLicenceMapper = hrLicenceMapper;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}

	public int insertHrLicence(HrLicence hrLicence) {
		return hrLicenceMapper.insert(hrLicence);
	}

	public int deleteHrLicence(HrLicence hrLicence) {
		return hrLicenceMapper.delete(hrLicence);
	}

	/**
	 * 批量删除证照
	 *
	 * @param orgId 机构码
	 * @param list 证照Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteHrLicenceByIds(String orgId, List<String> list) {
		if (list.size() == 0) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(HrLicence.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("licenceId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrLicenceMapper.deleteByExample(example));
		}
	}

	public int updateHrLicence(Example example, HrLicence hrLicence) {
		return hrLicenceMapper.updateByExampleSelective(hrLicence, example);
	}

	public HrLicence selectOneHrLicence(HrLicence hrLicence) {
		return hrLicenceMapper.selectOne(hrLicence);
	}

	/**
	 * 获取证照列表
	 *
	 * @param orgId 机构码
	 * @param userId 用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime     开始时间
	 * @param endTime       结束时间
	 * @param licenceType 证照类型
	 * @param keyword 查询关键词
	 * @return 证照列表
	 */
	public List<Map<String, String>> getHrLicenceList(String orgId, String userId, String dateQueryType, String beginTime, String endTime, String licenceType, String keyword) {
		return hrLicenceMapper.getHrLicenceList(orgId, userId, dateQueryType, beginTime, endTime, licenceType, "%" + keyword + "%");
	}

	/**
	 * 获取证照详情
	 *
	 * @param orgId 机构码
	 * @param licenceId 证照类型
	 * @return 消息结构
	 */
	public Map<String, String> getHrLicenceById(String orgId, String licenceId) {
		return hrLicenceMapper.getHrLicenceById(orgId, licenceId);
	}

	/**
	 * 获取个人证照列表
	 *
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param page 当前页码
	 * @return 证照列表
	 */
	public List<Map<String, String>> getHrLicenceListForApp(String orgId, String accountId, Integer page) {
		return hrLicenceMapper.getHrLicenceListForApp(orgId, accountId, page);
	}

	/**
	 * 查询个人证照信息
	 *
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 个人证照信息
	 */
	public List<Map<String, String>> getMyHrLicenceList(String orgId, String accountId) {
		return hrLicenceMapper.getMyHrLicenceList(orgId, accountId);
	}

	/**
	 * 获取证照列表
	 *
	 * @param pageParam     分页参数
	 * @param userId 用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime     开始时间
	 * @param endTime       结束时间
	 * @param licenceType 证照类型
	 * @return 证照列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getHrLicenceList(PageParam pageParam, String userId, String dateQueryType, String beginTime, String endTime, String licenceType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getHrLicenceList(pageParam.getOrgId(), userId, dateQueryType, beginTime, endTime, licenceType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 查询个人证照信息
	 *
	 * @param pageParam 分页参数
	 * @return 个人证照信息
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyHrLicenceList(PageParam pageParam) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyHrLicenceList(pageParam.getOrgId(), pageParam.getAccountId());
		return new PageInfo<>(datalist);
	}

	/**
	 * 证照记录导入
	 *
	 * @param userInfo 用户地象
	 * @param file 导入文件模版
	 * @return 消息结构
	 * @throws IOException 服务器内类异常
	 */
	@Transactional(value = "generalTM")
	public RetDataBean importHrLicence(UserInfo userInfo, MultipartFile file) throws IOException {
		List<String> titleList = new ArrayList<>();
		List<HrLicence> hrLicenceList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		List<String> resList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("持证人员");
		titleList.add("证照名称");
		titleList.add("证照编号");
		titleList.add("生效日期");
		titleList.add("截止日期");
		titleList.add("发证机构");
		titleList.add("证照类型");
		titleList.add("备注");
		List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrLicence hrLicence = new HrLicence();
			hrLicence.setLicenceId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrLicence.setSortNo(Integer.parseInt(tempMap.get(s)));
					} else {
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("持证人员")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrUser hrUser = new HrUser();
						hrUser.setOrgId(userInfo.getOrgId());
						hrUser.setUserName(tempMap.get(s));
						try {
							hrUser = hrUserService.selectOneHrUser(hrUser);
							if (hrUser != null) {
								hrLicence.setUserId(hrUser.getUserId());
							} else {
								resList.add("持证人员不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						} catch (Exception e) {
							resList.add("持证人员查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("持证人员不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("证照名称")) {
					hrLicence.setName(tempMap.get(s));
				}
				if (s.equals("证照编号")) {
					hrLicence.setLicenceCode(tempMap.get(s));
				}
				if (s.equals("生效日期")) {
					hrLicence.setBeginTime(tempMap.get(s));
				}
				if (s.equals("截止日期")) {
					hrLicence.setEndTime(tempMap.get(s));
				}
				if (s.equals("发证机构")) {
					hrLicence.setNotifiedBody(tempMap.get(s));
				}
				if (s.equals("证照类型")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic licenceType = new HrDic();
						licenceType.setOrgId(userInfo.getOrgId());
						licenceType.setName(tempMap.get(s));
						licenceType.setCode("licenceType");
						try {
							licenceType = hrDicService.selectOneHrDic(licenceType);
							if (licenceType != null) {
								hrLicence.setLicenceType(licenceType.getKeyValue());
							} else {
								resList.add("证照类型不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						} catch (Exception e) {
							resList.add("证照类型查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("证照类型不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("备注")) {
					hrLicence.setRemark(tempMap.get(s));
				}
			}
			hrLicence.setCreateTime(createTime);
			hrLicence.setCreateUser(userInfo.getAccountId());
			hrLicence.setOrgId(userInfo.getOrgId());
			if (insertFlag) {
				hrLicenceList.add(hrLicence);
			}
		}
		for (HrLicence hrLicence : hrLicenceList) {
			insertHrLicence(hrLicence);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
	}

	/**
	 * 获取到期提醒的证件列表
	 *
	 * @param orgId 机构码
	 * @return 证件列表
	 */
	public List<HrLicence> getHrLicenceReminderRecord(String orgId) {
		HrLicence hrLicence = new HrLicence();
		hrLicence.setOrgId(orgId);
		hrLicence.setReminder("1");
		return hrLicenceMapper.select(hrLicence);
	}

}
