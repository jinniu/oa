package com.core136.service.hr;

import com.core136.bean.hr.HrCareRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrCareRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrCareRecordService {

    private HrCareRecordMapper hrCareRecordMapper;
	@Autowired
	public void setHrCareRecordMapper(HrCareRecordMapper hrCareRecordMapper) {
		this.hrCareRecordMapper = hrCareRecordMapper;
	}

	public int insertHrCareRecord(HrCareRecord hrCareRecord) {
        return hrCareRecordMapper.insert(hrCareRecord);
    }


    public int deleteHrCareRecord(HrCareRecord hrCareRecord) {
        return hrCareRecordMapper.delete(hrCareRecord);
    }

	/**
	 * 批量删除员工福利记录
	 * @param orgId 机构码
	 * @param list 关怀记录Id
	 * @return 消息结构
	 */
	public RetDataBean deleteHrCareRecordByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrCareRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrCareRecordMapper.deleteByExample(example));
		}
	}

    public int updateHrCareRecord(Example example, HrCareRecord hrCareRecord) {
        return hrCareRecordMapper.updateByExampleSelective(hrCareRecord, example);
    }

    public HrCareRecord selectOneHrCareRecord(HrCareRecord hrCareRecord) {
        return hrCareRecordMapper.selectOne(hrCareRecord);
    }

	/**
	 * 获取人员关怀列表
	 * @param orgId 机构码
	 * @param userId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param careType 关怀类型
	 * @param keyword 查询关键词
	 * @return 关怀列表
	 */
    public List<Map<String, String>> getHrCareRecordList(String orgId, String userId, String dateQueryType,String beginTime, String endTime, String careType, String keyword) {
        return hrCareRecordMapper.getHrCareRecordList(orgId, userId, dateQueryType,beginTime, endTime, careType, "%" + keyword + "%");
    }

	/**
	 * 获取人员关怀列表
	 * @param pageParam 分页参数
	 * @param userId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param careType 关怀类型
	 * @return 关怀列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrCareRecordList(PageParam pageParam, String userId, String dateQueryType,String beginTime, String endTime, String careType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrCareRecordList(pageParam.getOrgId(), userId, dateQueryType,beginTime, endTime, careType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }
}
