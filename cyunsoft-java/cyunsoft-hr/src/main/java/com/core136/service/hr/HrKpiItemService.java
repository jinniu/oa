package com.core136.service.hr;

import com.core136.bean.hr.HrKpiItem;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrKpiItemMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrKpiItemService {
    private HrKpiItemMapper hrKpiItemMapper;
	@Autowired
	public void setHrKpiItemMapper(HrKpiItemMapper hrKpiItemMapper) {
		this.hrKpiItemMapper = hrKpiItemMapper;
	}

	public int insertHrKpiItem(HrKpiItem hrKpiItem) {
        return hrKpiItemMapper.insert(hrKpiItem);
    }

    public int deleteHrKpiItem(HrKpiItem hrKpiItem) {
        return hrKpiItemMapper.delete(hrKpiItem);
    }

	/**
	 * 批量删除考核指标项
	 * @param orgId 机构码
	 * @param list 考核指标项Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteHrKpiItemByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrKpiItem.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("itemId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrKpiItemMapper.deleteByExample(example));
		}
	}

    public int updateHrKpiItem(Example example, HrKpiItem hrKpiItem) {
        return hrKpiItemMapper.updateByExampleSelective(hrKpiItem, example);
    }

    public HrKpiItem selectOneHrKpiItem(HrKpiItem hrKpiItem) {
        return hrKpiItemMapper.selectOne(hrKpiItem);
    }

	/**
	 * 获取考核指标列表
	 * @param orgId 机构码
	 * @param createUser 考核人
	 * @param kpiType 考核指标类型
	 * @param keyword 查询关键词
	 * @return 考核指标列表
	 */
    public List<Map<String, String>> getHrKpiItemList(String orgId, String createUser, String kpiType, String keyword) {
        return hrKpiItemMapper.getHrKpiItemList(orgId, createUser, kpiType, "%" + keyword + "%");
    }

	/**
	 * 获取考核指标列表
	 * @param pageParam 分页参数
	 * @param createUser 考核人
	 * @param kpiType 考核类型
	 * @return 考核指标列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrKpiItemList(PageParam pageParam, String createUser, String kpiType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrKpiItemList(pageParam.getOrgId(), createUser, kpiType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取考核指标集
	 * @param orgId 机构码
	 * @return 考核指标集
	 */
	public List<Map<String, String>> getHrKpiItemListForSelect(String orgId) {
        return hrKpiItemMapper.getHrKpiItemListForSelect(orgId);
    }
}
