package com.core136.service.hr;

import com.core136.bean.hr.HrTrainRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrTrainRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 人员培训记录
 *
 * @author lsq
 */
@Service
public class HrTrainRecordService {
    private HrTrainRecordMapper hrTrainRecordMapper;
	@Autowired
	public void setHrTrainRecordMapper(HrTrainRecordMapper hrTrainRecordMapper) {
		this.hrTrainRecordMapper = hrTrainRecordMapper;
	}

	public int insertHrTrainRecord(HrTrainRecord hrTrainRecord) {
        return hrTrainRecordMapper.insert(hrTrainRecord);
    }

    public int deleteHrTrainRecord(HrTrainRecord hrTrainRecord) {
        return hrTrainRecordMapper.delete(hrTrainRecord);
    }

	/**
	 * 批量删除培训记录
	 * @param orgId 机构码
	 * @param list 培训记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteHrTrainRecordByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrTrainRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrTrainRecordMapper.deleteByExample(example));
		}
	}
    public int updateHrTrainRecord(Example example, HrTrainRecord hrTrainRecord) {
        return hrTrainRecordMapper.updateByExampleSelective(hrTrainRecord, example);
    }

    public HrTrainRecord selectOneHrTrainRecord(HrTrainRecord hrTrainRecord) {
        return hrTrainRecordMapper.selectOne(hrTrainRecord);
    }

	/**
	 * 获取培训列表
	 * @param orgId 机构码
	 * @param createUser 创建人
	 * @param channel
	 * @param courseType
	 * @param status
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getHrTrainRecordList(String orgId, String createUser, String channel, String courseType, String status,String dateQueryType, String beginTime, String endTime, String keyword) {
        return hrTrainRecordMapper.getHrTrainRecordList(orgId, createUser, channel, courseType, status,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取培训列表
	 * @param pageParam 分页参数
	 * @param channel
	 * @param courseType
	 * @param status
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrTrainRecordList(PageParam pageParam, String channel, String courseType, String status,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrTrainRecordList(pageParam.getOrgId(), pageParam.getAccountId(), channel, courseType, status,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 *  获取待审批记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param channel
	 * @param courseType
	 * @param status
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getHrTrainRecordApprovedList(String orgId,String opFlag, String accountId, String channel, String courseType,String status,String dateQueryType, String beginTime, String endTime, String keyword) {
        return hrTrainRecordMapper.getHrTrainRecordApprovedList(orgId,opFlag, accountId, channel, courseType,status,dateQueryType, beginTime, endTime, keyword);
    }

	/**
	 * 获取待审批记录
	 * @param pageParam 分页参数
	 * @param channel
	 * @param courseType
	 * @param status
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrTrainRecordApprovedList(PageParam pageParam, String channel, String courseType,String status,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrTrainRecordApprovedList(pageParam.getOrgId(), pageParam.getOpFlag(),pageParam.getAccountId(), channel, courseType,status,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
