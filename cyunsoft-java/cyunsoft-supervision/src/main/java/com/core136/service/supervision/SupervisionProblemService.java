package com.core136.service.supervision;

import com.core136.bean.supervision.SupervisionProblem;
import com.core136.mapper.supervision.SupervisionProblemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SupervisionProblemService {
    private SupervisionProblemMapper supervisionProblemMapper;
	@Autowired
	public void setSupervisionProblemMapper(SupervisionProblemMapper supervisionProblemMapper) {
		this.supervisionProblemMapper = supervisionProblemMapper;
	}

	public int insertSupervisionProblem(SupervisionProblem supervisionProblem) {
        return supervisionProblemMapper.insert(supervisionProblem);
    }

    public int deleteSupervisionProblem(SupervisionProblem supervisionProblem) {
        return supervisionProblemMapper.delete(supervisionProblem);
    }

    public int updateSupervisionProblem(Example example, SupervisionProblem supervisionProblem) {
        return supervisionProblemMapper.updateByExampleSelective(supervisionProblem, example);
    }

    public SupervisionProblem selectOneSupervisionProblem(SupervisionProblem supervisionProblem) {
        return supervisionProblemMapper.selectOne(supervisionProblem);
    }

    /**
     * 获取问题列表
     *
     * @param orgId 机构码
     * @param processId 关联处理过程Id
     * @param accountId 用户账号
     * @return 问题列表
     */
    public List<Map<String, String>> getMySupervisionProblemList(String orgId, String processId,String opFlag, String accountId,Integer page) {
        return supervisionProblemMapper.getMySupervisionProblemList(orgId, processId, opFlag,accountId,page);
    }

	public List<Map<String, String>> getLeadSupervisionProblemList(String orgId,  String accountId,String processId,Integer page) {
		return supervisionProblemMapper.getLeadSupervisionProblemList(orgId, accountId,processId,page);
	}

	public List<Map<String, String>> getSupervisionProcessUserList(String orgId,String recordId)
	{
		return supervisionProblemMapper.getSupervisionProcessUserList(orgId,recordId);
	}

    public List<Map<String, String>> getMySupervisionAnswerList(String orgId, String accountId, String recordId) {
        return supervisionProblemMapper.getMySupervisionAnswerList(orgId, accountId,recordId);
    }


}
