package com.core136.service.supervision;

import com.core136.bean.supervision.SupervisionConfig;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.supervision.SupervisionConfigMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SupervisionConfigService {
    private SupervisionConfigMapper supervisionConfigMapper;
	@Autowired
	public void setSupervisionConfigMapper(SupervisionConfigMapper supervisionConfigMapper) {
		this.supervisionConfigMapper = supervisionConfigMapper;
	}

	public int insertSupervisionConfig(SupervisionConfig supervisionConfig) {
        return supervisionConfigMapper.insert(supervisionConfig);
    }

    public int deleteSupervisionConfig(SupervisionConfig supervisionConfig) {
        return supervisionConfigMapper.delete(supervisionConfig);
    }

    public int updateSupervisionConfig(Example example, SupervisionConfig supervisionConfig) {
        return supervisionConfigMapper.updateByExampleSelective(supervisionConfig, example);
    }

    public SupervisionConfig selectOneSupervisionConfig(SupervisionConfig supervisionConfig) {
        return supervisionConfigMapper.selectOne(supervisionConfig);
    }

    public List<SupervisionConfig> getAllConfig(Example example) {
        return supervisionConfigMapper.selectByExample(example);
    }

	/**
	 * 获取督查类型列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 类型列表
	 */
	public List<Map<String, String>> getAllSupervisionConfig(String orgId,String keyword) {
		return supervisionConfigMapper.getAllSupervisionConfig(orgId,"%"+keyword+"%");
	}
    public PageInfo<Map<String, String>> getAllSupervisionConfig(PageParam pageParam) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAllSupervisionConfig(pageParam.getOrgId(),pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取督查类型列表
	 * @param orgId 机构码
	 * @return 类型列表
	 */
	public List<Map<String, String>> getSupervisionConfigForSelect(String orgId) {
		return supervisionConfigMapper.getSupervisionConfigForSelect(orgId);
	}

	/**
	 * 获取类型与领导列表
	 * @param orgId 机构码
	 * @return 类型与领导列表
	 */
	public List<Map<String, String>> getAllSupervisionConfigList(String orgId) {
        return supervisionConfigMapper.getAllSupervisionConfigList(orgId);
    }

	/**
	 * 与我有关的分类
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 与我有关的分类列表
	 */
	public List<Map<String, String>> getMySupervisionConfigList(String orgId, String accountId) {
        return supervisionConfigMapper.getMySupervisionConfigList(orgId, accountId);
    }

	/**
	 * 按类型汇总
	 * @param orgId 机构码
	 * @return 类型汇总列表
	 */
	public List<Map<String, String>> getQuerySupervisionForType(String orgId) {
        return supervisionConfigMapper.getQuerySupervisionForType(orgId);
    }

    /**
     * 获取分类树结构
     *
     * @param orgId 机构码
     * @return 分类树结构
     */
    public List<Map<String, Object>> getSupervisionConfigTree(String orgId) {
        return supervisionConfigMapper.getSupervisionConfigTree(orgId);
    }

}
