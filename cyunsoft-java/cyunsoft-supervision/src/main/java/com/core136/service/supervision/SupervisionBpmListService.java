package com.core136.service.supervision;

import com.core136.bean.supervision.SupervisionBpmList;
import com.core136.mapper.supervision.SupervisionBpmListMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SupervisionBpmListService {
    private SupervisionBpmListMapper supervisionBpmListMapper;
	@Autowired
	public void setSupervisionBpmListMapper(SupervisionBpmListMapper supervisionBpmListMapper) {
		this.supervisionBpmListMapper = supervisionBpmListMapper;
	}

	public int insertSupervisionBpmList(SupervisionBpmList supervisionBpmList) {
        return supervisionBpmListMapper.insert(supervisionBpmList);
    }

    public int deleteSupervisionBpmList(SupervisionBpmList supervisionBpmList) {
        return supervisionBpmListMapper.delete(supervisionBpmList);
    }

    public int updateSupervisionBpmList(Example example, SupervisionBpmList supervisionBpmList) {
        return supervisionBpmListMapper.updateByExampleSelective(supervisionBpmList, example);
    }

    public SupervisionBpmList selectOneSupervisionBpmList(SupervisionBpmList supervisionBpmList) {
        return supervisionBpmListMapper.selectOne(supervisionBpmList);
    }

	/**
	 * 督查督办历史流程
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param processId 处理过程Id
	 * @return 历史流程列表
	 */
	public List<Map<String, String>> getSupervisionBpmRecordList(String orgId,String opFlag,String accountId, String processId) {
        return supervisionBpmListMapper.getSupervisionBpmRecordList(orgId,opFlag, accountId,processId);
    }

}
