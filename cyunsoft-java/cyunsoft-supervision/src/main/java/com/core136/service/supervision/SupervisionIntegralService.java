package com.core136.service.supervision;

import com.core136.bean.account.UserInfo;
import com.core136.bean.supervision.SupervisionIntegral;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.supervision.SupervisionIntegralMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class SupervisionIntegralService {
    private SupervisionIntegralMapper supervisionIntegralMapper;
	@Autowired
	public void setSupervisionIntegralMapper(SupervisionIntegralMapper supervisionIntegralMapper) {
		this.supervisionIntegralMapper = supervisionIntegralMapper;
	}

	public int insertSupervisionIntegral(SupervisionIntegral supervisionIntegral) {
        return supervisionIntegralMapper.insert(supervisionIntegral);
    }

    public int deleteSupervisionIntegral(SupervisionIntegral supervisionIntegral) {
        return supervisionIntegralMapper.delete(supervisionIntegral);
    }

    public int updateSupervisionIntegral(Example example, SupervisionIntegral supervisionIntegral) {
        return supervisionIntegralMapper.updateByExampleSelective(supervisionIntegral, example);
    }

    public SupervisionIntegral selectOneSupervisionIntegral(SupervisionIntegral supervisionIntegral) {
        return supervisionIntegralMapper.selectOne(supervisionIntegral);
    }

    public RetDataBean setIntegralRule(UserInfo user, SupervisionIntegral supervisionIntegral) {
        SupervisionIntegral tempSupervisionIntegral = new SupervisionIntegral();
        tempSupervisionIntegral.setOrgId(supervisionIntegral.getOrgId());
        tempSupervisionIntegral = selectOneSupervisionIntegral(tempSupervisionIntegral);
        if (tempSupervisionIntegral == null) {
            supervisionIntegral.setConfigId(SysTools.getGUID());
            supervisionIntegral.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            supervisionIntegral.setCreateUser(user.getAccountId());
            supervisionIntegral.setOrgId(user.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, insertSupervisionIntegral(supervisionIntegral));
        } else {
            Example example = new Example(SupervisionIntegral.class);
            example.createCriteria().andEqualTo("orgId", user.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, updateSupervisionIntegral(example, supervisionIntegral));
        }
    }

}
