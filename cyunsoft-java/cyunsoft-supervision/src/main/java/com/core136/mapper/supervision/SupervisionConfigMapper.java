package com.core136.mapper.supervision;

import com.core136.bean.supervision.SupervisionConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SupervisionConfigMapper extends MyMapper<SupervisionConfig> {
	/**
	 * 获取督查类型列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 类型列表
	 */
	List<Map<String, String>> getAllSupervisionConfig(@Param(value = "orgId") String orgId,@Param(value="keyword")String keyword);

	/**
	 * 获取督查类型列表
	 * @param orgId 机构码
	 * @return 类型列表
	 */
	List<Map<String, String>> getSupervisionConfigForSelect(@Param(value = "orgId") String orgId);

    /**
     * 获取分类树结构
     *
     * @param orgId 机构码
     * @return 分类树结构
     */
    List<Map<String, Object>> getSupervisionConfigTree(@Param(value = "orgId") String orgId);

	/**
	 * 获取类型与领导列表
	 * @param orgId 机构码
	 * @return 类型与领导列表
	 */
	List<Map<String, String>> getAllSupervisionConfigList(@Param(value = "orgId") String orgId);

	/**
	 * 与我有关的分类列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 我有关的分类列表
	 */
	List<Map<String, String>> getMySupervisionConfigList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 * 按类型汇总
	 * @param orgId 机构码
	 * @return 类型汇总列表
	 */
	List<Map<String, String>> getQuerySupervisionForType(@Param(value = "orgId") String orgId);
}
