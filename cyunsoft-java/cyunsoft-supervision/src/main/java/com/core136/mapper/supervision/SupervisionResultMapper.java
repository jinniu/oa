package com.core136.mapper.supervision;

import com.core136.bean.supervision.SupervisionResult;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SupervisionResultMapper extends MyMapper<SupervisionResult> {
    /**
     * 获取任务处理过程列表
     *
     * @param orgId 机构码
     * @param processId 处理过程Id
     * @return 任务处理过程列表
     */
    List<Map<String, String>> getSupervisionResultList(@Param(value = "orgId") String orgId, @Param(value = "processId") String processId);
}
