package com.core136.mapper.supervision;

import com.core136.bean.supervision.SupervisionProblem;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SupervisionProblemMapper extends MyMapper<SupervisionProblem> {

    /**
     * 获取问题列表
     *
     * @param orgId 机构码
     * @param processId
     * @param accountId 用户账号
     * @return 问题列表
     */
    List<Map<String, String>> getMySupervisionProblemList(@Param(value = "orgId") String orgId, @Param(value = "processId") String processId,
														  @Param(value = "opFlag") String opFlag,@Param(value = "accountId") String accountId,@Param(value="page") Integer page);

	List<Map<String, String>> getLeadSupervisionProblemList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value = "processId") String processId,@Param(value="page") Integer page);

    /**
     * 获取问题回复
     *
     * @param orgId 机构码
     * @param recordId
     * @return
     */
    List<Map<String, String>> getMySupervisionAnswerList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value = "recordId") String recordId);
	List<Map<String, String>> getSupervisionProcessUserList(@Param(value = "orgId") String orgId,@Param(value = "recordId") String recordId);


}
