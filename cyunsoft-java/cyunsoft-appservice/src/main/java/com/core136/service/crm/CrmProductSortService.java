package com.core136.service.crm;

import com.core136.bean.crm.CrmProductSort;
import com.core136.mapper.crm.CrmProductSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * Crm 产品分类服务类
 * @author lsq
 */
@Service
public class CrmProductSortService {
    private CrmProductSortMapper crmProductSortMapper;
	@Autowired
	public void setCrmProductSortMapper(CrmProductSortMapper crmProductSortMapper) {
		this.crmProductSortMapper = crmProductSortMapper;
	}

	public int insertCrmProductSort(CrmProductSort crmProductSort) {
        return crmProductSortMapper.insert(crmProductSort);
    }

    public int deleteCrmProductSort(CrmProductSort crmProductSort) {
        return crmProductSortMapper.delete(crmProductSort);
    }

    public int updateCrmProductSort(Example example, CrmProductSort crmProductSort) {
        return crmProductSortMapper.updateByExampleSelective(crmProductSort, example);
    }

    public CrmProductSort selectOneCrmProductSort(CrmProductSort crmProductSort) {
        return crmProductSortMapper.selectOne(crmProductSort);
    }

	/**
	 * 获取产品分类
	 * @param orgId 机构码
	 * @return 产品分类
	 */
	public List<CrmProductSort> getCrmProductSortTree(String orgId) {
		List<CrmProductSort> list = getAllCrmProductSort(orgId);
		List<CrmProductSort> crmProductSortList = new ArrayList<>();
		for (CrmProductSort productSort : list) {
			if (productSort.getParentId().equals("0")) {
				crmProductSortList.add(productSort);
			}
		}
		for (CrmProductSort crmProductSort : crmProductSortList) {
			crmProductSort.setChildren(getChildCrmProductSortList(crmProductSort.getSortId(), list));
		}
		return crmProductSortList;
	}

	/**
	 * 获取分类子表
	 * @param sortId 分类Id
	 * @param rootCrmProductSort 产品分类列表对象
	 * @return 分类子表
	 */
	public List<CrmProductSort> getChildCrmProductSortList(String sortId, List<CrmProductSort> rootCrmProductSort) {
		List<CrmProductSort> childList = new ArrayList<>();
		for (CrmProductSort crmProductSort : rootCrmProductSort) {
			if (crmProductSort.getParentId().equals(sortId)) {
				childList.add(crmProductSort);
			}
		}
		for (CrmProductSort crmProductSort : childList) {
			crmProductSort.setChildren(getChildCrmProductSortList(crmProductSort.getSortId(), rootCrmProductSort));
		}
		if (childList.isEmpty()) {
			return null;
		}
		return childList;
	}

	/**
	 * 判断分类下是否有子节点
	 * @param orgId 机构码
	 * @param parentId 父级Id
	 * @return 是否有子节点
	 */
    public boolean isExistChild(String orgId, String parentId) {
		CrmProductSort crmProductSort = new CrmProductSort();
		crmProductSort.setOrgId(orgId);
		crmProductSort.setParentId(parentId);
		return crmProductSortMapper.selectCount(crmProductSort) > 0;
    }

    public List<CrmProductSort> getAllCrmProductSort(String orgId) {
		CrmProductSort crmProductSort = new CrmProductSort();
		crmProductSort.setOrgId(orgId);
		return crmProductSortMapper.select(crmProductSort);
	}
}
