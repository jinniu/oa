package com.core136.service.office;

import com.core136.bean.account.UserInfo;
import com.core136.bean.office.Meeting;
import com.core136.bean.office.MeetingRoom;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.MeetingMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 会议操作服务类
 * @author lsq
 */
@Service
public class MeetingService {
	private MeetingMapper meetingMapper;
	private UserInfoService userInfoService;
	private MeetingRoomService meetingRoomService;
	@Autowired
	public void setMeetingMapper(MeetingMapper meetingMapper) {
		this.meetingMapper = meetingMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setMeetingRoomService(MeetingRoomService meetingRoomService) {
		this.meetingRoomService = meetingRoomService;
	}

	/**
	 * 创建会议记录
	 * @param meeting 会议对象
	 * @return 成功创建记录数
	 */
	public int insertMeeting(Meeting meeting) {
		return meetingMapper.insert(meeting);
	}

	/**
	 * 删除会议记录
	 * @param meeting 会议对象
	 * @return 成功创建记录数
	 */
	public int deleteMeeting(Meeting meeting) {
		return meetingMapper.delete(meeting);
	}

	/**
	 * 更新会议记录
	 * @param example 更新条件
	 * @param meeting 会议对象
	 * @return 成功更新记录数
	 */
	public int updateMeeting(Example example, Meeting meeting) {
		return meetingMapper.updateByExampleSelective(meeting, example);
	}

	/**
	 * 查询单个会议记录
	 * @param meeting 会议对象
	 * @return 会议对象
	 */
	public Meeting selectOneMeeting(Meeting meeting) {
		return meetingMapper.selectOne(meeting);
	}

	/**
	 * 添加会议
	 * @param meeting 会议对象
	 * @return 成功创建记录数
	 */
	public RetDataBean addMeeting(Meeting meeting) {
		int flag = isExist(meeting.getOrgId(), meeting.getRoomId(), meeting.getBeginTime(), meeting.getEndTime());
		if (flag == 0) {
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertMeeting(meeting));
		} else {
			return RetDataTools.NotOk(MessageCode.MSG_00033);
		}
	}

	/**
	 * 获取会议详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param meetingId 会议Id
	 * @return 会议详情
	 */
	public Map<String, String> getMyMeetingById(String orgId, String opFlag,String accountId, String deptId,String userLevel,String meetingId) {
		return meetingMapper.getMyMeetingById(orgId, opFlag, accountId, deptId,userLevel,meetingId);
	}


	/**
	 * 发送会议取消通知
	 * @param user1
	 * @param meeting
	 */
	public void sendCancelMeetingMsg(UserInfo user1, Meeting meeting) {

		if (StringUtils.isNotBlank(meeting.getMsgType())) {
			String sendTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
			List<MsgBody> msgList = new ArrayList<>();
			String chair = meeting.getChair();
			UserInfo userInfo = userInfoService.getUserByAccountId(chair, meeting.getOrgId());
			if (userInfo != null) {
				MsgBody msgBody = new MsgBody();
				msgBody.setUserInfo(userInfo);
				msgBody.setFormUserName(user1.getUserName());
				msgBody.setFromAccountId(user1.getAccountId());
				msgBody.setOrgId(user1.getOrgId());
				msgBody.setSendTime(sendTime);
				msgBody.setTitle(meeting.getSubject() + " 取消通知！");
				msgBody.setView("MEETING");
				msgBody.setRecordId(meeting.getMeetingId());
				msgBody.setContent("时间原定于：" + meeting.getBeginTime() + "-" + meeting.getEndTime() +
						"，主题为：" + meeting.getSubject() + "的会议已取消");
				msgList.add(msgBody);
			}
			String userJoin = meeting.getUserJoin();
			String deptJoin = meeting.getDeptJoin();
			String levelJoin = meeting.getLevelJoin();
			List<UserInfo> inRoleAccountList = userInfoService.getUserInRole(user1.getOrgId(), userJoin, deptJoin, levelJoin);
			for (UserInfo info : inRoleAccountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setUserInfo(info);
				msgBody.setFormUserName(user1.getUserName());
				msgBody.setFromAccountId(user1.getAccountId());
				msgBody.setOrgId(user1.getOrgId());
				msgBody.setSendTime(sendTime);
				msgBody.setTitle(meeting.getSubject() + " 取消通知！");
				msgBody.setView(GlobalConstant.MSG_TYPE_MEETING);
				msgBody.setRecordId(meeting.getMeetingId());
				msgBody.setContent("时间原定于：" + meeting.getBeginTime() + "-" + meeting.getEndTime() +
						"，主题为：" + meeting.getSubject() + "的会议已取消");
				msgList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(meeting.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgList);
		}
	}


	/**
	 * 发送会议提醒
	 * @param user1
	 * @param meeting
	 */
	public void sendMeetingMsg(UserInfo user1, Meeting meeting) {
		if (StringUtils.isNotBlank(meeting.getMsgType())) {
			String roomId = meeting.getRoomId();
			MeetingRoom meetingRoom = new MeetingRoom();
			meetingRoom.setRoomId(roomId);
			meetingRoom.setOrgId(user1.getOrgId());
			meetingRoom = meetingRoomService.selectOneMeetingRoom(meetingRoom);
			String sendTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
			List<MsgBody> msgList = new ArrayList<>();
			String chair = meeting.getChair();
			UserInfo userInfo = userInfoService.getUserByAccountId(chair, meeting.getOrgId());
			if (userInfo != null) {
				MsgBody msgBody = new MsgBody();
				msgBody.setUserInfo(userInfo);
				msgBody.setFormUserName(user1.getUserName());
				msgBody.setFromAccountId(user1.getAccountId());
				msgBody.setOrgId(user1.getOrgId());
				msgBody.setSendTime(sendTime);
				msgBody.setTitle(meeting.getSubject());
				msgBody.setAttachId(meeting.getAttachId());
				msgBody.setView("MEETING");
				msgBody.setRecordId(meeting.getMeetingId());
				msgBody.setContent("主题为：" + meeting.getSubject() + "的会议，您将是该会的主持。请您提前做好准备工作。会议时间：" + meeting.getBeginTime() + "-" + meeting.getEndTime() +
						"开会地址：" + meetingRoom.getName() + "具体地址：" + meetingRoom.getAddress());
				msgList.add(msgBody);
			}
			String userJoin = meeting.getUserJoin();
			String deptJoin = meeting.getDeptJoin();
			String levelJoin = meeting.getLevelJoin();
			List<UserInfo> inRoleAccountList = userInfoService.getUserInRole(user1.getOrgId(), userJoin, deptJoin, levelJoin);
			for (int i = 0; i < inRoleAccountList.size(); i++) {
				MsgBody msgBody = new MsgBody();
				msgBody.setUserInfo(inRoleAccountList.get(i));
				msgBody.setFormUserName(user1.getUserName());
				msgBody.setFromAccountId(user1.getAccountId());
				msgBody.setOrgId(user1.getOrgId());
				msgBody.setSendTime(sendTime);
				msgBody.setTitle(meeting.getSubject());
				msgBody.setAttachId(meeting.getAttachId());
				msgBody.setView(GlobalConstant.MSG_TYPE_MEETING);
				msgBody.setRecordId(meeting.getMeetingId());
				msgBody.setContent("主题为：" + meeting.getSubject() + "的会议，邀请您参加。请您提前做好准备工作。会议时间：" + meeting.getBeginTime() + "-" + meeting.getEndTime() +
						"开会地址：" + meetingRoom.getName() + "具体地址：" + meetingRoom.getAddress());
				msgList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(meeting.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgList);
		}
	}

	/**
	 * 获取待审批的会议列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param status 状态
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 会议列表
	 */
	public List<Map<String, String>> getApplyMeetingList(String orgId, String opFlag, String accountId, String status, String dateQueryType, String beginTime, String endTime, String keyword) {
		return meetingMapper.getApplyMeetingList(orgId, opFlag, accountId,status,dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 获取待审批的会议列表
	 * @param pageParam 分页参数
	 * @param status 状态
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 会议列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getApplyMeetingList(PageParam pageParam, String status, String dateQueryType,String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getApplyMeetingList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),status,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取桌面会议
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @return 桌面会议列表
	 */
	public List<Map<String, String>> getMyMeetingListForDesk(String orgId, String accountId, String deptId, String userLevel) {
		String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm");
		return meetingMapper.getMyMeetingListForDesk(orgId, accountId, deptId, userLevel, nowTime);
	}

	/**
	 * 获取移动端待办会议
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param page 页码
	 * @return 待办会议列表
	 */
	public List<Map<String, String>> getMyMeetingListForApp(String orgId, String accountId, String deptId, String userLevel, Integer page) {
		String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm");
		return meetingMapper.getMyMeetingListForApp(orgId, accountId, deptId, userLevel, nowTime, page);
	}

	/**
	 * 获取当前用户待参加会议
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param dateQueryType  日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 待参加会议列表
	 */
	public List<Map<String, String>> getMyMeetingList(String orgId, String accountId, String deptId, String userLevel,String dateQueryType, String beginTime, String endTime, String keyword) {
		return meetingMapper.getMyMeetingList(orgId, accountId, deptId, userLevel, dateQueryType,beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 获取当前用户待参加会议
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 待参加会议列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyMeetingList(PageParam pageParam,String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyMeetingList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(), dateQueryType,beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取没有会议纪要的会议列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 会议列表
	 */
	public List<Map<String, String>> getNotNotesMeetingList(String orgId, String accountId) {
		return meetingMapper.getNotNotesMeetingList(orgId, accountId);
	}

	/**
	 * 获取禁用的会议时间段
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dayStr 日期
	 * @return 会议信息
	 */
	public List<Map<String, Object>> getMeetingByDay(String orgId, String deptId, String dayStr) {
		List<Map<String,Object>> resList = new ArrayList<>();
		List<Map<String,Object>> roomList = meetingRoomService.getCanUseMeetingRoomList(orgId,deptId);
		for(Map<String,Object>map:roomList) {
			String roomId = map.get("roomId").toString();
			List<Meeting> meetingList = getMeetingListByDay(orgId,dayStr,roomId);
			map.put("meetingList",meetingList);
			resList.add(map);

		}
		return resList;
	}

	/**
	 * 获取指定日期的会议
	 * @param orgId 机构码
	 * @param dayStr 日期
	 * @param roomId 会议室Id
	 * @return 会议列表
	 */
	public List<Meeting> getMeetingListByDay(String orgId,String dayStr,String roomId) {
		Example example = new Example(Meeting.class);
		example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("roomId",roomId).andEqualTo("meetingDay",dayStr);
		example.and().orEqualTo("status","0").orEqualTo("status","1");
		return meetingMapper.selectByExample(example);
	}

	/**
	 * 获取个人历史会议申请记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param roomId 会议室Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 申请记录列表
	 */
	public List<Map<String, String>> getMyApplyMeetingList(String orgId,String meetingType, String accountId, String roomId,String dateQueryType, String beginTime, String endTime, String keyword) {
		return meetingMapper.getMyApplyMeetingList(orgId,meetingType, accountId, roomId,dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 按日期获取当前会议室使用情况
	 * @param orgId 机构码
	 * @param dayStr 日期
	 * @param accountId 用户账号
	 * @return 会议室使用情况
	 */
	public List<Map<String, String>> getMeetingListByDayStr(String orgId, String dayStr,String accountId) {
		return meetingMapper.getMeetingListByDayStr(orgId,dayStr,accountId);
	}

	/**
	 * 获取个人历史会议申请记录
	 * @param pageParam 分页参数
	 * @param dateQueryType
	 * @param roomId
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyApplyMeetingList(PageParam pageParam, String dateQueryType, String beginTime, String endTime,String roomId,String meetingType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyApplyMeetingList(pageParam.getOrgId(),meetingType, pageParam.getAccountId(), roomId,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 判断当前会议室时间段内是否有会议
	 * @param orgId 机构码
	 * @param roomId 会议室Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 是否有会议
	 */
	public int isExist(String orgId, String roomId, String beginTime, String endTime) {
		return meetingMapper.isExist(orgId, roomId, beginTime, endTime);
	}
}
