package com.core136.service.finance;

import com.core136.bean.finance.ContractSort;
import com.core136.mapper.finance.ContractSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lsq
 */
@Service
public class ContractSortService {
    private ContractSortMapper contractSortMapper;
	@Autowired
	public void setContractSortMapper(ContractSortMapper contractSortMapper) {
		this.contractSortMapper = contractSortMapper;
	}

	public int insertContractSort(ContractSort contractSort) {
        return contractSortMapper.insert(contractSort);
    }

    public ContractSort selectOneContractSort(ContractSort contractSort) {
        return contractSortMapper.selectOne(contractSort);
    }

    public int updateContractSort(ContractSort contractSort, Example example) {
        return contractSortMapper.updateByExampleSelective(contractSort, example);
    }

    public int deleteContractSort(ContractSort contractSort) {
        return contractSortMapper.delete(contractSort);
    }
	public List<ContractSort> getAllContractSort(String orgId)
	{
		Example example = new Example(ContractSort.class);
		example.createCriteria().andEqualTo("orgId",orgId);
		example.setOrderByClause("sort_no asc");
		return contractSortMapper.selectByExample(example);
	}
	/**
	 * 获取流程分类树
	 * @param orgId 机构码
	 * @return 流程分类列表
	 */
	public List<ContractSort> getContractSortTree(String orgId) {
		List<ContractSort> list = getAllContractSort(orgId);
		List<ContractSort> contractSortList = new ArrayList<>();
		for (ContractSort sort : list) {
			if (sort.getParentId().equals("0")) {
				contractSortList.add(sort);
			}
		}
		for (ContractSort contractSort : contractSortList) {
			contractSort.setChildren(getChildContractSortList(contractSort.getSortId(), list));
		}
		return contractSortList;
	}

	/**
	 * 获取分类子表
	 * @param sortId 分类Id
	 * @param rootContractSort 分类子表对象
	 * @return 分类子表列表
	 */
	public List<ContractSort> getChildContractSortList(String sortId, List<ContractSort> rootContractSort) {
		List<ContractSort> childList = new ArrayList<>();
		for (ContractSort bpmSort : rootContractSort) {
			if (bpmSort.getParentId().equals(sortId)) {
				childList.add(bpmSort);
			}
		}
		for (ContractSort contractSort : childList) {
			contractSort.setChildren(getChildContractSortList(contractSort.getSortId(), rootContractSort));
		}
		if (childList.isEmpty()) {
			return null;
		}
		return childList;
	}

	/**
     * 判断是否还有子集
     */

    public Boolean isExistChild(String orgId, String sortId) {
		Example example = new Example(ContractSort.class);
		example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("parentId",sortId);
		return contractSortMapper.selectCountByExample(example) > 0;
    }
}
