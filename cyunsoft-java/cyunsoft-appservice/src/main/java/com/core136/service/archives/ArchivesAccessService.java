package com.core136.service.archives;

import com.core136.bean.archives.ArchivesAccess;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesAccessMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesAccessService {
	private ArchivesAccessMapper archivesAccessMapper;
	@Autowired
	public void setArchivesAccessMapper(ArchivesAccessMapper archivesAccessMapper) {
		this.archivesAccessMapper = archivesAccessMapper;
	}

	/**
	 * 创建出入记录
	 * @param archivesAccess 出入记录对象
	 * @return 创建成功记录数
	 */
	public int insertArchivesAccess(ArchivesAccess archivesAccess) {
		return archivesAccessMapper.insert(archivesAccess);
	}

	/**
	 * 删除出入记录
	 * @param archivesAccess 出入记录对象
	 * @return 成功删除记录数
	 */
	public int deleteArchivesAccess(ArchivesAccess archivesAccess) {
		return archivesAccessMapper.delete(archivesAccess);
	}

	/**
	 * 批量删除进出管理记录
	 * @param orgId 机构码
	 * @param list 进出管理记录Id 列表
	 * @return 成功删除记录数
	 */
	public RetDataBean deleteArchivesAccessIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesAccess.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesAccessMapper.deleteByExample(example));
		}
	}

	/**
	 * 更新出入记录
	 * @param example 更新条件
	 * @param archivesAccess 出入记录对象
	 * @return 成功更新记录数
	 */
	public int updateArchivesAccess(Example example,ArchivesAccess archivesAccess) {
		return archivesAccessMapper.updateByExampleSelective(archivesAccess,example);
	}

	/**
	 * 查询单个出入记录
	 * @param archivesAccess 出入记录对象
	 * @return 出入记录对象
	 */
	public ArchivesAccess selectOneArchivesAccess(ArchivesAccess archivesAccess) {
		return archivesAccessMapper.selectOne(archivesAccess);
	}

	/**
	 * 获取档案仓库进出记录
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param isOut 人员类型
	 * @param accessType 进出类型
	 * @param keyword 查询关键词
	 * @return 进出记录列表
	 */
	public List<Map<String,String>>getArchivesAccessList(String orgId,String opFlag,String accountId,String dateQueryType,String beginTime,String endTime,String isOut,String accessType,String keyword) {
		return archivesAccessMapper.getArchivesAccessList(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,isOut,accessType,"%"+keyword+"%");
	}

	/**
	 * 获取档案仓库进出记录
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param isOut 人员类型
	 * @param accessType 进出类型
	 * @return 进出记录列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getArchivesAccessList(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String isOut, String accessType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesAccessList(pageParam.getOrgId(),pageParam.getOpFlag(), pageParam.getAccountId(),dateQueryType,beginTime,endTime, isOut,accessType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


}
