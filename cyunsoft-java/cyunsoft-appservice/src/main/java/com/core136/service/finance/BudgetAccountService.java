package com.core136.service.finance;

import com.core136.bean.finance.BudgetAccount;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.finance.BudgetAccountMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 预算科目吕操作服务类
 * @author lsq
 */
@Service
public class BudgetAccountService {
    private BudgetAccountMapper budgetAccountMapper;
	@Autowired
	public void setBudgetAccountMapper(BudgetAccountMapper budgetAccountMapper) {
		this.budgetAccountMapper = budgetAccountMapper;
	}

	/**
     * 创建预算科目
     * @param budgetAccount 预算科目对象
     * @return 成功创建记录数
     */
    public int insertBudgetAccount(BudgetAccount budgetAccount) {
        return budgetAccountMapper.insert(budgetAccount);
    }

    /**
     * 删除预算科目
     * @param budgetAccount 预算科目对象
     * @return 成功删除记录数
     */
    public int deleteBudgetAccount(BudgetAccount budgetAccount) {
        return budgetAccountMapper.delete(budgetAccount);
    }

    /**
     * 更新预算科目
     * @param example 更新条件
     * @param budgetAccount 预算科目对象
     * @return 成功更新记录数
     */
    public int updateBudgetAccount(Example example, BudgetAccount budgetAccount) {
        return budgetAccountMapper.updateByExampleSelective(budgetAccount, example);
    }

    /**
     * 查询单个预算科目
     * @param budgetAccount 预算科目对象
     * @return 预算科目对象
     */
    public BudgetAccount selectOneBudgetAccount(BudgetAccount budgetAccount) {
        return budgetAccountMapper.selectOne(budgetAccount);
    }

    /**
     * 批量删除预算科目
     * @param orgId 机构码
     * @param list 预算科目Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteBudgetAccountIds(String orgId, List<String> list)
    {
        if(list.isEmpty())
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(BudgetAccount.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("budgetAccountId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, budgetAccountMapper.deleteByExample(example));
        }
    }
    /**
     * 获取预算账套列表
     * @param orgId 机构码
     * @param keyword 查询关键词
     * @return 账套列表
     */
    public List<BudgetAccount> getAllBudgetAccountList(String orgId, String keyword) {
        if(StringUtils.isNotBlank(keyword))
        {
            keyword = "%"+keyword+"%";
            return budgetAccountMapper.getAllBudgetAccountList(orgId,keyword);
        }else
        {
            keyword = "%%";
        }
        List<BudgetAccount> list = budgetAccountMapper.getAllBudgetAccountList(orgId,keyword);
        List<BudgetAccount> budgetAccountList = new ArrayList<>();
        for (BudgetAccount account : list) {
            if (account.getParentId().equals("0")) {
                budgetAccountList.add(account);
            }
        }
        for (BudgetAccount budgetAccount : budgetAccountList) {
            budgetAccount.setChildren(getChildBudgetAccountList(budgetAccount.getBudgetAccountId(), list));
        }
        return budgetAccountList;
    }

    /**
     * 获取子账套列表
     * @param budgetAccountId 预算科目Id
     * @param rootBudgetAccount 账套列表列表
     * @return 子账套列表
     */
    public List<BudgetAccount> getChildBudgetAccountList(String budgetAccountId, List<BudgetAccount> rootBudgetAccount) {
        List<BudgetAccount> childList = new ArrayList<>();
        for (BudgetAccount budgetAccount : rootBudgetAccount) {
            if (budgetAccount.getParentId().equals(budgetAccountId)) {
                childList.add(budgetAccount);
            }
        }
        for (BudgetAccount budgetAccount : childList) {
            budgetAccount.setChildren(getChildBudgetAccountList(budgetAccount.getBudgetAccountId(), rootBudgetAccount));
        }
        if (childList.isEmpty()) {
            return Collections.emptyList();
        }
        return childList;
    }

    /**
     * 获取账套列表
     * @param orgId 机构码
     * @return 账套列表
     */
    public List<Map<String, String>> getBudgetAccountForSelect(String orgId) {
        return budgetAccountMapper.getBudgetAccountForSelect(orgId);
    }
    /**
     * 判断预算科目是否存在
     * @param budgetAccountId 预算科目Id
     * @param orgId 机构码
     * @return 预算科目是否存在
     */
    public Boolean isExistChild(String orgId,String budgetAccountId) {
        Example example = new Example(BudgetAccount.class);
        example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("parentId",budgetAccountId);
        return budgetAccountMapper.selectCountByExample(example) > 0;
    }

}
