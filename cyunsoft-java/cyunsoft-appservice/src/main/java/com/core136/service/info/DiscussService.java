package com.core136.service.info;

import com.core136.bean.info.Discuss;
import com.core136.mapper.info.DiscussMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 讨论区板块操作服务类
 * @author lsq
 */
@Service
public class DiscussService {
    private DiscussMapper discussMapper;
	@Autowired
	public void setDiscussMapper(DiscussMapper discussMapper) {
		this.discussMapper = discussMapper;
	}

	/**
     * 创建讨论区
     * @param discuss 讨论区对象
     * @return 成功创建记录数
     */
    public int insertDiscuss(Discuss discuss) {
        return discussMapper.insert(discuss);
    }

    /**
     * 删除讨论区记录
     * @param discuss 讨论区对象
     * @return 成功删除记录数
     */
    public int deleteDiscuss(Discuss discuss) {
        return discussMapper.delete(discuss);
    }

    /**
     * 更新讨论区记录
     * @param example 更新条件
     * @param discuss 讨论区对象
     * @return 成功更新记录数
     */
    public int updateDiscuss(Example example, Discuss discuss) {
        return discussMapper.updateByExampleSelective(discuss, example);
    }

    /**
     * 查询单个讨论区记录
     * @param discuss 讨论区对象
     * @return 讨论区对象
     */
    public Discuss selectOneDiscuss(Discuss discuss) {
        return discussMapper.selectOne(discuss);
    }
    /**
     * 获取版块信息
     * @param orgId 机构码
     * @return 版块信息
     */
    public List<Map<String,String>> getDiscussList(String orgId) {
        return discussMapper.getDiscussList(orgId);
    }

    /**
     * 获取个人讨论区列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param deptId 部门Id
     * @param userLevel 行政级别Id
     * @return 讨论区列表
     */
    public List<Map<String, String>> getMyDiscussList(String orgId, String accountId, String deptId, String userLevel) {
        return discussMapper.getMyDiscussList(orgId, accountId, deptId, userLevel);
    }

}
