package com.core136.service.archives;

import com.core136.bean.archives.ArchivesInspection;
import com.core136.mapper.archives.ArchivesInspectionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class ArchivesInspectionService {
    private ArchivesInspectionMapper archivesInspectionMapper;
	@Autowired
	public void setArchivesInspectionMapper(ArchivesInspectionMapper archivesInspectionMapper) {
		this.archivesInspectionMapper = archivesInspectionMapper;
	}

	public int insertArchivesInspection(ArchivesInspection archivesInspection)
    {
        return archivesInspectionMapper.insert(archivesInspection);
    }

    public int deleteArchivesInspection(ArchivesInspection archivesInspection)
    {
        return archivesInspectionMapper.delete(archivesInspection);
    }

    public int updateArchivesInspection(Example example,ArchivesInspection archivesInspection)
    {
        return archivesInspectionMapper.updateByExampleSelective(archivesInspection,example);
    }

    public ArchivesInspection selectOneArchivesInspection(ArchivesInspection archivesInspection)
    {
        return archivesInspectionMapper.selectOne(archivesInspection);
    }



}
