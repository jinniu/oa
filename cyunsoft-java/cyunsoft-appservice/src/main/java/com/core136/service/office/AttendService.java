package com.core136.service.office;

import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.UserInfo;
import com.core136.bean.office.Attend;
import com.core136.bean.system.AttendConfig;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.ExcelExportData;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.AttendMapper;
import com.core136.service.system.AttendConfigService;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class AttendService {
    private AttendMapper attendMapper;
    private AttendConfigService attendConfigService;
	@Autowired
	public void setAttendMapper(AttendMapper attendMapper) {
		this.attendMapper = attendMapper;
	}
	@Autowired
	public void setAttendConfigService(AttendConfigService attendConfigService) {
		this.attendConfigService = attendConfigService;
	}

	public int insertAttend(Attend attend) {
        return attendMapper.insert(attend);
    }

    /**
     * 打卡时判断是否已打过卡
     * @param attend 考勤记录对象
     * @return 成功创建记录数
     */
    public int addAttendRecord(UserInfo userInfo,Attend attend) {
		attend.setAttendId(SysTools.getGUID());
		attend.setCreateUser(userInfo.getAccountId());
		String nowTimeStr = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		attend.setCreateTime(nowTimeStr);
		attend.setYear(nowTimeStr.substring(0, 4));
		attend.setMonth(nowTimeStr.substring(5, 7));
		attend.setDay(nowTimeStr.substring(8, 10));
		attend.setNowTime(nowTimeStr.split(" ")[0]);
		attend.setTime(nowTimeStr.split(" ")[1]);
		attend.setOrgId(userInfo.getOrgId());
		AttendConfig attendConfig = new AttendConfig();
		attendConfig.setConfigId(attend.getConfigId());
		attendConfig.setOrgId(userInfo.getOrgId());
		attendConfig = attendConfigService.selectOneAttendConfig(attendConfig);
		if(attend.getStatus().equals("1"))
		{
			if((attendConfig.getBeginTime()+":00").compareTo(attend.getTime())<0)
			{
				if(StringUtils.isNotBlank(attend.getRemark()))
				{
					attend.setAttendType("1");
				}else {
					attend.setAttendType("2");
				}
			}else
			{
				attend.setAttendType("0");
			}
		}else if(attend.getStatus().equals("2"))
		{
			if((attendConfig.getEndTime()+":00").compareTo(attend.getTime())>0)
			{
				if(StringUtils.isNotBlank(attend.getRemark()))
				{
					attend.setAttendType("3");
				}else {
					attend.setAttendType("4");
				}

			}else
			{
				attend.setAttendType("0");
			}
		}
        Attend attend1 = new Attend();
        attend1.setStatus(attend.getStatus());
        attend1.setNowTime(attend.getNowTime());
        attend1.setCreateUser(attend.getCreateUser());
        attend1.setOrgId(attend.getOrgId());
        if (selectCountAttend(attend1) == 0) {
            return insertAttend(attend);
        } else {
            Example example = new Example(Attend.class);
            example.createCriteria().andEqualTo("status", attend.getStatus()).andEqualTo("nowTime", attend.getNowTime())
				.andEqualTo("createUser", attend.getCreateUser()).andEqualTo("orgId", attend.getOrgId());
            attend.setAttendId(null);
            return updateAttend(example, attend);
        }
    }

	/**
	 * 获取打卡次数
	 * @param attend 考勤记录对象
	 * @return 打卡次数
	 */
    public int selectCountAttend(Attend attend) {
        return attendMapper.selectCount(attend);
    }

	/**
	 * 删除打卡记录
	 * @param attend 考勤记录对象
	 * @return 成功删除记录数
	 */
    public int deleteAttend(Attend attend) {
        return attendMapper.delete(attend);
    }

	/**
	 * 更新打卡记录
	 * @param example 更新条件
	 * @param attend 考勤记录对象
	 * @return 成功更新记录数
	 */
    public int updateAttend(Example example, Attend attend) {
        return attendMapper.updateByExampleSelective(attend, example);
    }

	/**
	 * 查询单条打卡记录
	 * @param attend 考勤记录对象
	 * @return 成功更新记录数
	 */
    public Attend selectOneAttend(Attend attend) {
        return attendMapper.selectOne(attend);
    }

	/**
	 * 获取个人打卡年份
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 年份列表
	 */
    public List<Map<String,String>>getMyAttendYear(String orgId,String accountId)
    {
        return attendMapper.getMyAttendYear(orgId,accountId);
    }

	/**
	 * 获取年份列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 年份列表
	 */
    public List<Map<String, String>> getAttendYearList(String orgId, String accountId) {
        return attendMapper.getAttendYearList(orgId, accountId);
    }

	/**
	 *  获取月份
	 * @param orgId 机构码
	 * @param year 年份
	 * @param accountId 用户账号
	 * @return 月份列表
	 */
    public List<Map<String, String>> getMonthList(String orgId, String year, String accountId) {
        return attendMapper.getMonthList(orgId, year, accountId);
    }

	/**
	 * 获取个人考勤
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param attendType 类型
	 * @param status 状态
	 * @param year 年份
	 * @param month 月份
	 * @return 个人考勤列表
	 */
    public List<Map<String, String>> getMyAttendList(String orgId, String accountId, String attendType, String status, String year,String month) {
        return attendMapper.getMyAttendList(orgId, accountId, attendType, status, year,month);
    }

	/**
	 * 获取个人考勤
	 * @param pageParam 分页参数
	 * @param attendType 类型
	 * @param status 状态
	 * @param year 年份
	 * @param month 月份
	 * @return 个人考勤列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyAttendList(PageParam pageParam, String attendType, String status, String year, String month) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyAttendList(pageParam.getOrgId(),pageParam.getAccountId(), attendType, status, year, month);
		return new PageInfo<>(datalist);
	}

	/**
	 * 考勤统计查询
	 * @param orgId 机构码
	 * @param attendType 统计类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param deptId 部门Id
	 * @param createUser 考勤人员账号
	 * @return 考勤列表
	 */
    public List<Map<String, String>> getTotalAttendList(String orgId, String attendType, String beginTime, String endTime, String deptId, String createUser) {
        return attendMapper.getTotalAttendList(orgId, attendType, beginTime, endTime, deptId, createUser);
    }

	/**
	 * 考勤统计查询
	 * @param pageParam 分页参数
	 * @param attendType 类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param deptId 部门Id
	 * @param createUser 考勤人员账号
	 * @return 考勤列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getTotalAttendList(PageParam pageParam, String attendType, String beginTime, String endTime, String deptId, String createUser) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getTotalAttendList(pageParam.getOrgId(), attendType, beginTime, endTime, deptId, createUser);
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取人员请假列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param attendType 类型
	 * @return 请假列表
	 */
    public List<Map<String, String>> getMyLeaveList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime, String attendType) {
        return attendMapper.getMyLeaveList(orgId, accountId, dateQueryType,beginTime, endTime, attendType);
    }

	/**
	 * 获取人员请假列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param attendType 类型
	 * @return 请假列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyLeaveList(PageParam pageParam, String dateQueryType,String beginTime, String endTime, String attendType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyLeaveList(pageParam.getOrgId(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, attendType);
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取出差列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 出差列表
	 */
    public List<Map<String, String>> getMyTravelList(String orgId, String accountId, String dateQueryType,String beginTime, String endTime) {
        return attendMapper.getMyTravelList(orgId, accountId, dateQueryType,beginTime, endTime);
    }

	/**
	 * 获取出差列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 出差列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyTravelList(PageParam pageParam, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyTravelList(pageParam.getOrgId(), pageParam.getAccountId(), dateQueryType,beginTime, endTime);
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取外出列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 外出列表
	 */
    public List<Map<String, String>> getMyOutAttendList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime) {
        return attendMapper.getMyOutAttendList(orgId, accountId, dateQueryType,beginTime, endTime);
    }

	/**
	 * 获取外出列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 外出列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyOutAttendList(PageParam pageParam,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyOutAttendList(pageParam.getOrgId(), pageParam.getAccountId(), dateQueryType,beginTime, endTime);
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取加班列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 加班列表
	 */
    public List<Map<String, String>> getMyOverTimeList(String orgId, String accountId, String dateQueryType,String beginTime, String endTime) {
        return attendMapper.getMyOverTimeList(orgId, accountId,dateQueryType, beginTime, endTime);
    }

	/**
	 * 获取加班列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 加班列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyOverTimeList(PageParam pageParam, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyOverTimeList(pageParam.getOrgId(), pageParam.getAccountId(), dateQueryType,beginTime, endTime);
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取值班列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 值班列表
	 */
    public List<Map<String, String>> getMyDutyList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime) {
        return attendMapper.getMyDutyList(orgId, accountId, dateQueryType,beginTime, endTime);
    }

	/**
	 * 获取值班列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 值班列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyDutyList(PageParam pageParam, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyDutyList(pageParam.getOrgId(), pageParam.getAccountId(),dateQueryType, beginTime, endTime);
		return new PageInfo<>(datalist);
    }


	/**
	 * 判断个人上班考勤是否打卡
	 * @param user 用户对象
	 * @return 是否打卡
	 */
	public JSONObject getAttendStatusDay(UserInfo user) {
        Attend attend = new Attend();
        attend.setStatus("1");
        attend.setNowTime(SysTools.getTime("yyyy-MM-dd"));
        attend.setCreateUser(user.getAccountId());
        attend.setOrgId(user.getOrgId());
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status",attendMapper.selectCount(attend));
		return jsonObject;
    }

	/**
	 * 判断个人下班考勤是否打卡
	 * @param user 用户对象
	 * @return 是否打卡
	 */
	public JSONObject getOffWorkAttendStatusDay(UserInfo user) {
        Attend attend = new Attend();
        attend.setStatus("2");
        attend.setNowTime(SysTools.getTime("yyyy-MM-dd"));
        attend.setCreateUser(user.getAccountId());
        attend.setOrgId(user.getOrgId());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",attendMapper.selectCount(attend));
        return jsonObject;
    }

	/**
	 * 导出指定月份考勤详情
	 * @param orgId 机构码
	 * @param year 年份
	 * @param month 月份
	 * @param attendType 类型
	 * @param createUser 打卡人
	 * @param deptId 部门Id
	 * @param configId 考勤规则Id
	 * @return 考勤详情列表
	 */
	public List<Map<String,Object>> getExpAttendData(String orgId,String year, String month,String attendType,String createUser, String deptId,String configId)
	{
		return attendMapper.getTotalAttendListForExp(orgId,year,month,attendType,createUser,deptId,configId);
	}

	/**
	 * 获取考勤记录列表
	 * @param orgId 机构码
	 * @param year 年份
	 * @param month 月份
	 * @param attendType 类型
	 * @param createUser 打卡人
	 * @param deptId 部门Id
	 * @param configId 考勤规则Id
	 * @return 考勤记录列表
	 */
	public List<Map<String,String>> getTotalAttendListForUserExp2(String orgId,  String year, String month,String attendType,String createUser, String deptId,String configId)
	{
		return attendMapper.getTotalAttendListForUserExp2(orgId,year,month,attendType,createUser,deptId,configId);
	}

	/**
	 * 获取考勤记录列表
	 * @param orgId 机构码
	 * @param year 年份
	 * @param month 月份
	 * @param attendType 类型
	 * @param createUser 打卡人
	 * @param deptId 部门Id
	 * @param configId 考勤规则Id
	 * @return 勤记录列表
	 */
	public List<Map<String,String>> getTotalAttendListForExp2(String orgId, String year, String month,String attendType,String createUser, String deptId,String configId)
	{
		return attendMapper.getTotalAttendListForExp2(orgId,year,month,attendType,createUser,deptId,configId);
	}

	/**
	 * 获取移动端考勤记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param time 时间
	 * @param page 页码
	 * @return 考勤记录列表
	 */
	public List<Map<String,String>>getMyAttendListForApp(String orgId,String accountId,String time,Integer page)
	{
		String year = time.split("-")[0];
		String month = time.split("-")[1];
		return attendMapper.getMyAttendListForApp(orgId,accountId,year,month,page);
	}

	/**
	 * 导出考勤详情
	 * @param response HttpServletResponse
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param attendType 类型
	 * @param createUser 打卡人
	 * @param deptId 部门Id
	 */
	public void getExpAttendRecord(HttpServletResponse response, String orgId, String beginTime,String attendType,String createUser, String deptId,String configId)
	{
		String year = beginTime.split("-")[0];
		String month = beginTime.split("-")[1];
		List<Map<String, Object>> lists = getExpAttendData(orgId,year,month,attendType,createUser,deptId,configId);
		ExcelExportData excelExportData = new ExcelExportData();
		List<String> headers = new ArrayList<>();
		headers.add("考勤规则");
		headers.add("姓名");
		headers.add("考勤状态");
		headers.add("考勤类型");
		headers.add("年份");
		headers.add("月份");
		headers.add("打卡时间");
		headers.add("纬度");
		headers.add("经度");
		headers.add("打卡地址");
		headers.add("备注");
		try {
			excelExportData.export(response, SysTools.getTime("yyyy-MM")+"_考勤.xlsx", "考勤记录", headers, lists);
		} catch (IOException e) {
		}
	}

	/**
	 * 导出打卡汇总表
	 * @param response HttpServletResponse
	 * @param orgId orgId
	 * @param beginTime 开始时间
	 * @param attendType 类型
	 * @param deptId 部门Id
	 * @param createUser 打卡人
	 * @param configId 考勤规则Id
	 */
	public void getExpAttendRecord2(HttpServletResponse response, String orgId,String beginTime, String attendType,  String deptId, String createUser,String configId)
	{
		AttendConfig attendConfig = new AttendConfig();
		attendConfig.setConfigId(configId);
		attendConfig.setOrgId(orgId);
		attendConfig = attendConfigService.selectOneAttendConfig(attendConfig);
		String year = beginTime.split("-")[0];
		String month = beginTime.split("-")[1];
		List<Map<String, String>> userList = getTotalAttendListForUserExp2(orgId,year,month,attendType,createUser,deptId,configId);
		List<Map<String, String>> lists = getTotalAttendListForExp2(orgId,year,month,deptId,attendType,createUser,configId);
		List<Map<String,Object>> tableData = new ArrayList<>();
		for (Map<String, String> userMap : userList) {
			Map<String, Object> uMap = new HashMap<>();
			uMap.put("部门", userMap.get("deptName"));
			uMap.put("姓名", userMap.get("userName"));
			uMap.put("月份", beginTime);
			int zd = 0;
			int zt = 0;
			int qj = 0;
			int cc = 0;
			Map<String, String> tempMap = new HashMap<>();
			for (Map<String, String> map : lists) {
				if (map.get("userName").equals(userMap.get("userName"))) {
					String aStatus = "";
					if (uMap.get(map.get("day")) != null) {
						aStatus = uMap.get(map.get("day")).toString();
					}
					String remark = "";
					if (StringUtils.isNotBlank(map.get("remark"))) {
						remark = map.get("remark");
					}
					String crateTime = map.get("createTime").split(" ")[1].substring(0, 5) + remark;
					if (map.get("status").equals("1")) {
						if (StringUtils.isBlank(aStatus)) {
							aStatus = map.get("attendType").equals("0") ? "上" : "迟 " + crateTime;
						} else {
							aStatus += "," + (map.get("attendType").equals("0") ? "上" : "迟 " + crateTime);
						}
						if (map.get("attendType").equals("1") || map.get("attendType").equals("2")) {
							zd = zd + 1;
						}
						tempMap.put(map.get("day"), aStatus);
					} else if (map.get("status").equals("2")) {
						if (StringUtils.isBlank(aStatus)) {
							aStatus = map.get("attendType").equals("0") ? "下" : "退 " + crateTime;
						} else {
							aStatus += "," + (map.get("attendType").equals("0") ? "下" : "退 " + crateTime);
						}
						if (map.get("attendType").equals("3") || map.get("attendType").equals("4")) {
							zt = zt + 1;
						}
						tempMap.put(map.get("day"), aStatus);
					} else if (map.get("status").equals("3")) {
						if (StringUtils.isBlank(aStatus)) {
							aStatus = map.get("remark");
						}
						qj = qj + 1;
						tempMap.put(map.get("day"), aStatus);
					} else if (map.get("status").equals("4")) {
						if (StringUtils.isBlank(aStatus)) {
							aStatus = map.get("remark");
						}
						cc = cc + 1;
						tempMap.put(map.get("day"), aStatus);
					}
				}
				uMap.putAll(tempMap);
			}
			uMap.put("迟到", zd);
			uMap.put("早退", zt);
			uMap.put("请假", qj);
			uMap.put("出差", cc);
			uMap.put("方案", attendConfig.getTitle());
			tableData.add(uMap);
		}
		ExcelExportData excelExportData = new ExcelExportData();
		List<String> headers = new ArrayList<>();
		headers.add("部门");
		headers.add("姓名");
		headers.add("方案");
		headers.add("月份");
		headers.add("迟到");
		headers.add("早退");
		headers.add("缺勤");
		headers.add("请假");
		headers.add("出差");
		int dayCount = SysTools.getDaysOfMonth(beginTime);
		for(int i=1;i<=dayCount;i++) {
			if(i<10)
			{
				headers.add("0"+i);
			}else
			{
				headers.add(String.valueOf(i));
			}
		}
		for(Map<String,Object> map:tableData)
		{
			int qq=0;
			String key;
			for(int i=1;i<=dayCount;i++) {
				if(i<10)
				{
					key= "0"+i;
				}else
				{
					key=String.valueOf(i);
				}
				if(map.get(key)==null)
				{
					map.put(key,"缺");
					qq=qq+1;
				}
			}
			map.put("缺勤",qq);
		}
		try {
			excelExportData.export(response, SysTools.getTime("yyyy-MM")+"_"+attendConfig.getTitle()+"_考勤.xlsx", attendConfig.getTitle()+"考勤记录", headers, tableData);
		} catch (IOException e) {
		}
	}
}
