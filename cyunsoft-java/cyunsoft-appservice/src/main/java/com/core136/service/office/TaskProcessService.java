package com.core136.service.office;

import com.core136.bean.office.TaskProcess;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.TaskProcessMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class TaskProcessService {
    private TaskProcessMapper taskProcessMapper;
	@Autowired
	public void setTaskProcessMapper(TaskProcessMapper taskProcessMapper) {
		this.taskProcessMapper = taskProcessMapper;
	}

	public int insertTaskProcess(TaskProcess taskProcess) {
        return taskProcessMapper.insert(taskProcess);
    }

    public int deleteTaskProcess(TaskProcess taskProcess) {
        return taskProcessMapper.delete(taskProcess);
    }

    public int updateTaskProcess(Example example, TaskProcess taskProcess) {
        return taskProcessMapper.updateByExampleSelective(taskProcess, example);
    }

    public TaskProcess selectOneTaskProcess(TaskProcess taskProcess) {
        return taskProcessMapper.selectOne(taskProcess);
    }

    /**
     * 获取子任务的处理过程列表
     * @param orgId 机构码
     * @param accountId
     * @param createUser
     * @param taskType
     * @param dateQueryType
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param keyword
     * @return
     */
    public List<Map<String, String>> getMyTaskProcessList(String orgId, String accountId, String createUser, String taskType, String dateQueryType,String beginTime, String endTime, String keyword) {
        return taskProcessMapper.getMyTaskProcessList(orgId, accountId, createUser, taskType, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取子任务的处理过程列表
     * @param pageParam 分页参数
     * @param createUser
     * @param taskType
     * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getMyTaskProcessList(PageParam pageParam, String createUser, String taskType,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyTaskProcessList(pageParam.getOrgId(), pageParam.getAccountId(), createUser, taskType,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取处理事件详情
	 * @param orgId 机构码
	 * @param taskId
	 * @param accountId
	 * @return
	 */
    public List<Map<String, String>> getProcessInfo(String orgId, String taskId, String accountId) {
        return taskProcessMapper.getProcessInfo(orgId, taskId, accountId);
    }
}
