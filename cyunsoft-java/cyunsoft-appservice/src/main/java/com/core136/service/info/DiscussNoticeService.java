package com.core136.service.info;


import com.core136.bean.account.UserInfo;
import com.core136.bean.info.DiscussNotice;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.DiscussNoticeMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 讨论区通知公告服务类
 * @author lsq
 */
@Service
public class DiscussNoticeService {
    private DiscussNoticeMapper discussNoticeMapper;
	@Autowired
	public void setDiscussNoticeMapper(DiscussNoticeMapper discussNoticeMapper) {
		this.discussNoticeMapper = discussNoticeMapper;
	}

	/**
	 * 创建讨论区通知公告
	 * @param discussNotice 讨论区通知公告对象
	 * @return 成功创建记录数
	 */
    public int insertDiscussNotice(DiscussNotice discussNotice) {
        return discussNoticeMapper.insert(discussNotice);
    }

	/**
	 * 删除讨论区通知公告
	 * @param discussNotice 讨论区通知公告对象
	 * @return 成功删除记录数
	 */
    public int deleteDiscussNotice(DiscussNotice discussNotice) {
        return discussNoticeMapper.delete(discussNotice);
    }

	/**
	 * 更新讨论区通知公告
	 * @param example 更新条件
	 * @param discussNotice 讨论区通知公告对象
	 * @return 成功更新记录数
	 */
    public int updateDiscussNotice(Example example, DiscussNotice discussNotice) {
        return discussNoticeMapper.updateByExampleSelective(discussNotice, example);
    }

	/**
	 * 查询单一讨论区通知公告
	 * @param discussNotice 讨论区通知公告对象
	 * @return 讨论区通知公告对象
	 */
    public DiscussNotice selectOneDiscussNotice(DiscussNotice discussNotice) {
        return discussNoticeMapper.selectOne(discussNotice);
    }
	/**
	 * 获取讨论区通知列表
	 * @param userInfo 用户对象
	 * @return 通知列表
	 */
    public List<Map<String, String>> getDiscussNoticeList(UserInfo userInfo) {
        return discussNoticeMapper.getDiscussNoticeList(userInfo.getOrgId(), userInfo.getAccountId());
    }

	/**
	 * 批量删除讨论区通知公告
	 * @param orgId 机构码
	 * @param list 通知公告Id 列表
	 * @return 消息结构
	 */
    public RetDataBean deleteDiscussNoticeIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(DiscussNotice.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, discussNoticeMapper.deleteByExample(example));
		}
	}

	/**
	 *获取讨论区通知管理列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @return 通知列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>>getDiscussNoticeForManage(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String status) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getDiscussNoticeForManage(pageParam.getOrgId(), dateQueryType,beginTime,endTime,status,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


	/**
	 * 获取讨论区通知管理列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 通知列表
	 */
	public List<Map<String, String>> getDiscussNoticeForManage(String orgId,String dateQueryType, String beginTime,String endTime,String status,String keyword) {
		return discussNoticeMapper.getDiscussNoticeForManage(orgId,dateQueryType,beginTime,endTime,status,"%"+keyword+"%");
	}


}
