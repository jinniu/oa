package com.core136.service.info;

import com.core136.bean.account.UserInfo;
import com.core136.bean.info.LeadActivity;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.LeadActivityMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class LeadActivityService {
    private LeadActivityMapper leadActivityMapper;
	private UserInfoService userInfoService;
	@Autowired
	public void setLeadActivityMapper(LeadActivityMapper leadActivityMapper) {
		this.leadActivityMapper = leadActivityMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertLeadActivity(LeadActivity leadActivity) {
        return leadActivityMapper.insert(leadActivity);
    }

	/**
	 * 发布领导行程
	 * @param user 用户对象
	 * @param leadActivity 领导行程对象
	 * @return 消息结构
	 */
    public RetDataBean sendLeadActivity(UserInfo user, LeadActivity leadActivity) {
		insertLeadActivity(leadActivity);
		if (StringUtils.isNotBlank(leadActivity.getMsgType())) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(user.getOrgId(), leadActivity.getUserRole(), leadActivity.getDeptRole(), leadActivity.getLevelRole());
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(leadActivity.getTitle());
				msgBody.setContent(leadActivity.getRemark());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(userInfo);
				msgBody.setView(GlobalConstant.MSG_TYPE_LEAD_ACTIVITY);
				msgBody.setRecordId(leadActivity.getRecordId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(leadActivity.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS);
	}

	/**
	 * 更新领导日程
	 * @param user 用户对象
	 * @param example 更新条件
	 * @param leadActivity 领导日程对象
	 * @return 成功更新记录数
	 */
	public int updateLeadActivityAndSendMessage(UserInfo user,Example example, LeadActivity leadActivity) {
		if (StringUtils.isNotBlank(leadActivity.getMsgType())) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(user.getOrgId(), leadActivity.getUserRole(), leadActivity.getDeptRole(), leadActivity.getLevelRole());
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(leadActivity.getTitle());
				msgBody.setContent(leadActivity.getRemark());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(userInfo);
				msgBody.setView(GlobalConstant.MSG_TYPE_LEAD_ACTIVITY);
				msgBody.setRecordId(leadActivity.getRecordId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(leadActivity.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return leadActivityMapper.updateByExampleSelective(leadActivity, example);
	}


    public int deleteLeadActivity(LeadActivity leadActivity) {
        return leadActivityMapper.delete(leadActivity);
    }

	public RetDataBean deleteLeadActivityByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(LeadActivity.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, leadActivityMapper.deleteByExample(example));
		}
	}

    public int updateLeadActivity(Example example, LeadActivity leadActivity) {
        return leadActivityMapper.updateByExampleSelective(leadActivity, example);
    }

    public LeadActivity selectOneLeadActivity(LeadActivity leadActivity) {
        return leadActivityMapper.selectOne(leadActivity);
    }

	/**
	 * 获取领导行程列表
	 * @param orgId 机构码
	 * @param ledActType 行程类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param leader 领导账号
	 * @param keyword 查询关键词
	 * @return 领导行程列表
	 */
    public List<Map<String, String>> getLeadActivityList(String orgId, String ledActType, String dateQueryType,String beginTime, String endTime, String leader,String status, String keyword) {
        return leadActivityMapper.getLeadActivityList(orgId,ledActType,dateQueryType,beginTime, endTime, leader, status,"%" + keyword + "%");
    }

	/**
	 * 获取领导行程列表
	 * @param pageParam 分页参数
	 * @param ledActType 行程类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param leader 领导账号
	 * @return 领导行程列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getLeadActivityList(PageParam pageParam, String ledActType, String dateQueryType,String beginTime, String endTime, String leader,String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeadActivityList(pageParam.getOrgId(),ledActType, dateQueryType,beginTime, endTime, leader,status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    public List<Map<String, String>> getLeadActivityQueryList(String orgId, String ledActType, String dateQueryType,String beginTime, String endTime, String leader, String accountId, String deptId, String levelId, String keyword) {
        return leadActivityMapper.getLeadActivityQueryList(orgId,ledActType,dateQueryType, beginTime, endTime, leader, accountId, deptId, levelId, "%" + keyword + "%");
    }

    public PageInfo<Map<String, String>> getLeadActivityQueryList(PageParam pageParam,String ledActType,String dateQueryType, String beginTime, String endTime, String leader) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeadActivityQueryList(pageParam.getOrgId(),ledActType,dateQueryType, beginTime, endTime, leader, pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    public List<Map<String, String>> getLeadActivityListForDesk(String orgId, String accountId, String deptId, String levelId) {
        String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        return leadActivityMapper.getLeadActivityListForDesk(orgId, nowTime, accountId, deptId, levelId);
    }

    public List<Map<String, String>> getMyLeadActivityForApp(String orgId, String accountId, String deptId, String levelId, Integer page) {
        String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        return leadActivityMapper.getMyLeadActivityForApp(orgId, nowTime, accountId, deptId, levelId, page);
    }

	/**
	 * 获取领导行程详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别Id
	 * @param recordId 领导行程记录Id
	 * @return 领导行程详情
	 */
	public Map<String, String> getLeadActivityById(String orgId, String opFlag,String accountId, String deptId,String userLevel,String recordId) {
		return leadActivityMapper.getLeadActivityById(orgId, opFlag, accountId, deptId,userLevel,recordId);
	}
}
