package com.core136.service.office;

import com.core136.bean.account.UserInfo;
import com.core136.bean.office.MeetingRoom;
import com.core136.mapper.office.MeetingRoomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class MeetingRoomService {
    private MeetingRoomMapper meetingRoomMapper;
	@Autowired
	public void setMeetingRoomMapper(MeetingRoomMapper meetingRoomMapper) {
		this.meetingRoomMapper = meetingRoomMapper;
	}

	public int insertMeetingRoom(MeetingRoom meetingRoom) {
        return meetingRoomMapper.insert(meetingRoom);
    }

    public int deleteMeetingRoom(MeetingRoom meetingRoom) {
        return meetingRoomMapper.delete(meetingRoom);
    }

    public int updateMeetingRoom(Example example, MeetingRoom meetingRoom) {
        return meetingRoomMapper.updateByExampleSelective(meetingRoom, example);
    }

    public MeetingRoom selectOneMeetingRoom(MeetingRoom meetingRoom) {
        return meetingRoomMapper.selectOne(meetingRoom);
    }

    /**
     * 获取会议室列表
     * @param orgId 机构码
     * @return 会议室列表
     */
    public List<Map<String, String>>  getMeetingRoomList(String orgId) {
        return meetingRoomMapper.getMeetingRoomList(orgId);
    }

	/**
	 * 获取我历史申请过的会议室列表
	 * @param user 用户对象
	 * @return 会议室列表
	 */
	public List<Map<String, String>>  getMyApplyMeetingRoomList(UserInfo user) {
		return meetingRoomMapper.getMyApplyMeetingRoomList(user.getOrgId(),user.getDeptId(),user.getAccountId());
	}



    /**
     * 获取当前用户可用的会议室
     * @param orgId 机构码
     * @param deptId 部门Id
     * @return 户可用的会议室列表
     */
    public List<Map<String, Object>> getCanUseMeetingRoomList(String orgId, String deptId) {
        return meetingRoomMapper.getCanUseMeetingRoomList(orgId, deptId);
    }
}
