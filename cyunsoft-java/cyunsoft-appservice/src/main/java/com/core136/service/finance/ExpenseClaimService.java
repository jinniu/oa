package com.core136.service.finance;

import com.core136.bean.finance.ExpenseClaim;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ExpenseClaimMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExpenseClaimService {
	private ExpenseClaimMapper expenseClaimMapper;
	@Autowired
	public void setExpenseClaimMapper(ExpenseClaimMapper expenseClaimMapper) {
		this.expenseClaimMapper = expenseClaimMapper;
	}

	public int insertExpenseClaim(ExpenseClaim expenseClaim)
	{
		return expenseClaimMapper.insert(expenseClaim);
	}

	public int deleteExpenseClaim(ExpenseClaim expenseClaim)
	{
		return expenseClaimMapper.delete(expenseClaim);
	}

	public int updateExpenseClaim(Example example, ExpenseClaim expenseClaim)
	{
		return expenseClaimMapper.updateByExampleSelective(expenseClaim,example);
	}

	public ExpenseClaim selectOneExpenseClaim(ExpenseClaim expenseClaim)
	{
		return expenseClaimMapper.selectOne(expenseClaim);
	}

	/**
	 * 获取人员报销列表
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param accountId 用户账号
	 * @param expenseYear 查询年度
	 * @param expenseType 费用类型
	 * @return 消息结构
	 */
	public List<Map<String,String>>getExpenseClaimList(String orgId,String deptId,String accountId,String expenseType,String expenseYear)
	{
		String beginTime="";
		String endTime="";
		if(StringUtils.isNotBlank(expenseYear))
		{
			beginTime= expenseYear+"-01-01 00:00:00";
			endTime= expenseYear+"-12-31 23:59:59";
		}
		return expenseClaimMapper.getExpenseClaimList(orgId,deptId,accountId,expenseType,beginTime,endTime);
	}

	/**
	 * 获取人员报销列表
	 * @param pageParam 分页参数
	 * @param deptId 部门Id
	 * @param accountId 用户账号
	 * @param expenseYear 查询年度
	 * @param expenseType 费用类型
	 * @return 消息结构
	 */
	public PageInfo<Map<String, String>> getExpenseClaimList(PageParam pageParam, String deptId,String accountId,String expenseType, String expenseYear){
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getExpenseClaimList(pageParam.getOrgId(),deptId,accountId,expenseType,expenseYear);
		return new PageInfo<>(datalist);
	}

}
