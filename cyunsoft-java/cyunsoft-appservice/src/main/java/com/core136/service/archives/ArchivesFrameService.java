package com.core136.service.archives;

import com.core136.bean.archives.ArchivesFrame;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesFrameMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 档案架操作服务类
 * @author lsq
 */
@Service
public class ArchivesFrameService {
	private ArchivesFrameMapper archivesFrameMapper;
	private ArchivesRecordService archivesRecordService;
	@Autowired
	public void setArchivesFrameMapper(ArchivesFrameMapper archivesFrameMapper) {
		this.archivesFrameMapper = archivesFrameMapper;
	}
	@Autowired
	public void setArchivesRecordService(ArchivesRecordService archivesRecordService) {
		this.archivesRecordService = archivesRecordService;
	}

	/**
	 * 创建档案架
	 * @param archivesFrame 档案架对象
	 * @return 成功创建记录数
	 */
	public int insertArchivesFrame(ArchivesFrame archivesFrame) {
		return archivesFrameMapper.insert(archivesFrame);
	}

	/**
	 * 删除档案架
	 * @param archivesFrame 档案架对象
	 * @return 成功删除记录数
	 */
	public int deleteArchivesFrame(ArchivesFrame archivesFrame) {
		return archivesFrameMapper.delete(archivesFrame);
	}

	/**
	 * 批量删除档案号
	 * @param orgId 机构码
	 * @param list 档案架Id 列表
	 * @return 成功删除记录数
	 */
	public RetDataBean deleteArchivesFrameByIds(String orgId, List<String> list) {
		List<String> frameList = new ArrayList<>();
		for(String frameId:list) {
			if(archivesRecordService.getCountByFrameId(orgId,frameId)<1) {
				frameList.add(frameId);
			}
		}
		if (frameList.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesFrame.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("frameId", frameList);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesFrameMapper.deleteByExample(example));
		}
	}

	/**
	 * 更新档案架号
	 * @param example 更新条件
	 * @param archivesFrame 档案架号对象
	 * @return 成功更新记录数
	 */
	public int updateArchivesFrame(Example example,ArchivesFrame archivesFrame) {
		return archivesFrameMapper.updateByExampleSelective(archivesFrame,example);
	}

	/**
	 * 查询单个档案架号
	 * @param archivesFrame 档案架号对象
	 * @return 档案架号对象
	 */
	public ArchivesFrame selectOneArchivesFrame(ArchivesFrame archivesFrame) {
		return archivesFrameMapper.selectOne(archivesFrame);
	}

	/**
	 * 获取档案架号列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 档案架号列表
	 */
	public List<Map<String,String>>getArchivesFrameListForManage(String orgId,String keyword) {
		return archivesFrameMapper.getArchivesFrameListForManage(orgId,"%" + keyword + "%");
	}

	/**
	 * 获取档案架号列表
	 * @param pageParam 分页参数
	 * @return 档案架号列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getArchivesFrameListForManage(PageParam pageParam) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesFrameListForManage(pageParam.getOrgId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取档案架号列表
	 * @param orgId 机构码
	 * @return 档案架号列表
	 */
	public List<Map<String,String>> getArchivesFrameListForSelect(String orgId,String keyword) {
		if(StringUtils.isBlank(keyword)) {
			keyword = "";
		}
		return archivesFrameMapper.getArchivesFrameListForSelect(orgId,"%"+keyword+"%");
	}

	/**
	 * 获取档案仓库下的档案架数
	 * @param orgId 机构码
	 * @param roomId 档案室Id
	 * @return 档案架数
	 */
	public int getCountByRoomId(String orgId,String roomId) {
		ArchivesFrame archivesFrame = new ArchivesFrame();
		archivesFrame.setRoomId(roomId);
		archivesFrame.setOrgId(orgId);
		return archivesFrameMapper.selectCount(archivesFrame);
	}

}
