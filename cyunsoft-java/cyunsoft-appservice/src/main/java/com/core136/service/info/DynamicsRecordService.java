package com.core136.service.info;

import com.core136.bean.info.DynamicsRecord;
import com.core136.mapper.info.DynamicsRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DynamicsRecordService {
	private DynamicsRecordMapper dynamicsRecordMapper;
	@Autowired
	public void setDynamicsRecordMapper(DynamicsRecordMapper dynamicsRecordMapper) {
		this.dynamicsRecordMapper = dynamicsRecordMapper;
	}

	public int insertDynamicsRecord(DynamicsRecord dynamicsRecord){
		return dynamicsRecordMapper.insert(dynamicsRecord);
	}

	public int deleteDynamicsRecord(DynamicsRecord dynamicsRecord)
	{
		return dynamicsRecordMapper.delete(dynamicsRecord);
	}

	public int updateDynamicsRecord(Example example,DynamicsRecord dynamicsRecord)
	{
		return dynamicsRecordMapper.updateByExampleSelective(dynamicsRecord,example);
	}

	public DynamicsRecord selectOneDynamicsRecord(DynamicsRecord dynamicsRecord)
	{
		return dynamicsRecordMapper.selectOne(dynamicsRecord);
	}

	/**
	 * 获取动态列表
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param page 当前页码
	 * @param keyword 查询关键词
	 * @return 动态列表
	 */
	public List<Map<String,String>> getDynamicsRecordList(String orgId,String beginTime,String endTime,Integer page,String keyword)
	{
		return dynamicsRecordMapper.getDynamicsRecordList(orgId,beginTime,endTime,page,"%"+keyword+"%");
	}

	public List<Map<String,String>> getDynamicsRecordForDesk(String orgId)
	{
		return dynamicsRecordMapper.getDynamicsRecordForDesk(orgId);
	}


}
