package com.core136.service.archives;

import com.alibaba.fastjson.JSONObject;
import com.core136.bean.archives.ArchivesEnter;
import com.core136.bean.archives.ArchivesRecord;
import com.core136.bean.system.PageParam;
import com.core136.bean.system.SysConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.lucene.LuceneUtils;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesEnterMapper;
import com.core136.mapper.archives.ArchivesRecordMapper;
import com.core136.service.system.SysConfigService;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class ArchivesRecordService {
	@Value("${app.attachpath}")
	private String attachPath;
	private SysConfigService sysConfigService;
    private ArchivesEnterMapper  archivesEnterMapper;
    private ArchivesRecordMapper archivesRecordMapper;
	@Autowired
	public void setSysConfigService(SysConfigService sysConfigService) {
		this.sysConfigService = sysConfigService;
	}
	@Autowired
	public void setArchivesEnterMapper(ArchivesEnterMapper archivesEnterMapper) {
		this.archivesEnterMapper = archivesEnterMapper;
	}
	@Autowired
	public void setArchivesRecordMapper(ArchivesRecordMapper archivesRecordMapper) {
		this.archivesRecordMapper = archivesRecordMapper;
	}

	public int insertArchivesRecord(ArchivesRecord archivesRecord)
    {
        return archivesRecordMapper.insert(archivesRecord);
    }

    public int deleteArchivesRecord(ArchivesRecord archivesRecord)
    {
        return archivesRecordMapper.delete(archivesRecord);
    }


    public int updateArchivesRecord(Example example,ArchivesRecord archivesRecord)
    {
        return archivesRecordMapper.updateByExampleSelective(archivesRecord,example);
    }

    public ArchivesRecord selectOneArchivesRecord(ArchivesRecord archivesRecord)
    {
        return archivesRecordMapper.selectOne(archivesRecord);
    }

    /**
     * 批量删除档案记录
     * @param orgId
     * @param list
     * @return
     */
    public RetDataBean deleteArchivesRecordByIds(String orgId, List<String> list) {
        if (list.isEmpty()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            Example example = new Example(ArchivesRecord.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesRecordMapper.deleteByExample(example));
        }
    }


	/**
	 * 按分类获取档案列表
	 * @param orgId
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param classifiedLevel
	 * @param sortId
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>>getArchivesRecordListBySort(String orgId,String dateQueryType,String beginTime,String endTime,String classifiedLevel,String sortId,String keyword) {
		return archivesRecordMapper.getArchivesRecordListBySort(orgId,dateQueryType,beginTime,endTime,classifiedLevel,sortId,"%"+keyword+"%");
	}

	/**
	 * 按分类获取档案列表
	 * @param pageParam 分页参数
	 * @param date
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param classifiedLevel
	 * @param sortId
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getArchivesRecordListBySort(PageParam pageParam, String date, String beginTime, String endTime,String classifiedLevel,String sortId) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesRecordListBySort(pageParam.getOrgId(),date,beginTime,endTime, classifiedLevel,sortId,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
     * 获取档案下拉列表
     * @param orgId
     * @param keyword
     * @return
     */
    public List<Map<String,String>> getArchivesRecordListForSelect(String orgId, String keyword)
    {
        return archivesRecordMapper.getArchivesRecordListForSelect(orgId,"%"+keyword+"%");
    }


    /**
     * 档案鉴定
     * @param archivesRecord
     * @param enterId
     * @return
     */
    @Transactional(value = "generalTM")
    public RetDataBean setArchivesIdentify(ArchivesRecord archivesRecord,String enterId)
    {
        Example example = new Example(ArchivesEnter.class);
        example.createCriteria().andEqualTo("orgId",archivesRecord.getOrgId()).andEqualTo("recordId",enterId);
        ArchivesEnter archivesEnter = new ArchivesEnter();
        archivesEnter.setStatus("1");
        archivesEnterMapper.updateByExampleSelective(archivesEnter,example);
        if(archivesRecord.getStatus().equals("0"))
        {
           return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS,insertArchivesRecord(archivesRecord));
        }else {
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        }
    }

	/**
	 * 获取待归档档案列表
	 * @param orgId
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param isKind
	 * @param classifiedLevel
	 * @param keyword
	 * @return
	 */
    public List<Map<String,String>>getArchivesRecordListForFile(String orgId,String dateQueryType,String beginTime,String endTime,String status,String isKind,String classifiedLevel,String keyword)
    {
        return archivesRecordMapper.getArchivesRecordListForFile(orgId,dateQueryType,beginTime,endTime,status,isKind,classifiedLevel,"%"+keyword+"%");
    }

	/**
	 * 获取待归档档案列表
	 * @param pageParam
	 * @param date
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param isKind
	 * @param classifiedLevel
	 * @return
	 * @throws Exception
	 */
    public PageInfo<Map<String, String>> getArchivesRecordListForFile(PageParam pageParam, String date, String beginTime, String endTime,String status,String isKind,String classifiedLevel) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesRecordListForFile(pageParam.getOrgId(),date,beginTime,endTime, status,isKind,classifiedLevel,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取待上架档案列表
	 * @param orgId
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param frameStatus
	 * @param classifiedLevel
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>>getArchivesRecordListForFrame(String orgId,String dateQueryType,String beginTime,String endTime,String frameStatus,String classifiedLevel,String keyword) {
		return archivesRecordMapper.getArchivesRecordListForFrame(orgId,dateQueryType,beginTime,endTime,frameStatus,classifiedLevel,"%"+keyword+"%");
	}

	/**
	 * 获取待上架档案列表
	 * @param pageParam
	 * @param date
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param frameStatus
	 * @param classifiedLevel
	 * @return
	 * @throws Exception
	 */
	public PageInfo<Map<String, String>> getArchivesRecordListForFrame(PageParam pageParam, String date, String beginTime, String endTime,String frameStatus,String classifiedLevel) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesRecordListForFrame(pageParam.getOrgId(),date,beginTime,endTime, frameStatus,classifiedLevel,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


	public JSONObject searchIndex(String orgId, String keywords) throws Exception {
		SysConfig sysConfig = sysConfigService.getSysConfig();
		LuceneUtils luceneUtils = new LuceneUtils();
		return luceneUtils.searchIndex(orgId, keywords, attachPath+ File.separator+sysConfig.getArchivesCache());
	}

	/**
	 * 获取档案架上的档案数量
	 * @param orgId
	 * @param frameId
	 * @return
	 */
	public int getCountByFrameId(String orgId,String frameId) {
		ArchivesRecord archivesRecord = new ArchivesRecord();
		archivesRecord.setFrameId(frameId);
		archivesRecord.setOrgId(orgId);
		return archivesRecordMapper.selectCount(archivesRecord);
	}

	/**
	 * 获取档案分类的档案数量
	 * @param orgId
	 * @param sortId
	 * @return
	 */
	public int getCountBySortId(String orgId,String sortId) {
		ArchivesRecord archivesRecord = new ArchivesRecord();
		archivesRecord.setSortId(sortId);
		archivesRecord.setOrgId(orgId);
		return archivesRecordMapper.selectCount(archivesRecord);
	}

	/**
	 * 档案室的档案数
	 * @param orgId
	 * @param roomId
	 * @return
	 */
	public int getCountByRoomId(String orgId,String roomId) {
		ArchivesRecord archivesRecord = new ArchivesRecord();
		archivesRecord.setRoomId(roomId);
		archivesRecord.setOrgId(orgId);
		return archivesRecordMapper.selectCount(archivesRecord);
	}

}
