package com.core136.service.finance;

import com.core136.bean.finance.ExpenseUserConfig;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ExpenseUserConfigMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExpenseUserConfigService {
	private ExpenseUserConfigMapper expenseUserConfigMapper;
	@Autowired
	public void setExpenseUserConfigMapper(ExpenseUserConfigMapper expenseUserConfigMapper) {
		this.expenseUserConfigMapper = expenseUserConfigMapper;
	}

	public int insertExpenseUserConfig(ExpenseUserConfig expenseUserConfig) {
		return expenseUserConfigMapper.insert(expenseUserConfig);
	}

	public int deleteExpenseUserConfig(ExpenseUserConfig expenseUserConfig) {
		return expenseUserConfigMapper.delete(expenseUserConfig);
	}

	public int updateExpenseUserConfig(Example example,ExpenseUserConfig expenseUserConfig) {
		return expenseUserConfigMapper.updateByExampleSelective(expenseUserConfig,example);
	}

	public ExpenseUserConfig selectOneExpenseUserConfig(ExpenseUserConfig expenseUserConfig) {
		return expenseUserConfigMapper.selectOne(expenseUserConfig);
	}

	/**
	 * 批量删除人员年底预算
	 * @param orgId 机构码
	 * @param list 人员年度预算记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteExpenseUserConfigByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ExpenseUserConfig.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, expenseUserConfigMapper.deleteByExample(example));
		}
	}

	/**
	 * 设置人员预算
	 * @param expenseUserConfig 人员预算对象
	 * @return 消息结构
	 */
	public RetDataBean setExpenseUserConfig(ExpenseUserConfig expenseUserConfig) {
		ExpenseUserConfig tc = new ExpenseUserConfig();
		tc.setOrgId(expenseUserConfig.getOrgId());
		tc.setAccountId(expenseUserConfig.getAccountId());
		tc.setExpenseYear(expenseUserConfig.getExpenseYear());
		if(expenseUserConfigMapper.selectCount(tc)>0) {
			Example example = new Example(ExpenseUserConfig.class);
			example.createCriteria().andEqualTo("orgId",expenseUserConfig.getOrgId()).andEqualTo("accountId",expenseUserConfig.getAccountId()).andEqualTo("expenseYear",expenseUserConfig.getExpenseYear());
			updateExpenseUserConfig(example,expenseUserConfig);
		}else{
			expenseUserConfig.setRecordId(SysTools.getGUID());
			expenseUserConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			insertExpenseUserConfig(expenseUserConfig);
		}
		return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
	}


	public List<Map<String,String>> getExpenseUserConfigList(String orgId,String deptId,String accountId,String expenseYear) {
		return expenseUserConfigMapper.getExpenseUserConfigList(orgId,deptId,accountId,expenseYear);
	}

	/**
	 * 获取人员年度预算列表
	 * @param pageParam 分页参数
	 * @param deptId 部门Id
	 * @param accountId 人员账号
	 * @param expenseYear 年度
	 * @return 预算列表
	 */
	public PageInfo<Map<String, String>> getExpenseUserConfigList(PageParam pageParam,String deptId,String accountId, String expenseYear){
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getExpenseUserConfigList(pageParam.getOrgId(),deptId,accountId,expenseYear);
		return new PageInfo<>(datalist);
	}

}
