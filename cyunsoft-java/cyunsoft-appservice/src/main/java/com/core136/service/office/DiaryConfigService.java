package com.core136.service.office;

import com.core136.bean.office.DiaryConfig;
import com.core136.mapper.office.DiaryConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

/**
 * 工作日志权限设置
 * @author lsq
 */
@Service
public class DiaryConfigService {
    private DiaryConfigMapper diaryConfigMapper;
	@Autowired
	public void setDiaryConfigMapper(DiaryConfigMapper diaryConfigMapper) {
		this.diaryConfigMapper = diaryConfigMapper;
	}

	/**
	 * 设置工作日志权限
	 * @param diaryConfig 工作日志配置对象
	 * @return 成功修改数记数
	 */
	public int setDiaryConfig(DiaryConfig diaryConfig) {
        DiaryConfig newDiaryConfig = new DiaryConfig();
        newDiaryConfig.setOrgId(diaryConfig.getOrgId());
        if (isExist(newDiaryConfig) > 0) {
            Example example = new Example(DiaryConfig.class);
            example.createCriteria().andEqualTo("orgId", diaryConfig.getOrgId());
            return this.updateDiaryConfig(diaryConfig, example);
        } else {
            return this.insertDiaryConfig(diaryConfig);
        }
    }

    public int insertDiaryConfig(DiaryConfig diaryConfig)
    {
        return diaryConfigMapper.insert(diaryConfig);
    }


    public DiaryConfig selectOneDiaryConfig(DiaryConfig diaryConfig) {
        return diaryConfigMapper.selectOne(diaryConfig);
    }

    private int updateDiaryConfig(DiaryConfig diaryConfig, Example example) {
        return diaryConfigMapper.updateByExampleSelective(diaryConfig, example);
    }

    private int isExist(DiaryConfig diaryConfig) {
        return diaryConfigMapper.selectCount(diaryConfig);
    }

}
