package com.core136.service.office;

import com.core136.bean.office.TaskGanttLink;
import com.core136.mapper.office.TaskGanttLinkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class TaskGanttLinkService {
    private TaskGanttLinkMapper taskGanttLinkMapper;
	@Autowired
	public void setTaskGanttLinkMapper(TaskGanttLinkMapper taskGanttLinkMapper) {
		this.taskGanttLinkMapper = taskGanttLinkMapper;
	}

	public int insertTaskGanttLink(TaskGanttLink taskGanttLink) {
        return taskGanttLinkMapper.insert(taskGanttLink);
    }

    public int deleteTaskGanttLink(TaskGanttLink taskGanttLink) {
        return taskGanttLinkMapper.delete(taskGanttLink);
    }

    public int updateTaskGanttLink(Example example, TaskGanttLink taskGanttLink) {
        return taskGanttLinkMapper.updateByExampleSelective(taskGanttLink, example);
    }

    public TaskGanttLink selectOneTaskGanttLink(TaskGanttLink taskGanttLink) {
        return taskGanttLinkMapper.selectOne(taskGanttLink);
    }

	/**
	 * 获取子任务路径列表
	 * @param orgId 机构码
	 * @param taskId 任务列表
	 * @return 子任务路径列表
	 */
	public List<Map<String, String>> getGanttLinkList(String orgId, String taskId) {
        return taskGanttLinkMapper.getGanttLinkList(orgId, taskId);
    }

}
