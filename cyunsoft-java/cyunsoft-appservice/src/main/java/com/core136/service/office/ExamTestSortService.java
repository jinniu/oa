package com.core136.service.office;

import com.core136.bean.office.ExamTestSort;
import com.core136.mapper.office.ExamTestSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ExamTestSortService {
    private ExamTestSortMapper examTestSortMapper;
	@Autowired
	public void setExamTestSortMapper(ExamTestSortMapper examTestSortMapper) {
		this.examTestSortMapper = examTestSortMapper;
	}

	public int insertExamTestSort(ExamTestSort examTestSort) {
        return examTestSortMapper.insert(examTestSort);
    }

    public int deleteExamTestSort(ExamTestSort examTestSort) {
        return examTestSortMapper.delete(examTestSort);
    }

    public int updateExamTestSort(Example example, ExamTestSort examTestSort) {
        return examTestSortMapper.updateByExampleSelective(examTestSort, example);
    }

    public ExamTestSort selectOneExamTestSort(ExamTestSort examTestSort) {
        return examTestSortMapper.selectOne(examTestSort);
    }

    /**
     * 获取分类树结构
     * @param orgId 机构码
     * @return 分类树结构
     */
    public List<ExamTestSort> getExamTestSortTree(String orgId) {
        List<ExamTestSort> list = getAllExamTestSort(orgId);
        List<ExamTestSort> examSortList = new ArrayList<>();
        for (ExamTestSort examTestSort : list) {
            if (examTestSort.getParentId().equals("0")) {
                examSortList.add(examTestSort);
            }
        }
        for (ExamTestSort examSort : examSortList) {
            examSort.setChildren(getChildSortList(examSort.getSortId(), list));
        }
        return examSortList;
    }
    public List<ExamTestSort> getAllExamTestSort(String orgId) {
        ExamTestSort examTestSort = new ExamTestSort();
        examTestSort.setOrgId(orgId);
        return examTestSortMapper.select(examTestSort);
    }
    /**
     * 获取分类子表
     * @param sortId 分类Id
     * @param rootExamSort
     * @return 分类子表
     */
    public List<ExamTestSort> getChildSortList(String sortId, List<ExamTestSort> rootExamSort) {
        List<ExamTestSort> childList = new ArrayList<>();
        for (ExamTestSort examSort : rootExamSort) {
            if (examSort.getParentId().equals(sortId)) {
                childList.add(examSort);
            }
        }
        for (ExamTestSort examSort : childList) {
            examSort.setChildren(getChildSortList(examSort.getSortId(), rootExamSort));
        }
        if (childList.isEmpty()) {
            return Collections.emptyList();
        }
        return childList;
    }

    public int isExistChild(String orgId, String sortId) {
        ExamTestSort examTestSort = new ExamTestSort();
        examTestSort.setOrgId(orgId);
        examTestSort.setParentId(sortId);
        return examTestSortMapper.selectCount(examTestSort);
    }

}
