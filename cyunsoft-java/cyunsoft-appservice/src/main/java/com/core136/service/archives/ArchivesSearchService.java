package com.core136.service.archives;

import com.core136.bean.account.UserInfo;
import com.core136.bean.archives.ArchivesSearch;
import com.core136.bean.file.KnowledgeSearch;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesSearchMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesSearchService {
	private ArchivesSearchMapper archivesSearchMapper;
	@Autowired
	public void setArchivesSearchMapper(ArchivesSearchMapper archivesSearchMapper) {
		this.archivesSearchMapper = archivesSearchMapper;
	}

	public int insertArchivesSearch(ArchivesSearch ArchivesSearch)
	{
		return archivesSearchMapper.insert(ArchivesSearch);
	}

	public int deleteArchivesSearch(ArchivesSearch archivesSearch)
	{
		return archivesSearchMapper.delete(archivesSearch);
	}

	public int updateArchivesSearch(Example example,ArchivesSearch archivesSearch)
	{
		return archivesSearchMapper.updateByExampleSelective(archivesSearch,example);
	}

	public ArchivesSearch selectOneArchivesSearch(ArchivesSearch archivesSearch)
	{
		return archivesSearchMapper.selectOne(archivesSearch);
	}

	/**
	 * 获取热门关键字
	 * @param orgId
	 * @return
	 */
	public List<Map<String,String>> getHostKeywords(String orgId)
	{
		return archivesSearchMapper.getHostKeywords(orgId);
	}

	public List<Map<String,String>>getArchivesSearchList(String orgId,String keyword)
	{
		return archivesSearchMapper.getArchivesSearchList(orgId,keyword);
	}

	/**
	 * 添加查询关键字
	 * @param userInfo
	 * @param archivesSearch
	 * @return
	 */
	public int addArchivesSearch(UserInfo userInfo, ArchivesSearch archivesSearch) {
		ArchivesSearch archivesSearch1 = new ArchivesSearch();
		archivesSearch1.setKeyword(archivesSearch.getKeyword());
		archivesSearch1.setOrgId(archivesSearch.getOrgId());
		archivesSearch1 = selectOneArchivesSearch(archivesSearch);
		if (archivesSearch1 == null) {
			archivesSearch.setRecordId(SysTools.getGUID());
			archivesSearch.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			archivesSearch.setCreateUser(userInfo.getAccountId());
			archivesSearch.setCount(1);
			return insertArchivesSearch(archivesSearch);
		} else {
			archivesSearch1.setCount(archivesSearch1.getCount() + 1);
			Example example = new Example(KnowledgeSearch.class);
			example.createCriteria().andEqualTo("recordId", archivesSearch1.getRecordId()).andEqualTo("orgId", archivesSearch1.getOrgId());
			return updateArchivesSearch(example, archivesSearch1);
		}
	}


}
