package com.core136.service.invite;

import com.core136.bean.invite.InviteEvaluation;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.invite.InviteEvaluationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteEvaluationService {
	private InviteEvaluationMapper inviteEvaluationMapper;
	@Autowired
	public void setInviteEvaluationMapper(InviteEvaluationMapper inviteEvaluationMapper) {
		this.inviteEvaluationMapper = inviteEvaluationMapper;
	}

	public int insertInviteEvaluation(InviteEvaluation inviteEvaluation) {
		return inviteEvaluationMapper.insert(inviteEvaluation);
	}

	public int deleteInviteEvaluation(InviteEvaluation inviteEvaluation) {
		return inviteEvaluationMapper.delete(inviteEvaluation);
	}

	public int updateInviteEvaluation(Example example,InviteEvaluation inviteEvaluation) {
		return inviteEvaluationMapper.updateByExampleSelective(inviteEvaluation,example);
	}

	public InviteEvaluation selectOneInviteEvaluation(InviteEvaluation inviteEvaluation) {
		return inviteEvaluationMapper.selectOne(inviteEvaluation);
	}

	/**
	 * 批量删除评标记录
	 * @param orgId 机构码
	 * @param list 评标记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteInviteEvaluationByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteEvaluation.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteEvaluationMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取评标记录列表
	 * @param orgId 机构码
	 * @param inviteId 评标记录Id
	 * @return 评标记录列表
	 */
	public List<Map<String,String>>getInviteEvaluationByInviteIdList(String orgId, String inviteId) {
		return inviteEvaluationMapper.getInviteEvaluationByInviteIdList(orgId,inviteId);
	}




}
