package com.core136.service.office;

import com.core136.bean.account.UserInfo;
import com.core136.bean.office.Task;
import com.core136.bean.office.TaskGanttData;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.TaskMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
public class TaskService {
	private TaskMapper taskMapper;
	private UserInfoService userInfoService;
	private TaskGanttDataService taskGanttDataService;
	@Autowired
	public void setTaskMapper(TaskMapper taskMapper) {
		this.taskMapper = taskMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setTaskGanttDataService(TaskGanttDataService taskGanttDataService) {
		this.taskGanttDataService = taskGanttDataService;
	}

	public int insertTask(Task task) {
		return taskMapper.insert(task);
	}

	public Map<String,String> getTaskById(Task task) {
		Map<String,String> tempMap = taskMapper.getTaskById(task.getOrgId(),task.getTaskId());
		if(StringUtils.isNotBlank(tempMap.get("participantAccountId"))) {
			tempMap.put("participantUserName", userInfoService.getUserNamesByAccountIds(task.getOrgId(),Arrays.asList(tempMap.get("participantAccountId").split(","))));
		}

		return tempMap;
	}


	/**
	 * 创建任务
	 * @param user
	 * @param task
	 * @return
	 */
	public int addTask(UserInfo user, Task task) {
		if (StringUtils.isNotBlank(task.getMsgType())) {
			String participantAccountId = task.getParticipantAccountId();
			String chargeAccountId = task.getChargeAccountId();
			String supervisorAccountId = task.getSupervisorAccountId();
			List<String> userList = new ArrayList<>();
			List<String> arr2 = new ArrayList<>();
			List<String> arr3 = new ArrayList<>();
			if (StringUtils.isNotBlank(participantAccountId)) {
				userList = new ArrayList<>(Arrays.asList(participantAccountId.split(",")));
			}
			if (StringUtils.isNotBlank(chargeAccountId)) {
				arr2 = new ArrayList<>(Arrays.asList(chargeAccountId.split(",")));
			}
			if (StringUtils.isNotBlank(supervisorAccountId)) {
				arr3 = new ArrayList<>(Arrays.asList(supervisorAccountId.split(",")));
			}
			userList.addAll(arr2);
			userList.addAll(arr3);
			Set<String> set = new HashSet<>();
			set.addAll(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
			userList.clear();
			userList.addAll(set);
			List<MsgBody> msgBodyList = new ArrayList<>();
			for (String accountId : userList) {
				UserInfo user2 = new UserInfo();
				user2.setAccountId(accountId);
				user2.setOrgId(user.getOrgId());
				user2 = userInfoService.selectOneUser(user2);
				MsgBody msgBody = new MsgBody();
				msgBody.setTitle("任务提醒");
				msgBody.setContent("任务标题为：" + task.getTaskName() + "的查看提醒！");
				msgBody.setSendTime(task.getCreateTime());
				msgBody.setUserInfo(user2);
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setView(GlobalConstant.MSG_TYPE_TASK);
				msgBody.setRecordId(task.getTaskId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(task.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return insertTask(task);
	}

	public Task selectOneTask(Task task) {
		return taskMapper.selectOne(task);
	}

	public int updateTask(Task task, Example example) {
		return taskMapper.updateByExampleSelective(task, example);
	}

	/**
	 * 获取Gantt详情
	 * @param orgId 机构码
	 * @param taskId
	 * @return
	 */
	public Map<String, List<Map<String, String>>> getGanttInfoByTaskId(String orgId, String taskId) {
		Map<String, String> map = taskMapper.getGanttInfoByTaskId(orgId, taskId);
		List<Map<String, String>> tempDataList = new ArrayList<>();
		List<Map<String, String>> tempLinks = new ArrayList<>();
		tempDataList.add(map);
		Map<String, List<Map<String, String>>> ganttList = taskGanttDataService.getTaskGantInfo(orgId, taskId);
		List<Map<String, String>> dataList = ganttList.get("data");
		List<Map<String, String>> linksList = ganttList.get("links");
		for (int i = 0; i < dataList.size(); i++) {
			if (dataList.get(i).get("parent").equals("0")) {
				Map<String, String> tempMap = dataList.get(i);
				tempMap.put("parent", taskId);
				tempDataList.add(tempMap);
			} else {
				tempDataList.add(dataList.get(i));
			}
		}
		tempLinks.addAll(linksList);
		Map<String, List<Map<String, String>>> resMap = new HashMap<>();
		resMap.put("data", tempDataList);
		resMap.put("links", tempLinks);
		return resMap;
	}

	/**
	 * 更新任务并提醒
	 * @param user
	 * @param task
	 * @param example
	 * @return
	 */
	public int updateTask(UserInfo user, Task task, Example example) {
		if (StringUtils.isNotBlank(task.getMsgType())) {
			String participantAccountId = task.getParticipantAccountId();
			String chargeAccountId = task.getChargeAccountId();
			String supervisorAccountId = task.getSupervisorAccountId();
			List<String> userList = null;
			List<String> arr2 = null;
			List<String> arr3 = null;
			if (StringUtils.isNotBlank(participantAccountId)) {
				userList = new ArrayList<>(Arrays.asList(participantAccountId.split(",")));
			}
			if (StringUtils.isNotBlank(chargeAccountId)) {
				arr2 = new ArrayList<>(Arrays.asList(chargeAccountId.split(",")));
			}
			if (StringUtils.isNotBlank(supervisorAccountId)) {
				arr3 = new ArrayList<>(Arrays.asList(supervisorAccountId.split(",")));
			}
			userList.addAll(arr2);
			userList.addAll(arr3);
			Set<String> set = new HashSet<>();
			set.addAll(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
			userList.clear();
			userList.addAll(set);
			List<MsgBody> msgBodyList = new ArrayList<>();
			for (String accountId : userList) {
				UserInfo user2 = new UserInfo();
				user2.setAccountId(accountId);
				user2.setOrgId(user.getOrgId());
				user2 = userInfoService.selectOneUser(user2);
				MsgBody msgBody = new MsgBody();
				msgBody.setTitle("任务提醒");
				msgBody.setContent("任务标题为：" + task.getTaskName() + "的查看提醒！");
				msgBody.setSendTime(task.getCreateTime());
				msgBody.setUserInfo(user2);
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setView(GlobalConstant.MSG_TYPE_TASK);
				msgBody.setRecordId(task.getTaskId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(task.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return taskMapper.updateByExampleSelective(task, example);
	}

	public int deleteTask(Task task) {
		return taskMapper.delete(task);
	}

	/**
	 * 批量删除任务
	 *
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteTaskByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(Task.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("taskId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, taskMapper.deleteByExample(example));
		}
	}

	public List<Map<String, String>> getMyJoinTaskList(String orgId, String accountId, String status, String taskType, String dateQueryType, String beginTime, String endTime, String keyword) {
		return taskMapper.getMyJoinTaskList(orgId, accountId, status, taskType, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	public PageInfo<Map<String, String>> getMyJoinTaskList(PageParam pageParam, String status, String taskType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyJoinTaskList(pageParam.getOrgId(), pageParam.getAccountId(), status, taskType, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	public List<Map<String, String>> getMyShareTaskList(String orgId, String accountId,String deptId,String levelId, String status, String taskType, String dateQueryType, String beginTime, String endTime, String keyword) {
		return taskMapper.getMyShareTaskList(orgId, accountId,deptId,levelId, status, taskType, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	public PageInfo<Map<String, String>> getMyShareTaskList(PageParam pageParam, String status, String taskType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyShareTaskList(pageParam.getOrgId(), pageParam.getAccountId(),pageParam.getDeptId(),pageParam.getLevelId(), status, taskType, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取任务列表
	 *
	 * @param orgId
	 * @param accountId 用户账号
	 * @param opFlag 管理员标识
	 * @param status
	 * @param taskType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	public List<Map<String, String>> getManageTaskList(String orgId, String accountId, String opFlag, String status, String taskType, String dateQueryType, String beginTime, String endTime, String keyword) {
		return taskMapper.getManageTaskList(orgId, accountId, opFlag, status, taskType, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 获取任务列表
	 *
	 * @param pageParam 分页参数
	 * @param status
	 * @param taskType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getManageTaskList(PageParam pageParam, String status, String taskType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getManageTaskList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getOpFlag(), status, taskType, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取待分配任务列表
	 *
	 * @param orgId
	 * @param accountId 用户账号
	 * @param taskType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	public List<Object> getTaskAllocationList(String orgId, String accountId, String taskType, String dateQueryType, String beginTime, String endTime, String keyword) {
		if (StringUtils.isBlank(keyword)) {
			keyword = "%%";
		} else {
			keyword = "%" + keyword + "%";
		}
		List<Map<String, Object>> list = taskMapper.getTaskAllocationList(orgId, accountId, taskType, dateQueryType, beginTime, endTime, keyword);
		List<Object> resList = new ArrayList<>();
		for (Map<String, Object> map : list) {
			String taskId = map.get("id").toString();
			List<TaskGanttData> ganttList = taskGanttDataService.getAllTaskGanttDataList(orgId, taskId);
			map.put("children", ganttList);
			map.put("taskDataId", taskId);
			resList.add(map);
		}
		return resList;
	}

	/**
	 * 获取待分解任务列表
	 *
	 * @param orgId
	 * @param accountId 用户账号
	 * @param opFlag 管理员标识
	 * @param taskType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	public Map<String, List<Map<String, String>>> getAssignmentTaskList(String orgId, String accountId, String opFlag, String taskType, String dateQueryType, String beginTime, String endTime) {
		List<Map<String, String>> list = taskMapper.getAssignmentTaskList(orgId, accountId, opFlag, taskType, dateQueryType, beginTime, endTime);
		List<Map<String, String>> tempDataList = new ArrayList<>();
		List<Map<String, String>> tempLinks = new ArrayList<>();
		for (Map<String, String> map : list) {
			tempDataList.add(map);
			String taskId = map.get("id");
			Map<String, List<Map<String, String>>> ganttList = taskGanttDataService.getTaskGantInfo(orgId, taskId);
			List<Map<String, String>> dataList = ganttList.get("data");
			List<Map<String, String>> linksList = ganttList.get("links");
			for (Map<String, String> stringMap : dataList) {
				if (stringMap.get("parent").equals("0")) {
					Map<String, String> tempMap = stringMap;
					tempMap.put("parent", taskId);
					tempDataList.add(tempMap);
				} else {
					tempDataList.add(stringMap);
				}
			}
			tempLinks.addAll(linksList);
		}
		Map<String, List<Map<String, String>>> map = new HashMap<>();
		map.put("data", tempDataList);
		map.put("links", tempLinks);
		return map;
	}


	/**
	 * 获取个人负责的任务
	 *
	 * @param orgId
	 * @param accountId 用户账号
	 * @param status
	 * @param taskType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	public List<Map<String, String>> getMyChargeTaskList(String orgId, String accountId, String status, String taskType, String dateQueryType, String beginTime, String endTime, String keyword) {
		return taskMapper.getMyChargeTaskList(orgId, accountId, status, taskType, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 获取个人负责的任务
	 *
	 * @param pageParam 分页参数
	 * @param status
	 * @param taskType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyChargeTaskList(PageParam pageParam, String status, String taskType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyChargeTaskList(pageParam.getOrgId(), pageParam.getAccountId(), status, taskType, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


	/**
	 * 获取个人督查的任务列表
	 *
	 * @param orgId
	 * @param accountId 用户账号
	 * @param status
	 * @param taskType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	public List<Map<String, String>> getMySupervisorTaskList(String orgId, String accountId, String status, String taskType, String dateQueryType, String beginTime, String endTime, String keyword) {
		return taskMapper.getMySupervisorTaskList(orgId, accountId, status, taskType, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 获取个人督查的任务列表
	 *
	 * @param pageParam 分页参数
	 * @param status
	 * @param taskType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMySupervisorTaskList(PageParam pageParam, String status, String taskType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMySupervisorTaskList(pageParam.getOrgId(), pageParam.getAccountId(), status, taskType, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
