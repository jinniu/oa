package com.core136.service.office;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.office.ExamGrade;
import com.core136.bean.office.ExamQuestions;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.ExamGradeMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class ExamGradeService {
    private ExamGradeMapper examGradeMapper;
    private ExamQuestionsService examQuestionsService;
	@Autowired
	public void setExamGradeMapper(ExamGradeMapper examGradeMapper) {
		this.examGradeMapper = examGradeMapper;
	}
	@Autowired
	public void setExamQuestionsService(ExamQuestionsService examQuestionsService) {
		this.examQuestionsService = examQuestionsService;
	}

	public int insertExamGrade(ExamGrade examGrade) {
        return examGradeMapper.insert(examGrade);
    }

    public int deleteExamGrade(ExamGrade examGrade) {
        return examGradeMapper.delete(examGrade);
    }

    public int updateExamGrade(Example example, ExamGrade examGrade) {
        return examGradeMapper.updateByExampleSelective(examGrade, example);
    }

    public ExamGrade selectOneExamGrade(ExamGrade examGrade) {
        return examGradeMapper.selectOne(examGrade);
    }

    /**
     * 获取考试成绩列表
     *
     * @param orgId 机构码
     * @param examTestId 考试记录Id
     * @param accountId 用户Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 成绩列表
     */
    public List<Map<String, String>> getExamGradeList(String orgId, String examTestId, String accountId, String dateQueryType,String beginTime, String endTime, String keyword) {
        return examGradeMapper.getExamGradeList(orgId, examTestId, accountId, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取考试成绩列表
     *
     * @param pageParam 分页参数
     * @param examTestId 考试记录Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return 考试成绩列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getExamGradeList(PageParam pageParam, String examTestId, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamGradeList(pageParam.getOrgId(), examTestId, pageParam.getAccountId(),dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    public List<Map<String, String>> getExamGradeOldList(String orgId, String examTestId, String accountId, String dateQueryType,String beginTime, String endTime, String keyword) {
        return examGradeMapper.getExamGradeOldList(orgId, examTestId, accountId, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    public PageInfo<Map<String, String>> getExamGradeOldList(PageParam pageParam, String examTestId,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamGradeOldList(pageParam.getOrgId(), examTestId, pageParam.getAccountId(),dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    public Double addExamGrade(ExamGrade examGrade) {
		examGrade.setStatus("1");
		examGrade.setGrade(getGrade(examGrade));
		examGrade.setEndTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
		Example example = new Example(ExamGrade.class);
		example.createCriteria().andEqualTo("orgId",examGrade.getOrgId()).andEqualTo("examTestId",examGrade.getExamTestId()).andEqualTo("accountId",examGrade.getAccountId());
        updateExamGrade(example,examGrade);
        return examGrade.getGrade();
    }

	/**
	 * 自动判卷
	 * @param examGrade
	 * @return
	 */
	public double getGrade(ExamGrade examGrade) {
        double grade = 0.0;
        String result = examGrade.getResult();
        if (StringUtils.isNotBlank(result)) {
        	JSONObject json = JSON.parseObject(result);
			Iterator iter = json.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry entry = (Map.Entry) iter.next();
				String answerStr = entry.getValue().toString();
				ExamQuestions examQuestions = getRealAnswer(examGrade.getOrgId(), entry.getKey().toString());
				if (examQuestions.getGrade() == null) {
					examQuestions.setGrade(0.0);
				}
				if(examQuestions.getExamType().equals("1"))
				{
					String answer = examQuestions.getAnswer();
					String[] answerArr = answer.split(",");
					String[] answerStrArr = (answerStr.replace("[","")
						.replace("]","")
						.replace("\"",""))
						.split(",");
					Arrays.sort(answerArr);
					Arrays.sort(answerStrArr);
					String aStr = StringUtils.join(answerStrArr, ",");
					String bStr = StringUtils.join(answerArr, ",");
					if (aStr.equals(bStr)) {
						grade += examQuestions.getGrade();
					}
				}else if(examQuestions.getExamType().equals("0") && (examQuestions.getAnswer().equals(answerStr))) {
						grade += examQuestions.getGrade();
				}
			}
            return grade;
        } else {
            return 0.0;
        }
    }

	/**
	 * 判断当前考试是否已参与过
	 * @param orgId 机构码
	 * @param examTestId
	 * @param accountId 用户账号
	 * @return 是否已参与过
	 */
    public boolean getExamTestStatus(String orgId,String examTestId,String accountId)
	{
		Example example = new Example(ExamGrade.class);
		example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("examTestId",examTestId).andEqualTo("createUser",accountId).andEqualTo("status","1");
		int count = examGradeMapper.selectCountByExample(example);
		return count > 0;
	}

	public boolean getInitExamTestStatus(String orgId,String examTestId,String accountId)
	{
		Example example = new Example(ExamGrade.class);
		example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("examTestId",examTestId).andEqualTo("createUser",accountId);
		int count = examGradeMapper.selectCountByExample(example);
		return count > 0;
	}

    public ExamQuestions getRealAnswer(String orgId, String recordId) {
        ExamQuestions examQuestions = new ExamQuestions();
        examQuestions.setRecordId(recordId);
        examQuestions.setOrgId(orgId);
        examQuestions = examQuestionsService.selectOneExamQuestions(examQuestions);
        return examQuestions;
    }


}
