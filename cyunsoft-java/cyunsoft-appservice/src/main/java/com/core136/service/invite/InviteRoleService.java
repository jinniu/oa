package com.core136.service.invite;

import com.core136.bean.invite.InviteRole;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.invite.InviteRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class InviteRoleService {
	private InviteRoleMapper inviteRoleMapper;
	@Autowired
	public void setInviteRoleMapper(InviteRoleMapper inviteRoleMapper) {
		this.inviteRoleMapper = inviteRoleMapper;
	}

	public int insertInviteRole(InviteRole inviteRole)
	{
		return inviteRoleMapper.insert(inviteRole);
	}

	public int deleteInviteRole(InviteRole inviteRole)
	{
		return inviteRoleMapper.delete(inviteRole);
	}

	public int updateInviteRole(Example example,InviteRole inviteRole)
	{
		return inviteRoleMapper.updateByExampleSelective(inviteRole,example);
	}

	public InviteRole selectOneInviteRole(InviteRole inviteRole)
	{
		return inviteRoleMapper.selectOne(inviteRole);
	}

	public int getCount(String orgId)
	{
		InviteRole inviteRole = new InviteRole();
		inviteRole.setOrgId(orgId);
		return inviteRoleMapper.selectCount(inviteRole);
	}

	/**
	 * 设置招标权限
	 * @param inviteRole 招标权限对象
	 * @return 消息结构
	 */
	public RetDataBean setInviteRole(InviteRole inviteRole)
	{
		if(getCount(inviteRole.getOrgId())==0)
		{
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS,insertInviteRole(inviteRole));
		}else{
			Example example = new Example(InviteRole.class);
			example.createCriteria().andEqualTo("orgId",inviteRole.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS,updateInviteRole(example,inviteRole));
		}
	}


}
