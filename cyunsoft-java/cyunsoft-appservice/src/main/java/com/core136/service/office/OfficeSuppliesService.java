package com.core136.service.office;


import com.core136.bean.account.UnitDept;
import com.core136.bean.account.UserInfo;
import com.core136.bean.office.OfficeSupplies;
import com.core136.bean.office.OfficeSuppliesSort;
import com.core136.bean.system.PageParam;
import com.core136.bean.system.SysDic;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.account.UnitDeptMapper;
import com.core136.mapper.office.OfficeSuppliesMapper;
import com.core136.service.system.SysDicService;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class OfficeSuppliesService {
    private OfficeSuppliesMapper officeSuppliesMapper;
	private UnitDeptMapper unitDeptMapper;
	private OfficeSuppliesSortService officeSuppliesSortService;
	private SysDicService sysDicService;
	@Autowired
	public void setOfficeSuppliesMapper(OfficeSuppliesMapper officeSuppliesMapper) {
		this.officeSuppliesMapper = officeSuppliesMapper;
	}
	@Autowired
	public void setUnitDeptMapper(UnitDeptMapper unitDeptMapper) {
		this.unitDeptMapper = unitDeptMapper;
	}
	@Autowired
	public void setOfficeSuppliesSortService(OfficeSuppliesSortService officeSuppliesSortService) {
		this.officeSuppliesSortService = officeSuppliesSortService;
	}
	@Autowired
	public void setSysDicService(SysDicService sysDicService) {
		this.sysDicService = sysDicService;
	}

	public int insertOfficeSupplies(OfficeSupplies officeSupplies) {
        return officeSuppliesMapper.insert(officeSupplies);
    }

    public int deleteOfficeSupplies(OfficeSupplies officeSupplies) {
        return officeSuppliesMapper.delete(officeSupplies);
    }

    public int updateOfficeSupplies(Example example, OfficeSupplies officeSupplies) {
        return officeSuppliesMapper.updateByExampleSelective(officeSupplies, example);
    }

    public OfficeSupplies selectOneOfficeSupplies(OfficeSupplies officeSupplies) {
        return officeSuppliesMapper.selectOne(officeSupplies);
    }

	/**
	 * 批量删除物资记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteOfficeSuppliesByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(OfficeSupplies.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("suppliesId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, officeSuppliesMapper.deleteByExample(example));
		}
	}

	/**
	 *  获取办公用品列表
	 * @param orgId 机构码
	 * @param sortId
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getOfficeSuppliesListBySortId(String orgId, String sortId, String keyword) {
        return officeSuppliesMapper.getOfficeSuppliesListBySortId(orgId, sortId, "%" + keyword + "%");
    }

    public PageInfo<Map<String, String>> getOfficeSuppliesListBySortId(PageParam pageParam, String sortId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOfficeSuppliesListBySortId(pageParam.getOrgId(), sortId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取可以领用的办公用品列表
	 * @param orgId 机构码
	 * @param sortId
	 * @param deptId
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getApplyOfficeSuppliesList(String orgId, String sortId, String deptId, String keyword) {
        return officeSuppliesMapper.getApplyOfficeSuppliesList(orgId, sortId, deptId, "%" + keyword + "%");
    }

    public PageInfo<Map<String, String>> getApplyOfficeSuppliesList(PageParam pageParam, String sortId, String deptId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getApplyOfficeSuppliesList(pageParam.getOrgId(), sortId, deptId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 办公用品导入
	 * @param user
	 * @param file
	 * @return
	 * @throws IOException
	 */
    @Transactional(value = "generalTM")
    public RetDataBean importOfficeSuppliesInfo(UserInfo user, MultipartFile file) throws IOException {
		List<String> resList = new ArrayList<>();
		List<OfficeSupplies> officeSuppliesList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("办公用品名称");
		titleList.add("办公用品编号");
		titleList.add("所属分类");
		titleList.add("产品品牌");
		titleList.add("规格型号");
		titleList.add("计量单位");
		titleList.add("物品数量");
		titleList.add("可领用部门");
		titleList.add("备注");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			OfficeSupplies officeSupplies = new OfficeSupplies();
			officeSupplies.setSuppliesId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")){
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						officeSupplies.setSortNo(Integer.parseInt(tempMap.get(s)));
					}else{
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("办公用品名称")) {
					officeSupplies.setSuppliesName(tempMap.get(s));
				}
				if (s.equals("办公用品编号")) {
					officeSupplies.setSuppliesCode(tempMap.get(s));
				}
				if (s.equals("所属分类")){
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						OfficeSuppliesSort officeSuppliesSort = new OfficeSuppliesSort();
						officeSuppliesSort.setOrgId(user.getOrgId());
						officeSuppliesSort.setSortId(tempMap.get(s));
						try {
							officeSuppliesSort = officeSuppliesSortService.selectOneOfficeSuppliesSort(officeSuppliesSort);
							if (officeSuppliesSort != null) {
								officeSupplies.setSortId(officeSuppliesSort.getSortId());
							} else {
								resList.add("所属分类不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询所属分类出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("所属分类不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("产品品牌")) {
					officeSupplies.setBrand(tempMap.get(s));
				}
				if (s.equals("规格型号")) {
					officeSupplies.setModel(tempMap.get(s));
				}
				if (s.equals("计量单位")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						SysDic sysDic = new SysDic();
						sysDic.setOrgId(user.getOrgId());
						sysDic.setCode("unit");
						sysDic.setName(tempMap.get(s));
						try {
							sysDic = sysDicService.selectOneSysDic(sysDic);
							if (sysDic != null) {
								officeSupplies.setUnit(sysDic.getKeyValue());
							} else {
								resList.add("计量单位不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询计量单位出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
				}
				if (s.equals("物品数量")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						try{
							officeSupplies.setQuantity(Integer.parseInt(tempMap.get(s)));
						}catch (Exception e) {
							resList.add("物品数量格式不正确!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						officeSupplies.setQuantity(0);
					}
				}

				if (s.equals("可领用部门")){
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						if(tempMap.get(s).equals("全体部门")) {
							officeSupplies.setOwnDept("@all");
						}else{
							List<String> deptNameList = Arrays.asList(tempMap.get(s).split(","));
							Example example = new Example(UnitDept.class);
							example.createCriteria().andEqualTo("orgId",user.getOrgId()).andIn("deptName",deptNameList);
							List<UnitDept> unitDeptList = unitDeptMapper.selectByExample(example);
							List<String> deptIdList = new ArrayList<>();
							for(UnitDept unitDept:unitDeptList) {
								deptIdList.add(unitDept.getDeptId());
							}
							officeSupplies.setOwnDept(StringUtils.join(deptIdList,","));
						}
					}
				}
				if (s.equals("备注")) {
					officeSupplies.setRemark(tempMap.get(s));
				}
			}
			officeSupplies.setCreateTime(createTime);
			officeSupplies.setCreateUser(user.getAccountId());
			officeSupplies.setOrgId(user.getOrgId());
			if(insertFlag) {
				officeSuppliesList.add(officeSupplies);
			}
		}
		for(OfficeSupplies officeSupplies:officeSuppliesList) {
			insertOfficeSupplies(officeSupplies);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }
}
