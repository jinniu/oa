package com.core136.service.finance;

import com.core136.bean.finance.ExpenseAccount;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.finance.ExpenseAccountMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class ExpenseAccountService {
	private ExpenseAccountMapper expenseAccountMapper;
	@Autowired
	public void setExpenseAccountMapper(ExpenseAccountMapper expenseAccountMapper) {
		this.expenseAccountMapper = expenseAccountMapper;
	}

	public int insertExpenseAccount(ExpenseAccount expenseConfig) {
		return expenseAccountMapper.insert(expenseConfig);
	}

	public int deleteExpenseAccount(ExpenseAccount expenseAccount) {
		return expenseAccountMapper.delete(expenseAccount);
	}

	public int updateExpenseAccount(Example example, ExpenseAccount expenseAccount) {
		return expenseAccountMapper.updateByExampleSelective(expenseAccount,example);
	}

	public ExpenseAccount selectOneExpenseAccount(ExpenseAccount expenseAccount) {
		return expenseAccountMapper.selectOne(expenseAccount);
	}

	/**
	 * 批量删除费用科目
	 * @param orgId 机构Id
	 * @param list 费用名称Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteExpenseAccountByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ExpenseAccount.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, expenseAccountMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取所有科目
	 * @param orgId 机构
	 * @return 科目列表
	 */
	public List<ExpenseAccount> getAllExpenseAccount(String orgId,String keyword) {
		Example example = new Example(ExpenseAccount.class);
		if(StringUtils.isBlank(keyword)) {
			example.createCriteria().andEqualTo("orgId",orgId);
		}else {
			example.createCriteria().andEqualTo("orgId", orgId).andLike("title", "%"+keyword+"%");
		}
		example.setOrderByClause("sort_no asc");
		return expenseAccountMapper.selectByExample(example);
	}

	/**
	 * 获取科目树型结构
	 * @param orgId 机构码
	 * @return 科目列表
	 */
	public List<ExpenseAccount> getExpenseAccountTree(String orgId,String keyword) {
		List<ExpenseAccount> list = getAllExpenseAccount(orgId,keyword);
		if(StringUtils.isNotBlank(keyword)) {
			return list;
		}
		List<ExpenseAccount> expenseAccountList = new ArrayList<>();
		for (ExpenseAccount expenseAccount : list) {
			if (expenseAccount.getParentId().equals("0")) {
				expenseAccountList.add(expenseAccount);
			}
		}
		for (ExpenseAccount expenseAccount : list) {
			expenseAccount.setChildren(getChildExpenseAccountList(expenseAccount.getRecordId(), list));
		}
		return expenseAccountList;
	}

	public List<ExpenseAccount> getChildExpenseAccountList(String recordId, List<ExpenseAccount> rootExpenseAccount) {
		List<ExpenseAccount> childList = new ArrayList<>();
		for (ExpenseAccount expenseAccount : rootExpenseAccount) {
			if (expenseAccount.getParentId().equals(recordId)) {
				childList.add(expenseAccount);
			}
		}
		for (ExpenseAccount expenseAccount : childList) {
			expenseAccount.setChildren(getChildExpenseAccountList(expenseAccount.getRecordId(), rootExpenseAccount));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	public List<Map<String,String>>getExpenseAccountForSelect(String orgId) {
		return expenseAccountMapper.getExpenseAccountForSelect(orgId);
	}
}
