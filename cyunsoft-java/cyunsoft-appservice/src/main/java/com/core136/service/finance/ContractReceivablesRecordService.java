package com.core136.service.finance;

import com.core136.bean.finance.ContractReceivables;
import com.core136.bean.finance.ContractReceivablesRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ContractReceivablesRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ContractReceivablesRecordService {
    private ContractReceivablesRecordMapper contractReceivablesRecordMapper;
    private ContractReceivablesService contractReceivablesService;
	@Autowired
	public void setContractReceivablesRecordMapper(ContractReceivablesRecordMapper contractReceivablesRecordMapper) {
		this.contractReceivablesRecordMapper = contractReceivablesRecordMapper;
	}
	@Autowired
	public void setContractReceivablesService(ContractReceivablesService contractReceivablesService) {
		this.contractReceivablesService = contractReceivablesService;
	}

	/**
	 * 收款
	 * @param contractReceivablesRecord 收款对象
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
    public RetDataBean receivableAmount(ContractReceivablesRecord contractReceivablesRecord) {
        String receivablesId = contractReceivablesRecord.getReceivablesId();
        ContractReceivables contractReceivables = new ContractReceivables();
        contractReceivables.setReceivablesId(receivablesId);
        contractReceivables.setOrgId(contractReceivablesRecord.getOrgId());
        contractReceivables = contractReceivablesService.selectOneContractReceivables(contractReceivables);
        Double unrecevided = contractReceivables.getUnReceived();
        Double receivded = contractReceivables.getReceived();
        if (unrecevided < contractReceivablesRecord.getAmount()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
        } else {
            unrecevided = unrecevided - contractReceivablesRecord.getAmount();
            receivded = receivded + contractReceivablesRecord.getAmount();
            ContractReceivables contractReceivables1 = new ContractReceivables();
            contractReceivables1.setReceivablesId(contractReceivablesRecord.getReceivablesId());
            contractReceivables1.setUnReceived(unrecevided);
            contractReceivables1.setReceived(receivded);
            contractReceivables1.setReceivedTime(contractReceivablesRecord.getPayeeTime());
            contractReceivables1.setOrgId(contractReceivablesRecord.getOrgId());
            Example example = new Example(ContractReceivables.class);
            example.createCriteria().andEqualTo("orgId", contractReceivablesRecord.getOrgId()).andEqualTo("receivablesId", contractReceivables1.getReceivablesId());
            contractReceivablesService.updateContractReceivables(contractReceivables1, example);
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertContractReceivablesRecord(contractReceivablesRecord));
        }
    }


    public int insertContractReceivablesRecord(ContractReceivablesRecord contractReceivablesRecord) {
        return contractReceivablesRecordMapper.insert(contractReceivablesRecord);
    }

    public int deleteContractReceivablesRecord(ContractReceivablesRecord contractReceivablesRecord) {
        return contractReceivablesRecordMapper.delete(contractReceivablesRecord);
    }

    public int updateContractReceivablesRecord(Example example, ContractReceivablesRecord contractReceivablesRecord) {
        return contractReceivablesRecordMapper.updateByExampleSelective(contractReceivablesRecord, example);
    }

    public ContractReceivablesRecord selectOneContractReceivablesRecord(ContractReceivablesRecord contractReceivablesRecord) {
        return contractReceivablesRecordMapper.selectOne(contractReceivablesRecord);
    }

    /**
     * 获取收款记录
     * @param orgId 机构码
     * @param receivablesId 应收款记录Id
     * @return 收款记录列表
     */
    public List<Map<String, String>> getContractReceivablesRecordList(String orgId, String receivablesId) {
        return contractReceivablesRecordMapper.getContractReceivablesRecordList(orgId, receivablesId);
    }

    /**
     * 获取收款记录
     * @param pageParam 分页参数
     * @param receivablesId 应收款记录Id
     * @return 收款记录列表
     */
    public PageInfo<Map<String, String>> getContractReceivablesRecordList(PageParam pageParam, String receivablesId) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractReceivablesRecordList(pageParam.getOrgId(), receivablesId);
		return new PageInfo<>(datalist);
    }

    /**
     * 收款记录
     * @param orgId 机构码
     * @return 收款记录列表
     */
    public List<Map<String, String>> getReceivablesRecordTop(@Param(value = "orgId") String orgId) {
        return contractReceivablesRecordMapper.getReceivablesRecordTop(orgId);
    }
    /**
     * 查询历史收款记录列表
     * @param orgId 机构码
     * @param payReceivedType 付款种类
     * @param dateQueryType 日期范围
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 收款记录列表
     */
    public List<Map<String, String>> getAllContractReceivablesRecordList(String orgId,String payReceivedType,String dateQueryType,String beginTime,String endTime,String keyword)
    {
        return contractReceivablesRecordMapper.getAllContractReceivablesRecordList(orgId,payReceivedType,dateQueryType,beginTime,endTime,"%"+keyword+"%");
    }

    /**
     * 查询历史收款记录列表
     * @param pageParam 分页参数
     * @param payReceivedType 付款种类
     * @param date 日期范围
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 收款记录列表
     */
    public PageInfo<Map<String, String>> getAllContractReceivablesRecordList(PageParam pageParam, String payReceivedType,String date,String beginTime,String endTime)  {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAllContractReceivablesRecordList(pageParam.getOrgId(), payReceivedType,date,beginTime,endTime,pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }

}
