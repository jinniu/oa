package com.core136.service.office;

import com.core136.bean.office.ExamQuestions;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.ExamQuestionsMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExamQuestionsService {

    private ExamQuestionsMapper examQuestionsMapper;
	@Autowired
	public void setExamQuestionsMapper(ExamQuestionsMapper examQuestionsMapper) {
		this.examQuestionsMapper = examQuestionsMapper;
	}

	public int insertExamQuestions(ExamQuestions examQuestions) {
        return examQuestionsMapper.insert(examQuestions);
    }

    public int deleteExamQuestions(ExamQuestions examQuestions) {
        return examQuestionsMapper.delete(examQuestions);
    }

    public int updateExamQuestions(Example example, ExamQuestions examQuestions) {
        return examQuestionsMapper.updateByExampleSelective(examQuestions, example);
    }

    public ExamQuestions selectOneExamQuestions(ExamQuestions examQuestions) {
        return examQuestionsMapper.selectOne(examQuestions);
    }


	/**
	 * 批量删除试题
	 * @param orgId 机构码
	 * @param list 试题Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteExamQuestionsByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(ExamQuestions.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, examQuestionsMapper.deleteByExample(example));
		}
	}
    /**
     * 按分类获取试题分类
     *
     * @param orgId 机构码
     * @param sortId 分类Id
     * @param keyword 查询关键词
     * @return 试题分类
     */
    public List<Map<String, String>> getExamQuestionsListBySortId(String orgId, String sortId,String examType, String keyword) {
        return examQuestionsMapper.getExamQuestionsListBySortId(orgId, sortId, examType,"%" + keyword + "%");
    }

    /**
     * 按分类获取试题分类
     *
     * @param pageParam 分页参数
     * @param sortId 分类Id
     * @return 试题分类
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getExamQuestionsListBySortId(PageParam pageParam, String sortId,String examType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamQuestionsListBySortId(pageParam.getOrgId(), sortId,examType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 按分类获取试题列表
     *
     * @param orgId 机构码
     * @param sortId 分类Id
     * @return 试题列表
     */
    public List<Map<String, String>> getExamQuestListForSelectBySortId(String orgId, String sortId) {
        return examQuestionsMapper.getExamQuestListForSelectBySortId(orgId, sortId);
    }

}
