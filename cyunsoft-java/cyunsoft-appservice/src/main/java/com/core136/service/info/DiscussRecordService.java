package com.core136.service.info;

import com.core136.bean.info.DiscussRecord;
import com.core136.mapper.info.DiscussRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DiscussRecordService {
    private DiscussRecordMapper discussRecordMapper;
	@Autowired
	public void setDiscussRecordMapper(DiscussRecordMapper discussRecordMapper) {
		this.discussRecordMapper = discussRecordMapper;
	}

	public int insertDiscussRecord(DiscussRecord discussRecord) {
        return discussRecordMapper.insert(discussRecord);
    }

    public int deleteDiscussRecord(DiscussRecord discussRecord) {
        return discussRecordMapper.delete(discussRecord);
    }

    public int updateDiscussRecord(Example example, DiscussRecord discussRecord) {
        return discussRecordMapper.updateByExampleSelective(discussRecord, example);
    }

    public DiscussRecord selectOneDiscussRecord(DiscussRecord discussRecord) {
        return discussRecordMapper.selectOne(discussRecord);
    }


    /**
     * 获取帖子与子帖
     *
     * @param orgId 机构码
     * @param recordId 帖子Id
     * @return 主题列表
     */
    public List<Map<String, String>> getDiscussRecordListById(String orgId, String accountId, String recordId) {
        return discussRecordMapper.getDiscussRecordListById(orgId, accountId, recordId);
    }

	public List<Map<String, String>> getDiscussUserList(String orgId, String discussId) {
		return discussRecordMapper.getDiscussUserList(orgId, discussId);
	}

	/**
	 * 获取讨论主题列表
	 * @param orgId 机构码
	 * @param discussId 讨论主题Id
	 * @param accountId 用户账号
	 * @param keyword 查询关键词
	 * @param page 页码
	 * @return 主题列表
	 */
    public List<Map<String, String>> getTopDiscussRecordList(String orgId, String accountId,String discussId,  String keyword,Integer page) {
        return discussRecordMapper.getTopDiscussRecordList(orgId, discussId, accountId, "%" + keyword + "%",page);
    }


}
