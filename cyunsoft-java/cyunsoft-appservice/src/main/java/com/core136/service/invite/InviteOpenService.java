package com.core136.service.invite;

import com.core136.bean.invite.InviteOpen;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteOpenMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteOpenService {
	private InviteOpenMapper inviteOpenMapper;
	@Autowired
	public void setInviteOpenMapper(InviteOpenMapper inviteOpenMapper) {
		this.inviteOpenMapper = inviteOpenMapper;
	}

	public int insertInviteOpen(InviteOpen inviteOpen) {
		return inviteOpenMapper.insert(inviteOpen);
	}

	public int deleteInviteOpen(InviteOpen inviteOpen) {
		return inviteOpenMapper.delete(inviteOpen);
	}

	public int updateInviteOpen(Example example,InviteOpen inviteOpen) {
		return inviteOpenMapper.updateByExampleSelective(inviteOpen,example);
	}

	public InviteOpen selectOneInviteOpen(InviteOpen inviteOpen) {
		return inviteOpenMapper.selectOne(inviteOpen);
	}

	/**
	 * 批量删除开标记录
	 * @param orgId 机构码
	 * @param list 开标记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteInviteOpenByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteOpen.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteOpenMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取开标记录
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 开标记录列表
	 */
	public List<Map<String,String>>getInviteOpenList(String orgId, String dateQueryType, String beginTime, String endTime, String keyword) {
		return inviteOpenMapper.getInviteOpenList(orgId,dateQueryType,beginTime,endTime,"%"+keyword+"%");
	}

	/**
	 * 获取开标记录
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 开标记录列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getInviteOpenList(PageParam pageParam, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getInviteOpenList(pageParam.getOrgId(),dateQueryType,beginTime,endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
