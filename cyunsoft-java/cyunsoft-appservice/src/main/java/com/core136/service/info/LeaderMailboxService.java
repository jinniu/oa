package com.core136.service.info;

import com.core136.bean.account.UserInfo;
import com.core136.bean.info.LeaderMailbox;
import com.core136.bean.info.LeaderMailboxConfig;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.LeaderMailboxMapper;
import com.core136.service.account.UserInfoService;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LeaderMailboxService {
    private LeaderMailboxMapper leaderMailboxMapper;
    private LeaderMailboxConfigService leaderMailboxConfigService;
    private UserInfoService userInfoService;
	@Autowired
	public void setLeaderMailboxMapper(LeaderMailboxMapper leaderMailboxMapper) {
		this.leaderMailboxMapper = leaderMailboxMapper;
	}
	@Autowired
	public void setLeaderMailboxConfigService(LeaderMailboxConfigService leaderMailboxConfigService) {
		this.leaderMailboxConfigService = leaderMailboxConfigService;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertLeaderMailbox(LeaderMailbox leaderMailbox) {
        return leaderMailboxMapper.insert(leaderMailbox);
    }

    public int deleteLeaderMailbox(LeaderMailbox leaderMailbox) {
        return leaderMailboxMapper.delete(leaderMailbox);
    }

    public int updateLeaderMailbox(Example example, LeaderMailbox leaderMailbox) {
        return leaderMailboxMapper.updateByExampleSelective(leaderMailbox, example);
    }

    public LeaderMailbox selectOneLeaderMailbox(LeaderMailbox leaderMailbox) {
        return leaderMailboxMapper.selectOne(leaderMailbox);
    }

    public List<Map<String, String>> getMyLeaderMailBoxList(String orgId, String status, String mailType, String dateQueryType, String beginTime, String endTime, String accountId, String keyword) {
        return leaderMailboxMapper.getMyLeaderMailBoxList(orgId,status,mailType,dateQueryType,beginTime,endTime,accountId, "%" + keyword + "%");
    }

    public PageInfo<Map<String, String>> getMyLeaderMailBoxList(PageParam pageParam,String status, String mailType, String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyLeaderMailBoxList(pageParam.getOrgId(),status,mailType,dateQueryType,beginTime,endTime, pageParam.getAccountId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    public List<Map<String, String>> getLeaderMailBoxList(String orgId, String status, String mailType, String dateQueryType, String beginTime, String endTime, String keyword) {
        return leaderMailboxMapper.getLeaderMailBoxList(orgId, status,mailType,dateQueryType,beginTime,endTime,"%" + keyword + "%");
    }

    public PageInfo<Map<String, String>> getLeaderMailBoxList(PageParam pageParam,String status, String mailType, String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeaderMailBoxList(pageParam.getOrgId(),status,mailType,dateQueryType,beginTime,endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    public RetDataBean setLeaderMailBox(UserInfo user, LeaderMailbox leaderMailbox) {
        LeaderMailboxConfig leaderMailboxConfig = new LeaderMailboxConfig();
        leaderMailboxConfig.setOrgId(leaderMailbox.getOrgId());
        leaderMailboxConfig = leaderMailboxConfigService.selectOneLeaderMailboxConfig(leaderMailboxConfig);
        int i = userInfoService.isInUserRole(user.getOrgId(), user.getAccountId(), leaderMailboxConfig.getUserRole(), leaderMailboxConfig.getDeptRole(), leaderMailboxConfig.getLevelRole());
        if (i == 0) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
        } else {
            if (!leaderMailboxConfig.getIsAnonymous().equals("1")) {
                leaderMailbox.setAccountId(user.getAccountId());
            } else {
                leaderMailbox.setAccountId("");
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, insertLeaderMailbox(leaderMailbox));
        }
    }


}
