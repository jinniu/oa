package com.core136.service.archives;

import com.core136.bean.archives.ArchivesLibrary;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesLibraryMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesLibraryService {
    private ArchivesLibraryMapper archivesLibraryMapper;
	@Autowired
	public void setArchivesLibraryMapper(ArchivesLibraryMapper archivesLibraryMapper) {
		this.archivesLibraryMapper = archivesLibraryMapper;
	}

	public int insertArchivesLibrary(ArchivesLibrary archivesLibrary)
    {
        return archivesLibraryMapper.insert(archivesLibrary);
    }

    public int deleteArchivesLibrary(ArchivesLibrary archivesLibrary)
    {
        return archivesLibraryMapper.delete(archivesLibrary);
    }

    public int updateArchivesLibrary(Example example,ArchivesLibrary archivesLibrary)
    {
        return archivesLibraryMapper.updateByExampleSelective(archivesLibrary,example);
    }

    public ArchivesLibrary selectOneArchivesLibrary(ArchivesLibrary archivesLibrary)
    {
        return archivesLibraryMapper.selectOne(archivesLibrary);
    }

    /**
     * 批量删除档案卷库
     * @param orgId 机构码
     * @param list
     * @return
     */
    public RetDataBean deleteArchivesLibraryIds(String orgId, List<String> list) {
        if (list.isEmpty()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            Example example = new Example(ArchivesLibrary.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesLibraryMapper.deleteByExample(example));
        }
    }

    /**
     * 获取档案库列表
     * @param orgId 机构码
     * @param status 状态
     * @param storagePeriod 保存年限
     * @param classifiedLevel 涉密等级
     * @param keyword 查询关键词
     * @return 档案库列表
     */
    public List<Map<String,String>>getArchivesLibraryList(String orgId, String status, Integer storagePeriod, String classifiedLevel, String keyword) {
        return archivesLibraryMapper.getArchivesLibraryList(orgId,status,storagePeriod,classifiedLevel,"%"+keyword+"%");
    }

    /**
     * 获取档案库列表
     * @param pageParam 分页参数
     * @param status 状态
     * @param storagePeriod 保存年限
     * @param classifiedLevel 涉密等级
     * @return 档案库列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getArchivesLibraryList(PageParam pageParam, String status, Integer storagePeriod, String classifiedLevel) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesLibraryList(pageParam.getOrgId(),status, storagePeriod, classifiedLevel, pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }


}
