package com.core136.service.finance;

import com.core136.bean.finance.ContractPayable;
import com.core136.bean.finance.ContractPayableRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ContractPayableRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ContractPayableRecordService {
    private ContractPayableRecordMapper contractPayableRecordMapper;
    private ContractPayableService contractPayableService;
	@Autowired
	public void setContractPayableRecordMapper(ContractPayableRecordMapper contractPayableRecordMapper) {
		this.contractPayableRecordMapper = contractPayableRecordMapper;
	}
	@Autowired
	public void setContractPayableService(ContractPayableService contractPayableService) {
		this.contractPayableService = contractPayableService;
	}

	/**
	 * 添加付款记录
	 * @param contractPayableRecord 付款记录对象
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
    public RetDataBean payableAmount(ContractPayableRecord contractPayableRecord) {
        ContractPayable contractPayable = new ContractPayable();
        contractPayable.setPayableId(contractPayableRecord.getPayableId());
        contractPayable.setOrgId(contractPayableRecord.getOrgId());
        contractPayable = contractPayableService.selectOneContractPayable(contractPayable);
        Double payabled = contractPayable.getPayabled();
        Double unPayabled = contractPayable.getUnPayabled();
        if (unPayabled < contractPayableRecord.getAmount()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
        } else {
            unPayabled = unPayabled - contractPayableRecord.getAmount();
            payabled = payabled + contractPayableRecord.getAmount();
            ContractPayable contractPayable1 = new ContractPayable();
            contractPayable1.setPayableId(contractPayable.getPayableId());
            contractPayable1.setUnPayabled(unPayabled);
            contractPayable1.setPayabled(payabled);
            contractPayable1.setOrgId(contractPayable.getOrgId());
            Example example = new Example(ContractPayable.class);
            example.createCriteria().andEqualTo("orgId", contractPayable.getOrgId()).andEqualTo("payableId", contractPayable1.getPayableId());
            contractPayableService.updateContractPayable(contractPayable1, example);
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertContractPayableRecord(contractPayableRecord));
        }
    }


    public int insertContractPayableRecord(ContractPayableRecord contractPayableRecord) {
        return contractPayableRecordMapper.insert(contractPayableRecord);
    }

    public int deleteContractPayableRecord(ContractPayableRecord contractPayableRecord) {
        return contractPayableRecordMapper.delete(contractPayableRecord);
    }

    public int updateContractPayableRecord(Example example, ContractPayableRecord contractPayableRecord) {
        return contractPayableRecordMapper.updateByExampleSelective(contractPayableRecord, example);
    }

    public ContractPayableRecord selectOneContractPayableRecord(ContractPayableRecord contractPayableRecord) {
        return contractPayableRecordMapper.selectOne(contractPayableRecord);
    }

    /**
     * 获取付款记录列表
     * @param orgId 机构码
     * @param payableId 应付款记录Id
     * @return 付款记录列表
     */
    public List<Map<String, String>> getContractPayableRecordList(String orgId, String payableId) {
        return contractPayableRecordMapper.getContractPayableRecordList(orgId, payableId);
    }

    /**
     * 获取付款记录
     * @param pageParam 分页参数
     * @param payableId 应付款记录Id
     * @return 付款记录列表
     */
    public PageInfo<Map<String, String>> getContractPayableRecordList(PageParam pageParam, String payableId) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractPayableRecordList(pageParam.getOrgId(), payableId);
		return new PageInfo<>(datalist);
    }

    /**
     * 近期付款记录
     * @param orgId 机构码
     * @return 付款记录列表
     */
    public List<Map<String, String>> getPayableRecordTop(String orgId) {
        return contractPayableRecordMapper.getPayableRecordTop(orgId);
    }
    /**
     * 查询历史付款记录列表
     * @param orgId 机构码
     * @param payReceivedType 付款种类
     * @param dateQueryType 日期范围
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 付款记录列表
     */
    public List<Map<String, String>> getAllContractPayableRecordList(String orgId,String payReceivedType,String dateQueryType,String beginTime,String endTime,String keyword)
    {
        return contractPayableRecordMapper.getAllContractPayableRecordList(orgId,payReceivedType,dateQueryType,beginTime,endTime,"%"+keyword+"%");
    }

    /**
     * 获取历史付款记录
     * @param pageParam 分页参数
     * @param payReceivedType 付款种类
     * @param date 日期范围
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 付款记列表
     */
    public PageInfo<Map<String, String>> getAllContractPayableRecordList(PageParam pageParam, String payReceivedType,String date,String beginTime,String endTime) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAllContractPayableRecordList(pageParam.getOrgId(), payReceivedType,date,beginTime,endTime,pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }

}
