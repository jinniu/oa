package com.core136.service.office;

import com.core136.bean.office.OfficeSuppliesGrant;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.OfficeSuppliesGrantMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 办公用品发放
 * @author lsq
 */

@Service
public class OfficeSuppliesGrantService {
    private OfficeSuppliesGrantMapper officeSuppliesGrantMapper;
	@Autowired
	public void setOfficeSuppliesGrantMapper(OfficeSuppliesGrantMapper officeSuppliesGrantMapper) {
		this.officeSuppliesGrantMapper = officeSuppliesGrantMapper;
	}

	public int insertOfficeSuppliesGrant(OfficeSuppliesGrant officeSuppliesGrant) {
        return officeSuppliesGrantMapper.insert(officeSuppliesGrant);
    }

    public int deleteOfficeSuppliesGrant(OfficeSuppliesGrant officeSuppliesGrant) {
        return officeSuppliesGrantMapper.delete(officeSuppliesGrant);
    }

    public int updateOfficeSuppliesGrant(Example example, OfficeSuppliesGrant officeSuppliesGrant) {
        return officeSuppliesGrantMapper.updateByExampleSelective(officeSuppliesGrant, example);
    }

    public OfficeSuppliesGrant selectOneOfficeSuppliesGrant(OfficeSuppliesGrant officeSuppliesGrant) {
        return officeSuppliesGrantMapper.selectOne(officeSuppliesGrant);
    }

    /**
     * 获取办公用品发放列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 办公用品发放列表
     */
    public List<Map<String, String>> getGrantOfficeList(String orgId, String accountId, String dateQueryType,String beginTime, String endTime) {
        return officeSuppliesGrantMapper.getGrantOfficeList(orgId, accountId,dateQueryType, beginTime, endTime);
    }

    /**
     * 获取办公用品发放列表
     * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return 办公用品发放列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getGrantOfficeList(PageParam pageParam, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getGrantOfficeList(pageParam.getOrgId(), pageParam.getAccountId(), dateQueryType,beginTime, endTime);
		return new PageInfo<>(datalist);
    }

	/**
	 * 统计已发放了多少办公用品
	 * @param orgId 机构码
	 * @param applyId 申请记录Id
	 * @return 已发放数量
	 */
	public int getGrantCount(String orgId, String applyId) {
        try {
            return officeSuppliesGrantMapper.getGrantCount(orgId, applyId);
        } catch (Exception e) {
            return 0;
        }
    }

}
