package com.core136.service.info;

import com.core136.bean.info.DataUploadHandle;
import com.core136.bean.info.DataUploadInfo;
import com.core136.mapper.info.DataUploadHandleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class DataUploadHandleService {
    private DataUploadHandleMapper dataUploadHandleMapper;
    private DataUploadInfoService dataUploadInfoService;
	@Autowired
	public void setDataUploadHandleMapper(DataUploadHandleMapper dataUploadHandleMapper) {
		this.dataUploadHandleMapper = dataUploadHandleMapper;
	}
	@Autowired
	public void setDataUploadInfoService(DataUploadInfoService dataUploadInfoService) {
		this.dataUploadInfoService = dataUploadInfoService;
	}

	public int processDataInfo(DataUploadHandle dataUploadHandle) {
        DataUploadInfo dataUploadInfo = new DataUploadInfo();
        dataUploadInfo.setOrgId(dataUploadHandle.getOrgId());
        dataUploadInfo.setRecordId(dataUploadInfo.getRecordId());
        dataUploadInfo.setStatus("1");
        Example example = new Example(DataUploadInfo.class);
        example.createCriteria().andEqualTo("recordId", dataUploadInfo.getRecordId()).andEqualTo("orgId", dataUploadInfo.getOrgId());
        dataUploadInfoService.updateDataUploadInfo(example, dataUploadInfo);
        return insertDataUploadHandle(dataUploadHandle);
    }

    public int insertDataUploadHandle(DataUploadHandle dataUploadHandle) {
        return dataUploadHandleMapper.insert(dataUploadHandle);
    }

    public int deleteDataUploadHandle(DataUploadHandle dataUploadHandle) {
        return dataUploadHandleMapper.delete(dataUploadHandle);
    }

    public int updateDataUploadHandle(Example example, DataUploadHandle dataUploadHandle) {
        return dataUploadHandleMapper.updateByExampleSelective(dataUploadHandle, example);
    }

    public DataUploadHandle selectOneDataUploadHandle(DataUploadHandle dataUploadHandle) {
        return dataUploadHandleMapper.selectOne(dataUploadHandle);
    }

    /**
     * 获取处理结果列表
     * @param dataUploadHandle
     * @return
     */
    public List<DataUploadHandle> getDataUploadHandleList(DataUploadHandle dataUploadHandle) {
        return dataUploadHandleMapper.select(dataUploadHandle);
    }
}
