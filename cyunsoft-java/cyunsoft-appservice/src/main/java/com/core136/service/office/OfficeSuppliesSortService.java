package com.core136.service.office;

import com.core136.bean.office.OfficeSuppliesSort;
import com.core136.mapper.office.OfficeSuppliesSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author lsq
 */
@Service
public class OfficeSuppliesSortService {
    private OfficeSuppliesSortMapper officeSuppliesSortMapper;

	@Autowired
	public void setOfficeSuppliesSortMapper(OfficeSuppliesSortMapper officeSuppliesSortMapper) {
		this.officeSuppliesSortMapper = officeSuppliesSortMapper;
	}

	public int insertOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
        return officeSuppliesSortMapper.insert(officeSuppliesSort);
    }

    public int deleteOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
        return officeSuppliesSortMapper.delete(officeSuppliesSort);
    }

    public int updateOfficeSuppliesSort(Example example, OfficeSuppliesSort officeSuppliesSort) {
        return officeSuppliesSortMapper.updateByExampleSelective(officeSuppliesSort, example);
    }

    public OfficeSuppliesSort selectOneOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
        return officeSuppliesSortMapper.selectOne(officeSuppliesSort);
    }

	/**
	 * 获取办公用品分类
	 * @param orgId 机构码
	 * @return
	 */
	public List<OfficeSuppliesSort> getOfficeSuppliesSortTree(String orgId) {
		List<OfficeSuppliesSort> list = getAllOfficeSuppliesSort(orgId);
		List<OfficeSuppliesSort> officeSuppliesSortList = new ArrayList<>();
		for (OfficeSuppliesSort suppliesSort : list) {
			if (suppliesSort.getParentId().equals("0")) {
				officeSuppliesSortList.add(suppliesSort);
			}
		}
		for (OfficeSuppliesSort officeSuppliesSort : officeSuppliesSortList) {
			officeSuppliesSort.setChildren(getChildOfficeSuppliesSortList(officeSuppliesSort.getSortId(), list));
		}
		return officeSuppliesSortList;
	}

	/**
	 * 获取分类子表
	 * @param sortId 分类Id
	 * @param rootOfficeSuppliesSort 分类对象
	 * @return 分类子表
	 */
	public List<OfficeSuppliesSort> getChildOfficeSuppliesSortList(String sortId, List<OfficeSuppliesSort> rootOfficeSuppliesSort) {
		List<OfficeSuppliesSort> childList = new ArrayList<>();
		for (OfficeSuppliesSort officeSuppliesSort : rootOfficeSuppliesSort) {
			if (officeSuppliesSort.getParentId().equals(sortId)) {
				childList.add(officeSuppliesSort);
			}
		}
		for (OfficeSuppliesSort officeSuppliesSort : childList) {
			officeSuppliesSort.setChildren(getChildOfficeSuppliesSortList(officeSuppliesSort.getSortId(), rootOfficeSuppliesSort));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	/**
	 * 判断分类下是否有子节点
	 * @param orgId 机构码
	 * @param parentId 分类父级Id
	 * @return 是否有子节点
	 */
    public boolean isExistChild(String orgId, String parentId) {
		OfficeSuppliesSort officeSuppliesSort = new OfficeSuppliesSort();
		officeSuppliesSort.setOrgId(orgId);
		officeSuppliesSort.setParentId(parentId);
		return officeSuppliesSortMapper.selectCount(officeSuppliesSort) > 0;
    }

    public List<OfficeSuppliesSort> getAllOfficeSuppliesSort(String orgId) {
		OfficeSuppliesSort officeSuppliesSort = new OfficeSuppliesSort();
		officeSuppliesSort.setOrgId(orgId);
		return officeSuppliesSortMapper.select(officeSuppliesSort);
	}
}
