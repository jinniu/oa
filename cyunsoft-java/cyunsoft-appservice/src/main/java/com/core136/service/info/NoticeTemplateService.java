package com.core136.service.info;

import com.core136.bean.info.NoticeTemplate;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.NoticeTemplateMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 通知公告模版服务类
 * @author lsq
 */
@Service
public class NoticeTemplateService {
    private NoticeTemplateMapper noticeTemplateMapper;
	@Autowired
	public void setNoticeTemplateMapper(NoticeTemplateMapper noticeTemplateMapper) {
		this.noticeTemplateMapper = noticeTemplateMapper;
	}

	/**
	 * 创建通知公告模版
	 * @param noticeTemplate 通知公告模版对象
	 * @return 成功创建记录数
	 */
    public int insertNoticeTemplate(NoticeTemplate noticeTemplate) {
        return noticeTemplateMapper.insert(noticeTemplate);
    }

	/**
	 * 更新通知公告模版
	 * @param noticeTemplate 通知公告模版对象
	 * @param example 更新条件
	 * @return 成功更新记录数
	 */
    public int updateNoticeTemplate(NoticeTemplate noticeTemplate, Example example) {
        return noticeTemplateMapper.updateByExampleSelective(noticeTemplate, example);
    }

	/**
	 * 删除通知公告模版
	 * @param noticeTemplate 通知公告模版对象
	 * @return 成功删除记录数
	 */
    public int deleteNoticeTemplate(NoticeTemplate noticeTemplate) {
        return noticeTemplateMapper.delete(noticeTemplate);
    }

	/**
	 * 批量删除通知公告模版
	 * @param orgId 机构码
	 * @param list 公告模版Id 列表
	 * @return 成功删除记录数
	 */
	public RetDataBean deleteNoticeTemplateByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(NoticeTemplate.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("templateId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, noticeTemplateMapper.deleteByExample(example));
		}
	}

	/**
	 * 查询一个通知公告模版
	 * @param noticeTemplate 通知公告模版对象
	 * @return 通知公告模版对象
	 */
    public NoticeTemplate selectOneNoticeTemplate(NoticeTemplate noticeTemplate) {
        return noticeTemplateMapper.selectOne(noticeTemplate);
    }

	/**
	 * 获取通知公告模版
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 公告模版列表
	 */
    public List<Map<String, String>> getNoticeTemplateList(String orgId, String keyword) {
        return noticeTemplateMapper.getNoticeTemplateList(orgId, "%" + keyword + "%");
    }

	/**
	 * 获取通知公告模版
	 * @param pageParam 分页参数
	 * @return 公告模版列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getNoticeTemplateList(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getNoticeTemplateList(pageParam.getOrgId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 按分类获取红头列表
	 * @param orgId 机构码
	 * @param noticeType 公告类型
	 * @return 红头列表
	 */
    public List<Map<String, String>> getNoticeTemplateListByType(String orgId, String noticeType) {
        return noticeTemplateMapper.getNoticeTemplateListByType(orgId, noticeType);
    }
}
