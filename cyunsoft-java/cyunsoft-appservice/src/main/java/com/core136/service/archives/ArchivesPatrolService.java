package com.core136.service.archives;

import com.core136.bean.archives.ArchivesPatrol;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesPatrolMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesPatrolService {
    private ArchivesPatrolMapper archivesPatrolMapper;
	@Autowired
	public void setArchivesPatrolMapper(ArchivesPatrolMapper archivesPatrolMapper) {
		this.archivesPatrolMapper = archivesPatrolMapper;
	}

	/**
     * 创建巡检记录
     * @param archivesPatrol 巡检记录对象
     * @return 成功创建记录数
     */
    public int insertArchivesPatrol(ArchivesPatrol archivesPatrol)
    {
        return archivesPatrolMapper.insert(archivesPatrol);
    }

    /**
     * 删除巡检记录
     * @param archivesPatrol 巡检记录对象
     * @return 成功删除记录数
     */
    public int deleteArchivesPatrol(ArchivesPatrol archivesPatrol)
    {
        return archivesPatrolMapper.delete(archivesPatrol);
    }

    /**
     * 更新巡检记录
     * @param example 更新条件
     * @param archivesPatrol 巡检记录对象
     * @return 成功更新记录数
     */
    public int updateArchivesPatrol(Example example,ArchivesPatrol archivesPatrol)
    {
        return archivesPatrolMapper.updateByExampleSelective(archivesPatrol,example);
    }

    /**
     *  查询单个巡检记录
     * @param archivesPatrol 巡检记录对象
     * @return 巡检记录对象
     */
    public ArchivesPatrol selectOneArchivesPatrol(ArchivesPatrol archivesPatrol)
    {
        return archivesPatrolMapper.selectOne(archivesPatrol);
    }

    /**
     * 批量删除巡检记录
     * @param orgId 机构码
     * @param list 巡检记录Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteArchivesPatrolByIds(String orgId, List<String> list) {
        if (list.isEmpty()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            Example example = new Example(ArchivesPatrol.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesPatrolMapper.deleteByExample(example));
        }
    }

    /**
     * 获取档案巡检记录列表
     * @param orgId 机构码
     * @param opFlag 管理员标识
     * @param accountId 用户账号
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param patrolType 巡检类型
     * @param keyword 查询关键词
     * @return 巡检记录列表
     */
    public List<Map<String,String>>getArchivesPatrolList(String orgId, String opFlag, String accountId,String dateQueryType, String beginTime, String endTime, String patrolType, String keyword)
    {
        return archivesPatrolMapper.getArchivesPatrolList(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,patrolType,"%"+keyword+"%");
    }

    /**
     * 获取档案巡检记录列表
     * @param pageParam 分页参数
     * @param patrolType 巡检类型
     * @param date 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结构时间
     * @return 巡检记录列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getArchivesPatrolList(PageParam pageParam, String patrolType, String date, String beginTime, String endTime) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesPatrolList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),date,beginTime,endTime,patrolType, pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }


}
