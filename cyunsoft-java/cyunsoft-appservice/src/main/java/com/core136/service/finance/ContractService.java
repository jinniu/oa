package com.core136.service.finance;

import com.core136.bean.finance.Contract;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ContractMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class ContractService {
    private ContractMapper contractMapper;
	@Autowired
	public void setContractMapper(ContractMapper contractMapper) {
		this.contractMapper = contractMapper;
	}

	public Contract selectOneContract(Contract contract) {
        return contractMapper.selectOne(contract);
    }

    public int insertContract(Contract contract) {
        return contractMapper.insert(contract);
    }

    public int deleteContract(Contract contract) {
        return contractMapper.delete(contract);
    }

    /**
     * 批量删除合同
     * @param orgId
     * @param list
     * @return
     */
    public RetDataBean deleteContractByIds(String orgId, List<String> list)
    {
        if(list.isEmpty())
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(Contract.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("contractId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, contractMapper.deleteByExample(example));
        }
    }
    public int updateContract(Contract contract, Example example) {
        return contractMapper.updateByExampleSelective(contract, example);
    }

    /**
     * 获取年分的合同总数
     */

    public int getContractCount(String orgId) {
        //  Auto-generated method stub
        return contractMapper.getContractCount(orgId);
    }

    public Map<String,String>getContractById(String orgId,String contractId)
    {
        return contractMapper.getContractById(orgId,contractId);
    }

    /**
     * 合同查询
     * @param orgId 要构码
     * @param sortId 分类Id
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param contractType 合同类型
     * @param keyword 查询关键词
     * @return 合同列表
     */
    public List<Map<String, String>> queryContract(String orgId,String sortId, String dateQueryType,String beginTime, String endTime, String contractType, String keyword) {
        return contractMapper.queryContract(orgId, sortId,dateQueryType,beginTime, endTime, contractType, "%" + keyword + "%");
    }

    /**
     * 合同查询
     * @param pageParam 分页参数
     * @param sortId 分类Id
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param contractType 合同类型
     * @return 合同列表
     */
    public PageInfo<Map<String, String>> queryContract(PageParam pageParam, String sortId, String dateQueryType,String beginTime, String endTime, String contractType) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = queryContract(pageParam.getOrgId(),sortId, dateQueryType,beginTime, endTime, contractType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取合同管理列表
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param sortId
     * @param dateQueryType
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param contractType
     * @param keyword
     * @return
     */
    public List<Map<String, String>> getContractManageList(String orgId, String opFlag, String accountId,String sortId,String dateQueryType, String beginTime, String endTime, String contractType, String keyword) {
        return contractMapper.getContractManageList(orgId, opFlag, accountId, sortId,dateQueryType,beginTime, endTime, contractType, "%" + keyword + "%");
    }
    /**
     *  获取合同管理列表
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getContractForSelect(String orgId,String keyword) {
        return contractMapper.getContractForSelect(orgId,"%"+keyword+"%");
    }

    /**
     *  获取合同管理列表
     * @param pageParam 分页参数
     * @return
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getContractForSelect(PageParam pageParam) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractForSelect(pageParam.getOrgId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }
    /**
     * 获取合同管理列表
     * @param pageParam 分页参数
     * @param sortId 分类Id
     * @param dateQueryType 日期类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param contractType 合同类型
     * @return 合同管理列表
     */
    public PageInfo<Map<String, String>> getContractManageList(PageParam pageParam,String sortId, String dateQueryType,String beginTime, String endTime, String contractType) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractManageList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), sortId,dateQueryType,beginTime, endTime, contractType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取近期的合同列表
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getContractTop(String orgId) {
        return contractMapper.getContractTop(orgId);
    }
}
