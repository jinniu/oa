package com.core136.service.office;

import com.core136.bean.office.MeetingNotes;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.MeetingNotesMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class MeetingNotesService {
    private MeetingNotesMapper meetingNotesMapper;
	@Autowired
	public void setMeetingNotesMapper(MeetingNotesMapper meetingNotesMapper) {
		this.meetingNotesMapper = meetingNotesMapper;
	}

	public int insertMeetingNotes(MeetingNotes meetingNotes) {
        return meetingNotesMapper.insert(meetingNotes);
    }

    public int deleteMeetingNotes(MeetingNotes meetingNotes) {
        return meetingNotesMapper.delete(meetingNotes);
    }

	/**
	 * 批量删除会议纪要
	 * @param orgId 机构码
	 * @param list 会议纪要Id 列表
	 * @return 会议纪要
	 */
	public RetDataBean deleteMeetingNotesByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(MeetingNotes.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("notesId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, meetingNotesMapper.deleteByExample(example));
		}
	}

    public int updateMeetingNotes(Example example, MeetingNotes meetingNotes) {
        return meetingNotesMapper.updateByExampleSelective(meetingNotes, example);
    }

    public MeetingNotes selectOneMeetingNotes(MeetingNotes meetingNotes) {
        return meetingNotesMapper.selectOne(meetingNotes);
    }

    /**
     * 获取会议记要列表
     * @param orgId 机构码
     * @param opFlag 管理员标识
     * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 会议记要列表
     */
    public List<Map<String, String>> getMeetingNotesList(String orgId, String opFlag, String accountId,String dateQueryType, String beginTime, String endTime, String keyword) {
        return meetingNotesMapper.getMeetingNotesList(orgId, opFlag, accountId,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	public List<Map<String, String>> getMyMeetingNotesListForDesk(String orgId, String accountId, String deptId, String levelId) {
		return meetingNotesMapper.getMyMeetingNotesListForDesk(orgId, accountId, deptId, levelId);
	}
	/**
	 * 会议记要查询
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param levelId 用户行政Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 会议记要列表
	 */
    public List<Map<String, String>> queryMeetingNotesList(String orgId, String opFlag,String meetingType, String accountId, String deptId, String levelId,String dateQueryType, String beginTime, String endTime, String keyword) {
        return meetingNotesMapper.queryMeetingNotesList(orgId, opFlag,meetingType, accountId, deptId, levelId,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }


    /**
     *  获取会议记要列表
     * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return 会议记要
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getMeetingNotesList(PageParam pageParam, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMeetingNotesList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 会议记要查询
	 * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 会议记要
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> queryMeetingNotesList(PageParam pageParam,String meetingType, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = queryMeetingNotesList(pageParam.getOrgId(), pageParam.getOpFlag(),meetingType, pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(),dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取会议记要详情
	 * @param orgId 机构码
	 * @param notesId 会议记要Id
	 * @return 会议记要详情
	 */
	public Map<String, String> getMeetingNotesInfo(String orgId, String notesId) {
        return meetingNotesMapper.getMeetingNotesInfo(orgId, notesId);
    }

}
