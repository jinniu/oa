package com.core136.service.office;

import com.core136.bean.office.LicenceType;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.office.LicenceTypeMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class LicenceTypeService {
    private LicenceTypeMapper licenceTypeMapper;
	@Autowired
	public void setLicenceTypeMapper(LicenceTypeMapper licenceTypeMapper) {
		this.licenceTypeMapper = licenceTypeMapper;
	}

	public int insertLicenceType(LicenceType licenceType)
    {
        return licenceTypeMapper.insert(licenceType);
    }

    public int deleteLicenceType(LicenceType licenceType)
    {
        return licenceTypeMapper.delete(licenceType);
    }

    public int updateLicenceType(Example example,LicenceType licenceType)
    {
        return licenceTypeMapper.updateByExampleSelective(licenceType,example);
    }

    public LicenceType selectOneLicenceType(LicenceType licenceType){
        return licenceTypeMapper.selectOne(licenceType);
    }

	/**
	 * 获取分类列表
	 * @param orgId 机构码
	 * @return 分类列表
	 */
	public List<Map<String,String>>getLicenceTypeList(String orgId) {
		return licenceTypeMapper.getLicenceTypeList(orgId);
	}


	/**
	 * 批量删除证照分类
	 * @param orgId 机构码
	 * @param list 分类Id 列表
	 * @return 分类列表
	 */
	public RetDataBean deleteLicenceTypeByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(LicenceType.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("typeId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, licenceTypeMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取所有分类
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 所有分类
	 */
	public List<LicenceType> getAllLicenceType(String orgId, String keyword) {
		if(StringUtils.isBlank(keyword)) {
			keyword="";
		}
		return licenceTypeMapper.getAllLicenceType(orgId,"%"+keyword+"%");
	}

	/**
	 * 获取分类子表
	 * @param typeId 类型Id
	 * @param rootLicenceType 分类证照类型列表
	 * @return 分类子表列表
	 */
	public List<LicenceType> getChildTypeList(String typeId, List<LicenceType> rootLicenceType) {
		List<LicenceType> childList = new ArrayList<>();
		for (LicenceType licenceType : rootLicenceType) {
			if (licenceType.getParentId().equals(typeId)) {
				childList.add(licenceType);
			}
		}
		for (LicenceType proSort : childList) {
			proSort.setChildren(getChildTypeList(proSort.getTypeId(), rootLicenceType));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	/**
	 * 获取分类树型结构
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 分类树型结构
	 */
	public List<LicenceType> getLicenceTypeTree(String orgId,String keyword) {
		List<LicenceType> list = getAllLicenceType(orgId,keyword);
		List<LicenceType> LicenceTypeList = new ArrayList<>();
		for (LicenceType type : list) {
			if (type.getParentId().equals("0")) {
				LicenceTypeList.add(type);
			}
		}
		for (LicenceType licenceType : LicenceTypeList) {
			licenceType.setChildren(getChildTypeList(licenceType.getTypeId(), list));
		}
		return LicenceTypeList;
	}

}
