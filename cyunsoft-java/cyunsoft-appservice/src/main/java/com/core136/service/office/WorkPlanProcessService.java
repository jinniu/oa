package com.core136.service.office;

import com.core136.bean.office.WorkPlanProcess;
import com.core136.mapper.office.WorkPlanProcessMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class WorkPlanProcessService {
    private WorkPlanProcessMapper workPlanProcessMapper;
	@Autowired
	public void setWorkPlanProcessMapper(WorkPlanProcessMapper workPlanProcessMapper) {
		this.workPlanProcessMapper = workPlanProcessMapper;
	}

	public int insertWorkPlanProcess(WorkPlanProcess workPlanProcess) {
        return workPlanProcessMapper.insert(workPlanProcess);
    }

    public int deleteWorkPlanProcess(WorkPlanProcess workPlanProcess) {
        return workPlanProcessMapper.delete(workPlanProcess);
    }

    public int updateWorkPlanProcess(Example example, WorkPlanProcess workPlanProcess) {
        return workPlanProcessMapper.updateByExampleSelective(workPlanProcess, example);
    }

    public WorkPlanProcess selectOneWorkPlanProcess(WorkPlanProcess workPlanProcess) {
        return workPlanProcessMapper.selectOne(workPlanProcess);
    }

	/**
	 * 获取工作计划处理反馈列表
	 * @param workPlanProcess
	 * @return
	 */
	public List<WorkPlanProcess> getWorkPlanProcessList(WorkPlanProcess workPlanProcess) {
        return workPlanProcessMapper.select(workPlanProcess);
    }

}
