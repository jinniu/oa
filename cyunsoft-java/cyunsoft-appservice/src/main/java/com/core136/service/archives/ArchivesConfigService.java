package com.core136.service.archives;

import com.core136.bean.account.UserInfo;
import com.core136.bean.archives.ArchivesConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

/**
 * 档案参数配置操作服务类
 * @author lsq
 */
@Service
public class ArchivesConfigService {
	private ArchivesConfigMapper archivesConfigMapper;
	@Autowired
	public void setArchivesConfigMapper(ArchivesConfigMapper archivesConfigMapper) {
		this.archivesConfigMapper = archivesConfigMapper;
	}

	/**
	 * 创建档案参数配置
	 * @param archivesConfig 档案参数配置对象
	 * @return 成功创建记录数
	 */
	public int insertArchivesConfig(ArchivesConfig archivesConfig)
	{
		return archivesConfigMapper.insert(archivesConfig);
	}

	/**
	 * 删除档案参数配置
	 * @param archivesConfig 档案参数配置对象
	 * @return 成功删除记录数
	 */
	public int deleteArchivesConfig(ArchivesConfig archivesConfig)
	{
		return archivesConfigMapper.delete(archivesConfig);
	}

	/**
	 * 更新档案参数配置
	 * @param example 更新条件
	 * @param archivesConfig 档案参数配置对象
	 * @return 成功更新记录数
	 */
	public int updateArchivesConfig(Example example,ArchivesConfig archivesConfig)
	{
		return archivesConfigMapper.updateByExampleSelective(archivesConfig,example);
	}

	/**
	 * 查询单个档案参数配置
	 * @param archivesConfig 档案参数配置对象
	 * @return 档案参数配置对象
	 */
	public ArchivesConfig selectOneArchivesConfig(ArchivesConfig archivesConfig)
	{
		return  archivesConfigMapper.selectOne(archivesConfig);
	}
	/**
	 * 设置档案参数
	 * @param archivesConfig 档案参数配置对象
	 * @return 消息结构
	 */
	public RetDataBean setArchivesConfig(UserInfo userInfo, ArchivesConfig archivesConfig)
	{
		ArchivesConfig archivesConfig1 = new ArchivesConfig();
		archivesConfig1.setOrgId(userInfo.getOrgId());
		if(archivesConfigMapper.selectCount(archivesConfig1)>0)
		{
			Example example = new Example(ArchivesConfig.class);
			example.createCriteria();
			archivesConfigMapper.updateByExample(archivesConfig, example);
		}else
		{
			archivesConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			archivesConfig.setCreateUser(userInfo.getAccountId());
			archivesConfigMapper.insert(archivesConfig);
		}
		return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
	}

	/**
	 * 判断当前用户是否为可借阅，当前用户创建则可借阅
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @return 是否可借阅
	 */
	public int getBorrowingRoleFlag(String orgId,String accountId,String deptId,String userLevel)
	{
		return archivesConfigMapper.getBorrowingRoleFlag(orgId,accountId,deptId,userLevel);
	}


}
