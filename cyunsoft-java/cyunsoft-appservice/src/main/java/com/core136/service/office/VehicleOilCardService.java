package com.core136.service.office;

import com.core136.bean.info.News;
import com.core136.bean.office.VehicleOilCard;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.VehicleOilCardMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class VehicleOilCardService {
    private VehicleOilCardMapper vehicleOilCardMapper;
	@Autowired
	public void setVehicleOilCardMapper(VehicleOilCardMapper vehicleOilCardMapper) {
		this.vehicleOilCardMapper = vehicleOilCardMapper;
	}

	public int insertVehicleOilCard(VehicleOilCard vehicleOilCard) {
        return vehicleOilCardMapper.insert(vehicleOilCard);
    }

    public int deleteVehicleOilCard(VehicleOilCard vehicleOilCard) {
        return vehicleOilCardMapper.delete(vehicleOilCard);
    }

    public int updateVehicleOilCard(Example example, VehicleOilCard vehicleOilCard) {
        return vehicleOilCardMapper.updateByExampleSelective(vehicleOilCard, example);
    }

    public VehicleOilCard selectOneVehicleOilCard(VehicleOilCard vehicleOilCard) {
        return vehicleOilCardMapper.selectOne(vehicleOilCard);
    }

	/**
	 * 批量删除油卡信息
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteVehicleOilCardByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(News.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("cardId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, vehicleOilCardMapper.deleteByExample(example));
		}
	}
	/**
	 * 获取油卡列表
	 * @param orgId
	 * @param oilType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
    public List<Map<String, String>> getVehicleOilCardList(String orgId, String oilType,String dateQueryType, String beginTime, String endTime, String keyword) {
        return vehicleOilCardMapper.getVehicleOilCardList(orgId, oilType, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取油卡列表
	 * @param pageParam 分页参数
	 * @param oilType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getVehicleOilCardList(PageParam pageParam, String oilType, String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleOilCardList(pageParam.getOrgId(), oilType,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取可用油卡列表
	 * @param orgId
	 * @return
	 */
	public List<Map<String, String>> getCanUsedOilCardList(String orgId) {
        return vehicleOilCardMapper.getCanUsedOilCardList(orgId);
    }
}
