package com.core136.service.invite;

import com.core136.bean.invite.InviteRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteRecordService {
    private InviteRecordMapper inviteRecordMapper;
	@Autowired
	public void setInviteRecordMapper(InviteRecordMapper inviteRecordMapper) {
		this.inviteRecordMapper = inviteRecordMapper;
	}

	public int insertInviteRecord(InviteRecord inviteRecord)
    {
        return inviteRecordMapper.insert(inviteRecord);
    }

    public int deleteInviteRecord(InviteRecord inviteRecord)
    {
        return inviteRecordMapper.delete(inviteRecord);
    }

    public int updateInviteRecord(Example example,InviteRecord inviteRecord)
    {
        return inviteRecordMapper.updateByExampleSelective(inviteRecord,example);
    }

    public InviteRecord selectOneInviteRecord(InviteRecord inviteRecord)
    {
        return inviteRecordMapper.selectOne(inviteRecord);
    }

	/**
	 * 批量删除招标信息
	 * @param orgId 机构码
	 * @param list 招标信息Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteInviteRecordByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteRecordMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取招标信息选择列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 选择列表
	 */
	public List<Map<String,String>>getInviteRecordListForSelect(String orgId,String keyword) {
		return inviteRecordMapper.getInviteRecordListForSelect(orgId,"%"+keyword+"%");
	}

	/**
	 * 获取招标信息列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param inviteType 招标类型
	 * @param keyword 查询关键词
	 * @return 招标信息列表
	 */
	public List<Map<String,String>>getInviteRecordList(String orgId,String opFlag,String accountId,String dateQueryType,String beginTime,String endTime,String status,String inviteType,String keyword) {
		return inviteRecordMapper.getInviteRecordList(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,status,inviteType,"%"+keyword+"%");
	}

	/**
	 * 获取招标信息列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param inviteType 招标类型
	 * @return 招标信息列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getInviteRecordList(PageParam pageParam, String dateQueryType,String beginTime,String endTime,String status,String inviteType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getInviteRecordList(pageParam.getOrgId(),pageParam.getOpFlag(),pageParam.getAccountId(),dateQueryType,beginTime,endTime,status,inviteType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
