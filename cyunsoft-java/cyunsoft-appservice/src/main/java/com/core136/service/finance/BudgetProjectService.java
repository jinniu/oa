package com.core136.service.finance;

import com.core136.bean.finance.BudgetProject;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.BudgetProjectMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class BudgetProjectService {
    private BudgetProjectMapper budgetProjectMapper;
	@Autowired
	public void setBudgetProjectMapper(BudgetProjectMapper budgetProjectMapper) {
		this.budgetProjectMapper = budgetProjectMapper;
	}

	public int insertBudgetProject(BudgetProject budgetProject) {
        return budgetProjectMapper.insert(budgetProject);
    }

    public int deleteBudgetProject(BudgetProject budgetProject) {
        return budgetProjectMapper.delete(budgetProject);
    }

    public int updateBudgetProject(Example example, BudgetProject budgetProject) {
        return budgetProjectMapper.updateByExampleSelective(budgetProject, example);
    }

    public BudgetProject selectOneBudgetProject(BudgetProject budgetProject) {
        return budgetProjectMapper.selectOne(budgetProject);
    }

    public int getCountChild(String orgId, String parentId) {
        BudgetProject budgetProject = new BudgetProject();
        budgetProject.setOrgId(orgId);
        budgetProject.setParentId(parentId);
        return budgetProjectMapper.selectCount(budgetProject);
    }

	/**
	 * 获取所有父级项目
	 * @param orgId 机构码
	 * @return 项目列表
	 */
	public List<Map<String, String>> getAllParentBudgetProject(String orgId) {
		return budgetProjectMapper.getAllParentBudgetProject(orgId);
	}

	/**
	 * 获取预算项目列表
	 * @param orgId 机构码
	 * @return 预算项目列表
	 */
	public List<BudgetProject> getAllBudgetProjectList(String orgId, String keyword) {
		if(StringUtils.isBlank(keyword)) {
			keyword = "%%";
		}else {
			keyword = "%"+keyword+"%";
		}
		List<BudgetProject> list = budgetProjectMapper.getAllBudgetProjectList(orgId,keyword);
		List<BudgetProject> budgetProjectList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getParentId().equals("0")) {
				budgetProjectList.add(list.get(i));
			}
		}
		for (BudgetProject budgetProject : budgetProjectList) {
			budgetProject.setChildren(getChildBudgetProjectList(budgetProject.getProjectId(), list));
		}
		return budgetProjectList;
	}

	/**
	 * 获取子部门列表
	 * @param projectId 项目Id
	 * @param rootBudgetProject 父级预算项目对象
	 * @return 预算项目列表
	 */
	public List<BudgetProject> getChildBudgetProjectList(String projectId, List<BudgetProject> rootBudgetProject) {
		List<BudgetProject> childList = new ArrayList<>();
		for (BudgetProject budgetProject : rootBudgetProject) {
			if (budgetProject.getParentId().equals(projectId)) {
				childList.add(budgetProject);
			}
		}
		for (BudgetProject budgetProject : childList) {
			budgetProject.setChildren(getChildBudgetProjectList(budgetProject.getProjectId(), rootBudgetProject));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}




    public RetDataBean deleteAndCheckBudgetProject(BudgetProject budgetProject) {
        if (getCountChild(budgetProject.getOrgId(), budgetProject.getProjectId()) > 1) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
        } else {
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetProjectMapper.delete(budgetProject));
        }
    }

	/**
	 * 批量删除项目预算
	 * @param orgId 机构码
	 * @param list 项目预算Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteBudgetProjectByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(BudgetProject.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("projectId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, budgetProjectMapper.deleteByExample(example));
		}
	}


	/**
	 * 更新预算项目信息
	 * @param example 更新条件
	 * @param budgetProject 预算项目对象
	 * @return 成功更澵记录数
	 */
	public int updateBudgetProjectAndParentId(Example example, BudgetProject budgetProject) {
        if (StringUtils.isBlank(budgetProject.getParentId())||budgetProject.getParentId().equals("0")) {
            budgetProject.setLevelId(budgetProject.getProjectId());
        } else {
            BudgetProject budgetProject1 = new BudgetProject();
            budgetProject1.setProjectId(budgetProject.getProjectId());
            budgetProject1.setOrgId(budgetProject.getOrgId());
            budgetProject1 = selectOneBudgetProject(budgetProject1);
            budgetProject.setParentId(budgetProject1.getLevelId() + "/" + budgetProject.getProjectId());
        }
        return updateBudgetProject(example, budgetProject);
    }


	/**
	 * 创建预算项目
	 * @param budgetProject 预算项目对象
	 * @return 成功创建记录数
	 */
	public int addBudgetProject(BudgetProject budgetProject) {
        if (budgetProject.getParentId().equals("0")) {
            budgetProject.setLevelId(budgetProject.getProjectId());
        } else {
            BudgetProject budgetProject1 = new BudgetProject();
            budgetProject1.setProjectId(budgetProject.getParentId());
            budgetProject1.setOrgId(budgetProject.getOrgId());
            budgetProject1 = selectOneBudgetProject(budgetProject1);
            if(budgetProject1==null) {
				budgetProject.setLevelId(budgetProject.getProjectId());
			}else {
				budgetProject.setLevelId(budgetProject1.getLevelId()+ "/" + budgetProject.getProjectId());
			}
        }
        return insertBudgetProject(budgetProject);
    }

	/**
	 *  获取项目列表
	 * @param orgId 机构码
	 * @param projectType 项目类型
	 * @param budgetAccount 预算账套
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 项目列表
	 */
    public List<Map<String, String>> getBudgetProjectList(String orgId, String projectType,String budgetAccount,String dateQueryType,String beginTime,String endTime,String keyword) {
        return budgetProjectMapper.getBudgetProjectList(orgId, projectType, budgetAccount,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取项目列表
	 * @param pageParam 分页参数
	 * @param projectType 项目类型
	 * @param budgetAccount 预算账套
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 项目列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getBudgetProjectList(PageParam pageParam, String projectType, String budgetAccount,String dateQueryType,String beginTime,String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetProjectList(pageParam.getOrgId(), projectType, budgetAccount, dateQueryType,beginTime,endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
