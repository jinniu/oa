package com.core136.service.archives;

import com.core136.bean.archives.ArchivesTempHum;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesTempHumMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesTempHumService {
	private ArchivesTempHumMapper archivesTempHumMapper;
	@Autowired
	public void setArchivesTempHumMapper(ArchivesTempHumMapper archivesTempHumMapper) {
		this.archivesTempHumMapper = archivesTempHumMapper;
	}

	public int insertArchivesTempHum(ArchivesTempHum archivesTempHum) {
		return archivesTempHumMapper.insert(archivesTempHum);
	}

	public int deleteArchivesTempHum(ArchivesTempHum archivesTempHum) {
		return archivesTempHumMapper.delete(archivesTempHum);
	}

	/**
	 * 批量删除温湿度记录
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteArchivesTempHumIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesTempHum.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesTempHumMapper.deleteByExample(example));
		}
	}

	public int updateArchivesTempHum(Example example, ArchivesTempHum archivesTempHum) {
		return archivesTempHumMapper.updateByExampleSelective(archivesTempHum,example);
	}

	public ArchivesTempHum selectOneArchivesTempHum(ArchivesTempHum  archivesTempHum) {
		return archivesTempHumMapper.selectOne(archivesTempHum);
	}

	/**
	 * 获取档案库房温湿度检查列表
	 * @param orgId
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param checkType
	 * @return
	 */
	public List<Map<String,String>>getArchivesTempHumList(String orgId,String opFlag,String accountId,String dateQueryType,String beginTime,String endTime,String checkType,String keyword) {
		return archivesTempHumMapper.getArchivesTempHumList(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,checkType,"%"+keyword+"%");
	}

	/**
	 * 获取档案库房温湿度检查列表
	 * @param pageParam
	 * @param date
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param checkType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getArchivesTempHumList(PageParam pageParam,String date, String beginTime, String endTime,String checkType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesTempHumList(pageParam.getOrgId(),pageParam.getOpFlag(), pageParam.getAccountId(),date,beginTime,endTime, checkType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}



}
