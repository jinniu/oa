package com.core136.service.office;

import com.core136.bean.office.OfficeSuppliesApply;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.OfficeSuppliesApplyMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class OfficeSuppliesApplyService {
    private OfficeSuppliesApplyMapper officeSuppliesApplyMapper;
	@Autowired
	public void setOfficeSuppliesApplyMapper(OfficeSuppliesApplyMapper officeSuppliesApplyMapper) {
		this.officeSuppliesApplyMapper = officeSuppliesApplyMapper;
	}

	public int insertOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
        return officeSuppliesApplyMapper.insert(officeSuppliesApply);
    }

    public int deleteOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
        return officeSuppliesApplyMapper.delete(officeSuppliesApply);
    }

    public int updateOfficeSuppliesApply(Example example, OfficeSuppliesApply officeSuppliesApply) {
        return officeSuppliesApplyMapper.updateByExampleSelective(officeSuppliesApply, example);
    }

    public OfficeSuppliesApply selectOneOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
        return officeSuppliesApplyMapper.selectOne(officeSuppliesApply);
    }

    /**
     * 获取个人历史申请记录
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @param keyword 查询关键词
     * @return 申请记录列表
     */
    public List<Map<String, String>> getMyApplyOfficeSuppliesList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime, String status, String keyword) {
        return officeSuppliesApplyMapper.getMyApplyOfficeSuppliesList(orgId, accountId, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

    /**
     * 获取个人历史申请记录
     * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status 状态
     * @return 申请记录列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getMyApplyOfficeSuppliesList(PageParam pageParam, String dateQueryType,String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyApplyOfficeSuppliesList(pageParam.getOrgId(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取审批列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param dateQueryType 日期范围
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @param keyword 查询关键词
     * @return 审批列表
     */
    public List<Map<String, String>> getOfficeSuppliesApplyRecordList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime, String status, String keyword) {
        return officeSuppliesApplyMapper.getOfficeSuppliesApplyRecordList(orgId, accountId,dateQueryType, beginTime, endTime, status, "%" + keyword + "%");
    }


    public PageInfo<Map<String, String>> getOfficeSuppliesApplyRecordList(PageParam pageParam,String dateQueryType, String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOfficeSuppliesApplyRecordList(pageParam.getOrgId(), pageParam.getAccountId(), dateQueryType,beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 待发放用品列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
     * @param beginTime 开始时间
     * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 用品列表
	 */
    public List<Map<String, String>> getGrantOfficeSuppliesList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime, String keyword) {
        return officeSuppliesApplyMapper.getGrantOfficeSuppliesList(orgId, accountId, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 待发放用品列表
	 * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 待发放用品列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getGrantOfficeSuppliesList(PageParam pageParam, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getGrantOfficeSuppliesList(pageParam.getOrgId(), pageParam.getAccountId(), dateQueryType,beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
