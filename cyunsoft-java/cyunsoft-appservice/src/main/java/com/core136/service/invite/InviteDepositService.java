package com.core136.service.invite;

import com.core136.bean.invite.InviteDeposit;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteDepositMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteDepositService {
	private InviteDepositMapper inviteDepositMapper;
	@Autowired
	public void setInviteDepositMapper(InviteDepositMapper inviteDepositMapper) {
		this.inviteDepositMapper = inviteDepositMapper;
	}

	public int insertInviteDeposit(InviteDeposit inviteDeposit) {
		return inviteDepositMapper.insert(inviteDeposit);
	}

	public int deleteInviteDeposit(InviteDeposit inviteDeposit) {
		return inviteDepositMapper.delete(inviteDeposit);
	}

	public int updateInviteDeposit(Example example,InviteDeposit inviteDeposit) {
		return inviteDepositMapper.updateByExampleSelective(inviteDeposit,example);
	}

	public InviteDeposit selectOneInviteDeposit(InviteDeposit inviteDeposit){
		return inviteDepositMapper.selectOne(inviteDeposit);
	}

	/**
	 * 批量删除招标保证金
	 * @param orgId 机构码
	 * @param list 保证金Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteInviteDepositByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteDeposit.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteDepositMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取招标保证金列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param entId 企业Id
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 保证金列表
	 */
	public List<Map<String,String>>getInviteDepositList(String orgId, String dateQueryType, String beginTime, String endTime, String entId, String status, String keyword) {
		return inviteDepositMapper.getInviteDepositList(orgId,dateQueryType,beginTime,endTime,entId,status,"%"+keyword+"%");
	}

	/**
	 * 获取招标保证金列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param entId 企业Id
	 * @param status 状态
	 * @return 保证金列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getInviteDepositList(PageParam pageParam, String dateQueryType, String beginTime, String endTime,String entId, String status) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getInviteDepositList(pageParam.getOrgId(),dateQueryType,beginTime,endTime,entId,status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
