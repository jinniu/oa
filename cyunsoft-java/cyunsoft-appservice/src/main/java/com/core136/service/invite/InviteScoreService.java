package com.core136.service.invite;

import com.core136.bean.invite.InviteScore;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.invite.InviteScoreMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteScoreService {
	private InviteScoreMapper inviteScoreMapper;
	@Autowired
	public void setInviteScoreMapper(InviteScoreMapper inviteScoreMapper) {
		this.inviteScoreMapper = inviteScoreMapper;
	}

	public int insertInviteScore(InviteScore inviteScore) {
		return inviteScoreMapper.insert(inviteScore);
	}

	public int deleteInviteScore(InviteScore inviteScore) {
		return inviteScoreMapper.delete(inviteScore);
	}

	public int updateInviteScore(Example example,InviteScore inviteScore) {
		return inviteScoreMapper.updateByExampleSelective(inviteScore,example);
	}

	public InviteScore selectOneInviteScore(InviteScore inviteScore) {
		return inviteScoreMapper.selectOne(inviteScore);
	}

	/**
	 * 供应商评分
	 * @param inviteScore 评分对象
	 * @return 消息结构
	 */
	public RetDataBean setInviteScore(InviteScore inviteScore) {
		InviteScore inviteScore1 = new InviteScore();
		inviteScore1.setAccountId(inviteScore.getAccountId());
		inviteScore1.setEntId(inviteScore.getEntId());
		inviteScore1.setOrgId(inviteScore.getOrgId());
		if(inviteScoreMapper.selectCount(inviteScore1)==0) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteScoreMapper.insert(inviteScore));
		}else {
			inviteScore.setRecordId(null);
			Example example = new Example(InviteScore.class);
			example.createCriteria().andEqualTo("orgId",inviteScore.getOrgId()).andEqualTo("entId",inviteScore.getEntId()).andEqualTo("accountId",inviteScore1.getAccountId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, updateInviteScore(example,inviteScore));
		}
	}

	/**
	 * 批量删除供应商打分记录
	 * @param orgId 机构码
	 * @param list 供应商打分记录对象
	 * @return 消息结构
	 */
	public RetDataBean deleteInviteScoreByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteScore.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteScoreMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取企业评分列表
	 * @param orgId 机构码
	 * @param entId 企业Id
	 * @return 评分列表
	 */
	public List<Map<String,String>>getEntScoreListByEntId(String orgId, String entId) {
		return inviteScoreMapper.getEntScoreListByEntId(orgId,entId);
	}

}
