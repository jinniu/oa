package com.core136.service.crm;



import com.core136.bean.account.UserInfo;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.bi.option.property.OptionSeries;
import com.core136.bi.option.property.OptionTitle;
import com.core136.bi.option.resdata.LegendData;
import com.core136.bi.option.resdata.SeriesData;
import com.core136.bi.option.style.Emphasis;
import com.core136.bi.option.style.ItemStyle;
import com.core136.bi.option.units.LineOption;
import com.core136.bi.option.units.PieOption;
import com.core136.mapper.crm.EchartsCrmMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EchartsCrmService {
    private final PieOption pieOption = new PieOption();
    private final LineOption lineOption = new LineOption();
    private EchartsCrmMapper echartsCrmMapper;
	@Autowired
	public void setEchartsCrmMapper(EchartsCrmMapper echartsCrmMapper) {
		this.echartsCrmMapper = echartsCrmMapper;
	}

	/**
	 * 部门报价单占比前10的占比
	 * @param user
	 * @return
	 */
	public OptionConfig getBiQuotationByDeptPie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiQuotationByDeptPie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("人员部门");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("部门报价单统计");
        optionTitle.setSubtext("部门报价单占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getBiQuotationByDeptPie(String orgId) {
        return echartsCrmMapper.getBiQuotationByDeptPie(orgId);
    }

	/**
	 * 获取支付类型分类
	 * @param user 用户对象
	 * @return 消息结构
	 */
	public OptionConfig getBiInquiryByPayTypePie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiInquiryByPayTypePie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("支付分类");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("支付统计");
        optionTitle.setSubtext("支付占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getBiInquiryByPayTypePie(String orgId) {
        return echartsCrmMapper.getBiInquiryByPayTypePie(orgId);
    }

	/**
	 * 按月份统计报价单
	 * @param user 用户对象
	 * @return 消息结构
	 */
	public OptionConfig getBiQuotationByMonthLine(UserInfo user) {
        OptionConfig optionConfig;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date y = c.getTime();
        String endTime = format.format(y);
        c.add(Calendar.YEAR, -1);
        y = c.getTime();
        String beginTime = format.format(y);
        List<Map<String, Object>> resList = getBiQuotationByMonthLine(user.getOrgId(), beginTime, endTime);
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    public List<Map<String, Object>> getBiQuotationByMonthLine(String orgId, String beginTime, String endTime) {
        return echartsCrmMapper.getBiQuotationByMonthLine(orgId, beginTime, endTime);
    }

	/**
	 * 获取报价单人员占比
	 * @param user 用户对象
	 * @return 报价单人员占比信息
	 */
	public OptionConfig getBiQuotationByAccountPie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiQuotationByAccountPie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("员工");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("员工报价单统计");
        optionTitle.setSubtext("员工报价单占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

	/**
	 * 获取报价单人员占比
	 * @param orgId 机构码
	 * @return 报单统计列表
	 */
	public List<Map<String, String>> getBiQuotationByAccountPie(String orgId) {
        return echartsCrmMapper.getBiQuotationByAccountPie(orgId);
    }

	/**
	 * 部门询价单占比前10的占比
	 * @param user 用户对象
	 * @return 图表配置项信息
	 */
	public OptionConfig getBiInquiryByDeptPie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiInquiryByDeptPie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("人员部门");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("部门询价单统计");
        optionTitle.setSubtext("部门询价单占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getBiInquiryByDeptPie(String orgId) {
        return echartsCrmMapper.getBiInquiryByDeptPie(orgId);
    }
	/**
	 * 按月份统计工作量
	 * @param user 用户对象
	 * @return 工作量信息
	 */
	public OptionConfig getBiInquiryByMonthLine(UserInfo user) {
        OptionConfig optionConfig;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date y = c.getTime();
        String endTime = format.format(y);
        c.add(Calendar.YEAR, -1);
        y = c.getTime();
        String beginTime = format.format(y);
        List<Map<String, Object>> resList = getBiInquiryByMonthLine(user.getOrgId(), beginTime, endTime);
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    public List<Map<String, Object>> getBiInquiryByMonthLine(String orgId, String beginTime, String endTime) {
        return echartsCrmMapper.getBiInquiryByMonthLine(orgId, beginTime, endTime);
    }

	/**
	 * 获取询价单人员占比
	 * @param user
	 * @return
	 */
	public OptionConfig getBiInquiryByAccountPie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiInquiryByAccountPie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("员工");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("员工询价单统计");
        optionTitle.setSubtext("员工询价单占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

	/**
	 * 获取询价单人员占比
	 * @param orgId
	 * @return
	 */
	public List<Map<String, String>> getBiInquiryByAccountPie(String orgId) {
        return echartsCrmMapper.getBiInquiryByAccountPie(orgId);
    }

	/**
	 * 获取CRM客户等级占比
	 * @param user
	 * @return
	 */
	public OptionConfig getBiCustomerLevelPie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiCustomerLevelPie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("等级");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("客户等级数据统计");
        optionTitle.setSubtext("客户等级占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

	/**
	 * 获取CRM客户等级占比
	 * @param orgId
	 * @return
	 */
	public List<Map<String, String>> getBiCustomerLevelPie(String orgId) {
        return echartsCrmMapper.getBiCustomerLevelPie(orgId);
    }


	public List<Map<String, String>> getBiCustomerSourcePie(String orgId) {
		return echartsCrmMapper.getBiCustomerSourcePie(orgId);
	}

	/**
	 * 获取CRM客户等级占比
	 * @param user
	 * @return
	 */
	public OptionConfig getBiCustomerSourcePie(UserInfo user) {
		OptionConfig optionConfig = new OptionConfig();
		List<Map<String, String>> resdataList = getBiCustomerSourcePie(user.getOrgId());
		OptionSeries[] optionSeriesArr = new OptionSeries[1];
		SeriesData[] dataArr = new SeriesData[resdataList.size()];
		int selectedLeng = 0;
		if (dataArr.length >= 10) {
			selectedLeng = 10;
		} else {
			selectedLeng = dataArr.length;
		}
		String[] selected = new String[selectedLeng];
		LegendData[] legendDatas = new LegendData[dataArr.length];
		for (int i = 0; i < dataArr.length; i++) {
			if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
				resdataList.get(i).put("name", "other" + i);
			}
			if (i < selectedLeng) {
				selected[i] = resdataList.get(i).get("name");
			}
			LegendData legendData = new LegendData();
			legendData.setName(resdataList.get(i).get("name"));
			legendDatas[i] = legendData;
			SeriesData seriesData = new SeriesData();
			seriesData.setName(resdataList.get(i).get("name"));
			seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
			dataArr[i] = seriesData;
		}
		OptionSeries optionSeries = new OptionSeries();
		optionSeries.setName("等级");
		optionSeries.setType("pie");
		optionSeries.setRadius("55%");
		optionSeries.setCenter(new String[]{"40%", "50%"});
		Emphasis emphasis = new Emphasis();
		ItemStyle itemStyle = new ItemStyle();
		itemStyle.setShadowBlur(10);
		itemStyle.setShadowOffsetX(0);
		itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
		emphasis.setItemStyle(itemStyle);
		optionSeries.setData(dataArr);
		optionSeriesArr[0] = optionSeries;
		optionConfig.setSeries(optionSeriesArr);
		optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
		OptionTitle optionTitle = new OptionTitle();
		optionTitle.setText("客户来源数据统计");
		optionTitle.setSubtext("客户来源占比");
		optionTitle.setLeft("left");
		optionConfig.setTitle(optionTitle);
		return optionConfig;
	}

	/**
	 * 获取CRM销售人员的占比
	 * @param user
	 * @return
	 */
	public OptionConfig getBiCustomerKeepUserPie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiCustomerKeepUserPie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("业务员");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("业务员客户数量统计");
        optionTitle.setSubtext("客户数量数据占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

	/**
	 * 获取CRM销售人员的占比
	 * @param orgId
	 * @return
	 */
	public List<Map<String, String>> getBiCustomerKeepUserPie(String orgId) {
        return echartsCrmMapper.getBiCustomerKeepUserPie(orgId);
    }

	/**
	 * 获取财务门户的收支
	 * @param user
	 * @return
	 */
	public OptionConfig getBiCustomerIndustryPie(UserInfo user) {

        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiCustomerIndustryPie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("行业");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("客户行业数据统计");
        optionTitle.setSubtext("客户行业数据占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

	/**
	 * 获取财务门户的收支
	 * @param orgId 机构码
	 * @return 收支列表
	 */
	public List<Map<String, String>> getBiCustomerIndustryPie(String orgId) {
        return echartsCrmMapper.getBiCustomerIndustryPie(orgId);
    }



}
