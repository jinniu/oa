package com.core136.service.invite;

import com.core136.bean.invite.InviteTeam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.invite.InviteTeamMapper;
import com.core136.service.account.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class InviteTeamService {
	private UserInfoService userInfoService;
	private InviteTeamMapper inviteTeamMapper;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setInviteTeamMapper(InviteTeamMapper inviteTeamMapper) {
		this.inviteTeamMapper = inviteTeamMapper;
	}

	public int insertInviteTeam(InviteTeam inviteTeam) {
		return inviteTeamMapper.insert(inviteTeam);
	}

	public int deleteInviteTeam(InviteTeam inviteTeam) {
		return inviteTeamMapper.delete(inviteTeam);
	}

	public int updateInviteTeam(InviteTeam inviteTeam, Example example) {
		return inviteTeamMapper.updateByExampleSelective(inviteTeam,example);
	}

	public InviteTeam selectOneInviteTeam(InviteTeam inviteTeam) {
		return inviteTeamMapper.selectOne(inviteTeam);
	}

	/**
	 *获取评标小组列表
	 * @param orgId 机构码
	 * @return 评标小组列表
	 */
	public List<Map<String,String>>getInviteTeamList(String orgId) {
		List<Map<String,String>> list= inviteTeamMapper.getInviteTeamList(orgId);
		for(Map<String,String> map:list) {
			String member = map.get("member");
			List<String> accountList = Arrays.asList(member.split(","));
			map.put("memberName",userInfoService.getUserNamesByAccountIds(orgId,accountList));
		}
		return list;
	}

	/**
	 * 获取评标小组列表
	 * @param orgId 机构码
	 * @return 评标小组列表
	 */
	public List<Map<String,String>> getInviteTeamListForSelect(String orgId) {
		return inviteTeamMapper.getInviteTeamListForSelect(orgId);
	}


	/**
	 * 批量删除评标小组
	 * @param orgId 机构码
	 * @param list 评标小组Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteInviteTeamByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteTeam.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("teamId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteTeamMapper.deleteByExample(example));
		}
	}


}
