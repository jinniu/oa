package com.core136.service.office;

import com.core136.bean.account.UserInfo;
import com.core136.bean.office.Diary;
import com.core136.bean.office.DiaryConfig;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.DiaryMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * 工作日志操作类
 * @author lsq
 */
@Service
public class DiaryService {
    private DiaryMapper diaryMapper;
	private DiaryConfigService diaryConfigService;
    private DiaryCommentsService diaryCommentsService;
    private UserInfoService userInfoService;
	@Autowired
	public void setDiaryMapper(DiaryMapper diaryMapper) {
		this.diaryMapper = diaryMapper;
	}
	@Autowired
	public void setDiaryConfigService(DiaryConfigService diaryConfigService) {
		this.diaryConfigService = diaryConfigService;
	}
	@Autowired
	public void setDiaryCommentsService(DiaryCommentsService diaryCommentsService) {
		this.diaryCommentsService = diaryCommentsService;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	/**
	 * 获取工作日志详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param diaryId 日志Id
	 * @return 日志详情
	 */
	public Map<String, String> getMyDiaryById(String orgId, String opFlag,String accountId, String deptId,String userLevel,String diaryId) {
		return diaryMapper.getMyDiaryById(orgId, opFlag, accountId, deptId,userLevel,diaryId);
	}

	/**
	 * 创建工作日志
	 * @param diary 工作日志对象
	 * @return 成功创建记录数
	 */
	public int insertDiary(Diary diary) {
        return diaryMapper.insert(diary);
    }

    public int createDiary(UserInfo user, Diary diary) {
		DiaryConfig diaryConfig = new DiaryConfig();
		diaryConfig.setOrgId(user.getOrgId());
		diaryConfig = diaryConfigService.selectOneDiaryConfig(diaryConfig);
        if (diaryConfig != null) {
            if (diaryConfig.getShareStatus() == 0) {
                diary.setUserRole("@all");
                diary.setDeptRole("");
                diary.setLevelRole("");
            } else if (diaryConfig.getShareStatus() == 1) {
                diary.setUserRole("");
                diary.setDeptRole(user.getDeptId());
                diary.setLevelRole("");
            } else if (diaryConfig.getShareStatus() == 3) {
                diary.setUserRole("");
                diary.setDeptRole("");
                diary.setLevelRole("");
            }
        } else {
            diary.setUserRole("");
            diary.setDeptRole("");
            diary.setLevelRole("");
        }
        if (StringUtils.isNotBlank(diary.getMsgType())) {
            List<Map<String, String>> userList = userInfoService.getAccountIdInRole(diary.getOrgId(), diary.getUserRole(), diary.getDeptRole(), diary.getLevelRole());
            List<MsgBody> msgBodyList = new ArrayList<>();
			for (Map<String, String> stringMap : userList) {
				UserInfo user2 = new UserInfo();
				user2.setAccountId(stringMap.get("accountId"));
				user2.setOrgId(user.getOrgId());
				user2 = userInfoService.selectOneUser(user2);
				MsgBody msgBody = new MsgBody();
				msgBody.setTitle("工作日志分享提醒");
				msgBody.setContent("标题为：" + diary.getTitle() + "的工作日志分享提醒！");
				msgBody.setSendTime(diary.getCreateTime());
				msgBody.setUserInfo(user2);
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setView(GlobalConstant.MSG_TYPE_DIARY);
				msgBody.setRecordId(diary.getDiaryId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(diary.getMsgType().split(","));
            MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
            messageUnitService.sendMessage(msgTypeList, msgBodyList);
        }
        return insertDiary(diary);

    }

	/**
	 * 删除工作日志
	 * @param diary 工作日志对象
	 * @return 成功删除记录数
	 */
	public int deleteDiary(Diary diary) {
        return diaryMapper.delete(diary);
    }

	/**
	 * 查询单个工作日志
	 * @param diary 工作日志对象
	 * @return 工作日志对象
	 */
    public Diary selectOneDiary(Diary diary) {
        return diaryMapper.selectOne(diary);
    }

	/**
	 * 更新工作日志
	 * @param user 用户对象
	 * @param example 更新条件
	 * @param diary 工作日志对象
	 * @param diaryConfig 工作日志配置
	 * @return 成功更新记录数
	 */
    public int updateDiary(UserInfo user, Example example, Diary diary, DiaryConfig diaryConfig) {
        if (diaryConfig != null) {
            if (diaryConfig.getShareStatus() == 0) {
                diary.setUserRole("@all");
                diary.setDeptRole("");
                diary.setLevelRole("");
            } else if (diaryConfig.getShareStatus() == 1) {
                diary.setUserRole("");
                diary.setDeptRole(user.getDeptId());
                diary.setLevelRole("");
            } else if (diaryConfig.getShareStatus() == 3) {
                diary.setUserRole("");
                diary.setDeptRole("");
                diary.setLevelRole("");
            }
        } else {
            diary.setUserRole("");
            diary.setDeptRole("");
            diary.setLevelRole("");
        }
        if (StringUtils.isNotBlank(diary.getMsgType())) {
            List<Map<String, String>> userList = userInfoService.getAccountIdInRole(diary.getOrgId(), diary.getUserRole(), diary.getDeptRole(), diary.getLevelRole());
            List<MsgBody> msgBodyList = new ArrayList<>();
			for (Map<String, String> stringMap : userList) {
				UserInfo user2 = new UserInfo();
				user2.setAccountId(stringMap.get("accountId"));
				user2.setOrgId(user.getOrgId());
				user2 = userInfoService.selectOneUser(user2);
				MsgBody msgBody = new MsgBody();
				msgBody.setTitle("工作日志分享提醒");
				msgBody.setContent("标题为：" + diary.getTitle() + "的工作日志分享提醒！");
				msgBody.setSendTime(diary.getCreateTime());
				msgBody.setUserInfo(user2);
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setView(GlobalConstant.MSG_TYPE_DIARY);
				msgBody.setRecordId(diary.getDiaryId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(diary.getMsgType().split(","));
            MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
            messageUnitService.sendMessage(msgTypeList, msgBodyList);
        }
        return diaryMapper.updateByExampleSelective(diary, example);
    }

	/**
	 * 获取当前用户历史工作日志
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param diaryDay
	 * @param keyword
	 * @return
	 */
    public List<Map<String, Object>> getMyDiaryList(String orgId, String accountId,String dateQueryType,String beginTime, String endTime,String diaryDay,String keyword) {
        return diaryMapper.getMyDiaryList(orgId, accountId,dateQueryType, beginTime,endTime,diaryDay,"%" + keyword + "%");
    }

	/**
	 * 移动端获取个人工作日志列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param page
	 * @return
	 */
    public List<Map<String, String>> getMyDiaryListForApp(String orgId, String accountId, int page) {
        return diaryMapper.getMyDiaryListForApp(orgId, accountId, page);
    }

	/**
	 * 获取当前用户历史工作日志
	 * @param pageParam 分页参数
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param diaryDay
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, Object>> getMyDiaryList(PageParam pageParam, String dateQueryType,String beginTime, String endTime,String diaryDay) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, Object>> datalist = getMyDiaryList(pageParam.getOrgId(),pageParam.getAccountId(), dateQueryType, beginTime,endTime,diaryDay,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取他人的工作日志
	 * @param pageNumber
	 * @param pageSize
	 * @param orderBy
	 * @param orgId
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public PageInfo<Map<String, Object>> getOtherDiaryList(int pageNumber, int pageSize, String orderBy, String orgId, String accountId, String beginTime, String endTime, String keyword) {
        PageMethod.startPage(pageNumber, pageSize, orderBy);
        List<Map<String, Object>> datalist = diaryMapper.getOtherDiaryList(orgId, accountId, keyword, beginTime, endTime);
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取我下属的工作晶志列表
	 * @param orgId
	 * @param createUser
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getMySubordinatesDiaryList(String orgId, String createUser, String dateQueryType, String beginTime, String endTime, String keyword) {
        return diaryMapper.getMySubordinatesDiaryList(orgId, createUser,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取我下属的工作晶志列表
	 * @param pageParam 分页参数
	 * @param createUser
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMySubordinatesDiaryList(PageParam pageParam, String createUser,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = diaryMapper.getMySubordinatesDiaryList(pageParam.getOrgId(), createUser,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取个人日志总数
	 * @param orgId
	 * @param accountId
	 * @return
	 */
	public int getMyDiaryCount(String orgId, String accountId) {
        return diaryMapper.getMyDiaryCount(orgId, accountId);
    }

	/**
	 * 被他人评论数
	 * @param orgId
	 * @param accountId
	 * @return
	 */
	public int getDiaryCommentCount(String orgId, String accountId) {
        return diaryMapper.getDiaryCommentCount(orgId, accountId);
    }

	/**
	 * 获取个人日志信息
	 * @param orgId
	 * @param accountId
	 * @return
	 */
	public Map<String, Object> getMyDiaryInfo(String orgId, String accountId) {
        Map<String, Object> tempMap = new HashMap<>();
        tempMap.put("diaryCount", getMyDiaryCount(orgId, accountId));
        tempMap.put("commToMeCount", getDiaryCommentCount(orgId, accountId));
        tempMap.put("commCount", diaryCommentsService.getMyDiaryCommentsCount(orgId, accountId));
        return tempMap;
    }

	/**
	 * 获取共享人员列表
	 * @param orgId
	 * @param accountId 用户账号
	 * @param deptId
	 * @param levelId
	 * @return
	 */
	public List<Map<String, String>> getShareDiaryUserList(String orgId,String accountId,String deptId,String levelId) {
		return diaryMapper.getShareDiaryUserList(orgId,accountId,deptId,levelId);
	}

	/**
	 * 获取他人分享的工作日志
	 * @param orgId
	 * @param accountId 用户账号
	 * @param deptId
	 * @param levelId
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getShareDiaryList(String orgId, String accountId, String deptId, String levelId, String dateQueryType,String beginTime, String endTime,String createUser, String keyword) {
        return diaryMapper.getShareDiaryList(orgId, accountId, deptId, levelId, dateQueryType,beginTime, endTime, createUser,"%" + keyword + "%");
    }

	/**
	 * 获取他人分享的工作日志
	 * @param pageParam 分页参数
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getShareDiaryList(PageParam pageParam,String dateQueryType, String beginTime, String endTime,String createUser) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getShareDiaryList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(),dateQueryType, beginTime, endTime,createUser, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
