package com.core136.service.crm;

import com.core136.bean.account.UserInfo;
import com.core136.bean.crm.CrmProduct;
import com.core136.bean.crm.CrmProductSort;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmProductMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Crm 产品服务类
 * @author lsq
 */
@Service
public class CrmProductService {
	private CrmProductMapper crmProductMapper;
	private CrmProductSortService crmProductSortService;
	@Autowired
	public void setCrmProductMapper(CrmProductMapper crmProductMapper) {
		this.crmProductMapper = crmProductMapper;
	}
	@Autowired
	public void setCrmProductSortService(CrmProductSortService crmProductSortService) {
		this.crmProductSortService = crmProductSortService;
	}

	public int insertCrmProduct(CrmProduct crmProduct) {
		return crmProductMapper.insert(crmProduct);
	}

	public int deleteCrmProduct(CrmProduct crmProduct) {
		return crmProductMapper.delete(crmProduct);
	}

	public int updateCrmProduct(Example example, CrmProduct crmProduct) {
		return crmProductMapper.updateByExampleSelective(crmProduct, example);
	}

	public CrmProduct selectOneCrmProduct(CrmProduct crmProduct) {
		return crmProductMapper.selectOne(crmProduct);
	}

	public List<Map<String,String>>getAllProductListForSelect(String orgId,String productId,String keyword) {
		return crmProductMapper.getAllProductListForSelect(orgId,productId,"%"+keyword+"%");
	}

	/**
	 * 批量删除物资记录
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteCrmProductByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(CrmProduct.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("productId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, crmProductMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取产品名称
	 * @param orgId
	 * @param ids
	 * @return
	 */
	public List<Map<String, String>> getCrmProductNameByIds(String orgId,String ids) {
		if(StringUtils.isBlank(ids)) {
			return null;
		}else {
			List<String> list = Arrays.asList(ids.split(","));
			return crmProductMapper.getCrmProductNameByIds(orgId,list);
		}
	}

	public List<Map<String, String>> getCrmProductListByIdsForSelect(String orgId,List<String> list) {
		if(list.isEmpty()) {
			return null;
		}else {
			return crmProductMapper.getCrmProductListByIdsForSelect(orgId,list);
		}
	}

	/**
	 *  获取办公用品列表
	 * @param orgId
	 * @param sortId
	 * @param keyword
	 * @return
	 */
	public List<Map<String, String>> getCrmProductListBySortId(String orgId, String sortId, String keyword) {
		return crmProductMapper.getCrmProductListBySortId(orgId, sortId, "%" + keyword + "%");
	}

	public List<Map<String,String>>getProductListForSelect(String orgId) {
		return crmProductMapper.getProductListForSelect(orgId);
	}

	public PageInfo<Map<String, String>> getCrmProductListBySortId(PageParam pageParam, String sortId) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getCrmProductListBySortId(pageParam.getOrgId(), sortId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 办公用品导入
	 * @param user
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@Transactional(value = "generalTM")
	public RetDataBean importCrmProductInfo(UserInfo user, MultipartFile file) throws IOException {
		List<CrmProduct> crmProductList = new ArrayList<>();
		List<String> resList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("产品名称");
		titleList.add("物料编号");
		titleList.add("所属分类");
		titleList.add("产品品牌");
		titleList.add("规格型号");
		titleList.add("价格");
		titleList.add("指导价");
		titleList.add("是否可销售");
		titleList.add("备注");
		List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			CrmProduct crmProduct = new CrmProduct();
			crmProduct.setProductId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						crmProduct.setSortNo(Integer.parseInt(tempMap.get(s)));
					}else{
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if(s.equals("产品名称")) {
					crmProduct.setName(tempMap.get(s));
				}
				if(s.equals("物料编号")) {
					crmProduct.setMaterialCode(tempMap.get(s));
				}
				if (s.equals("所属分类")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						CrmProductSort crmProductSort = new CrmProductSort();
						crmProductSort.setOrgId(user.getOrgId());
						crmProductSort.setSortName(tempMap.get(s));
						try{
							crmProductSort = crmProductSortService.selectOneCrmProductSort(crmProductSort);
							if(crmProductSort!=null) {
								crmProduct.setSortId(crmProductSort.getSortId());
							}else{
								resList.add("所属分类不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("所属分类查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("所属分类不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if(s.equals("产品品牌")) {
					crmProduct.setBrand(tempMap.get(s));
				}
				if(s.equals("规格型号")) {
					crmProduct.setModel(tempMap.get(s));
				}
				if(s.equals("价格")) {
					if(StringUtils.isNotBlank(tempMap.get(s))){
						try {
							crmProduct.setPrice(Double.parseDouble(tempMap.get(s)));
						}catch (Exception e) {
							resList.add("价格格式不正确!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						crmProduct.setPrice(0.0);
					}
				}
				if(s.equals("指导价")) {
					if(StringUtils.isNotBlank(tempMap.get(s))){
						try {
							crmProduct.setGuidePrice(Double.parseDouble(tempMap.get(s)));
						}catch (Exception e) {
							resList.add("指导价格式不正确!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						crmProduct.setGuidePrice(0.0);
					}
				}
				if(s.equals("是否可销售")) {
					if(StringUtils.isNotBlank(tempMap.get(s))){
						if(tempMap.get(s).equals("是")) {
							crmProduct.setStatus("0");
						}else{
							crmProduct.setStatus("1");
						}
					}else{
						crmProduct.setStatus("1");
					}
				}
				if(s.equals("备注")) {
					crmProduct.setRemark(tempMap.get(s));
				}
			}
			crmProduct.setCreateTime(createTime);
			crmProduct.setCreateUser(user.getAccountId());
			crmProduct.setOrgId(user.getOrgId());
			if(insertFlag) {
				crmProductList.add(crmProduct);
			}
		}
		for(CrmProduct crmProduct:crmProductList){
			insertCrmProduct(crmProduct);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
	}

}
