package com.core136.service.finance;

import com.core136.bean.finance.ContractDelivery;
import com.core136.bean.finance.ContractPayable;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ContractDeliveryMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ContractDeliveryService {
    private ContractDeliveryMapper contractDeliveryMapper;
	@Autowired
	public void setContractDeliveryMapper(ContractDeliveryMapper contractDeliveryMapper) {
		this.contractDeliveryMapper = contractDeliveryMapper;
	}

	public int insertContractDelivery(ContractDelivery contractDelivery) {
        return contractDeliveryMapper.insert(contractDelivery);
    }
    /**
     * 批量删除发货记录
     * @param orgId 机构码
     * @param list 发货记录Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteContractDeliveryByIds(String orgId, List<String> list)
    {
        if(list.isEmpty())
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(ContractPayable.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, contractDeliveryMapper.deleteByExample(example));
        }
    }
    public int deleteContractDelivery(ContractDelivery contractDelivery) {
        return contractDeliveryMapper.delete(contractDelivery);
    }

    public int updateContractDelivery(Example example, ContractDelivery contractDelivery) {
        return contractDeliveryMapper.updateByExampleSelective(contractDelivery, example);
    }

    public ContractDelivery selectOneContractDelivery(ContractDelivery contractDelivery) {
        return contractDeliveryMapper.selectOne(contractDelivery);
    }

    /**
     * 获取发货列表
     * @param orgId 机构码
     * @param contractType 合同类型
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 发货列表
     */
    public List<Map<String, String>> getContractDeliveryList(String orgId, String contractType, String dateQueryType,String beginTime, String endTime, String keyword) {
        return contractDeliveryMapper.getContractDeliveryList(orgId, contractType, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取发货列表
     * @param pageParam 分页参数
     * @param contractType 合同类型
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return 发货列表
     */
    public PageInfo<Map<String, String>> getContractDeliveryList(PageParam pageParam, String contractType, String dateQueryType,String beginTime, String endTime) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractDeliveryList(pageParam.getOrgId(), contractType, dateQueryType,beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
