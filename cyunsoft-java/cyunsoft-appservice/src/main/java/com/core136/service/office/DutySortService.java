package com.core136.service.office;

import com.core136.bean.office.DutySort;
import com.core136.mapper.office.DutySortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class DutySortService {
	private DutySortMapper dutySortMapper;
	@Autowired
	public void setDutySortMapper(DutySortMapper dutySortMapper) {
		this.dutySortMapper = dutySortMapper;
	}

	public int insertDutySort(DutySort dutySort)
	{
		return dutySortMapper.insert(dutySort);
	}

	public int deleteDutySort(DutySort dutySort)
	{
		return dutySortMapper.delete(dutySort);
	}

	public int updateDutySort(Example example,DutySort dutySort)
	{
		return dutySortMapper.updateByExampleSelective(dutySort,example);
	}

	public DutySort selectOneDutySort(DutySort dutySort)
	{
		return dutySortMapper.selectOne(dutySort);
	}

	/**
	 * 获取值班分类
	 * @param orgId 机构码
	 * @return
	 */
	public List<DutySort>getDutySortList(String orgId)
	{
		DutySort dutySort = new DutySort();
		dutySort.setOrgId(orgId);
		return dutySortMapper.select(dutySort);
	}

	/**
	 * 获取已启用的分类列表
	 * @param orgId 机构码
	 * @return
	 */
	public List<DutySort>getDutySortListForSelect(String orgId)
	{
		DutySort dutySort = new DutySort();
		dutySort.setOrgId(orgId);
		dutySort.setStatus("1");
		return dutySortMapper.select(dutySort);
	}


}
