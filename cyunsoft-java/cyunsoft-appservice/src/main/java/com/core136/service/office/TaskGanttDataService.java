package com.core136.service.office;

import com.core136.bean.office.TaskGanttData;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.TaskGanttDataMapper;
import com.core136.service.account.UserInfoService;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
public class TaskGanttDataService {
    private TaskGanttDataMapper taskGanttDataMapper;
    private UserInfoService userInfoService;
    private TaskGanttLinkService taskGanttLinkService;
	@Autowired
	public void setTaskGanttDataMapper(TaskGanttDataMapper taskGanttDataMapper) {
		this.taskGanttDataMapper = taskGanttDataMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setTaskGanttLinkService(TaskGanttLinkService taskGanttLinkService) {
		this.taskGanttLinkService = taskGanttLinkService;
	}

	public int insertTaskGanttData(TaskGanttData taskGanttData) {
        return taskGanttDataMapper.insert(taskGanttData);
    }

    public int deleteTaskGanttData(TaskGanttData taskGanttData) {
        return taskGanttDataMapper.delete(taskGanttData);
    }

    public int updateTaskGanttData(Example example, TaskGanttData taskGanttData) {
        return taskGanttDataMapper.updateByExampleSelective(taskGanttData, example);
    }

    public TaskGanttData selectOneTaskGanttData(TaskGanttData taskGanttData) {
        return taskGanttDataMapper.selectOne(taskGanttData);
    }

	/**
	 * 获取任务列表
	 * @param orgId 机构码
	 * @param taskId
	 * @return
	 */
	public List<TaskGanttData> getAllTaskGanttDataList(String orgId,String taskId) {
		Example example = new Example(TaskGanttData.class);
		example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("taskId",taskId);
		List<TaskGanttData> list = taskGanttDataMapper.selectByExample(example);
		List<TaskGanttData> taskGanttDataList = new ArrayList<TaskGanttData>();
		for (TaskGanttData ganttData : list) {
			if (StringUtils.isNotBlank(ganttData.getAccountId())) {
				ganttData.setUserName(userInfoService.getUserNamesByAccountIds(orgId, Arrays.asList(ganttData.getAccountId().split(","))));
			}
			if (ganttData.getParent().equals(taskId)) {
				taskGanttDataList.add(ganttData);
			}
		}
		for (TaskGanttData taskGanttData : taskGanttDataList) {
			taskGanttData.setChildren(getChildTaskGanttDataList(taskGanttData.getTaskDataId(), list));
		}
		return taskGanttDataList;
	}

	/**
	 * 获取任务列表的父子结构
	 * @param taskDataId
	 * @param rootTaskGanttData
	 * @return
	 */
	public List<TaskGanttData> getChildTaskGanttDataList(String taskDataId, List<TaskGanttData> rootTaskGanttData) {
		List<TaskGanttData> childList = new ArrayList<TaskGanttData>();
		for (TaskGanttData taskGanttData : rootTaskGanttData) {
			if (taskGanttData.getParent().equals(taskDataId)) {
				childList.add(taskGanttData);
			}
		}
		for (TaskGanttData taskGanttData : childList) {
			taskGanttData.setChildren(getChildTaskGanttDataList(taskGanttData.getTaskDataId(), rootTaskGanttData));
		}
		if (childList.isEmpty()) {
			return null;
		}
		return childList;
	}

	/**
	 * 获取子任务列表
	 * @param orgId 机构码
	 * @param taskId
	 * @return
	 */
	public List<Map<String, String>> getGanttDataList(String orgId, String taskId) {
        return taskGanttDataMapper.getGanttDataList(orgId, taskId);
    }

	/**
	 * 获取子任甘特图信息
	 * @param orgId 机构码
	 * @param taskId
	 * @return
	 */
	public Map<String, List<Map<String, String>>> getTaskGantInfo(String orgId, String taskId) {
        Map<String, List<Map<String, String>>> map = new HashMap<String, List<Map<String, String>>>();
        map.put("data", getGanttDataList(orgId, taskId));
        map.put("links", taskGanttLinkService.getGanttLinkList(orgId, taskId));
        return map;
    }


	/**
	 * 获取我的待办任务
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param taskType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getMyTaskWorkList(String orgId, String accountId, String taskType,String status,String dateQueryType, String beginTime, String endTime, String keyword) {
        return taskGanttDataMapper.getMyTaskWorkList(orgId, accountId, taskType,status, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取移动端我的任务
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param keyword
	 * @param page
	 * @return
	 */
	public List<Map<String, String>>getMyTaskWorkListForApp(String orgId,String accountId,String keyword,Integer page) {
		return taskGanttDataMapper.getMyTaskWorkListForApp(orgId,accountId,"%"+keyword+"%",page);
	}

	/**
	 * 获取我的待办任务
	 * @param pageParam 分页参数
	 * @param taskType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyTaskWorkList(PageParam pageParam, String taskType,String status,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyTaskWorkList(pageParam.getOrgId(), pageParam.getAccountId(), taskType,status,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取个人桌面的任务待办列表
	 * @param orgId 机构码
	 * @param accountId
	 * @return
	 */
	public List<Map<String, String>> getTaskListForDesk(String orgId, String accountId) {
        return taskGanttDataMapper.getTaskListForDesk(orgId, accountId);
    }

    public List<Map<String, String>> getMyTaskListForDesk(String orgId, String accountId) {
        return taskGanttDataMapper.getMyTaskListForDesk(orgId, accountId);
    }

    public List<Map<String, String>> getMobileMyTaskList(String orgId, String accountId, Integer page) {
        return taskGanttDataMapper.getMobileMyTaskList(orgId, accountId, page);
    }

}
