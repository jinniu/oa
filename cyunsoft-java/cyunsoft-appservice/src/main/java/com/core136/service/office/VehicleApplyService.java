package com.core136.service.office;

import com.core136.bean.office.VehicleApply;
import com.core136.bean.office.VehicleDriver;
import com.core136.bean.office.VehicleInfo;
import com.core136.bean.office.VehicleOilCard;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.VehicleApplyMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class VehicleApplyService {
    private VehicleApplyMapper vehicleApplyMapper;
    private VehicleDriverService vehicleDriverService;
    private VehicleInfoService vehicleInfoService;
    private VehicleOilCardService vehicleOilCardService;
	@Autowired
	public void setVehicleApplyMapper(VehicleApplyMapper vehicleApplyMapper) {
		this.vehicleApplyMapper = vehicleApplyMapper;
	}
	@Autowired
	public void setVehicleDriverService(VehicleDriverService vehicleDriverService) {
		this.vehicleDriverService = vehicleDriverService;
	}
	@Autowired
	public void setVehicleInfoService(VehicleInfoService vehicleInfoService) {
		this.vehicleInfoService = vehicleInfoService;
	}
	@Autowired
	public void setVehicleOilCardService(VehicleOilCardService vehicleOilCardService) {
		this.vehicleOilCardService = vehicleOilCardService;
	}

	public int insertVehicleApply(VehicleApply vehicleApply) {
        return vehicleApplyMapper.insert(vehicleApply);
    }

    public int deleteVehicleApply(VehicleApply vehicleApply) {
        return vehicleApplyMapper.delete(vehicleApply);
    }

    public int updateVehicleApply(Example example, VehicleApply vehicleApply) {
        return vehicleApplyMapper.updateByExampleSelective(vehicleApply, example);
    }

    public VehicleApply selectOneVehicleApply(VehicleApply vehicleApply) {
        return vehicleApplyMapper.selectOne(vehicleApply);
    }

	/**
	 * 获取申请记录
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param status
	 * @param accountId 用户账号
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getVehicleApplyList(String orgId, String opFlag, String status, String accountId, String dateQueryType,String beginTime, String endTime,String carType, String keyword) {
        return vehicleApplyMapper.getVehicleApplyList(orgId, opFlag, status, accountId,dateQueryType, beginTime, endTime, carType,"%" + keyword + "%");
    }

	public Map<String, String>getVehicleApprovalResult(String orgId, String applyId) {
		return vehicleApplyMapper.getVehicleApprovalResult(orgId, applyId);
	}
	/**
	 * 获取待审批列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param status
	 * @param accountId 用户账号
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getVehicleApprovedList(String orgId, String opFlag, String status,String accountId, String dateQueryType,String beginTime, String endTime,String carType, String keyword) {
        return vehicleApplyMapper.getVehicleApprovedList(orgId, opFlag, status,accountId,dateQueryType, beginTime, endTime, carType,"%" + keyword + "%");
    }

	/**
	 * 获取申请记录
	 * @param pageParam 分页参数
	 * @param status
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getVehicleApplyList(PageParam pageParam, String status,String dateQueryType, String beginTime, String endTime,String carType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleApplyList(pageParam.getOrgId(), pageParam.getOpFlag(), status, pageParam.getAccountId(), dateQueryType,beginTime, endTime,carType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取待审批列表
	 * @param pageParam 分页参数
	 * @param status
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getVehicleApprovedList(PageParam pageParam,String status,String dateQueryType, String beginTime, String endTime,String carType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleApprovedList(pageParam.getOrgId(), pageParam.getOpFlag(),status,pageParam.getAccountId(), dateQueryType,beginTime, endTime,carType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取待还车记录
	 * @param orgId
	 * @param returnFlag
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getVehicleReturnList(String orgId,String carType, String returnFlag, String dateQueryType, String beginTime, String endTime, String keyword) {
        return vehicleApplyMapper.getVehicleReturnList(orgId, carType,returnFlag, dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 *  获取待还车记录
	 * @param pageParam 分页参数
	 * @param returnFlag
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getVehicleReturnList(PageParam pageParam, String carType,String returnFlag,String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleReturnList(pageParam.getOrgId(), carType,returnFlag, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


	/**
	 * 调度员审批
	 * @param vehicleApply
	 * @return
	 */
	@Transactional(value = "generalTM")
    public RetDataBean setApprovalStatus(VehicleApply vehicleApply) {
		VehicleApply vehicleApply1 = new VehicleApply();
		vehicleApply1.setOrgId(vehicleApply.getOrgId());
		vehicleApply1.setApplyId(vehicleApply.getApplyId());
		vehicleApply1 = vehicleApplyMapper.selectOne(vehicleApply1);
        if (vehicleApply.getStatus().equals("1")) {
            Example example = new Example(VehicleInfo.class);
            example.createCriteria().andEqualTo("orgId", vehicleApply1.getOrgId()).andEqualTo("vehicleId", vehicleApply1.getVehicleId());
            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.setStatus("1");
            vehicleInfoService.updateVehicleInfo(example, vehicleInfo);

			if(StringUtils.isNotBlank(vehicleApply1.getOilCard())) {
				Example example2 = new Example(VehicleOilCard.class);
				example2.createCriteria().andEqualTo("orgId", vehicleApply1.getOrgId()).andEqualTo("cardId", vehicleApply1.getOilCard());
				VehicleOilCard vehicleOilCard = new VehicleOilCard();
				vehicleInfo.setStatus("1");
				vehicleOilCardService.updateVehicleOilCard(example2, vehicleOilCard);
			}
			if(StringUtils.isNotBlank(vehicleApply1.getTargetDriver())) {
				Example example2 = new Example(VehicleDriver.class);
				example2.createCriteria().andEqualTo("orgId", vehicleApply1.getOrgId()).andEqualTo("driverId", vehicleApply1.getTargetDriver());
				VehicleDriver vehicleDriver = new VehicleDriver();
				vehicleDriver.setStatus("1");
				vehicleDriverService.updateVehicleDriver(example2, vehicleDriver);
			}

        }
        Example example = new Example(VehicleApply.class);
        example.createCriteria().andEqualTo("orgId", vehicleApply1.getOrgId()).andEqualTo("applyId", vehicleApply1.getApplyId());
        updateVehicleApply(example, vehicleApply);
        return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS);
    }


	/**
	 * 车辆归还
	 * @param vehicleApply
	 * @param mileage
	 * @param balance
	 * @return
	 */
	@Transactional(value = "generalTM")
    public RetDataBean setReturnVehicle(VehicleApply vehicleApply,Double mileage,Double balance) {
        VehicleApply vehicleApply1 = new VehicleApply();
        vehicleApply1.setOrgId(vehicleApply.getOrgId());
        vehicleApply1.setApplyId(vehicleApply.getApplyId());
        vehicleApply1 = selectOneVehicleApply(vehicleApply1);
        Example example1 = new Example(VehicleInfo.class);
        example1.createCriteria().andEqualTo("orgId", vehicleApply1.getOrgId()).andEqualTo("vehicleId", vehicleApply1.getVehicleId());
        VehicleInfo vehicleInfo = new VehicleInfo();
        vehicleInfo.setStatus("0");
        vehicleInfo.setMileage(mileage);
        vehicleInfoService.updateVehicleInfo(example1, vehicleInfo);

        if(StringUtils.isNotBlank(vehicleApply1.getOilCard())) {
			Example example2 = new Example(VehicleOilCard.class);
			example2.createCriteria().andEqualTo("orgId", vehicleApply1.getOrgId()).andEqualTo("cardId", vehicleApply1.getOilCard());
			VehicleOilCard vehicleOilCard = new VehicleOilCard();
			vehicleOilCard.setBalance(balance);
			vehicleInfo.setStatus("0");
			vehicleOilCardService.updateVehicleOilCard(example2, vehicleOilCard);
		}
		if(StringUtils.isNotBlank(vehicleApply1.getTargetDriver())) {
			Example example2 = new Example(VehicleDriver.class);
			example2.createCriteria().andEqualTo("orgId", vehicleApply1.getOrgId()).andEqualTo("driverId", vehicleApply1.getTargetDriver());
			VehicleDriver vehicleDriver = new VehicleDriver();
			vehicleDriver.setStatus("0");
			vehicleDriverService.updateVehicleDriver(example2, vehicleDriver);
		}
        Example example = new Example(VehicleApply.class);
        example.createCriteria().andEqualTo("orgId", vehicleApply1.getOrgId()).andEqualTo("applyId", vehicleApply1.getApplyId());
        updateVehicleApply(example, vehicleApply);
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

}
