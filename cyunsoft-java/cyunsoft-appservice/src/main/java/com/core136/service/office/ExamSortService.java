package com.core136.service.office;

import com.core136.bean.office.ExamSort;
import com.core136.mapper.office.ExamSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExamSortService {
    private ExamSortMapper examSortMapper;
	@Autowired
	public void setExamSortMapper(ExamSortMapper examSortMapper) {
		this.examSortMapper = examSortMapper;
	}

	public int insertExamSort(ExamSort examSort) {
        return examSortMapper.insert(examSort);
    }

    public int deleteExamSort(ExamSort examSort) {
        return examSortMapper.delete(examSort);
    }

    public int updateExamSort(Example example, ExamSort examSort) {
        return examSortMapper.updateByExampleSelective(examSort, example);
    }

    public ExamSort selectOneExamSort(ExamSort examSort) {
        return examSortMapper.selectOne(examSort);
    }

    public List<ExamSort> getAllExamSort(String orgId) {
		Example example = new Example(ExamSort.class);
		example.createCriteria().andEqualTo("orgId",orgId);
		example.setOrderByClause("sort_no desc");
        return examSortMapper.selectByExample(example);
    }

	/**
	 * 获取分类树结构
	 * @param orgId 机构码
	 * @return
	 */
	public List<ExamSort> getExamSortTree(String orgId) {
		List<ExamSort> list = getAllExamSort(orgId);
		List<ExamSort> examSortList = new ArrayList<>();
		for (ExamSort sort : list) {
			if (sort.getParentId().equals("0")) {
				examSortList.add(sort);
			}
		}
		for (ExamSort examSort : examSortList) {
			examSort.setChildren(getChildSortList(examSort.getSortId(), list));
		}
		return examSortList;
	}

	/**
	 * 获取分类子表
	 * @param sortId 分类Id
	 * @param rootExamSort 分类对象列表
	 * @return 分类子表
	 */
	public List<ExamSort> getChildSortList(String sortId, List<ExamSort> rootExamSort) {
		List<ExamSort> childList = new ArrayList<>();
		for (ExamSort examSort : rootExamSort) {
			if (examSort.getParentId().equals(sortId)) {
				childList.add(examSort);
			}
		}
		for (ExamSort examSort : childList) {
			examSort.setChildren(getChildSortList(examSort.getSortId(), rootExamSort));
		}
		if (childList.isEmpty()) {
			return null;
		}
		return childList;
	}

	/**
	 * 判断分类下是否有子分类
	 * @param orgId 机构码
	 * @param sortId
	 * @return
	 */
    public int isExistChild(String orgId, String sortId) {
		ExamSort examSort = new ExamSort();
		examSort.setOrgId(orgId);
		examSort.setParentId(sortId);
        return examSortMapper.selectCount(examSort);
    }

}
