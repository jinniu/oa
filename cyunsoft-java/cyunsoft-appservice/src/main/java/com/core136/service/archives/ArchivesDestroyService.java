package com.core136.service.archives;

import com.core136.bean.archives.ArchivesDestroy;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesDestroyMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 档案销毁记录操作服务类
 * @author lsq
 */
@Service
public class ArchivesDestroyService {
    private ArchivesDestroyMapper archivesDestroyMapper;
	@Autowired
	public void setArchivesDestroyMapper(ArchivesDestroyMapper archivesDestroyMapper) {
		this.archivesDestroyMapper = archivesDestroyMapper;
	}

	/**
     * 创建档案销毁记录
     * @param archivesDestroy 档案销毁记录对象
     * @return 成功创建记录数
     */
    public int insertArchivesDestroy(ArchivesDestroy archivesDestroy)
    {
        return archivesDestroyMapper.insert(archivesDestroy);
    }

    /**
     * 删除档案销毁记录
     * @param archivesDestroy 档案销毁记录对象
     * @return 成功删除记录数
     */
    public int deleteArchivesDestroy(ArchivesDestroy archivesDestroy)
    {
        return archivesDestroyMapper.delete(archivesDestroy);
    }

    /**
     * 批量删除档案销毁记录
     * @param orgId 机构码
     * @param list 销毁记录Id 数组
     * @return 成功删除记录数
     */
    public RetDataBean deleteArchivesDestroyIds(String orgId, List<String> list) {
        if (list.isEmpty()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            Example example = new Example(ArchivesDestroy.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesDestroyMapper.deleteByExample(example));
        }
    }

    /**
     * 更新档案销毁记录
     * @param example 更新条件
     * @param archivesDestroy 档案销毁记录对象
     * @return 成功更新记录数
     */
    public int updateArchivesDestroy(Example example,ArchivesDestroy archivesDestroy)
    {
        return archivesDestroyMapper.updateByExampleSelective(archivesDestroy,example);
    }

    /**
     * 查询单个档案销毁记录
     * @param archivesDestroy 档案销毁记录对象
     * @return 档案销毁记录对象
     */
    public ArchivesDestroy selectOneArchivesDestroy(ArchivesDestroy archivesDestroy)
    {
        return archivesDestroyMapper.selectOne(archivesDestroy);
    }

    /**
     * 获取档案销毁记录
     * @param orgId 机构码
     * @param opFlag 管理员标识
     * @param accountId 用户账号
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param destroyType 销毁类型
     * @param keyword 查询关键词
     * @return 销毁记录列表
     */
    public List<Map<String,String>>getArchivesDestroyList(String orgId, String opFlag, String accountId, String dateQueryType, String beginTime, String endTime, String destroyType, String keyword)
    {
        return archivesDestroyMapper.getArchivesDestroyList(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,destroyType,"%"+keyword+"%");
    }

    /**
     * 获取档案销毁记录
     * @param pageParam 分页参数
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param destroyType 销毁类型
     * @return 销毁记录列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getArchivesDestroyList(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String destroyType) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesDestroyList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),dateQueryType,beginTime,endTime,destroyType, pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }

}
