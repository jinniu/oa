package com.core136.service.office;

import com.core136.bean.office.MeetingDevice;
import com.core136.mapper.office.MeetingDeviceMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class MeetingDeviceService {
    private MeetingDeviceMapper meetingDeviceMapper;
	@Autowired
	public void setMeetingDeviceMapper(MeetingDeviceMapper meetingDeviceMapper) {
		this.meetingDeviceMapper = meetingDeviceMapper;
	}

	public int insertMeetingDevice(MeetingDevice meetingDevice) {
        return meetingDeviceMapper.insert(meetingDevice);
    }

    public int deleteMeetingDevice(MeetingDevice meetingDevice) {
        return meetingDeviceMapper.delete(meetingDevice);
    }

    public int updateMeetingDevice(Example example, MeetingDevice meetingDevice) {
        return meetingDeviceMapper.updateByExampleSelective(meetingDevice, example);
    }

    public MeetingDevice selectOneMeetingDevice(MeetingDevice meetingDevice) {
        return meetingDeviceMapper.selectOne(meetingDevice);
    }

    /**
     * 获取会议设备列表
     * @param orgId 机构码
     * @return 会议设备列表
     */
    public List<Map<String, String>> getMeetingDeviceList(String orgId) {
        return meetingDeviceMapper.getMeetingDeviceList(orgId);
    }


	/**
	 * 获取权限内可用的会议室设备
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @return 可用的会议室设备
	 */
	public List<Map<String, String>> getCanUseDeviceList(String orgId, String deptId) {
        return meetingDeviceMapper.getCanUseDeviceList(orgId, deptId);
    }

	/**
	 * 获取设备名称列表
	 * @param orgId 机构码
	 * @param deviceIds 设备Id字符串
	 * @return 设备名称列表
	 */
    public String getMeetingDeviceListName(String orgId, String deviceIds) {
        if (StringUtils.isNotBlank(deviceIds)) {
            String[] deviceIdArr = deviceIds.split(",");
            List<String> list = Arrays.asList(deviceIdArr);
            List<Map<String, String>> listMap = meetingDeviceMapper.getDeviceListName(orgId, list);
            List<String> list2 = new ArrayList<>();
			for (Map<String, String> map : listMap) {
				list2.add(map.get("deviceName"));
			}
            return StringUtils.join(list2, ",");
        } else {
            return "";
        }
    }
}
