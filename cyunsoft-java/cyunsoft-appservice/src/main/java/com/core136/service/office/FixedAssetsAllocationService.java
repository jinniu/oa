package com.core136.service.office;

import com.core136.bean.office.FixedAssets;
import com.core136.bean.office.FixedAssetsAllocation;
import com.core136.bean.office.FixedAssetsApply;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.FixedAssetsAllocationMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class FixedAssetsAllocationService {
    private FixedAssetsAllocationMapper fixedAssetsAllocationMapper;
    private FixedAssetsService fixedAssetsService;
    private FixedAssetsApplyService fixedAssetsApplyService;
	@Autowired
	public void setFixedAssetsAllocationMapper(FixedAssetsAllocationMapper fixedAssetsAllocationMapper) {
		this.fixedAssetsAllocationMapper = fixedAssetsAllocationMapper;
	}
	@Autowired
	public void setFixedAssetsService(FixedAssetsService fixedAssetsService) {
		this.fixedAssetsService = fixedAssetsService;
	}
	@Autowired
	public void setFixedAssetsApplyService(FixedAssetsApplyService fixedAssetsApplyService) {
		this.fixedAssetsApplyService = fixedAssetsApplyService;
	}

	public int insertFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation) {
        return fixedAssetsAllocationMapper.insert(fixedAssetsAllocation);
    }

    public int deleteFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation) {
        return fixedAssetsAllocationMapper.delete(fixedAssetsAllocation);
    }

    public int updateFixedAssetsAllocation(Example example, FixedAssetsAllocation fixedAssetsAllocation) {
        return fixedAssetsAllocationMapper.updateByExampleSelective(fixedAssetsAllocation, example);
    }

    public FixedAssetsAllocation selectOneFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation) {
        return fixedAssetsAllocationMapper.selectOne(fixedAssetsAllocation);
    }

	/**
	 * 调拨
	 * @param fixedAssetsAllocation 调拨对象
	 * @param applyId 申请Id
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
    public RetDataBean addFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation, String applyId) {
        FixedAssetsApply fixedAssetsApply = new FixedAssetsApply();
        fixedAssetsApply.setOrgId(fixedAssetsAllocation.getOrgId());
        fixedAssetsApply.setApplyId(applyId);
        fixedAssetsApply = fixedAssetsApplyService.selectOneFixedAssetsApply(fixedAssetsApply);
        FixedAssets fixedAssets = new FixedAssets();
        fixedAssets.setAssetsId(fixedAssetsApply.getAssetsId());
        fixedAssets.setOrgId(fixedAssetsAllocation.getOrgId());
        fixedAssets = fixedAssetsService.selectOneFixedAssets(fixedAssets);
        if (fixedAssets.getStatus().equals("0")) {
            fixedAssetsAllocation.setOldOwnDept(fixedAssets.getOwnDept());
            fixedAssetsAllocation.setNewOwnDept(fixedAssetsApply.getUsedDept());
            fixedAssetsAllocation.setAssetsId(fixedAssets.getAssetsId());
            insertFixedAssetsAllocation(fixedAssetsAllocation);
            fixedAssets.setStatus("1");
            fixedAssets.setOwnDept(fixedAssetsAllocation.getNewOwnDept());
            Example example = new Example(FixedAssets.class);
            example.createCriteria().andEqualTo("orgId", fixedAssets.getOrgId()).andEqualTo("assetsId", fixedAssets.getAssetsId());
            fixedAssetsService.updateFixedAssets(example, fixedAssets);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } else if (fixedAssets.getStatus().equals("1")) {
            return RetDataTools.NotOk(MessageCode.MSG_00029);
        } else if (fixedAssets.getStatus().equals("2")) {
            return RetDataTools.NotOk(MessageCode.MSG_00030);
        } else {
            return RetDataTools.NotOk(MessageCode.MSG_00031);
        }

    }

    /**
     * 历史调拨记录
     * @param orgId 机构码
     * @param dateQueryType
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param deptId 部门Id
     * @param sortId 分类Id
     * @param keyword 查询关键词
     * @return 调拨记录
     */
    public List<Map<String, String>> getFixedAssetsAllocationOldList(String orgId,String dateQueryType,String beginTime,String endTime, String deptId, String sortId, String keyword) {
        return fixedAssetsAllocationMapper.getFixedAssetsAllocationOldList(orgId, dateQueryType,beginTime,endTime, deptId, sortId, "%" + keyword + "%");
    }

    /**
     * 历史调拨记录
     * @param pageParam 分页参数
     * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param deptId 部门Id
     * @param sortId 分类Id
     * @return 历史调拨记录
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getFixedAssetsAllocationOldList(PageParam pageParam, String dateQueryType,String beginTime,String endTime,String deptId, String sortId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFixedAssetsAllocationOldList(pageParam.getOrgId(), dateQueryType,beginTime,endTime,deptId, sortId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
