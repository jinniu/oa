package com.core136.mapper.finance;

import com.core136.bean.finance.BudgetCostApply;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BudgetCostApplyMapper extends MyMapper<BudgetCostApply> {
	/**
	 * 获取申请记录列表
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param projectId
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getBudgetCostApplyList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                     @Param(value = "accountId") String accountId, @Param(value = "projectId") String projectId,
                                                     @Param(value="dateQueryType") String dateQueryType,
                                                     @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                     @Param(value = "status") String status, @Param(value = "keyword") String keyword);

	/**
	 * 获取预算费用审批记录
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param projectId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getBudgetCostApprovalList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
														@Param(value = "accountId") String accountId, @Param(value = "projectId") String projectId,
														@Param(value="dateQueryType") String dateQueryType,
														@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
														@Param(value = "status") String status, @Param(value = "keyword") String keyword);

}
