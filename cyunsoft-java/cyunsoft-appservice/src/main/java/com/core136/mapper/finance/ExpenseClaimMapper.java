package com.core136.mapper.finance;

import com.core136.bean.finance.ExpenseClaim;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExpenseClaimMapper extends MyMapper<ExpenseClaim> {
	/**
	 * 获取人员报销列表
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param accountId 用户账号
	 * @param expenseType 费用类型
	 * @param beginTime 年度开始时间
	 * @param endTime 年度结束时间
	 * @return 消息结构
	 */
	List<Map<String,String>> getExpenseClaimList(@Param(value="orgId")String orgId, @Param(value="deptId")String deptId,
												 @Param(value="accountId")String accountId, @Param(value="expenseType")String expenseType,
												 @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime);
}
