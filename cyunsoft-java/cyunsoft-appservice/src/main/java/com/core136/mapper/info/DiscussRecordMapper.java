package com.core136.mapper.info;

import com.core136.bean.info.DiscussRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DiscussRecordMapper extends MyMapper<DiscussRecord> {


	List<Map<String, String>> getDiscussUserList(@Param(value = "orgId") String orgId, @Param(value = "discussId") String discussId);

    /**
     * 获取帖子与子帖
     *
     * @param orgId
     * @param recordId
     * @return
     */
    List<Map<String, String>> getDiscussRecordListById(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "recordId") String recordId);
	/**
	 * 获取讨论主题列表
	* @param orgId 机构码
	 * @param discussId
	 * @param accountId 用户账号
	 * @param keyword
	 * @param page
	 * @return
	 */
    List<Map<String, String>> getTopDiscussRecordList(@Param(value = "orgId") String orgId,
													  @Param(value = "discussId") String discussId,
													  @Param(value = "accountId") String accountId,
													  @Param(value = "keyword") String keyword,
													  @Param(value="page") Integer page);


}
