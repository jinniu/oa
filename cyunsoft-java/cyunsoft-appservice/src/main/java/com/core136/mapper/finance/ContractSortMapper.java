package com.core136.mapper.finance;

import com.core136.bean.finance.ContractSort;
import com.core136.common.dbutils.MyMapper;

/**
 * @author lsq
 */
public interface ContractSortMapper extends MyMapper<ContractSort> {


}
