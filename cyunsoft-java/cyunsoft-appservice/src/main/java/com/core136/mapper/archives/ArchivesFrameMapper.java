package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesFrame;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 档案架接口
 * @author lsq
 */
public interface ArchivesFrameMapper extends MyMapper<ArchivesFrame> {
	/**
	 * 获取档案架号列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 档案架号列表
	 */
	List<Map<String,String>>getArchivesFrameListForManage(@Param(value="orgId")String orgId,@Param(value = "keyword")String keyword);

	/**
	 * 获取档案架号列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 档案架号列表
	 */
	List<Map<String,String>> getArchivesFrameListForSelect(@Param(value = "orgId")String orgId,@Param(value="keyword")String keyword);
}
