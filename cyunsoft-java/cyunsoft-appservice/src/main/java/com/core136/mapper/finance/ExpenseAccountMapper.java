package com.core136.mapper.finance;

import com.core136.bean.finance.ExpenseAccount;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExpenseAccountMapper extends MyMapper<ExpenseAccount> {
	List<Map<String,String>> getExpenseAccountForSelect(@Param(value="orgId")String orgId);
}
