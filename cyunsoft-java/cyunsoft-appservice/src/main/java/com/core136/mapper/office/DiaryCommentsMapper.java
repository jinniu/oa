package com.core136.mapper.office;

import com.core136.bean.office.DiaryComments;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DiaryCommentsMapper extends MyMapper<DiaryComments> {

	/**
	 * 获取评论列表
	* @param orgId 机构码
	 * @param diaryId
	 * @return
	 */
    List<Map<String, String>> getDiaryCommentsList(@Param(value = "orgId") String orgId, @Param(value = "diaryId") String diaryId,@Param(value="accountId")String accountId);

	/**
	 * 获取评论数
	* @param orgId 机构码
	 * @param accountId
	 * @return
	 */
	Integer getMyDiaryCommentsCount(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
