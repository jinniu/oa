package com.core136.mapper.info;

import com.core136.bean.info.Discuss;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DiscussMapper extends MyMapper<Discuss> {

	/**
	 * 获取版块信息
	 * @param orgId 机构码
	 * @return 版块信息
	 */
    List<Map<String, String>> getDiscussList(@Param(value = "orgId") String orgId);

	/**
	 * 获取个人讨论区列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @return 讨论区列表
	 */
    List<Map<String, String>> getMyDiscussList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                               @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel);

}
