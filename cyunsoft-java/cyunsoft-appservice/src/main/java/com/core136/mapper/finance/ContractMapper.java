package com.core136.mapper.finance;

import com.core136.bean.finance.Contract;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
public interface ContractMapper extends MyMapper<Contract> {
	/**
	 *
	* @param orgId 机构码
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getContractForSelect(@Param(value="orgId") String orgId,@Param(value="keyword")String keyword);

	/**
	 * 获取年分的合同总数
	 * @param orgId
	 * @return
	 */
	int getContractCount(@Param(value = "orgId") String orgId);

    /**
     * 合同查询
     * @param orgId
     * @param sortId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param contractType
     * @param keyword
     * @return
     */
    List<Map<String, String>> queryContract(
            @Param(value = "orgId") String orgId,
            @Param(value = "sortId") String sortId,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "contractType") String contractType,
            @Param(value = "keyword") String keyword
    );

	/**
	 * 获取合同详情
	 * @param orgId 机构码
	 * @param contractId 合同Id
	 * @return 合同详情
	 */
	Map<String,String>getContractById(@Param(value="orgId")String orgId,@Param(value="contractId")String contractId);

    /**
     * 获取合同管理列表
     * @param orgId
     * @param opFlag
     * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param contractType
     * @param keyword
     * @return
     */
    List<Map<String, String>> getContractManageList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "sortId") String sortId,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "contractType") String contractType,
            @Param(value = "keyword") String keyword
    );

    /**
     * 获取近期的合同列表
     * @param orgId
     * @return
     */
    List<Map<String, String>> getContractTop(@Param(value = "orgId") String orgId);

}
