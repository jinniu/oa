package com.core136.mapper.office;

import com.core136.bean.office.TaskGanttData;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TaskGanttDataMapper extends MyMapper<TaskGanttData> {

	/**
	 * 获取子任务列表
	* @param orgId 机构码
	 * @param taskId
	 * @return
	 */
	List<Map<String, String>> getGanttDataList(@Param(value = "orgId") String orgId, @Param(value = "taskId") String taskId);

	/**
	 * 获取移动端我的任务
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param keyword
	 * @param page
	 * @return
	 */
    List<Map<String,String>>getMyTaskWorkListForApp(@Param(value="orgId")String orgId,@Param(value="accountId")String accountId,@Param(value="keyword")String keyword,@Param(value="page")Integer page);

	/**
	 *  获取我的待办任务
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param taskType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getMyTaskWorkList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "taskType") String taskType,
			@Param(value = "status") String status,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );

	/**
	 * 获取个人桌面的任务待办列表
	* @param orgId 机构码
	 * @param accountId
	 * @return
	 */
	List<Map<String, String>> getTaskListForDesk(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId);

    List<Map<String, String>> getMyTaskListForDesk(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId);

    List<Map<String, String>> getMobileMyTaskList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "page") Integer page);
}
