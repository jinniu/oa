package com.core136.mapper.crm;

import com.core136.bean.crm.CrmLinkMan;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CrmLinkManMapper extends MyMapper<CrmLinkMan> {


	List<Map<String, String>> getCrmLinkManListForSelect(@Param(value = "orgId") String orgId, @Param(value = "customerId") String customerId);
	/**
	 * 获取客户联系人列表
	 * @param orgId 机构码
	 * @param customerId
	 * @return
	 */
    List<Map<String, String>> getCrmLinkManList(@Param(value = "orgId") String orgId, @Param(value = "customerId") String customerId);


	/**
	 * 获取CRM联系人列表
	 * @param orgId 机构码
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getCrmLinkManAllList(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

	/**
	 * 业务员客户联系人
	* @param orgId 机构码
	 * @param keepUser
	 * @param keyword
	 * @return
	 */
    List<Map<String, Object>> getMyCrmLinkManAllList(@Param(value = "orgId") String orgId, @Param(value = "keepUser") String keepUser, @Param(value = "keyword") String keyword);

	/**
	 * 联系人基本信息
	* @param orgId 机构码
	 * @param linkManId
	 * @return
	 */
	Map<String, Object> getCrmLinkManInfo(@Param(value = "orgId") String orgId, @Param(value = "linkManId") String linkManId);


}
