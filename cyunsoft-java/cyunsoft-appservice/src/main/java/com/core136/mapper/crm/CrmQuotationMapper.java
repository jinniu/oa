package com.core136.mapper.crm;

import com.core136.bean.crm.CrmQuotation;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CrmQuotationMapper extends MyMapper<CrmQuotation> {

	/**
	 * 获取报价列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getMyCrmQuotationList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value="dateQueryType")String dateQueryType,
                                                    @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "status") String status,
													@Param(value = "keyword") String keyword);

	/**
	 * 获取审批列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param opFlag 管理员标识
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getMyApprovedList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,@Param(value = "accountId") String accountId,
												@Param(value = "status") String status,@Param(value = "keyword") String keyword);
	/**
	 * 获取报价单明细
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param quotationId
	 * @return
	 */
	Map<String,String>getCrmQuotationFormJsonById(@Param(value="orgId")String orgId,@Param(value="accountId")String accountId,@Param(value="quotationId")String quotationId);



	/**
	 * 获取报价单列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getCrmQuotationListForSelect(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value="keyword")String keyword);


}
