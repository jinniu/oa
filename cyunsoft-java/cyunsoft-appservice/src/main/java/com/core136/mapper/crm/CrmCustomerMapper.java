package com.core136.mapper.crm;

import com.core136.bean.crm.CrmCustomer;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CrmCustomerMapper extends MyMapper<CrmCustomer> {
	/**
	 * 获取客户详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param customerId
	 * @return
	 */
	Map<String,String>getCrmCustomerById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
										 @Param(value="accountId")String accountId,@Param(value="customerId")String customerId);

	/**
	 * 获取个人的客户列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param industry
	 * @param customerType
	 * @param crmLevel
	 * @param intention
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getCrmCustomerList(@Param(value = "orgId")String orgId,@Param(value="accountId")String accountId,
												 @Param(value="industry")String industry,@Param(value="customerType")String customerType,
												 @Param(value = "crmLevel")String crmLevel,@Param(value="intention")String intention,
												 @Param(value="keyword")String keyword);

	/**
	 * 获取权限内所有客户
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param status 状态
	 * @param industry 企业类型
	 * @param customerType 客户类型
	 * @param crmLevel 等级
	 * @param intention 意向
	 * @param keyword 查询关键词
	 * @return 客户列表
	 */
    List<Map<String, String>> getAllCrmCustomerList(@Param(value = "orgId")String orgId,@Param(value="opFlag")String opFlag,@Param(value="accountId")String accountId,
													@Param(value="status")String status,@Param(value="industry")String industry,@Param(value="customerType")String customerType,
													@Param(value = "crmLevel")String crmLevel,@Param(value="intention")String intention,
													@Param(value="keyword")String keyword);

	/**
	 * 获取个人客户列表
	 * @param orgId 机构码
	 * @param keepUser
	 * @return
	 */
    List<Map<String, String>> getMyCustomerListForSelect(@Param(value = "orgId") String orgId, @Param(value = "keepUser") String keepUser,@Param(value = "customerId") String customerId,@Param(value = "keyword") String keyword);


	List<Map<String, String>> getAllCustomerListForSelect(@Param(value = "orgId") String orgId, @Param(value = "customerId") String customerId, @Param(value = "keyword") String keyword);

}
