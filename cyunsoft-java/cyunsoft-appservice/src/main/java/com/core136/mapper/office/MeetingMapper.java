package com.core136.mapper.office;

import com.core136.bean.office.Meeting;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 会议接口
 * @author lsq
 *
 */
public interface MeetingMapper extends MyMapper<Meeting> {
	/**
	 * 获取会议详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param meetingId 会议Id
	 * @return 会议详情
	 */
	Map<String,String>getMyMeetingById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
											@Param(value="accountId")String accountId,@Param(value="deptId")String deptId,
											@Param(value="userLevel")String userLevel,@Param(value="meetingId")String meetingId);

	/**
	 * 按日期获取当前会议室使用情况
	 * @param orgId 机构码
	 * @param dayStr 日期
	 * @param accountId 用户账号
	 * @return 会议室使用情况
	 */
	List<Map<String, String>> getMeetingListByDayStr(@Param(value = "orgId") String orgId, @Param(value = "dayStr") String dayStr,@Param(value = "accoumtId")String accountId);
	/**
	 * 获取待审批的会议列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param status 状态
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 会议列表
	 */
    List<Map<String, String>> getApplyMeetingList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                  @Param(value = "accountId") String accountId,@Param(value = "status") String status, @Param(value = "dateQueryType") String dateQueryType,
												  @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);

	/**
	 * 获取没有会议纪要的会议列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 会议列表
	 */
    List<Map<String, String>> getNotNotesMeetingList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 * 获取个人历史会议申请记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param roomId 会议室Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 申请记录列表
	 */
    List<Map<String, String>> getMyApplyMeetingList(
            @Param(value = "orgId") String orgId,
			@Param(value = "meetingType") String meetingType,
            @Param(value = "accountId") String accountId,
            @Param(value = "roomId") String roomId,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );

	/**
	 * 获取当前用户待参加会议
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param dateQueryType  日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 待参加会议列表
	 */
    List<Map<String, String>> getMyMeetingList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "userLevel") String userLevel,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );

	/**
	 * 获取桌面会议
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param nowTime 当前时间
	 * @return 桌面会议列表
	 */
    List<Map<String, String>> getMyMeetingListForDesk(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                      @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel, @Param(value = "nowTime") String nowTime);
	/**
	 * 获取移动端待办会议
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param nowTime 当前时间
	 * @param page 页码
	 * @return 待办会议列表
	 */
    List<Map<String, String>> getMyMeetingListForApp(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                     @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel, @Param(value = "nowTime") String nowTime, @Param(value = "page") Integer page);

	/**
	 * 判断当前会议室时间段内是否有会议
	 * @param orgId 机构码
	 * @param roomId 会议室Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 是否有会议
	 */
    int isExist(@Param(value = "orgId") String orgId, @Param(value = "roomId") String roomId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);
}
