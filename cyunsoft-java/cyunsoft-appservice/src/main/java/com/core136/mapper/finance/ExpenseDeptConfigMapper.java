package com.core136.mapper.finance;

import com.core136.bean.finance.ExpenseDeptConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExpenseDeptConfigMapper extends MyMapper<ExpenseDeptConfig> {
	List<Map<String,String>> getExpenseDeptConfigList(@Param(value="orgId")String orgId, @Param(value="deptId")String deptId,@Param(value="expenseYear")String expenseYear);
}
