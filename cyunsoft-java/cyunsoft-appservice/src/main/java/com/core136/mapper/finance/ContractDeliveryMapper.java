package com.core136.mapper.finance;

import com.core136.bean.finance.ContractDelivery;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ContractDeliveryMapper extends MyMapper<ContractDelivery> {

    /**
     * 获取发货列表
     * @param orgId
     * @param contractType
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword
     * @return
     */
    List<Map<String, String>> getContractDeliveryList(
            @Param(value = "orgId") String orgId, @Param(value = "contractType") String contractType,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );
}
