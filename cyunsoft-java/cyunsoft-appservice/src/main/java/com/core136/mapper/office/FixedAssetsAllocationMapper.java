package com.core136.mapper.office;

import com.core136.bean.office.FixedAssetsAllocation;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FixedAssetsAllocationMapper extends MyMapper<FixedAssetsAllocation> {

	/**
	 * 历史调拨记录
	* @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param deptId
	 * @param sortId
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getFixedAssetsAllocationOldList(@Param(value = "orgId") String orgId, @Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                              @Param(value = "endTime") String endTime,@Param(value = "deptId") String deptId, @Param(value = "sortId") String sortId, @Param(value = "keyword") String keyword);
}
