package com.core136.mapper.info;

import com.core136.bean.info.Emergency;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 紧急事件接口
 * @author lsq
 */
public interface EmergencyMapper extends MyMapper<Emergency> {
	/**
	 * 获取紧急事件列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param emergencyType 紧急事件类型
	 * @param keyword 查询关键词
	 * @return 紧急事件列表
	 */
	List<Map<String,String>> getEmergencyListForManage(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
													   @Param(value="accountId")String accountId,@Param(value="dateQueryType")String dateQueryType,
													   @Param(value="beginTime")String beginTime,@Param(value = "endTime")String endTime,
													   @Param(value="emergencyType")String emergencyType,@Param(value="keyword")String keyword);

	/**
	 * 获取当前紧急事件列表
	 * @param orgId 机构码
	 * @param nowTime 当前时间
	 * @param accountId 用户对象
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @return 紧急事件列表
	 */
	List<Map<String,String>>getEmergencyListForDesk(@Param(value="orgId")String orgId, @Param(value="nowTime")String nowTime,
													@Param(value="accountId")String accountId, @Param(value="deptId")String deptId,
													@Param(value="userLevel")String userLevel);

}
