package com.core136.mapper.office;

import com.core136.bean.office.LicenceType;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LicenceTypeMapper extends MyMapper<LicenceType> {

	List<LicenceType> getAllLicenceType(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

	List<Map<String,String>>getLicenceTypeList(@Param(value="orgId")String orgId);
}
