package com.core136.mapper.invite;

import com.core136.bean.invite.InviteFile;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteFileMapper extends MyMapper<InviteFile> {
    /**
     * 获取招标文件列表
     * @param orgId
     * @param status
     * @param fileType
     * @param keyword
     * @return
     */
    List<Map<String,String>> getInviteFileList(@Param(value="orgId")String orgId,@Param(value="status") String status,@Param(value="fileType")String fileType,@Param(value="keyword")String keyword);

    /**
     * 获取招标文件列表
     * @param orgId
     * @param keyword
     * @return
     */
    List<Map<String,String>>getInviteFileListForSelect(@Param(value="orgId")String orgId,@Param(value="keyword")String keyword);
}
