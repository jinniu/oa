package com.core136.mapper.invite;

import com.core136.bean.invite.InviteOpen;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteOpenMapper extends MyMapper<InviteOpen> {
    /**
     * 获取开标记录
     * @param orgId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword
     * @return
     */
    List<Map<String,String>> getInviteOpenList(@Param(value="orgId")String orgId, @Param(value="dateQueryType")String dateQueryType,
                                               @Param(value="beginTime")String beginTime, @Param(value="endTime")String endTime,
                                               @Param(value="keyword")String keyword);
}
