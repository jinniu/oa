package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesLibrary;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesLibraryMapper extends MyMapper<ArchivesLibrary> {

	/**
	 * 获取档案库列表
	 * @param orgId 机构码
	 * @param status 状态
	 * @param storagePeriod 保存年限
	 * @param classifiedLevel 涉密等级
	 * @param keyword 查询关键词
	 * @return 档案库列表
	 */
	List<Map<String,String>> getArchivesLibraryList(@Param(value="orgId")String orgId, @Param(value = "status")String status,
													@Param(value="storagePeriod") Integer storagePeriod,@Param(value="classifiedLevel") String classifiedLevel,
													@Param(value = "keyword")String keyword);
}
