package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesPatrol;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesPatrolMapper extends MyMapper<ArchivesPatrol> {
    /**
     * 获取档案巡检记录列表
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param dataQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param patrolType
     * @param keyword
     * @return
     */
    List<Map<String,String>> getArchivesPatrolList(@Param(value="orgId")String orgId,@Param(value="opFlag") String opFlag,
                                                   @Param(value="accountId")String accountId,@Param(value="dateQueryType")String dataQueryType,
                                                   @Param(value = "beginTime")String beginTime,@Param(value="endTime")String endTime,
                                                   @Param(value="patrolType")String patrolType,@Param(value="keyword")String keyword);
}
