package com.core136.mapper.office;

import com.core136.bean.office.VehicleRepairRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface VehicleRepairRecordMapper extends MyMapper<VehicleRepairRecord> {

	/**
	 * 获取维修列表
	* @param orgId 机构码
	 * @param repairUser
	 * @param repairType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getVehicleRepairRecordList(
            @Param(value = "orgId") String orgId, @Param(value = "repairUser") String repairUser,
            @Param(value = "repairType") String repairType,@Param(value = "dateQueryType") String dateQueryType,
			@Param(value = "beginTime") String beginTime,@Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword
    );
	List<Map<String, String>> getVehicleRepairUserList(@Param(value = "orgId") String orgId);
}
