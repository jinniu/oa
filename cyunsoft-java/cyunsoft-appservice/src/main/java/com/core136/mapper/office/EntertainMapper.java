package com.core136.mapper.office;

import com.core136.bean.office.Entertain;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EntertainMapper extends MyMapper<Entertain> {
	/**
	 * 获取接待事件列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param diningType 用餐类型
	 * @param entertainType 接待事件类型
	 * @param keyword 查询关键词
	 * @return 事件列表
	 */
	List<Map<String,String>> getEntertainListForManage(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
													   @Param(value="accountId")String accountId,@Param(value="dateQueryType")String dateQueryType,
													   @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
													   @Param(value="status")String status,@Param(value="diningType")String diningType,
													   @Param(value="entertainType")String entertainType,@Param(value="keyword")String keyword);


	/**
	 * 获取待审批事件
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param diningType 用餐类型
	 * @param entertainType 接待事件类型
	 * @param keyword 查询关键词
	 * @return 审批事件列表
	 */
	List<Map<String,String>>getEntertainListForApprove(@Param(value="orgId")String orgId,@Param(value="dateQueryType")String dateQueryType,
													   @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,@Param(value="diningType")String diningType,
													   @Param(value="entertainType")String entertainType,@Param(value="keyword")String keyword);

	/**
	 * 查询招待事件列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param diningType 用餐类型
	 * @param entertainType 接待事件类型
	 * @param keyword 查询关键词
	 * @return 事件列表
	 */
	List<Map<String,String>> getEntertainListForSearch(@Param(value="orgId")String orgId,@Param(value="dateQueryType")String dateQueryType,
													   @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
													   @Param(value="status")String status,@Param(value="diningType")String diningType,
													   @Param(value="entertainType")String entertainType,@Param(value="keyword")String keyword);

	/**
	 * 获取桌面待办招待事件
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param nowTime 当前时间
	 * @return 招待事件列表
	 */
	List<Map<String,String>>getMyEntertainListForDesk(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
													  @Param(value="accountId")String accountId,@Param(value="nowTime")String nowTime);

}
