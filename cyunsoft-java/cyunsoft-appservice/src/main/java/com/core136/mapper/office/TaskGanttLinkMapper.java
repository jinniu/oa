package com.core136.mapper.office;

import com.core136.bean.office.TaskGanttLink;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TaskGanttLinkMapper extends MyMapper<TaskGanttLink> {

	/**
	 * 获取子任务路径列表
	* @param orgId 机构码
	 * @param taskId
	 * @return
	 */
	List<Map<String, String>> getGanttLinkList(@Param(value = "orgId") String orgId, @Param(value = "taskId") String taskId);

}
