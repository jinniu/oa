package com.core136.mapper.info;

import com.core136.bean.info.DataUploadHandle;
import com.core136.common.dbutils.MyMapper;

public interface DataUploadHandleMapper extends MyMapper<DataUploadHandle> {

}
