package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesTempHum;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesTempHumMapper extends MyMapper<ArchivesTempHum> {
	/**
	 * 获取档案库房温湿度检查列表
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType  日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param checkType
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>> getArchivesTempHumList(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
													@Param(value="accountId")String accountId,@Param(value="dateQueryType")String dateQueryType,
													@Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
													@Param(value="checkType")String checkType,@Param(value="keyword")String keyword);

}
