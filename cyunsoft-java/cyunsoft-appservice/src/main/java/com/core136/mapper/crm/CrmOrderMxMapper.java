package com.core136.mapper.crm;

import com.core136.bean.crm.CrmOrderMx;
import com.core136.common.dbutils.MyMapper;

public interface CrmOrderMxMapper extends MyMapper<CrmOrderMx> {
}
