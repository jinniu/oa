package com.core136.mapper.invite;

import com.core136.bean.invite.InviteBidding;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteBiddingMapper extends MyMapper<InviteBidding> {
	/**
	 *获取待评招记录列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>> getInviteBiddingManageList(@Param(value="orgId")String orgId,@Param(value="accountId")String accountId,
												 		@Param(value = "dateQueryType")String dateQueryType,@Param(value="beginTime")String beginTime,
														@Param(value = "endTime")String endTime,@Param(value="keyword")String keyword);


	/**
	 * 获取投标记录列表
	* @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>> getInviteBiddingList(@Param(value="orgId")String orgId,@Param(value = "dateQueryType")String dateQueryType,
												  @Param(value="beginTime")String beginTime,@Param(value = "endTime")String endTime,
												  @Param(value="status")String status,@Param(value="keyword")String keyword);

}
