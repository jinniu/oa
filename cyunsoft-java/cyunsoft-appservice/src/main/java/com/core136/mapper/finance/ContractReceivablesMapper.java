package com.core136.mapper.finance;

import com.core136.bean.finance.ContractReceivables;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ContractReceivablesMapper extends MyMapper<ContractReceivables> {

    /**
     * 获取应收款列表
     * @param orgId
     * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status
     * @param keyword
     * @return
     */
    List<Map<String, Object>> getContractReceivablesList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "status") String status,
            @Param(value = "keyword") String keyword
    );
}
