package com.core136.mapper.office;

import com.core136.bean.office.DutyRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DutyRecordMapper extends MyMapper<DutyRecord> {
	/**
	 * 获取值班列表
	* @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param sortId
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String,String>> getDutyRecordList(@Param(value="orgId")String orgId,@Param(value="dateQueryType")String dateQueryType,
                                               @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
                                               @Param(value="sortId")String sortId,@Param(value="status")String status,@Param(value="keyword") String keyword);

	/**
	 * 获取值班待审批列表
	 * @param orgId
	 * @return
	 */
	List<Map<String,String>>getDutyRecordListForApproval(@Param(value="orgId")String orgId);

	/**
	 * 获取当前值班列表
	* @param orgId 机构码
	 * @param nowTime
	 * @param sortId
	 * @return
	 */
	List<Map<String,String>>getDutyListBySortId(@Param(value="orgId")String orgId,@Param(value="nowTime")String nowTime,@Param(value="sortId")String sortId);


	Map<String,String>getDutyRecordById(@Param(value="orgId")String orgId,@Param(value="recordId")String recordId);


}
