package com.core136.mapper.finance;

import com.core136.bean.finance.ExpenseComConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExpenseComConfigMapper extends MyMapper<ExpenseComConfig> {
	/**
	 * 获取公司年度费用预算列表
	 * @param orgId 机构码
	 * @param expenseYear 年度
	 * @return 费用预算列表
	 */
	List<Map<String,String>> getExpenseComConfigList(@Param(value="orgId")String orgId,@Param(value="expenseYear") String expenseYear);
}
