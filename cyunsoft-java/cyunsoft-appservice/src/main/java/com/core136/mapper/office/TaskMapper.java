package com.core136.mapper.office;

import com.core136.bean.office.Task;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TaskMapper extends MyMapper<Task> {

	Map<String, String> getTaskById(@Param(value = "orgId") String orgId,@Param(value = "taskId") String taskId);

    /**
     * 获取我的任务列表
     * @param orgId
     * @param accountId
     * @param opFlag
     * @param status
     * @param taskType
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword
     * @return
     */
    List<Map<String, String>> getManageTaskList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "status") String status,
            @Param(value = "taskType") String taskType,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword);

	List<Map<String, String>> getMyJoinTaskList(
		@Param(value = "orgId") String orgId,
		@Param(value = "accountId") String accountId,
		@Param(value = "status") String status,
		@Param(value = "taskType") String taskType,
		@Param(value = "dateQueryType") String dateQueryType,
		@Param(value = "beginTime") String beginTime,
		@Param(value = "endTime") String endTime,
		@Param(value = "keyword") String keyword);

	List<Map<String, String>> getMyShareTaskList(
		@Param(value = "orgId") String orgId,
		@Param(value = "accountId") String accountId,
		@Param(value = "deptId") String deptId,
		@Param(value = "levelId") String levelId,
		@Param(value = "status") String status,
		@Param(value = "taskType") String taskType,
		@Param(value = "dateQueryType") String dateQueryType,
		@Param(value = "beginTime") String beginTime,
		@Param(value = "endTime") String endTime,
		@Param(value = "keyword") String keyword);


	/**
	 * 获取个人负责的任务
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param status
	 * @param taskType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getMyChargeTaskList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "status") String status,
            @Param(value = "taskType") String taskType,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword);

	/**
	 * 获取个人督查的任务列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param status
	 * @param taskType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getMySupervisorTaskList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "status") String status,
            @Param(value = "taskType") String taskType,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword);

	/**
	 * 获取待分解任务列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param opFlag 管理员标识
	 * @param taskType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
    List<Map<String, String>> getAssignmentTaskList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "taskType") String taskType,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime);


	Map<String, String> getGanttInfoByTaskId(@Param(value = "orgId") String orgId,@Param(value = "taskId") String taskId);

	List<Map<String, Object>> getTaskAllocationList(
		@Param(value = "orgId") String orgId,
		@Param(value = "accountId") String accountId,
		@Param(value = "taskType") String taskType,
		@Param(value = "dateQueryType") String dateQueryType,
		@Param(value = "beginTime") String beginTime,
		@Param(value = "endTime") String endTime,
		@Param(value = "keyword") String keyword);




}
