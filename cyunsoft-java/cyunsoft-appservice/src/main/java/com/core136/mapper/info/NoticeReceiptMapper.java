package com.core136.mapper.info;

import com.core136.bean.info.NoticeReceipt;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface NoticeReceiptMapper extends MyMapper<NoticeReceipt> {
	/**
	 * 按公告Id获取相关的回执列表
	 * @param orgId 机构码
	 * @param noticeId 通知公告Id
	 * @return 回执列表
	 */
	List<Map<String,String>> getNoticeReceiptList(@Param(value="orgId")String orgId, @Param(value="noticeId")String noticeId);
}
