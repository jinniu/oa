package com.core136.mapper.info;

import com.core136.bean.info.NoticeConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 通知公告配
 * @author lsq
 */
public interface NoticeConfigMapper extends MyMapper<NoticeConfig> {
	/**
	 * 获取通知公告配置列表
	 * @param orgId
	 * @return
	 */
    List<Map<String, String>> getNoticeConfigList(@Param(value = "orgId") String orgId);
}
