package com.core136.mapper.office;

import com.core136.bean.office.ExamGrade;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExamGradeMapper extends MyMapper<ExamGrade> {
	/**
	 * 获取考试成绩列表
	* @param orgId 机构码
	 * @param examTestId
	 * @param accountId 用户账号
	 * @param dateQueryType  日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getExamGradeList(@Param(value = "orgId") String orgId, @Param(value = "examTestId") String examTestId,
                                               @Param(value = "accountId") String accountId, @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime,
                                               @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);

    List<Map<String, String>> getExamGradeOldList(@Param(value = "orgId") String orgId, @Param(value = "examTestId") String examTestId,
												  @Param(value = "accountId") String accountId, @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime,
												  @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);
}
