package com.core136.mapper.office;

import com.core136.bean.office.ExamQuestionRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExamQuestionRecordMapper extends MyMapper<ExamQuestionRecord> {

    /**
     * 获取试卷列表
     * @param orgId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param sortId
     * @param keyword
     * @return
     */
    public List<Map<String, String>> getExamQuestionRecordList(@Param(value = "orgId") String orgId,@Param(value="dateQueryType")String dateQueryType,
                                                                @Param(value="beginTime")String beginTime,@Param(value = "endTime")String endTime,
                                                                @Param(value="sortId")String sortId,@Param(value = "keyword") String keyword);

    /**
     * 获取试卷列表
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getExamQuestionRecordForSelect(@Param(value = "orgId") String orgId,@Param(value="keyword")String keyword);
}
