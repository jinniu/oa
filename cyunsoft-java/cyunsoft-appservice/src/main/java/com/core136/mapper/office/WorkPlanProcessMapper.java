package com.core136.mapper.office;

import com.core136.bean.office.WorkPlanProcess;
import com.core136.common.dbutils.MyMapper;

public interface WorkPlanProcessMapper extends MyMapper<WorkPlanProcess> {

}
