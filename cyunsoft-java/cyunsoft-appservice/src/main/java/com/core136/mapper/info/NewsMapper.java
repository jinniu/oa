package com.core136.mapper.info;

import com.core136.bean.info.News;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface NewsMapper extends MyMapper<News> {
	/**
	 * 获取新闻详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别Id
	 * @param newsId 新闻Id
	 * @return 新闻详情
	 */
	Map<String, Object>getMyNewsById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
									 @Param(value="accountId")String accountId,@Param(value="deptId")String deptId,
									 @Param(value="userLevel")String userLevel,@Param(value="newsId")String newsId);
	/**
	 * 获取新闻维护列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param newsType 新离类型
	 * @param status 状态
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 新闻列表
	 */
    List<Map<String, Object>> getNewsManageList(@Param(value = "orgId") String orgId,
                                                @Param(value = "opFlag") String opFlag,
                                                @Param(value = "accountId") String accountId,
                                                @Param(value = "newsType") String newsType,
                                                @Param(value = "status") String status,
                                                @Param(value = "dateQueryType") String dateQueryType,
                                                @Param(value = "beginTime") String beginTime,
                                                @Param(value = "endTime") String endTime,
                                                @Param(value = "keyword") String keyword);

	/**
	 *  获取个人权限内的新闻
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别Id
	 * @param newsType 新闻类型
	 * @param status 状诚
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 新闻列表
	 */
    List<Map<String, Object>> getMyNewsList(@Param(value = "orgId") String orgId,
                                            @Param(value = "accountId") String accountId,
                                            @Param(value = "deptId") String deptId,
                                            @Param(value = "userLevel") String userLevel,
                                            @Param(value = "newsType") String newsType,
                                            @Param(value = "status") String status,
                                            @Param(value = "dateQueryType") String dateQueryType,
                                            @Param(value = "beginTime") String beginTime,
                                            @Param(value = "endTime") String endTime,
                                            @Param(value = "keyword") String keyword);

	/**
	 * 我的桌面上的新闻
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别Id
	 * @return 新闻列表
	 */
    List<Map<String, String>> getMyNewsListForDesk(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "userLevel") String userLevel);

    List<Map<String, String>> getMyPicNewsListForDesk(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "userLevel") String userLevel);

	/**
	 * 获取移动端新闻列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别Id
	 * @param page 当前页
	 * @return 新闻列表
	 */
    List<Map<String, String>> getMyNewsListForApp(@Param(value = "orgId") String orgId,
                                                  @Param(value = "accountId") String accountId,
                                                  @Param(value = "deptId") String deptId,
                                                  @Param(value = "userLevel") String userLevel,
                                                  @Param(value = "page") Integer page,
												  @Param(value = "keyword")String keyword);

    /***
     * 获取新闻人员查看状态
     * @param orgId 机构码
     * @param newsId 用户账号
     * @param list 人员列表
     * @return 查看状态
     */
    List<Map<String, String>> getNewsReadStatus(@Param(value = "orgId") String orgId, @Param(value = "newsId") String newsId, @Param(value = "list") List<String> list);
}
