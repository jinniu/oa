package com.core136.mapper.finance;

import com.core136.bean.finance.ContractPayable;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ContractPayableMapper extends MyMapper<ContractPayable> {
    /**
     * 获取应付款列表
     * @param orgId
     * @param accountId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status
     * @param keyword
     * @return
     */
    List<Map<String, String>> getContractPayableList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "status") String status,
            @Param(value = "keyword") String keyword
    );

}
