package com.core136.mapper.finance;

import com.core136.bean.finance.BudgetAccount;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 预算科目接口
 * @author lsqs
 */
public interface BudgetAccountMapper extends MyMapper<BudgetAccount> {
	/**
	 * 获取预算账套列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 账套列表
	 */
    List<BudgetAccount>getAllBudgetAccountList(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

	/**
	 * 获取账套列表
	 * @param orgId 机构码
	 * @return 账套列表
	 */
	List<Map<String,String>>getBudgetAccountForSelect(@Param(value = "orgId") String orgId);

}
