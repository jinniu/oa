package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesRoom;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesRoomMapper extends MyMapper<ArchivesRoom> {
    /**
     * 获取档案库房列表
     * @param orgId 机构码
     * @param keyword 果询关键词
     * @return
     */
    List<Map<String,String>> getArchivesRoomListForSelect(@Param(value="orgId")String orgId,@Param(value="keyword")String keyword);
}
