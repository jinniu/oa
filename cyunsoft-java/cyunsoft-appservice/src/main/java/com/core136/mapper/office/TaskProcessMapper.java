package com.core136.mapper.office;

import com.core136.bean.office.TaskProcess;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TaskProcessMapper extends MyMapper<TaskProcess> {
    /**
     * 获取子任务的处理过程列表
     * @param orgId
     * @param accountId
     * @param createUser
     * @param taskType
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword
     * @return
     */
    List<Map<String, String>> getMyTaskProcessList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "createUser") String createUser,
            @Param(value = "taskType") String taskType,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );

	/**
	 * 获取处理事件详情
	* @param orgId 机构码
	 * @param taskId
	 * @param accountId
	 * @return
	 */
    List<Map<String, String>> getProcessInfo(@Param(value = "orgId") String orgId, @Param(value = "taskId") String taskId, @Param(value = "accountId") String accountId);
}
