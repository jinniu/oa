package com.core136.mapper.crm;

import com.core136.bean.crm.CrmProduct;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CrmProductMapper extends MyMapper<CrmProduct> {
	/**
	 * 获取产品列表
	 * @param orgId 机构码
	 * @param sortId
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getCrmProductListBySortId(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId, @Param(value = "keyword") String keyword);

	List<Map<String,String>>getProductListForSelect(@Param(value="orgId")String orgId);

	List<Map<String,String>>getCrmProductNameByIds(@Param(value="orgId")String orgId,@Param(value = "list")List<String>list);

	List<Map<String,String>>getCrmProductListByIdsForSelect(@Param(value="orgId")String orgId,@Param(value = "list")List<String>list);

	/**
	 * 获取产品列表
	* @param orgId 机构码
	 * @param productId
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>>getAllProductListForSelect(@Param(value="orgId")String orgId,@Param(value="productId")String productId,@Param(value="keyword")String keyword);


}
