package com.core136.mapper.invite;

import com.core136.bean.invite.InviteEnt;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteEntMapper extends MyMapper<InviteEnt> {
    /**
     * 获取供应商列表
     * @param orgId
     * @param entLevel
     * @param entType
     * @param status
     * @param creditRating
     * @param enterpriseSize
     * @param keyword
     * @return
     */
    List<Map<String,String>> getInviteEntList(@Param(value="orgId")String orgId,@Param(value="entLevel")String entLevel,
                                              @Param(value="entType")String entType,@Param(value="status")String status,
                                              @Param(value="creditRating")String creditRating,@Param(value="enterpriseSize")String enterpriseSize,
                                              @Param(value="keyword")String keyword);

    /**
     * 获取供应商选择列表
     * @param orgId
     * @param keyword
     * @return
     */
    List<Map<String,String>>getInviteEntListForSelect(@Param(value="orgId")String orgId,@Param(value="keyword")String keyword);
}
