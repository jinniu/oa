package com.core136.mapper.info;

import com.core136.bean.info.DynamicsRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DynamicsRecordMapper extends MyMapper<DynamicsRecord> {
	/**
	 * 获取动态列表
	* @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param page
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>> getDynamicsRecordList(@Param(value="orgId")String orgId,@Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,@Param(value="page")Integer page,@Param(value = "keyword")String keyword);

	List<Map<String,String>> getDynamicsRecordForDesk(@Param(value="orgId")String orgId);


}
