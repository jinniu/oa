package com.core136.mapper.info;

import com.core136.bean.info.VoteResult;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface VoteResultMapper extends MyMapper<VoteResult> {
	/**
	 * 获取投票结果
	 * @param orgId 机构码
	 * @param voteId
	 * @param parentId
	 * @return
	 */
    List<Map<String, String>> getVoteResult(@Param(value = "orgId") String orgId, @Param(value = "voteId") String voteId,@Param(value="parentId")String parentId);

	/**
	 * 获取投票人员列表
	 * @param orgId 机构码
	 * @param voteId
	 * @param itemId
	 * @return
	 */
    List<Map<String, String>> voteDetailsForUser(@Param(value = "orgId") String orgId, @Param(value = "voteId") String voteId, @Param(value = "itemId") String itemId);
}
