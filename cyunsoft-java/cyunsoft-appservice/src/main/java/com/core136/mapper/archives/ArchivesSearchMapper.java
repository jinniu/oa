package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesSearch;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesSearchMapper extends MyMapper<ArchivesSearch> {

	/**
	 * 获取热门关键字
	 * @param orgId 机构码
	 * @return
	 */
	List<Map<String, String>> getHostKeywords(@Param(value = "orgId") String orgId);


	List<Map<String, String>> getArchivesSearchList(@Param(value = "orgId") String orgId,  @Param(value = "keyword") String keyword);
}
