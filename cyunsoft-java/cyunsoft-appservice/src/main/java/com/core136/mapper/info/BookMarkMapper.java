package com.core136.mapper.info;

import com.core136.bean.info.BookMark;
import com.core136.common.dbutils.MyMapper;

public interface BookMarkMapper extends MyMapper<BookMark> {
}
