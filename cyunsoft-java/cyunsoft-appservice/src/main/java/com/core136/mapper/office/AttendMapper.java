package com.core136.mapper.office;

import com.core136.bean.office.Attend;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface AttendMapper extends MyMapper<Attend> {
	/**
	 * 获取个人打卡年份
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 年份列表
	 */
   List<Map<String,String>>getMyAttendYear(@Param(value = "orgId")String orgId,@Param(value = "accountId") String accountId);

	/**
	 * 获取个人考勤
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param attendType 类型
	 * @param status 状态
	 * @param year 年份
	 * @param month 月份
	 * @return 个人考勤列表
	 */
	List<Map<String,String>>getMyAttendList(@Param(value = "orgId")String orgId,@Param(value = "accountId") String accountId,
											   @Param(value="attendType")String attendType,@Param(value="status")String status,
											   @Param(value="year")String year,@Param(value = "month")String month);

	/**
	 * 获取年份列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 年份列表
	 */
    List<Map<String, String>> getAttendYearList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 *  获取月份
	 * @param orgId 机构码
	 * @param year 年份
	 * @param accountId 用户账号
	 * @return 月份列表
	 */
	List<Map<String, String>> getMonthList(@Param(value = "orgId") String orgId, @Param(value = "year") String year, @Param(value = "accountId") String accountId);


	/**
	 * 考勤统计查询
	 * @param orgId 机构码
	 * @param attendType 统计类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param deptId 部门Id
	 * @param createUser 考勤人员账号
	 * @return 考勤列表
	 */
    List<Map<String, String>> getTotalAttendList(@Param(value = "orgId") String orgId, @Param(value = "attendType") String attendType, @Param(value = "beginTime") String beginTime,
                                                 @Param(value = "endTime") String endTime, @Param(value = "deptId") String deptId, @Param(value = "createUser") String createUser);



	/**
	 * 获取人员请假列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param attendType 类型
	 * @return 请假列表
	 */
    List<Map<String, String>> getMyLeaveList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value="dateQueryType")String dateQueryType,
											 @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "attendType") String attendType);

	/**
	 * 获取出差列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 出差列表
	 */
    List<Map<String, String>> getMyTravelList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,  @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);


	/**
	 * 获取加班列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 加班列表
	 */
    List<Map<String, String>> getMyOverTimeList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value="dateQueryType") String dateQueryType, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

	/**
	 * 获取值班列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 值班列表
	 */
    List<Map<String, String>> getMyDutyList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value="dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

	/**
	 * 获取外出列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 外出列表
	 */
    List<Map<String, String>> getMyOutAttendList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

	/**
	 * 导出指定月份考勤详情
	 * @param orgId 机构码
	 * @param year 年份
	 * @param month 月份
	 * @param attendType 类型
	 * @param createUser 打卡人
	 * @param deptId 部门Id
	 * @param configId 考勤规则Id
	 * @return 考勤详情列表
	 */
    List<Map<String, Object>> getTotalAttendListForExp(@Param(value = "orgId") String orgId, @Param(value = "year") String year,@Param(value = "month") String month,  @Param(value = "attendType") String attendType,
													   @Param(value = "createUser") String createUser,@Param(value = "deptId") String deptId,@Param(value = "configId") String configId);

	/**
	 * 获取考勤记录列表
	* @param orgId 机构码
	 * @param year
	 * @param month
	 * @param attendType
	 * @param createUser
	 * @param deptId
	 * @param configId
	 * @return
	 */
	List<Map<String, String>> getTotalAttendListForUserExp2(@Param(value = "orgId") String orgId, @Param(value = "year") String year,@Param(value = "month") String month,
															@Param(value = "attendType") String attendType, @Param(value = "createUser") String createUser,@Param(value = "deptId") String deptId, @Param(value = "configId") String configId);

	/**
	 * 获取考勤记录列表
	 * @param orgId 机构码
	 * @param year 年份
	 * @param month 月份
	 * @param attendType 类型
	 * @param createUser 打卡人
	 * @param deptId 部门Id
	 * @param configId 考勤规则Id
	 * @return 考勤记录列表
	 */
	List<Map<String, String>> getTotalAttendListForExp2(@Param(value = "orgId") String orgId, @Param(value = "year") String year,@Param(value = "month") String month,
														@Param(value = "attendType") String attendType, @Param(value = "createUser") String createUser,@Param(value = "deptId") String deptId, @Param(value = "configId") String configId);
	/**
	 *获取移动端考勤记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param year 年份
	 * @param month 月份
	 * @param page 页码
	 * @return 考勤记录列表
	 */
	List<Map<String,String>>getMyAttendListForApp(@Param(value="orgId")String orgId,@Param(value="accountId")String accountId,@Param(value="year")String year,@Param(value="month") String month,@Param(value="page")Integer page);
}
