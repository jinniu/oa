package com.core136.mapper.info;

import com.core136.bean.info.NoticeTemplate;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
public interface NoticeTemplateMapper extends MyMapper<NoticeTemplate> {

	/**
	 * 获取模版列表
	* @param orgId 机构码
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getNoticeTemplateList(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

	/**
	 * 按分类获取红头列表
	* @param orgId 机构码
	 * @param noticeType
	 * @return
	 */
    List<Map<String, String>> getNoticeTemplateListByType(@Param(value = "orgId") String orgId, @Param(value = "noticeType") String noticeType);
}
