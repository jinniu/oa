package com.core136.mapper.office;

import com.core136.bean.office.MeetingNotes;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface MeetingNotesMapper extends MyMapper<MeetingNotes> {


	List<Map<String, String>> getMyMeetingNotesListForDesk(
		@Param(value = "orgId") String orgId,
		@Param(value = "accountId") String accountId,
		@Param(value = "deptId") String deptId,
		@Param(value = "levelId") String levelId
	);
    /**
     * 获取会议记要列表
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword
     * @return
     */
    List<Map<String, String>> getMeetingNotesList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "dateQueryType")String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );

	/**
	 * 会议记要查询
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param meetingType
	 * @param accountId 用户账号
	 * @param deptId
	 * @param levelId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> queryMeetingNotesList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "meetingType")String meetingType,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "levelId") String levelId,
			@Param(value = "dateQueryType")String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );


	/**
	 * 获取会议记要详情
	* @param orgId 机构码
	 * @param notesId
	 * @return
	 */
	Map<String, String> getMeetingNotesInfo(@Param(value = "orgId") String orgId, @Param(value = "notesId") String notesId);
}
