package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 档案配置接口
 * @author lsq
 */
public interface ArchivesConfigMapper extends MyMapper<ArchivesConfig> {

    /**
     * 判断当前用户是否为可借阅，当前用户创建则可借阅
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param deptId 部门Id
     * @param userLevel 行政级别Id
     * @return 是否可借阅
     */
    int getBorrowingRoleFlag(@Param(value="orgId")String orgId,@Param(value="accountId")String accountId, @Param(value="deptId")String deptId, @Param(value="userLevel")String userLevel);
}
