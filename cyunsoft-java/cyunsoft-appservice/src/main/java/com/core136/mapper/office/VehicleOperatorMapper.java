package com.core136.mapper.office;

import com.core136.bean.office.VehicleOperator;
import com.core136.common.dbutils.MyMapper;

public interface VehicleOperatorMapper extends MyMapper<VehicleOperator> {

}
