package com.core136.mapper.office;

import com.core136.bean.office.VehicleInfo;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface VehicleInfoMapper extends MyMapper<VehicleInfo> {

	/**
	 * 获取车辆列表
	* @param orgId 机构码
	 * @param onwer
	 * @param carType
	 * @param nature
	 * @param dateQueryType1
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param dateQueryType2
	 * @param beginTime1 开始时间
	 * @param endTime1 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getManageVehicleInfoList(
            @Param(value = "orgId") String orgId,
            @Param(value = "onwer") String onwer,
            @Param(value = "carType") String carType,
            @Param(value = "nature") String nature,
			@Param(value = "dateQueryType1") String dateQueryType1,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
			@Param(value = "dateQueryType2") String dateQueryType2,
            @Param(value = "beginTime1") String beginTime1,
            @Param(value = "endTime1") String endTime1,
            @Param(value = "keyword") String keyword
    );

	/**
	 * 获取可调度车辆列表
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getCanUsedVehicleList(@Param(value = "orgId") String orgId,@Param(value="carType")String carType);

	/**
	 * 获取车辆列表
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getVehicleListForSelect(@Param(value = "orgId") String orgId);

}
