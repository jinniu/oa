package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesDestroy;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 案销毁记录接口
 * @author lsq
 */
public interface ArchivesDestroyMapper extends MyMapper<ArchivesDestroy> {
    /**
     * 获取档案销毁记录
     * @param orgId 机构码
     * @param opFlag 管理员标识
     * @param accountId 用户账号
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param destroyType 销毁类型
     * @param keyword 查询关键词
     * @return 销毁记录列表
     */
    List<Map<String,String>> getArchivesDestroyList(@Param(value="orgId")String orgId, @Param(value="opFlag")String opFlag,
                                                    @Param(value = "accountId")String accountId, @Param(value="dateQueryType")String dateQueryType,
                                                    @Param(value="beginTime")String beginTime, @Param(value="endTime") String endTime,
                                                    @Param(value="destroyType")String destroyType, @Param(value="keyword")String keyword);



}
