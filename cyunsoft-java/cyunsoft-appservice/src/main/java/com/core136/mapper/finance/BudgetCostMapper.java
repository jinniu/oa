package com.core136.mapper.finance;

import com.core136.bean.finance.BudgetCost;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BudgetCostMapper extends MyMapper<BudgetCost> {

	/**
	 * 获取子项目费用支出列表
	* @param orgId 机构码
	 * @param projectId
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getChildBudgetCostList(@Param(value = "orgId") String orgId, @Param(value = "projectId") String projectId,@Param(value = "dateQueryType") String dateQueryType,
                                                     @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);


	/**
	 * 获取项目预算费用支出列表
	* @param orgId 机构码
	 * @param projectId
	 * @return
	 */
    List<Map<String, String>> getBudgetCostList(@Param(value = "orgId") String orgId, @Param(value = "projectId") String projectId);
}
