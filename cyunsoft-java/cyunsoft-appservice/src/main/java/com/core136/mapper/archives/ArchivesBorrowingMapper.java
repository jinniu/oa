package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesBorrowing;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 档案借阅接口
 * @author lsq
 */
public interface ArchivesBorrowingMapper extends MyMapper<ArchivesBorrowing> {
    /**
     * 获取档案借阅列表
     * @param orgId 机构码
     * @param opFlag 管理员标识
     * @param accountId 用户账号
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param toOut 是不借出
     * @param status 状态
     * @param keyword 查询关键词
     * @return 借阅列表
     */
    List<Map<String,String>> getArchivesBorrowingList(@Param(value="orgId")String orgId,@Param(value="opFlag") String opFlag,@Param(value="accountId") String accountId,
                                                      @Param(value="dateQueryType")String dateQueryType,@Param(value="beginTime")String beginTime,
                                                      @Param(value="endTime")String endTime,@Param(value="toOut")String toOut,
                                                      @Param(value="status")String status,@Param(value = "keyword")String keyword);
}
