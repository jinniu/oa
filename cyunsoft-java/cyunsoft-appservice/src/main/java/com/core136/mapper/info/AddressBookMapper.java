package com.core136.mapper.info;

import com.core136.bean.info.AddressBook;
import com.core136.common.dbutils.MyMapper;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;
import java.util.Map;

public interface AddressBookMapper extends MyMapper<AddressBook> {

	/**
	 * 获取他人共享的人员列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param bookType
	 * @return
	 */
	List<Map<String,String>> getMyShareAddressBookList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "bookType") String bookType,@Param(value="keyword")String keyword);


	/**
	 * 获取同事列表
	 * @param orgId 机构码
	 * @param levelId
	 * @return
	 */
    List<Map<String, String>> getMyFriendsByLevel(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId,@Param(value="keyword")String keyword);

	/**
	 * 获取同事联系方式
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 联系方式
	 */
	Map<String, String> getMyFriendsInfo(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	List<Map<String,String>> getMyAddressBook(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,String bookType,@Param(value="keyword")String keyword);



}
