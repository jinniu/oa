package com.core136.mapper.office;

import com.core136.bean.office.WorkPlan;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 工作计划接口
 * @author lsq
 */
public interface WorkPlanMapper extends MyMapper<WorkPlan> {
	/**
	 * 获取工作计划详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param planId 工作计划Id
	 * @return 工作计划详情
	 */
	Map<String, String> getMyWorkPlanById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
										  @Param(value="accountId")String accountId,@Param(value="deptId")String deptId,
										  @Param(value="userLevel")String userLevel,@Param(value="planId")String planId);
	/**
	 * 获取工作列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param createUser 创建人账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @param keyword 查询关键词
	 * @return 工作列表
	 */
    List<Map<String, String>> getManageWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value = "createUser") String createUser,
            @Param(value = "opFlag") String opFlag, @Param(value = "dateQueryType") String dateQueryType,
			@Param(value = "beginTime") String beginTime,@Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "keyword") String keyword
    );

	/**
	 * 我负责的计划列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @param keyword 查询关键词
	 * @return 计划列表
	 */
    List<Map<String, String>> getHoldWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
            @Param(value = "opFlag") String opFlag,@Param(value = "dateQueryType")String dateQueryType,  @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "keyword") String keyword
    );
	/**
	 * 我督查的计划列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @param keyword 查询关键词
	 * @return 计划列表
	 */
    List<Map<String, String>> getSupWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
            @Param(value = "opFlag") String opFlag, @Param(value = "dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "keyword") String keyword
    );

	/**
	 * 我参与的工作计划
	 * @param orgId 机构码
	 * @param type 参与类型
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @param keyword 查询关键词
	 * @return 工作计划列表
	 */
    List<Map<String, String>> getMyWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value="type")String type,@Param(value = "accountId") String accountId,
			@Param(value = "deptId") String deptId,@Param(value = "userLevel") String userLevel,@Param(value = "dateQueryType")String dateQueryType, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "keyword") String keyword
    );
	/**
	 * 获取个人桌面的工作计划列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 计划列表
	 */
	List<Map<String, String>> getMyWorkPlanForDesk( @Param(value = "orgId") String orgId,@Param(value = "accountId") String accountId);

	/**
	 * 移动端我参与的工作计划
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param keyword 查询关键词
	 * @param page 页码
	 * @return 工作计划列表
	 */
	List<Map<String, String>> getMyWorkPlanListForApp(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
		@Param(value = "deptId") String deptId,@Param(value = "userLevel") String userLevel, @Param(value = "keyword") String keyword,@Param(value="page")Integer page);


}
