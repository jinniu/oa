package com.core136.mapper.info;

import com.core136.bean.info.LeaderMailbox;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LeaderMailboxMapper extends MyMapper<LeaderMailbox> {

    List<Map<String, String>> getMyLeaderMailBoxList(@Param(value = "orgId") String orgId,@Param(value = "status") String status, @Param(value = "mailType") String mailType,
													 @Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,@Param(value = "endTime") String endTime,
													 @Param(value = "accountId") String accountId, @Param(value = "keyword") String keyword);

    List<Map<String, String>> getLeaderMailBoxList(@Param(value = "orgId") String orgId,@Param(value = "status") String status, @Param(value = "mailType") String mailType,
												   @Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,@Param(value = "endTime") String endTime,
												   @Param(value = "keyword") String keyword);

}
