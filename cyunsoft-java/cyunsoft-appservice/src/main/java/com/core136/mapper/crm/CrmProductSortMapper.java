package com.core136.mapper.crm;

import com.core136.bean.crm.CrmProductSort;
import com.core136.common.dbutils.MyMapper;

/**
 * @author lsq
 *
 */
public interface CrmProductSortMapper extends MyMapper<CrmProductSort> {

}
