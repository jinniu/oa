package com.core136.mapper.office;

import com.core136.bean.office.FixedAssetsSort;
import com.core136.common.dbutils.MyMapper;

/**
 * @author lsq
 *
 */

public interface FixedAssetsSortMapper extends MyMapper<FixedAssetsSort> {

}
