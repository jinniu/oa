package com.core136.mapper.invite;

import com.core136.bean.invite.InviteRole;
import com.core136.common.dbutils.MyMapper;

public interface InviteRoleMapper extends MyMapper<InviteRole> {
}
