package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesDic;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 档案字典接口
 * @author lsq
 */
public interface ArchivesDicMapper extends MyMapper<ArchivesDic> {
	/**
	 * 获取字典分类列表
	 * @param orgId 机构码
	 * @return 分类列表
	 */
	List<Map<String,String>> getArchivesDicParentList(@Param(value = "orgId") String orgId);
	/**
	 * 获取字典列表
	 * @param orgId 机构码
	 * @param parentId 父级字典Id
	 * @param keyword 查询关键词
	 * @return 字典列表
	 */
	List<Map<String,String>>getArchivesDicList(@Param(value = "orgId") String orgId, @Param(value = "parentId") String parentId, @Param(value = "keyword") String keyword);


	/**
	 * 按标识获取分类码列表
	 * @param orgId
	 * @param code
	 * @return
	 */
	List<Map<String, Object>> getArchivesDicByCode(@Param(value = "orgId") String orgId, @Param(value = "code") String code);
}
