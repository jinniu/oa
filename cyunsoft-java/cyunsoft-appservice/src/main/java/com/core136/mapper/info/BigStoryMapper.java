package com.core136.mapper.info;

import com.core136.bean.info.BigStory;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BigStoryMapper extends MyMapper<BigStory> {

	/**
	 * 获取大纪事管理列表
	 * @param orgId 机构码
	 * @param eventType 类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 大纪事列表
	 */
    List<Map<String, String>> getBigStoryListForManage(@Param(value = "orgId") String orgId, @Param(value = "eventType") String eventType,@Param(value="dateQueryType")String dateQueryType,
													   @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);

	/**
	 * 获取所有大记事
	 * @param orgId 机构码
	 * @return 大记事列表
	 */
	List<Map<String, String>> getBigStoryList(@Param(value = "orgId") String orgId);

}
