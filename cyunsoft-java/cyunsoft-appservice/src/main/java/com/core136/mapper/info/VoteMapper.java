package com.core136.mapper.info;

import com.core136.bean.info.Vote;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface VoteMapper extends MyMapper<Vote> {

	Map<String, String>getMyVoteById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
									 @Param(value="accountId")String accountId,@Param(value="deptId")String deptId,
									 @Param(value="userLevel")String userLevel,@Param(value="voteId")String voteId);
	/**
	 * 获取投票管理列表
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param voteType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getVoteListForManage(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                   @Param(value = "accountId") String accountId, @Param(value = "voteType") String voteType,
												   @Param(value="dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                   @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "keyword") String keyword);


	/**
	 * 获取历史投票列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param voteType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getMyOldVoteListForVote(@Param(value = "orgId") String orgId,
                                                      @Param(value = "accountId") String accountId, @Param(value = "voteType") String voteType,
													  @Param(value = "dateQueryType") String dateQueryType, @Param(value = "beginTime") String beginTime,
                                                      @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "keyword") String keyword);


	/**
	 * 获取待我投票列表
	* @param orgId 机构码
	 * @param nowTime
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @return
	 */
    List<Map<String, String>> getMyVoteListForVote(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime,
                                                   @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel);

	List<Map<String, String>> getMyVoteListForVoteForDesk(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime,
												   @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel);


	List<Map<String, String>> getVoteStatus(@Param(value = "orgId") String orgId, @Param(value = "voteId") String voteId, @Param(value = "list") List<String> list);

	/**
	 * 获取移动端个人投票列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @param keyword
	 * @param page
	 * @return
	 */
	List<Map<String,String>>getMyVoteListForApp(@Param(value = "orgId") String orgId,@Param(value = "accountId") String accountId,
												@Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel,
												@Param(value="nowTime")String nowTime,@Param(value="keyword")String keyword,@Param(value="page")Integer page);
}
