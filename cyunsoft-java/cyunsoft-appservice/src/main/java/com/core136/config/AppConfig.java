package com.core136.config;

import com.core136.unit.fileutils.FileTools;
import com.zhuozhengsoft.pageoffice.poserver.AdminSeal;
import com.zhuozhengsoft.pageoffice.poserver.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.Servlet;


@Configuration
public class AppConfig implements WebMvcConfigurer {
	@Value("${pageOffice.seal.adminPwd}")
	private String sealPassword;
	@Value("${pageOffice.lic.path}")
	private String licPath;
/*
	@Autowired
	AuthorizationInterceptor authorizationInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(authorizationInterceptor).addPathPatterns("/**");
	}
*/


	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		AntPathMatcher matcher = new AntPathMatcher();
		matcher.setCaseSensitive(false);
		configurer.setPathMatcher(matcher);
	}

	@Bean
	public ServletRegistrationBean<Servlet> pageOfficeRegistrationBean() {
		Server poserver = new Server();
		String licFilePath = FileTools.isPathExistAndCreate(licPath);
		poserver.setSysPath(licFilePath);
		ServletRegistrationBean<Servlet> srb = new ServletRegistrationBean<>(poserver);
		srb.addUrlMappings("/poserver.zz");
		srb.addUrlMappings("/posetup.exe");
		srb.addUrlMappings("/pageoffice.js");
		srb.addUrlMappings("/jquery.min.js");
		srb.addUrlMappings("/pobstyle.css");
		srb.addUrlMappings("/sealsetup.exe");
		return srb;//
	}

	/**
	 * 添加印章管理程序Servlet（可选）
	 * 设置印章数据库文件poseal.db存放的目录
	 * 如果当前项目是打成jar或者war包运行，强烈建议将poseal.db文件的路径更换成某个固定的绝对路径下,不要放当前项目文件夹下,为了防止每次重新打包程序导致poseal.db被替换的问题。
	 * 比如windows服务器下：D:/lic/，linux服务器下:/root/lic/
	 * @return ServletRegistrationBean
	 */
	@Bean
	public ServletRegistrationBean<Servlet> zoomSealRegistrationBean() {
		AdminSeal adminSeal = new AdminSeal();
		adminSeal.setAdminPassword(sealPassword);//设置印章管理员admin的登录密码（为了安全起见，强烈建议修改此密码）
		String licFilePath = FileTools.isPathExistAndCreate(licPath);
		adminSeal.setSysPath(licFilePath);
		ServletRegistrationBean<Servlet> srb = new ServletRegistrationBean<>(adminSeal);
		srb.addUrlMappings("/adminseal.zz");
		srb.addUrlMappings("/sealimage.zz");
		srb.addUrlMappings("/loginseal.zz");
		return srb;//
	}

}
