package com.core136.config;


import com.core136.service.system.WebStartTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;



/**
 * 系统启动后初始化工作
 * @author lsq
 */
@Component
public class WebStartTask implements CommandLineRunner {
	private final Logger logger = LoggerFactory.getLogger(getClass());
    private WebStartTaskService webStartTaskService;
	@Autowired
	public void setWebStartTaskService(WebStartTaskService webStartTaskService){
		this.webStartTaskService = webStartTaskService;
	}
    @Override
    public void run(String... args) {
        try {
            webStartTaskService.initializationApp();
            //new ImService().startup();
			logger.info("智能办公系统已启动,相关初始化工作已完成,欢迎您对本系统的支持！");
        } catch (Exception e) {
			logger.error(e.getMessage());
        }
    }

}
