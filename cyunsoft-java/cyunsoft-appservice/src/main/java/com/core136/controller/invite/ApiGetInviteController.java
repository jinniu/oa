package com.core136.controller.invite;

import com.core136.bean.account.UserInfo;
import com.core136.bean.invite.InviteRole;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.invite.*;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/get/invite")
@CrossOrigin
@Api(value="招标GetController",tags={"招标获取数据接口"})
public class ApiGetInviteController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private UserInfoService userInfoService;
	private InviteTeamService inviteTeamService;
	private InviteDicService inviteDicService;
	private InviteEntService inviteEntService;
	private InviteRecordService inviteRecordService;
	private InviteRoleService inviteRoleService;
	private InviteEntLicService inviteEntLicService;
	private InviteScoreService inviteScoreService;
	private InviteFileService inviteFileService;
	private InviteDepositService inviteDepositService;
	private InviteFileSellService inviteFileSellService;
	private InviteBiddingService inviteBiddingService;
	private InviteOpenService inviteOpenService;
	private InviteEvaluationService inviteEvaluationService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setInviteTeamService(InviteTeamService inviteTeamService) {
		this.inviteTeamService = inviteTeamService;
	}
	@Autowired
	public void setInviteDicService(InviteDicService inviteDicService) {
		this.inviteDicService = inviteDicService;
	}
	@Autowired
	public void setInviteEntService(InviteEntService inviteEntService) {
		this.inviteEntService = inviteEntService;
	}
	@Autowired
	public void setInviteRecordService(InviteRecordService inviteRecordService) {
		this.inviteRecordService = inviteRecordService;
	}
	@Autowired
	public void setInviteRoleService(InviteRoleService inviteRoleService) {
		this.inviteRoleService = inviteRoleService;
	}
	@Autowired
	public void setInviteEntLicService(InviteEntLicService inviteEntLicService) {
		this.inviteEntLicService = inviteEntLicService;
	}
	@Autowired
	public void setInviteScoreService(InviteScoreService inviteScoreService) {
		this.inviteScoreService = inviteScoreService;
	}
	@Autowired
	public void setInviteFileService(InviteFileService inviteFileService) {
		this.inviteFileService = inviteFileService;
	}
	@Autowired
	public void setInviteDepositService(InviteDepositService inviteDepositService) {
		this.inviteDepositService = inviteDepositService;
	}
	@Autowired
	public void setInviteFileSellService(InviteFileSellService inviteFileSellService) {
		this.inviteFileSellService = inviteFileSellService;
	}
	@Autowired
	public void setInviteBiddingService(InviteBiddingService inviteBiddingService) {
		this.inviteBiddingService = inviteBiddingService;
	}
	@Autowired
	public void setInviteOpenService(InviteOpenService inviteOpenService) {
		this.inviteOpenService = inviteOpenService;
	}
	@Autowired
	public void setInviteEvaluationService(InviteEvaluationService inviteEvaluationService) {
		this.inviteEvaluationService = inviteEvaluationService;
	}

	/**
	 * 获取评标记录列表
	 * @param inviteId 评标主记Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteEvaluationByInviteIdList")
	public RetDataBean getInviteEvaluationByInviteIdList(String inviteId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteEvaluationService.getInviteEvaluationByInviteIdList(userInfo.getOrgId(),inviteId));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取开标记录
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteOpenList")
	public RetDataBean getInviteOpenList(PageParam pageParam, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("o.occur_time");
			} else {
				pageParam.setProp("o." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteOpenService.getInviteOpenList(pageParam,date,timeArr[0],timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取招标记录列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteBiddingList")
	public RetDataBean getInviteBiddingList(PageParam pageParam, String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("i.rev_time");
			} else {
				pageParam.setProp("i." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteBiddingService.getInviteBiddingList(pageParam,date,timeArr[0],timeArr[1],status));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取待评招记录列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteBiddingManageList")
	public RetDataBean getInviteBiddingManageList(PageParam pageParam, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("i.rev_time");
			} else {
				pageParam.setProp("i." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteBiddingService.getInviteBiddingManageList(pageParam,date,timeArr[0],timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取招标文件出售记录
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param entId 企业Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteFileSellList")
	public RetDataBean getInviteFileSellList(PageParam pageParam, String date,String entId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.create_time");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteFileSellService.getInviteFileSellList(pageParam,date,timeArr[0],timeArr[1],entId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取招标保证金列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param entId 企业Id
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteDepositList")
	public RetDataBean getInviteDepositList(PageParam pageParam, String date,String entId, String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.create_time");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteDepositService.getInviteDepositList(pageParam,date,timeArr[0],timeArr[1],entId,status));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取招标文件列表
	 * @param pageParam 分页参数
	 * @param status 状态
	 * @param fileType 文件类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteFileList")
	public RetDataBean getInviteFileList(PageParam pageParam, String status, String fileType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("f.create_time");
			} else {
				pageParam.setProp("f." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteFileService.getInviteFileList(pageParam,status,fileType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取招标文件列表
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteFileListForSelect")
	public RetDataBean getInviteFileListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteFileService.getInviteFileListForSelect(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取招标信息列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param status 状态
	 * @param inviteType 招标类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteRecordList")
	public RetDataBean getInviteRecordList(PageParam pageParam, String date,String status,String inviteType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.begin_time");
			} else {
				pageParam.setProp("r." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = inviteRecordService.getInviteRecordList(pageParam,date,timeArr[0],timeArr[1], status,inviteType);
			List<Map<String, String>> recordList = pageInfo.getList();
			List<Map<String, String>> resList = new ArrayList<>();
			for (Map<String, String> tmpMap : recordList) {
				String joinEnt = tmpMap.get("joinEnt");
				tmpMap.put("joinEntName", inviteEntService.getEntNameStrByEntId(userInfo.getOrgId(), joinEnt));
				resList.add(tmpMap);
			}
			pageInfo.setList(resList);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取招标信息选择列表
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteRecordListForSelect")
	public RetDataBean getInviteRecordListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteRecordService.getInviteRecordListForSelect(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取供应商选择列表
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteEntListForSelect")
	public RetDataBean getInviteEntListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteEntService.getInviteEntListForSelect(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取供应商列表
	 * @param pageParam 分页参数
	 * @param entLevel 级别
	 * @param entType 企业类型
	 * @param status 状态
	 * @param creditRating 成立时间
	 * @param enterpriseSize 企来规模
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteEntList")
	public RetDataBean getInviteEntList(PageParam pageParam, String entLevel,String entType,String status,String creditRating,String enterpriseSize) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("e.sort_no");
			} else {
				pageParam.setProp("e." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteEntService.getInviteEntList(pageParam, entLevel,entType,status,creditRating,enterpriseSize));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按企业获取证照
	 * @param entId 企业Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteEntLicListByEntId")
	public RetDataBean getInviteEntLicListByEntId(String entId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteEntLicService.getInviteEntLicListByEntId(userInfo.getOrgId(),entId));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取供应商证照列表
	 * @param pageParam 分页参数
	 * @param entId 企业Id
	 * @param licType 照证类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteEntLicList")
	public RetDataBean getInviteEntLicList(PageParam pageParam,String entId, String licType,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.create_time");
			} else {
				pageParam.setProp("l." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteEntLicService.getInviteEntLicList(pageParam, entId,licType,status));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取成员列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteTeamList")
	public RetDataBean getInviteTeamList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteTeamService.getInviteTeamList(userInfo.getOrgId()));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取评标小组列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteTeamListForSelect")
	public RetDataBean getInviteTeamListForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteTeamService.getInviteTeamListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取字典分类列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteDicParentList")
	public RetDataBean getInviteDicParentList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteDicService.getInviteDicParentList(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteDicTree")
	public RetDataBean getInviteDicTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteDicService.getInviteDicTree(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按模块获取分类列表
	 *
	 * @param code 字典分类码
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteDicByCode")
	public RetDataBean getInviteDicByCode(String code) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteDicService.getInviteDicByCode(userInfo.getOrgId(), code));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取企业评分列表
	 * @param entId 企业Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getEntScoreListByEntId")
	public RetDataBean getEntScoreListByEntId(String entId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteScoreService.getEntScoreListByEntId(userInfo.getOrgId(), entId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典列表
	 *
	 * @param pageParam 分页参数
	 * @param parentId 父级字典Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteDicList")
	public RetDataBean getInviteDicList(PageParam pageParam, String parentId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.sort_no");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteDicService.getInviteDicList(pageParam, parentId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取招标权限
	 * @return 消息结构
	 */
	@GetMapping(value = "/getInviteRole")
	public RetDataBean getInviteRole() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			InviteRole inviteRole = new InviteRole();
			inviteRole.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, inviteRoleService.selectOneInviteRole(inviteRole));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


}
