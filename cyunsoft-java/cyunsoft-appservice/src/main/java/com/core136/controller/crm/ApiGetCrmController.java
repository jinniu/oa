package com.core136.controller.crm;

import com.alibaba.fastjson.JSON;
import com.core136.bean.account.UserInfo;
import com.core136.bean.crm.CrmRole;
import com.core136.bean.system.PageParam;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.crm.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/get/crm")
@CrossOrigin
@Api(value="客户管理GetController",tags={"Crm获取数据接口"})
public class ApiGetCrmController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private UserInfoService userInfoService;
	private EchartsCrmService echartsCrmService;
	private CrmRoleService crmRoleService;
	private CrmProductSortService crmProductSortService;
	private CrmDicService crmDicService;
	private CrmProductService crmProductService;
	private CrmCustomerService crmCustomerService;
	private CrmContractRecordService crmContractRecordService;
	private CrmLinkManService crmLinkManService;
	private CrmInquiryService crmInquiryService;
	private CrmQuotationService crmQuotationService;
	private CrmCareService crmCareService;
	private CrmOrderService crmOrderService;
	private CrmOrderMxService crmOrderMxService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setEchartsCrmService(EchartsCrmService echartsCrmService) {
		this.echartsCrmService = echartsCrmService;
	}
	@Autowired
	public void setCrmRoleService(CrmRoleService crmRoleService) {
		this.crmRoleService = crmRoleService;
	}
	@Autowired
	public void setCrmProductSortService(CrmProductSortService crmProductSortService) {
		this.crmProductSortService = crmProductSortService;
	}
	@Autowired
	public void setCrmDicService(CrmDicService crmDicService) {
		this.crmDicService = crmDicService;
	}
	@Autowired
	public void setCrmProductService(CrmProductService crmProductService) {
		this.crmProductService = crmProductService;
	}
	@Autowired
	public void setCrmCustomerService(CrmCustomerService crmCustomerService) {
		this.crmCustomerService = crmCustomerService;
	}
	@Autowired
	public void setCrmContractRecordService(CrmContractRecordService crmContractRecordService) {
		this.crmContractRecordService = crmContractRecordService;
	}
	@Autowired
	public void setCrmLinkManService(CrmLinkManService crmLinkManService) {
		this.crmLinkManService = crmLinkManService;
	}
	@Autowired
	public void setCrmInquiryService(CrmInquiryService crmInquiryService) {
		this.crmInquiryService = crmInquiryService;
	}
	@Autowired
	public void setCrmQuotationService(CrmQuotationService crmQuotationService) {
		this.crmQuotationService = crmQuotationService;
	}
	@Autowired
	public void setCrmCareService(CrmCareService crmCareService) {
		this.crmCareService = crmCareService;
	}
	@Autowired
	public void setCrmOrderService(CrmOrderService crmOrderService) {
		this.crmOrderService = crmOrderService;
	}
	@Autowired
	public void setCrmOrderMxService(CrmOrderMxService crmOrderMxService) {
		this.crmOrderMxService = crmOrderMxService;
	}

	/**
	 * 获取订单明细
	 * @param orderId 订单Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmOrderMxList")
	public RetDataBean getCrmOrderMxList(String orderId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmOrderMxService.getCrmOrderMxList(userInfo.getOrgId(), orderId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取订单列表
	 * @param pageParam 分页参oct
	 * @param date 日期范围类型
	 * @param status 状态
	 * @param payType 支付对方式
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmOrderList")
	public RetDataBean getCrmOrderList(PageParam pageParam,String date,String status,String payType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmOrderService.getCrmOrderList(pageParam, date,timeArr[0], timeArr[1], status,payType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取客户关怀审批列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param status 状态
	 * @param careType 关怀类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getApprovedCrmCareList")
	public RetDataBean getApprovedCrmCareList(PageParam pageParam,String date,String status,String careType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCareService.getApprovedCrmCareList(pageParam, date,timeArr[0], timeArr[1], status,careType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取客户关怀记录列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param status 状态
	 * @param careType 关心怀类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmCareList")
	public RetDataBean getCrmCareList(PageParam pageParam,String date,String status,String careType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCareService.getCrmCareList(pageParam, date,timeArr[0], timeArr[1], status,careType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取报价列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyCrmQuotationList")
	public RetDataBean getMyCrmQuotationList(PageParam pageParam,String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("i.create_time");
			} else {
				pageParam.setProp("i."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmQuotationService.getMyCrmQuotationList(pageParam, date,timeArr[0], timeArr[1], status));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取审批列表
	 * @param pageParam 分页参数
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyApprovedList")
	public RetDataBean getMyApprovedList(PageParam pageParam,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("i.create_time");
			} else {
				pageParam.setProp("i."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  crmQuotationService.getMyApprovedList(pageParam,status));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获限权限内的询价单列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param customerNature 客户类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmInquiryList")
	public RetDataBean getCrmInquiryList(PageParam pageParam,String date,String customerNature,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("i.create_time");
			} else {
				pageParam.setProp("i."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			CrmRole crmRole = new CrmRole();
			crmRole.setOrgId(userInfo.getOrgId());
			crmRole = crmRoleService.selectOneCrmRole(crmRole);
			if (crmRole == null) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (("," + crmRole.getSender() + ",").contains("," + userInfo.getAccountId() + ",")) {
				pageParam.setOpFlag("1");
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			PageInfo<Map<String, String>> pageInfo = crmInquiryService.getCrmInquiryList(pageParam,date, timeArr[0], timeArr[1], customerNature, status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取询价单列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmInquiryListForSelect")
	public RetDataBean getCrmInquiryListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmInquiryService.getCrmInquiryListForSelect(userInfo.getOrgId(), userInfo.getAccountId(),keyword));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取报价单列表
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmQuotationListForSelect")
	public RetDataBean getCrmQuotationListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmQuotationService.getCrmQuotationListForSelect(userInfo.getOrgId(), userInfo.getAccountId(),keyword));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取询价单明细
	 * @param inquiryId 询价单Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmInquiryFormJsonById")
	public RetDataBean getCrmInquiryFormJsonById(String inquiryId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmInquiryService.getCrmInquiryFormJsonById(userInfo.getOrgId(), userInfo.getAccountId(),inquiryId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取报价单明细
	 * @param quotationId 报价单Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmQuotationFormJsonById")
	public RetDataBean getCrmQuotationFormJsonById(String quotationId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmQuotationService.getCrmQuotationFormJsonById(userInfo.getOrgId(), userInfo.getAccountId(),quotationId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取企业联系记录
	 * @param pageParam 分页参数
	 * @param customerId 客户企业Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getRecordByCustomerId")
	public RetDataBean getRecordByCustomerId(PageParam pageParam,String customerId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = crmContractRecordService.getRecordListByCustomerId(pageParam,customerId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取企业联系记录
	 * @param pageParam 分页参数
	 * @param linkType 联系方式
	 * @param recordType 联系类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractRecordList")
	public RetDataBean getContractRecordList(PageParam pageParam,String linkType, String recordType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			PageInfo<Map<String, String>> pageInfo = crmContractRecordService.getContractRecordList(pageParam,linkType,recordType,date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人客户列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyCustomerListForSelect")
	public RetDataBean getMyCustomerListForSelect(String customerId,String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCustomerService.getMyCustomerListForSelect(userInfo.getOrgId(), userInfo.getAccountId(),customerId,keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有客户列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllCustomerListForSelect")
	public RetDataBean getAllCustomerListForSelect(String customerId,String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCustomerService.getAllCustomerListForSelect(userInfo.getOrgId(),customerId,keyword ));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有产品列表
	 * @param productId 产品Id
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllProductListForSelect")
	public RetDataBean getAllProductListForSelect(String productId,String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmProductService.getAllProductListForSelect(userInfo.getOrgId(),productId,keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取客户联系人
	 * @param customerId 客户Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmLinkManListForSelect")
	public RetDataBean getCrmLinkManListForSelect(String customerId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmLinkManService.getCrmLinkManListForSelect(userInfo.getOrgId(), customerId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取CRM联系人列表
	 *
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmLinkManAllList")
	public RetDataBean getCrmLinkManAllList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = new PageInfo<>();
			if (userInfo.getOpFlag().equals("1")) {
				pageInfo = crmLinkManService.getCrmLinkManAllList(pageParam);
			} else {
				CrmRole crmRole = new CrmRole();
				crmRole.setOrgId(userInfo.getOrgId());
				crmRole = crmRoleService.selectOneCrmRole(crmRole);
				if (StringUtils.isNotBlank(crmRole.getManager()) && ("," + crmRole.getManager() + ",").contains("," + userInfo.getAccountId() + ",")) {
					pageParam.setOpFlag("1");
					pageInfo = crmLinkManService.getCrmLinkManAllList(pageParam);
				}
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取个人客户联系人列表
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyCrmLinkManAllList")
	public RetDataBean getMyCrmLinkManAllList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmLinkManService.getMyCrmLinkManAllList(pageParam));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取客户列表
	 * @param pageParam 分页参数
	 * @param industry 企业类型
	 * @param customerType 客户类型
	 * @param crmLevel 等级
	 * @param intention 意向
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmCustomerList")
	public RetDataBean getCrmCustomerList(PageParam pageParam,String industry,String customerType,String crmLevel, String intention) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = crmCustomerService.getCrmCustomerList(pageParam,industry,customerType,crmLevel,intention);
			List<Map<String, String>> recordList = pageInfo.getList();
			List<Map<String, String>> resList = new ArrayList<>();
			for (Map<String, String> tmpMap : recordList) {
				String focusProduct = tmpMap.get("focusProduct");
				if (StringUtils.isNotBlank(focusProduct)) {
					List<Map<String, String>> productListMap = crmProductService.getCrmProductNameByIds(userInfo.getOrgId(), focusProduct);
					List<String> productList = new ArrayList<>();
					for (Map<String, String> map : productListMap) {
						productList.add(map.get("name"));
					}
					tmpMap.put("focusProductName", StringUtils.join(productList, ","));
				}
				resList.add(tmpMap);
			}
			pageInfo.setList(resList);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取产品列表
	 * @param ids 产品Id 数组
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmProductListByIdsForSelect")
	public RetDataBean getCrmProductListByIdsForSelect(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmProductService.getCrmProductListByIdsForSelect(userInfo.getOrgId(), idsList));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取权限内所有客户
	 * @param pageParam 分页参数
	 * @param industry 企业类型
	 * @param customerType 客户类型
	 * @param crmLevel 客户等级
	 * @param intention 意向
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllCrmCustomerList")
	public RetDataBean getAllCrmCustomerList(PageParam pageParam,String status,String industry,String customerType,String crmLevel, String intention) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo;
			if (!userInfo.getOpFlag().equals("1")) {
				CrmRole crmRole = new CrmRole();
				crmRole.setOrgId(userInfo.getOrgId());
				crmRole = crmRoleService.selectOneCrmRole(crmRole);
				if (("," + crmRole.getManager() + ",").contains("," + userInfo.getAccountId() + ",")) {
					pageParam.setOpFlag("1");
					pageInfo = crmCustomerService.getAllCrmCustomerList(pageParam,status,industry,customerType,crmLevel,intention);
				} else {
					return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
				}
			} else {
				pageInfo = crmCustomerService.getAllCrmCustomerList(pageParam,status,industry,customerType,crmLevel,intention);
			}
			List<Map<String, String>> recordList = pageInfo.getList();
			List<Map<String, String>> resList = new ArrayList<>();
			for (Map<String, String> tmpMap : recordList) {
				String focusProduct = tmpMap.get("focusProduct");
				if (StringUtils.isNotBlank(focusProduct)) {
					List<Map<String, String>> productListMap = crmProductService.getCrmProductNameByIds(userInfo.getOrgId(), focusProduct);
					List<String> productList = new ArrayList<>();
					for (Map<String, String> map : productListMap) {
						productList.add(map.get("name"));
					}
					tmpMap.put("focusProductName", StringUtils.join(productList, ","));
				}
				resList.add(tmpMap);
			}
			pageInfo.setList(resList);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典分类列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmDicParentList")
	public RetDataBean getCrmDicParentList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmDicService.getCrmDicParentList(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取字典数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmDicTree")
	public RetDataBean getCrmDicTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmDicService.getCrmDicTree(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按模块获取分类列表
	 *
	 * @param code 模块标识
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmDicByCode")
	public RetDataBean getCrmDicByCode(String code) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmDicService.getCrmDicByCode(userInfo.getOrgId(), code));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典列表
	 *
	 * @param pageParam 分页参数
	 * @param parentId 字典父组Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmDicList")
	public RetDataBean getCrmDicList(PageParam pageParam, String parentId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.sort_no");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmDicService.getCrmDicList(pageParam, parentId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取产品分类
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmProductSortTree")
	public RetDataBean getCrmProductSortTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(crmProductSortService.getCrmProductSortTree(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取产品列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProductListForSelect")
	public RetDataBean getProductListForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmProductService.getProductListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取产品列表
	 * @param pageParam 分页参数
	 * @param sortId 分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmProductListBySortId")
	public RetDataBean getCrmProductListBySortId(PageParam pageParam, String sortId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("p.sort_no");
			} else {
				pageParam.setProp("p."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmProductService.getCrmProductListBySortId(pageParam, sortId));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取CRM管理权限
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmRole")
	public RetDataBean getCrmRole() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			CrmRole crmRole = new CrmRole();
			crmRole.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmRoleService.selectOneCrmRole(crmRole));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 部门报价单占比前10的占比
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiQuotationByDeptPie")
	public RetDataBean getBiQuotationByDeptPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiQuotationByDeptPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取支付类型分类
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiInquiryByPayTypePie")
	public RetDataBean getBiInquiryByPayTypePie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiInquiryByPayTypePie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按月份统计报价单
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiQuotationByMonthLine")
	public RetDataBean getBiQuotationByMonthLine() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiQuotationByMonthLine(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取报价单人员占比
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiQuotationByAccountPie")
	public RetDataBean getBiQuotationByAccountPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiQuotationByAccountPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 部门询价单占比前10的占比
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiInquiryByDeptPie")
	public RetDataBean getBiInquiryByDeptPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiInquiryByDeptPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按月份统计工作量
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiInquiryByMonthLine")
	public RetDataBean getBiInquiryByMonthLine() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiInquiryByMonthLine(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取询价单人员占比
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiInquiryByAccountPie")
	public RetDataBean getBiInquiryByAccountPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiInquiryByAccountPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取crm的客户行业占比
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiCustomerIndustryPie")
	public RetDataBean getBiCustomerIndustryPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiCustomerIndustryPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取CRM销售人员的占比
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiCustomerKeepUserPie")
	public RetDataBean getBiCustomerKeepUserPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiCustomerKeepUserPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取CRM客户等级占比
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiCustomerLevelPie")
	public RetDataBean getBiCustomerLevelPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiCustomerLevelPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取CRM客户来源分类
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBiCustomerSourcePie")
	public RetDataBean getBiCustomerSourcePie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsCrmService.getBiCustomerSourcePie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取审批人员列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmApprovedUserList")
	public RetDataBean getCrmApprovedUserList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			CrmRole crmRole = new CrmRole();
			crmRole.setOrgId(userInfo.getOrgId());
			crmRole = crmRoleService.selectOneCrmRole(crmRole);
			String manageStr = crmRole.getManager();
			if (StringUtils.isNotBlank(manageStr)) {
				String[] accountArr = manageStr.split(",");
				List<String> accountList = Arrays.asList(accountArr);
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, userInfoService.getUserNamesByAccountIds(accountList, userInfo.getOrgId()));
			}else
			{
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, new ArrayList<>());
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取客户详情
	 * @param customerId 客户Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCrmCustomerById")
	public RetDataBean getCrmCustomerById(String customerId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(customerId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCustomerService.getCrmCustomerById(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId(),customerId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

}
