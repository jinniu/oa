package com.core136.controller.finance;

import com.core136.bean.account.UserInfo;
import com.core136.bean.finance.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.finance.*;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/finance")
@CrossOrigin
@Api(value="财务SetController",tags={"财务更新数据接口"})
public class ApiSetFinanceController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
    private UserInfoService userInfoService;
    private BudgetConfigService budgetConfigService;
    private BudgetAccountService budgetAccountService;
    private BudgetProjectService budgetProjectService;
	private BudgetAdjustmentService budgetAdjustmentService;
	private BudgetCostApplyService budgetCostApplyService;
	private BudgetCostService budgetCostService;
	private ContractSortService contractSortService;
	private ContractService contractService;
	private ContractReceivablesService contractReceivablesService;
	private ContractReceivablesRecordService contractReceivablesRecordService;
	private ContractPayableService contractPayableService;
	private ContractPayableRecordService contractPayableRecordService;
	private ContractBillService contractBillService;
	private ContractDeliveryService contractDeliveryService;
	private ExpenseAccountService expenseAccountService;
	private ExpenseComConfigService expenseComConfigService;
	private ExpenseUserConfigService expenseUserConfigService;
	private ExpenseDeptConfigService expenseDeptConfigService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
		this.budgetConfigService = budgetConfigService;
	}
	@Autowired
	public void setBudgetAccountService(BudgetAccountService budgetAccountService) {
		this.budgetAccountService = budgetAccountService;
	}
	@Autowired
	public void setBudgetProjectService(BudgetProjectService budgetProjectService) {
		this.budgetProjectService = budgetProjectService;
	}
	@Autowired
	public void setBudgetAdjustmentService(BudgetAdjustmentService budgetAdjustmentService) {
		this.budgetAdjustmentService = budgetAdjustmentService;
	}
	@Autowired
	public void setBudgetCostApplyService(BudgetCostApplyService budgetCostApplyService) {
		this.budgetCostApplyService = budgetCostApplyService;
	}
	@Autowired
	public void setBudgetCostService(BudgetCostService budgetCostService) {
		this.budgetCostService = budgetCostService;
	}
	@Autowired
	public void setContractSortService(ContractSortService contractSortService) {
		this.contractSortService = contractSortService;
	}
	@Autowired
	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	}
	@Autowired
	public void setContractReceivablesService(ContractReceivablesService contractReceivablesService) {
		this.contractReceivablesService = contractReceivablesService;
	}
	@Autowired
	public void setContractReceivablesRecordService(ContractReceivablesRecordService contractReceivablesRecordService) {
		this.contractReceivablesRecordService = contractReceivablesRecordService;
	}
	@Autowired
	public void setContractPayableService(ContractPayableService contractPayableService) {
		this.contractPayableService = contractPayableService;
	}
	@Autowired
	public void setContractPayableRecordService(ContractPayableRecordService contractPayableRecordService) {
		this.contractPayableRecordService = contractPayableRecordService;
	}
	@Autowired
	public void setContractBillService(ContractBillService contractBillService) {
		this.contractBillService = contractBillService;
	}
	@Autowired
	public void setContractDeliveryService(ContractDeliveryService contractDeliveryService) {
		this.contractDeliveryService = contractDeliveryService;
	}
	@Autowired
	public void setExpenseAccountService(ExpenseAccountService expenseAccountService) {
		this.expenseAccountService = expenseAccountService;
	}
	@Autowired
	public void setExpenseComConfigService(ExpenseComConfigService expenseComConfigService) {
		this.expenseComConfigService = expenseComConfigService;
	}
	@Autowired
	public void setExpenseUserConfigService(ExpenseUserConfigService expenseUserConfigService) {
		this.expenseUserConfigService = expenseUserConfigService;
	}
	@Autowired
	public void setExpenseDeptConfigService(ExpenseDeptConfigService expenseDeptConfigService) {
		this.expenseDeptConfigService = expenseDeptConfigService;
	}

	/**
	 * 设置部门年度费用预算
	 * @param expenseDeptConfig 部门年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExpenseDeptConfig")
	public RetDataBean insertExpenseDeptConfig(ExpenseDeptConfig expenseDeptConfig) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			expenseDeptConfig.setRecordId(SysTools.getGUID());
			expenseDeptConfig.setBalance(null);
			expenseDeptConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			expenseDeptConfig.setCreateUser(userInfo.getCreateUser());
			expenseDeptConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, expenseDeptConfigService.insertExpenseDeptConfig(expenseDeptConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除部门年度费用预算
	 * @param expenseDeptConfig 部门年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExpenseDeptConfig")
	public RetDataBean deleteExpenseDeptConfig(ExpenseDeptConfig expenseDeptConfig) {
		try {
			if (StringUtils.isBlank(expenseDeptConfig.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			expenseDeptConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, expenseDeptConfigService.deleteExpenseDeptConfig(expenseDeptConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新部门年度费用预算
	 * @param expenseDeptConfig 部门年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExpenseDeptConfig")
	public RetDataBean updateExpenseDeptConfig(ExpenseDeptConfig expenseDeptConfig) {
		try {
			if (StringUtils.isBlank(expenseDeptConfig.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			expenseDeptConfig.setBalance(null);
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExpenseDeptConfig.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", expenseDeptConfig.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, expenseDeptConfigService.updateExpenseDeptConfig(example,expenseDeptConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除部门年度费用预算
	 * @param ids 部门年度费用预算记录Id 列表
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExpenseDeptConfigByIds")
	public RetDataBean deleteExpenseDeptConfigByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return expenseDeptConfigService.deleteExpenseDeptConfigByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置人员年度费用预算
	 * @param expenseUserConfig 人员年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExpenseUserConfig")
	public RetDataBean insertExpenseUserConfig(ExpenseUserConfig expenseUserConfig) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			expenseUserConfig.setRecordId(SysTools.getGUID());
			expenseUserConfig.setBalance(null);
			expenseUserConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			expenseUserConfig.setCreateUser(userInfo.getCreateUser());
			expenseUserConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, expenseUserConfigService.insertExpenseUserConfig(expenseUserConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员年度费用预算
	 * @param expenseUserConfig 人员年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExpenseUserConfig")
	public RetDataBean deleteExpenseUserConfig(ExpenseUserConfig expenseUserConfig) {
		try {
			if (StringUtils.isBlank(expenseUserConfig.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			expenseUserConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, expenseUserConfigService.deleteExpenseUserConfig(expenseUserConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新人员年度费用预算
	 * @param expenseUserConfig 人员年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExpenseUserConfig")
	public RetDataBean updateExpenseUserConfig(ExpenseUserConfig expenseUserConfig) {
		try {
			if (StringUtils.isBlank(expenseUserConfig.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			expenseUserConfig.setBalance(null);
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExpenseUserConfig.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", expenseUserConfig.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, expenseUserConfigService.updateExpenseUserConfig(example,expenseUserConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除人员年度费用预算
	 * @param ids 人员年度费用预算记录Id 列表
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExpenseUserConfigByIds")
	public RetDataBean deleteExpenseUserConfigByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return expenseUserConfigService.deleteExpenseUserConfigByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 设置公司年度费用预算
	 * @param expenseComConfig 公司年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExpenseComConfig")
	public RetDataBean insertExpenseComConfig(ExpenseComConfig expenseComConfig) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			expenseComConfig.setRecordId(SysTools.getGUID());
			expenseComConfig.setBalance(null);
			expenseComConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			expenseComConfig.setCreateUser(userInfo.getCreateUser());
			expenseComConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, expenseComConfigService.insertExpenseComConfig(expenseComConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除公司年度费用预算
	 * @param expenseComConfig 公司年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExpenseComConfig")
	public RetDataBean deleteExpenseComConfig(ExpenseComConfig expenseComConfig) {
		try {
			if (StringUtils.isBlank(expenseComConfig.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			expenseComConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, expenseComConfigService.deleteExpenseComConfig(expenseComConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新公司年度费用预算
	 * @param expenseComConfig 公司年度费用预算对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExpenseComConfig")
	public RetDataBean updateExpenseComConfig(ExpenseComConfig expenseComConfig) {
		try {
			if (StringUtils.isBlank(expenseComConfig.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			expenseComConfig.setBalance(null);
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExpenseComConfig.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", expenseComConfig.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, expenseComConfigService.updateExpenseComConfig(expenseComConfig,example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除公司年度费用预算
	 * @param ids 公司年度费用预算记录Id 列表
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExpenseComConfigByIds")
	public RetDataBean deleteExpenseComConfigByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return expenseComConfigService.deleteExpenseComConfigByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建报用报销名称
	 * @param expenseAccount 费用报销名称对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExpenseAccount")
	public RetDataBean insertExpenseAccount(ExpenseAccount expenseAccount) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			expenseAccount.setRecordId(SysTools.getGUID());
			if(StringUtils.isBlank(expenseAccount.getParentId()))
			{
				expenseAccount.setParentId("0");
			}
			expenseAccount.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			expenseAccount.setCreateUser(userInfo.getAccountId());
			expenseAccount.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, expenseAccountService.insertExpenseAccount(expenseAccount));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除费用报销名称
	 * @param expenseAccount 费用报销名称对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExpenseAccount")
	public RetDataBean deleteExpenseAccount(ExpenseAccount expenseAccount) {
		try {
			if (StringUtils.isBlank(expenseAccount.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			expenseAccount.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, expenseAccountService.deleteExpenseAccount(expenseAccount));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新费用报销名称
	 * @param expenseAccount 费用名称对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExpenseAccount")
	public RetDataBean updateExpenseAccount(ExpenseAccount expenseAccount) {
		try {
			if (StringUtils.isBlank(expenseAccount.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExpenseAccount.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", expenseAccount.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, expenseAccountService.updateExpenseAccount(example, expenseAccount));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除费用科目
	 * @param ids 费用名称Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExpenseAccountByIds")
	public RetDataBean deleteExpenseAccountByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return expenseAccountService.deleteExpenseAccountByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加发货记录
	 * @param contractDelivery 发货记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertContractDelivery")
	public RetDataBean insertContractDelivery(ContractDelivery contractDelivery) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			contractDelivery.setRecordId(SysTools.getGUID());
			contractDelivery.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			contractDelivery.setCreateUser(userInfo.getAccountId());
			contractDelivery.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractDeliveryService.insertContractDelivery(contractDelivery));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除发货记录
	 * @param contractDelivery 发货记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractDelivery")
	public RetDataBean deleteContractDelivery(ContractDelivery contractDelivery) {
		try {
			if (StringUtils.isBlank(contractDelivery.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			contractDelivery.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractDeliveryService.deleteContractDelivery(contractDelivery));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除发票
	 * @param ids 发票Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractDeliveryByIds")
	public RetDataBean deleteContractDeliveryByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return contractDeliveryService.deleteContractDeliveryByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新发货记录
	 * @param contractDelivery 发货记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateContractDelivery")
	public RetDataBean updateContractDelivery(ContractDelivery contractDelivery) {
		try {
			if (StringUtils.isBlank(contractDelivery.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ContractDelivery.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", contractDelivery.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractDeliveryService.updateContractDelivery(example, contractDelivery));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加票据
	 * @param contractBill 合同发票对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertContractBill")
	public RetDataBean insertContractBill(ContractBill contractBill) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			contractBill.setBillId(SysTools.getGUID());
			contractBill.setStatus("1");
			contractBill.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			contractBill.setCreateUser(userInfo.getAccountId());
			contractBill.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractBillService.insertContractBill(contractBill));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除票据
	 * @param contractBill 合同发票对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractBill")
	public RetDataBean deleteContractBill(ContractBill contractBill) {
		try {
			if (StringUtils.isBlank(contractBill.getBillId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			contractBill.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractBillService.deleteContractBill(contractBill));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除发票
	 * @param ids 发票Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractBillByIds")
	public RetDataBean deleteContractBillByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return contractBillService.deleteContractBillByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新票据记录
	 * @param contractBill 票据记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateContractBill")
	public RetDataBean updateContractBill(ContractBill contractBill) {
		try {
			if (StringUtils.isBlank(contractBill.getBillId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ContractBill.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("billId", contractBill.getBillId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractBillService.updateContractBill(contractBill, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加付款记录
	 * @param contractPayableRecord 付款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertContractPayableRecord")
	public RetDataBean insertContractPayableRecord(ContractPayableRecord contractPayableRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			contractPayableRecord.setRecordId(SysTools.getGUID());
			contractPayableRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			contractPayableRecord.setCreateUser(userInfo.getAccountId());
			contractPayableRecord.setOrgId(userInfo.getOrgId());
			return contractPayableRecordService.payableAmount(contractPayableRecord);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除付款记录
	 * @param contractPayableRecord 付款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractPayableRecord")
	public RetDataBean deleteContractPayableRecord(ContractPayableRecord contractPayableRecord) {
		try {
			if (StringUtils.isBlank(contractPayableRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			contractPayableRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractPayableRecordService.deleteContractPayableRecord(contractPayableRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新付款记录
	 * @param contractPayableRecord 付款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateContractPayableRecord")
	public RetDataBean updateContractPayableRecord(ContractPayableRecord contractPayableRecord) {
		try {
			if (StringUtils.isBlank(contractPayableRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ContractPayableRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", contractPayableRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractPayableRecordService.updateContractPayableRecord(example, contractPayableRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建应付款记录
	 * @param contractPayable 应付款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertContractPayable")
	public RetDataBean insertContractPayable(ContractPayable contractPayable) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			contractPayable.setPayableId(SysTools.getGUID());
			contractPayable.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			contractPayable.setCreateUser(userInfo.getAccountId());
			contractPayable.setPayabled(0.0);
			contractPayable.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractPayableService.insertContractPayable(contractPayable));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除应付款记录
	 * @param contractPayable 应付款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractPayable")
	public RetDataBean deleteContractPayable(ContractPayable contractPayable) {
		try {
			if (StringUtils.isBlank(contractPayable.getPayableId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (userInfo.getOpFlag().equals("1")) {
				contractPayable.setOrgId(userInfo.getOrgId());
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractPayableService.deleteContractPayable(contractPayable));
			} else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除应收款
	 * @param ids 应收款Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractPayableByIds")
	public RetDataBean deleteContractPayableByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return contractPayableService.deleteContractPayableByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新应付款记录
	 * @param contractPayable 应付款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateContractPayable")
	public RetDataBean updateContractPayable(ContractPayable contractPayable) {
		try {
			if (StringUtils.isBlank(contractPayable.getPayableId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ContractPayable.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("payableId", contractPayable.getPayableId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractPayableService.updateContractPayable(contractPayable, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加收款记录
	 * @param contractReceivablesRecord 收款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertContractReceivablesRecord")
	public RetDataBean insertContractReceivablesRecord(ContractReceivablesRecord contractReceivablesRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			contractReceivablesRecord.setRecordId(SysTools.getGUID());
			contractReceivablesRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			contractReceivablesRecord.setCreateUser(userInfo.getAccountId());
			contractReceivablesRecord.setOrgId(userInfo.getOrgId());
			return contractReceivablesRecordService.receivableAmount(contractReceivablesRecord);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除收款记录
	 * @param contractReceivablesRecord 收款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractReceivablesRecord")
	public RetDataBean deleteContractReceivablesRecord(ContractReceivablesRecord contractReceivablesRecord) {
		try {
			if (StringUtils.isBlank(contractReceivablesRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			contractReceivablesRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractReceivablesRecordService.deleteContractReceivablesRecord(contractReceivablesRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新收款记录
	 * @param contractReceivablesRecord 收款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateContractReceivablesRecord")
	public RetDataBean updateContractReceivablesRecord(ContractReceivablesRecord contractReceivablesRecord) {
		try {
			if (StringUtils.isBlank(contractReceivablesRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ContractReceivablesRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", contractReceivablesRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractReceivablesRecordService.updateContractReceivablesRecord(example, contractReceivablesRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建应收款记录
	 * @param contractReceivables 应收款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertContractReceivables")
	public RetDataBean insertContractReceivables(ContractReceivables contractReceivables) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			contractReceivables.setReceivablesId(SysTools.getGUID());
			contractReceivables.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			contractReceivables.setCreateUser(userInfo.getAccountId());
			contractReceivables.setReceived(0.0);
			contractReceivables.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractReceivablesService.insertContractReceivables(contractReceivables));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除应收款记录
	 * @param contractReceivables 应收款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractReceivables")
	public RetDataBean deleteContractReceivables(ContractReceivables contractReceivables) {
		try {
			if (StringUtils.isBlank(contractReceivables.getReceivablesId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (userInfo.getOpFlag().equals("1")) {
				contractReceivables.setOrgId(userInfo.getOrgId());
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractReceivablesService.deleteContractReceivables(contractReceivables));
			} else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除应收款
	 * @param ids 应收款记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractReceivablesByIds")
	public RetDataBean deleteContractReceivablesByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return contractReceivablesService.deleteContractReceivablesByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新应收款记录
	 * @param contractReceivables 应收款记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateContractReceivables")
	public RetDataBean updateContractReceivables(ContractReceivables contractReceivables) {
		try {
			if (StringUtils.isBlank(contractReceivables.getReceivablesId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ContractReceivables.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("receivablesId", contractReceivables.getReceivablesId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractReceivablesService.updateContractReceivables(contractReceivables, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加合同
	 * @param contract 合同对象
	 * @return 消息结构
	 */
	@PostMapping("/insertContract")
	public RetDataBean insertContract(Contract contract) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			contract.setContractId(SysTools.getGUID());
			contract.setOrgId(userInfo.getOrgId());
			contract.setCreateUser(userInfo.getAccountId());
			contract.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractService.insertContract(contract));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除合同
	 * @param contract 合同对象
	 * @return 消息结构
	 */
	@PostMapping("/deleteContract")
	public RetDataBean deleteContract(Contract contract) {
		try {
			if (StringUtils.isBlank(contract.getContractId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				contract.setCreateUser(userInfo.getAccountId());
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractService.deleteContract(contract));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除合同
	 * @param ids 合同记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractByIds")
	public RetDataBean deleteContractByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return contractService.deleteContractByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 编辑合同
	 * @param contract 合同对象
	 * @return 消息结构
	 */
	@PostMapping("/updateContract")
	public RetDataBean updateContract(Contract contract) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(contract.getContractId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(Contract.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("contractId", contract.getContractId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractService.updateContract(contract, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建合同分类
	 * @param contractSort 合同分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertContractSort")
	public RetDataBean insertContractSort(ContractSort contractSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			contractSort.setSortId(SysTools.getGUID());
			contractSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			contractSort.setCreateUser(userInfo.getAccountId());
			if (StringUtils.isBlank(contractSort.getParentId())) {
				contractSort.setParentId("0");
			}
			contractSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, contractSortService.insertContractSort(contractSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新合同分类信息
	 * @param contractSort 合同分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateContractSort")
	public RetDataBean updateContractSort(ContractSort contractSort) {
		try {
			if (StringUtils.isBlank(contractSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ContractSort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", contractSort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, contractSortService.updateContractSort(contractSort, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除分类
	 * @param contractSort 合同分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteContractSort")
	public RetDataBean deleteContractSort(ContractSort contractSort) {
		try {
			if (StringUtils.isBlank(contractSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			contractSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, contractSortService.deleteContractSort(contractSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 录入费用
	 * @param budgetCost 费用记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertBudgetCost")
	public RetDataBean insertBudgetCost(BudgetCost budgetCost) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetCost.setRecordId(SysTools.getGUID());
			budgetCost.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			budgetCost.setCreateUser(userInfo.getAccountId());
			budgetCost.setOrgId(userInfo.getOrgId());
			return budgetCostService.addBudgetCost(budgetCost);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除费用
	 * @param budgetCost 预算费用对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteBudgetCost")
	public RetDataBean deleteBudgetCost(BudgetCost budgetCost) {
		try {
			if (StringUtils.isBlank(budgetCost.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetCost.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetCostService.deleteBudgetCost(budgetCost));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新费用
	 * @param budgetCost 费用对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateBudgetCost")
	public RetDataBean updateBudgetCost(BudgetCost budgetCost) {
		try {
			if (StringUtils.isBlank(budgetCost.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetCost.setOrgId(userInfo.getOrgId());
			Example example = new Example(BudgetCost.class);
			example.createCriteria().andEqualTo("orgId", budgetCost.getOrgId()).andEqualTo("recordId", budgetCost.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetCostService.updateBudgetCost(example, budgetCost));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 申请项目预算费用
	 * @param budgetCostApply 项目预算费用对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertBudgetCostApply")
	public RetDataBean insertBudgetCostApply(BudgetCostApply budgetCostApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetCostApply.setRecordId(SysTools.getGUID());
			budgetCostApply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			budgetCostApply.setCreateUser(userInfo.getAccountId());
			budgetCostApply.setStatus("0");
			budgetCostApply.setOrgId(userInfo.getOrgId());
			return budgetCostApplyService.addBudgetCostApply(budgetCostApply);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除预算费用申请
	 * @param budgetCostApply 项目预算费用申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteBudgetCostApply")
	public RetDataBean deleteBudgetCostApply(BudgetCostApply budgetCostApply) {
		try {
			if (StringUtils.isBlank(budgetCostApply.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetCostApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetCostApplyService.deleteBudgetCostApply(budgetCostApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新预算费用申请
	 * @param budgetCostApply 项目预算费用申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateBudgetCostApply")
	public RetDataBean updateBudgetCostApply(BudgetCostApply budgetCostApply) {
		try {
			if (StringUtils.isBlank(budgetCostApply.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetCostApply.setOrgId(userInfo.getOrgId());
			Example example = new Example(BudgetCostApply.class);
			example.createCriteria().andEqualTo("orgId", budgetCostApply.getOrgId()).andEqualTo("recordId", budgetCostApply.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetCostApplyService.updateBudgetCostApply(example, budgetCostApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 预算费用申请审批
	 * @param budgetCostApply 项目预算费用申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setCostApprovalStatus")
	public RetDataBean setCostApprovalStatus(BudgetCostApply budgetCostApply) {
		try {
			if (StringUtils.isBlank(budgetCostApply.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetCostApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, budgetCostApplyService.setCostApprovalStatus(budgetCostApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加费用调整申请
	 * @param budgetAdjustment 费用调整申请对象
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
	@PostMapping(value = "/insertBudgetAdjustment")
	public RetDataBean insertBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
		try {
			String time = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetAdjustment.setRecordId(SysTools.getGUID());
			budgetAdjustment.setCreateTime(time);
			budgetAdjustment.setStatus("0");
			budgetAdjustment.setCreateUser(userInfo.getAccountId());
			budgetAdjustment.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetAdjustmentService.insertBudgetAdjustment(budgetAdjustment));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 删除预算调整申请
	 * @param budgetAdjustment 费用调整申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteBudgetAdjustment")
	public RetDataBean deleteBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
		try {
			if (StringUtils.isBlank(budgetAdjustment.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetAdjustment.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetAdjustmentService.deleteBudgetAdjustment(budgetAdjustment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 费用调整申请
	 * @param budgetAdjustment 费用调整申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateBudgetAdjustment")
	public RetDataBean updateBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
		try {
			if (StringUtils.isBlank(budgetAdjustment.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetAdjustment.setOrgId(userInfo.getOrgId());
			Example example = new Example(BudgetAdjustment.class);
			example.createCriteria().andEqualTo("orgId", budgetAdjustment.getOrgId()).andEqualTo("recordId", budgetAdjustment.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetAdjustmentService.updateBudgetAdjustment(example, budgetAdjustment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 审批时更新项目预算
	 * @param budgetAdjustment 费用调整申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setBudgetAdjustmentApprovalStatus")
	public RetDataBean setBudgetAdjustmentApprovalStatus(BudgetAdjustment budgetAdjustment) {
		try {
			if (StringUtils.isBlank(budgetAdjustment.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			budgetAdjustment.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetAdjustmentService.setBudgetAdjustmentApprovalStatus(budgetAdjustment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
     * 创建子项目
     * @param budgetProject 预算项目对象
     * @return 消息结构
     */
    @PostMapping(value = "/insertChildBudgetProject")
    public RetDataBean insertChildBudgetProject(BudgetProject budgetProject) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("budget:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
            UserInfo userInfo = userInfoService.getRedisUser();
            if (StringUtils.isBlank(budgetProject.getLevelId())) {
                budgetProject.setLevelId("0");
            }
            budgetProject.setProjectId(SysTools.getGUID());
            budgetProject.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetProject.setCreateUser(userInfo.getAccountId());
            budgetProject.setOrgId(userInfo.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetProjectService.addBudgetProject(budgetProject));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建预算项目
     * @param budgetProject 预算项目对象
     * @return 消息结构
     */
    @PostMapping(value = "/insertBudgetProject")
    public RetDataBean insertBudgetProject(BudgetProject budgetProject) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("budget:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
            UserInfo userInfo = userInfoService.getRedisUser();
            if (StringUtils.isBlank(budgetProject.getParentId())) {
                budgetProject.setParentId("0");
            }
            budgetProject.setProjectId(SysTools.getGUID());
            budgetProject.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetProject.setCreateUser(userInfo.getAccountId());
            budgetProject.setOrgId(userInfo.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetProjectService.addBudgetProject(budgetProject));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除预算项目
     * @param budgetProject 预算项目对象
     * @return 消息结构
     */
    @PostMapping(value = "/deleteBudgetProject")
    public RetDataBean deleteBudgetProject(BudgetProject budgetProject) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("budget:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
            if (StringUtils.isBlank(budgetProject.getProjectId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = userInfoService.getRedisUser();
            budgetProject.setOrgId(userInfo.getOrgId());
            return budgetProjectService.deleteAndCheckBudgetProject(budgetProject);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 *  批量删除项目预算
	 * @param ids 项目预算记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteBudgetProjectByIds")
	public RetDataBean deleteBudgetProjectByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("budget:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return budgetProjectService.deleteBudgetProjectByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
    /**
     * 更新预算项目
     * @param budgetProject 预算费用对象
     * @return 消息结构
     */
    @PostMapping(value = "/updateBudgetProject")
    public RetDataBean updateBudgetProject(BudgetProject budgetProject) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("budget:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
            if (StringUtils.isBlank(budgetProject.getProjectId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = userInfoService.getRedisUser();
            Example example = new Example(BudgetProject.class);
            example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("projectId", budgetProject.getProjectId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetProjectService.updateBudgetProjectAndParentId(example, budgetProject));
        } catch (Exception e) {
        	logger.error(e.getMessage());
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *  添加预算科目
     * @param budgetAccount 预算科目对象
     * @return 消息结构
     */
    @PostMapping(value = "/insertBudgetAccount")
    public RetDataBean insertContractSort(BudgetAccount budgetAccount) {
        try {
            UserInfo userInfo = userInfoService.getRedisUser();
            budgetAccount.setBudgetAccountId(SysTools.getGUID());
            budgetAccount.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetAccount.setCreateUser(userInfo.getAccountId());
            if (StringUtils.isBlank(budgetAccount.getParentId())) {
                budgetAccount.setParentId("0");
            }
            budgetAccount.setStatus("1");
            budgetAccount.setOrgId(userInfo.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetAccountService.insertBudgetAccount(budgetAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *  批量删除预算科目
     * @param ids 预算科目记录Id 数组
     * @return 消息结构
     */
    @PostMapping(value = "/deleteBudgetAccountIds")
    public RetDataBean deleteBudgetAccountIds(@RequestParam(value = "ids[]") String[] ids) {
        try {
            UserInfo userInfo = userInfoService.getRedisUser();
            if (!userInfo.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                List<String> idsList = Arrays.asList(ids);
                return budgetAccountService.deleteBudgetAccountIds(userInfo.getOrgId(), idsList);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
    /**
     * 删除预算科目
     * @param budgetAccount 预算科目对象
     * @return 消息结构
     */
    @PostMapping(value = "/deleteBudgetAccount")
    public RetDataBean deleteBudgetAccount(BudgetAccount budgetAccount) {
        try {
            if (StringUtils.isBlank(budgetAccount.getBudgetAccountId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = userInfoService.getRedisUser();
            budgetAccount.setOrgId(userInfo.getOrgId());
            if(budgetAccountService.isExistChild(userInfo.getOrgId(),budgetAccount.getBudgetAccountId()))
			{
				return RetDataTools.NotOk(MessageCode.MESSAGE_EXIST_CHILD);
			}else {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetAccountService.deleteBudgetAccount(budgetAccount));
			}
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新预算科目
     * @param budgetAccount 预算科目对象
     * @return 消息结构
     */
    @PostMapping(value = "/updateBudgetAccount")
    public RetDataBean updateBudgetAccount(BudgetAccount budgetAccount) {
        try {
            if (StringUtils.isBlank(budgetAccount.getBudgetAccountId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = userInfoService.getRedisUser();
            Example example = new Example(BudgetAccount.class);
            example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("budgetAccountId", budgetAccount.getBudgetAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetAccountService.updateBudgetAccount(example, budgetAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 创建配置
	 * @param budgetConfig 预算配置对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setBudgetConfig")
    public RetDataBean setBudgetConfig(BudgetConfig budgetConfig) {
        try {
            UserInfo userInfo = userInfoService.getRedisUser();
            budgetConfig.setConfigId(SysTools.getGUID());
            budgetConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetConfig.setCreateUser(userInfo.getAccountId());
            budgetConfig.setOrgId(userInfo.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetConfigService.setBudgetConfig(budgetConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 删除配置
	 * @param budgetConfig 预算配置对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteBudgetConfig")
    public RetDataBean deleteBudgetConfig(BudgetConfig budgetConfig) {
        try {
            if (StringUtils.isBlank(budgetConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = userInfoService.getRedisUser();
            budgetConfig.setOrgId(userInfo.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetConfigService.deleteBudgetConfig(budgetConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 更新配置
	 * @param budgetConfig 预算配置对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateBudgetConfig")
    public RetDataBean updateBudgetConfig(BudgetConfig budgetConfig) {
        try {
            if (StringUtils.isBlank(budgetConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = userInfoService.getRedisUser();
            budgetConfig.setOrgId(userInfo.getOrgId());
            Example example = new Example(BudgetConfig.class);
            example.createCriteria().andEqualTo("orgId", budgetConfig.getOrgId()).andEqualTo("configId", budgetConfig.getConfigId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetConfigService.updateBudgetConfig(example, budgetConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
