package com.core136.controller.office;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.UserInfo;
import com.core136.bean.bpm.BpmList;
import com.core136.bean.bpm.BpmStepRun;
import com.core136.bean.office.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.bpm.BpmFormDataService;
import com.core136.service.office.*;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/office")
@CrossOrigin
@Api(value="行政SetController",tags={"行政更新数据接口"})
public class ApiSetOfficeController {
	private UserInfoService userInfoService;
	private DiaryConfigService diaryConfigService;
	private DiaryService diaryService;
	private DiaryCommentsService diaryCommentsService;
	private OfficeSuppliesSortService officeSuppliesSortService;
	private OfficeSuppliesService officeSuppliesService;
	private OfficeSuppliesApplyService officeSuppliesApplyService;
	private OfficeSuppliesGrantService officeSuppliesGrantService;
	private MeetingRoomService meetingRoomService;
	private MeetingDeviceService meetingDeviceService;
	private WorkPlanService workPlanService;
	private WorkPlanProcessService workPlanProcessService;
	private MeetingService meetingService;
	private MeetingNotesService meetingNotesService;
	private FixedAssetsStorageService fixedAssetsStorageService;
	private FixedAssetsSortService fixedAssetsSortService;
	private FixedAssetsService fixedAssetsService;
	private FixedAssetsApplyService fixedAssetApplyService;
	private FixedAssetsRepairService fixedAssetsRepairService;
	private FixedAssetsAllocationService fixedAssetsAllocationService;
	private VehicleOilCardService vehicleOilCardService;
	private VehicleDriverService vehicleDriverService;
	private VehicleInfoService vehicleInfoService;
	private VehicleRepairRecordService vehicleRepairRecordService;
	private VehicleApplyService vehicleApplyService;
	private VehicleOperatorService vehicleOperatorService;
	private TaskService taskService;
	private TaskGanttDataService taskGanttDataService;
	private TaskGanttLinkService taskGanttLinkService;
	private TaskProcessService taskProcessService;
	private ExamSortService examSortService;
	private ExamQuestionsService examQuestionsService;
	private ExamTestSortService examTestSortService;
	private ExamQuestionRecordService examQuestionRecordService;
	private ExamTestService examTestService;
	private ExamGradeService examGradeService;
	private AttendService attendService;
	private DutySortService dutySortService;
	private DutyRecordService dutyRecordService;
	private LicenceRecordService licenceRecordService;
	private LicenceBorrowingService licenceBorrowingService;
	private LicenceTypeService licenceTypeService;
	private BpmFormDataService bpmFormDataService;
	private EntertainService entertainService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setDiaryConfigService(DiaryConfigService diaryConfigService) {
		this.diaryConfigService = diaryConfigService;
	}
	@Autowired
	public void setDiaryService(DiaryService diaryService) {
		this.diaryService = diaryService;
	}
	@Autowired
	public void setDiaryCommentsService(DiaryCommentsService diaryCommentsService) {
		this.diaryCommentsService = diaryCommentsService;
	}
	@Autowired
	public void setOfficeSuppliesSortService(OfficeSuppliesSortService officeSuppliesSortService) {
		this.officeSuppliesSortService = officeSuppliesSortService;
	}
	@Autowired
	public void setOfficeSuppliesService(OfficeSuppliesService officeSuppliesService) {
		this.officeSuppliesService = officeSuppliesService;
	}
	@Autowired
	public void setOfficeSuppliesApplyService(OfficeSuppliesApplyService officeSuppliesApplyService) {
		this.officeSuppliesApplyService = officeSuppliesApplyService;
	}
	@Autowired
	public void setOfficeSuppliesGrantService(OfficeSuppliesGrantService officeSuppliesGrantService) {
		this.officeSuppliesGrantService = officeSuppliesGrantService;
	}
	@Autowired
	public void setMeetingRoomService(MeetingRoomService meetingRoomService) {
		this.meetingRoomService = meetingRoomService;
	}
	@Autowired
	public void setMeetingDeviceService(MeetingDeviceService meetingDeviceService) {
		this.meetingDeviceService = meetingDeviceService;
	}
	@Autowired
	public void setWorkPlanService(WorkPlanService workPlanService) {
		this.workPlanService = workPlanService;
	}
	@Autowired
	public void setWorkPlanProcessService(WorkPlanProcessService workPlanProcessService) {
		this.workPlanProcessService = workPlanProcessService;
	}
	@Autowired
	public void setMeetingService(MeetingService meetingService) {
		this.meetingService = meetingService;
	}
	@Autowired
	public void setMeetingNotesService(MeetingNotesService meetingNotesService) {
		this.meetingNotesService = meetingNotesService;
	}
	@Autowired
	public void setFixedAssetsStorageService(FixedAssetsStorageService fixedAssetsStorageService) {
		this.fixedAssetsStorageService = fixedAssetsStorageService;
	}
	@Autowired
	public void setFixedAssetsSortService(FixedAssetsSortService fixedAssetsSortService) {
		this.fixedAssetsSortService = fixedAssetsSortService;
	}
	@Autowired
	public void setFixedAssetsService(FixedAssetsService fixedAssetsService) {
		this.fixedAssetsService = fixedAssetsService;
	}
	@Autowired
	public void setFixedAssetApplyService(FixedAssetsApplyService fixedAssetApplyService) {
		this.fixedAssetApplyService = fixedAssetApplyService;
	}
	@Autowired
	public void setFixedAssetsRepairService(FixedAssetsRepairService fixedAssetsRepairService) {
		this.fixedAssetsRepairService = fixedAssetsRepairService;
	}
	@Autowired
	public void setFixedAssetsAllocationService(FixedAssetsAllocationService fixedAssetsAllocationService) {
		this.fixedAssetsAllocationService = fixedAssetsAllocationService;
	}
	@Autowired
	public void setVehicleOilCardService(VehicleOilCardService vehicleOilCardService) {
		this.vehicleOilCardService = vehicleOilCardService;
	}
	@Autowired
	public void setVehicleDriverService(VehicleDriverService vehicleDriverService) {
		this.vehicleDriverService = vehicleDriverService;
	}
	@Autowired
	public void setVehicleInfoService(VehicleInfoService vehicleInfoService) {
		this.vehicleInfoService = vehicleInfoService;
	}
	@Autowired
	public void setVehicleRepairRecordService(VehicleRepairRecordService vehicleRepairRecordService) {
		this.vehicleRepairRecordService = vehicleRepairRecordService;
	}
	@Autowired
	public void setVehicleApplyService(VehicleApplyService vehicleApplyService) {
		this.vehicleApplyService = vehicleApplyService;
	}
	@Autowired
	public void setVehicleOperatorService(VehicleOperatorService vehicleOperatorService) {
		this.vehicleOperatorService = vehicleOperatorService;
	}
	@Autowired
	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
	@Autowired
	public void setTaskGanttDataService(TaskGanttDataService taskGanttDataService) {
		this.taskGanttDataService = taskGanttDataService;
	}
	@Autowired
	public void setTaskGanttLinkService(TaskGanttLinkService taskGanttLinkService) {
		this.taskGanttLinkService = taskGanttLinkService;
	}
	@Autowired
	public void setTaskProcessService(TaskProcessService taskProcessService) {
		this.taskProcessService = taskProcessService;
	}
	@Autowired
	public void setExamSortService(ExamSortService examSortService) {
		this.examSortService = examSortService;
	}
	@Autowired
	public void setExamQuestionsService(ExamQuestionsService examQuestionsService) {
		this.examQuestionsService = examQuestionsService;
	}
	@Autowired
	public void setExamTestSortService(ExamTestSortService examTestSortService) {
		this.examTestSortService = examTestSortService;
	}
	@Autowired
	public void setExamQuestionRecordService(ExamQuestionRecordService examQuestionRecordService) {
		this.examQuestionRecordService = examQuestionRecordService;
	}
	@Autowired
	public void setExamTestService(ExamTestService examTestService) {
		this.examTestService = examTestService;
	}
	@Autowired
	public void setExamGradeService(ExamGradeService examGradeService) {
		this.examGradeService = examGradeService;
	}
	@Autowired
	public void setAttendService(AttendService attendService) {
		this.attendService = attendService;
	}
	@Autowired
	public void setDutySortService(DutySortService dutySortService) {
		this.dutySortService = dutySortService;
	}
	@Autowired
	public void setDutyRecordService(DutyRecordService dutyRecordService) {
		this.dutyRecordService = dutyRecordService;
	}
	@Autowired
	public void setLicenceRecordService(LicenceRecordService licenceRecordService) {
		this.licenceRecordService = licenceRecordService;
	}
	@Autowired
	public void setLicenceBorrowingService(LicenceBorrowingService licenceBorrowingService) {
		this.licenceBorrowingService = licenceBorrowingService;
	}
	@Autowired
	public void setLicenceTypeService(LicenceTypeService licenceTypeService) {
		this.licenceTypeService = licenceTypeService;
	}
	@Autowired
	public void setBpmFormDataService(BpmFormDataService bpmFormDataService) {
		this.bpmFormDataService = bpmFormDataService;
	}
	@Autowired
	public void setEntertainService(EntertainService entertainService) {
		this.entertainService = entertainService;
	}

	/**
	 * 发起出借申请
	 * @param licenceBorrowing 证件借阅对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/createBpmByLicenceRecordRule")
	public RetDataBean createBpmByLicenceRecordRule(LicenceBorrowing licenceBorrowing) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			licenceBorrowing.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			licenceBorrowing.setCreateUser(userInfo.getAccountId());
			licenceBorrowing.setOrgId(userInfo.getOrgId());
			LicenceRecord licenceRecord = new LicenceRecord();
			licenceRecord.setRecordId(licenceBorrowing.getLicId());
			licenceRecord.setOrgId(userInfo.getOrgId());
			licenceRecord = licenceRecordService.selectOneLicenceRecord(licenceRecord);
			LicenceType licenceType = new LicenceType();
			licenceType.setTypeId(licenceRecord.getLicType());
			licenceType.setOrgId(userInfo.getOrgId());
			licenceType = licenceTypeService.selectOneLicenceType(licenceType);
			BpmList bpmList = new BpmList();
			bpmList.setFlowTitle("《"+licenceRecord.getTitle()+"》证照出借审批");
			bpmList.setUrgency("0");
			bpmList.setFlowId(licenceType.getFlowId());
			JSONObject formDataJson = new JSONObject();
			if(StringUtils.isNotBlank(licenceType.getDataMap())) {
				String[] dataMapList = licenceType.getDataMap().split(",");
				JSONObject vJson = (JSONObject) JSON.toJSON(licenceBorrowing);
				for (String vkStr : dataMapList) {
					String[] vkArr = vkStr.split(":");
					formDataJson.put(vkArr[1], vJson.get(vkArr[0]));
				}
			}
			RetDataBean retdataBean = bpmFormDataService.createBpmFormData(userInfo, bpmList, new BpmStepRun(), formDataJson);
			JSONObject resJson = JSON.parseObject(retdataBean.getData().toString());
			licenceBorrowing.setRunId(resJson.getString("runId"));
			licenceBorrowing.setStatus("0");
			licenceBorrowing.setRecordId(SysTools.getGUID());
			licenceBorrowingService.insertLicenceBorrowing(licenceBorrowing);
			return retdataBean;
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加出借记录
	 * @param licenceBorrowing 出借记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertLicenceBorrowing")
	public RetDataBean insertLicenceBorrowing(LicenceBorrowing licenceBorrowing) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			licenceBorrowing.setRecordId(SysTools.getGUID());
			licenceBorrowing.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			licenceBorrowing.setCreateUser(userInfo.getAccountId());
			licenceBorrowing.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, licenceBorrowingService.insertLicenceBorrowing(licenceBorrowing));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除出借记录
	 * @param licenceBorrowing 出借记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLicenceBorrowing")
	public RetDataBean deleteLicenceBorrowing(LicenceBorrowing licenceBorrowing) {
		try {
			if (StringUtils.isBlank(licenceBorrowing.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			licenceBorrowing.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, licenceBorrowingService.deleteLicenceBorrowing(licenceBorrowing));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  批量删除出借记录
	 * @param ids 出借记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLicenceBorrowingByIds")
	public RetDataBean deleteLicenceBorrowingByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return licenceBorrowingService.deleteLicenceBorrowingByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新出借记录
	 * @param licenceBorrowing 出借记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateLicenceBorrowing")
	public RetDataBean updateLicenceBorrowing(LicenceBorrowing licenceBorrowing) {
		try {
			if (StringUtils.isBlank(licenceBorrowing.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isNotBlank(licenceBorrowing.getBackTime()))
			{
				licenceBorrowing.setStatus("3");
			}
			Example example = new Example(LicenceBorrowing.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", licenceBorrowing.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, licenceBorrowingService.updateLicenceBorrowing(example, licenceBorrowing));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加证照类型
	 * @param licenceType 证照类型
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertLicenceType")
	public RetDataBean insertLicenceType(LicenceType licenceType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			licenceType.setTypeId(SysTools.getGUID());
			licenceType.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			licenceType.setCreateUser(userInfo.getAccountId());
			licenceType.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, licenceTypeService.insertLicenceType(licenceType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除证照类型
	 * @param licenceType 证照类型对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLicenceType")
	public RetDataBean deleteLicenceType(LicenceType licenceType) {
		try {
			if (StringUtils.isBlank(licenceType.getTypeId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			licenceType.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, licenceTypeService.deleteLicenceType(licenceType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  批量删除证照类型
	 * @param ids 证照类型Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLicenceTypeByIds")
	public RetDataBean deleteLicenceTypeByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return licenceTypeService.deleteLicenceTypeByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新证照类型
	 * @param licenceType 证照类型对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateLicenceType")
	public RetDataBean updateLicenceType(LicenceType licenceType) {
		try {
			if (StringUtils.isBlank(licenceType.getTypeId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(LicenceType.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("typeId", licenceType.getTypeId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, licenceTypeService.updateLicenceType(example, licenceType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加证照
	 * @param licenceRecord 证照记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertLicenceRecord")
	public RetDataBean insertLicenceRecord(LicenceRecord licenceRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			licenceRecord.setRecordId(SysTools.getGUID());
			licenceRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			licenceRecord.setCreateUser(userInfo.getAccountId());
			licenceRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, licenceRecordService.insertLicenceRecord(licenceRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除证照
	 * @param licenceRecord 证照记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLicenceRecord")
	public RetDataBean deleteLicenceRecord(LicenceRecord licenceRecord) {
		try {
			if (StringUtils.isBlank(licenceRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			licenceRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, licenceRecordService.deleteLicenceRecord(licenceRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  批量删除证照
	 * @param ids 证照Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLicenceRecordByIds")
	public RetDataBean deleteLicenceRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return licenceRecordService.deleteLicenceRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新证照
	 * @param licenceRecord 证照记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateLicenceRecord")
	public RetDataBean updateLicenceRecord(LicenceRecord licenceRecord) {
		try {
			if (StringUtils.isBlank(licenceRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(LicenceRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", licenceRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, licenceRecordService.updateLicenceRecord(example, licenceRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加值班记录
	 * @param dutyRecord 值班记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDutyRecord")
	public RetDataBean insertDutyRecord(DutyRecord dutyRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			dutyRecord.setRecordId(SysTools.getGUID());
			dutyRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			dutyRecord.setCreateUser(userInfo.getAccountId());
			dutyRecord.setStatus("0");
			dutyRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, dutyRecordService.insertDutyRecord(dutyRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除值班记录
	 * @param dutyRecord 值班记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDutyRecord")
	public RetDataBean deleteDutyRecord(DutyRecord dutyRecord) {
		try {
			if (StringUtils.isBlank(dutyRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dutyRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, dutyRecordService.deleteDutyRecord(dutyRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 审批值班记录
	 * @param recordId 值班记录Id
	 * @param status 状态
	 * @return 消息结构
	 */
	@PostMapping(value = "/approvalDutyRecord")
	public RetDataBean approvalDutyRecord(String recordId,String status) {
		try {
			if (StringUtils.isBlank(recordId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return dutyRecordService.approvalDutyRecord(userInfo.getOrgId(),recordId,status);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新值班记录
	 * @param dutyRecord 值班记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDutyRecord")
	public RetDataBean updateDutyRecord(DutyRecord dutyRecord) {
		try {
			if (StringUtils.isBlank(dutyRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(DutyRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", dutyRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, dutyRecordService.updateDutyRecord(example, dutyRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加值班分类
	 * @param dutySort 值班分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDutySort")
	public RetDataBean insertDutySort(DutySort dutySort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			dutySort.setSortId(SysTools.getGUID());
			dutySort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			dutySort.setCreateUser(userInfo.getAccountId());
			dutySort.setStatus("1");
			dutySort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, dutySortService.insertDutySort(dutySort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除值班分类
	 * @param dutySort 值班分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDutySort")
	public RetDataBean deleteDutySort(DutySort dutySort) {
		try {
			if (StringUtils.isBlank(dutySort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dutySort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, dutySortService.deleteDutySort(dutySort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新值班分类
	 * @param dutySort 值班分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDutySort")
	public RetDataBean updateDutySort(DutySort dutySort) {
		try {
			if (StringUtils.isBlank(dutySort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(DutySort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", dutySort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, dutySortService.updateDutySort(example, dutySort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 提交考试成绩
	 * @param examGrade 成绩对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/addExamGrade")
	public RetDataBean addExamGrade(ExamGrade examGrade) {
		try {
			if (StringUtils.isBlank(examGrade.getExamTestId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if(examGradeService.getExamTestStatus(userInfo.getOrgId(),examGrade.getExamTestId(),userInfo.getAccountId()))
			{
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			examGrade.setOrgId(userInfo.getOrgId());
			examGrade.setAccountId(userInfo.getAccountId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examGradeService.addExamGrade(examGrade));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除考试成绩
	 * @param examGrade 成绩对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExamGrade")
	public RetDataBean deleteExamGrade(ExamGrade examGrade) {
		try {
			if (StringUtils.isBlank(examGrade.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			examGrade.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examGradeService.deleteExamGrade(examGrade));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新考试成绩
	 * @param examGrade 成绩对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExamGrade")
	public RetDataBean updateExamGrade(ExamGrade examGrade) {
		try {
			if (StringUtils.isBlank(examGrade.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExamGrade.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", examGrade.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examGradeService.updateExamGrade(example, examGrade));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 试卷判卷
	 * @param recordId 试卷Id
	 * @param totalGrade 考试成绩对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/approvalExamTest")
	public RetDataBean approvalExamTest(String recordId, Double totalGrade) {
		try {
			if (StringUtils.isBlank(recordId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			ExamGrade examGrade = new ExamGrade();
			UserInfo userInfo = userInfoService.getRedisUser();
			examGrade.setOrgId(userInfo.getOrgId());
			examGrade.setRecordId(recordId);
			examGrade = examGradeService.selectOneExamGrade(examGrade);
			examGrade.setGrade(examGrade.getGrade() + totalGrade);
			examGrade.setStatus("2");
			Example example = new Example(ExamTest.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", recordId);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, examGradeService.updateExamGrade(example, examGrade));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加试题测试
	 * @param examTest 试题测试对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExamTest")
	public RetDataBean insertExamTest(ExamTest examTest) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			examTest.setRecordId(SysTools.getGUID());
			examTest.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			examTest.setCreateUser(userInfo.getAccountId());
			examTest.setStatus("0");
			examTest.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examTestService.addExamTest(userInfo,examTest));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除试卷
	 * @param examTest 考试对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExamTest")
	public RetDataBean deleteExamTest(ExamTest examTest) {
		try {
			if (StringUtils.isBlank(examTest.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			examTest.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examTestService.deleteExamTest(examTest));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  批量删除试题
	 * @param ids 试题记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExamTestByIds")
	public RetDataBean deleteExamTestByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return examTestService.deleteExamTestByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新试题测试状态
	 * @param examTest 考试对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExamTestStatus")
	public RetDataBean updateExamTestStatus(ExamTest examTest) {
		try {
			if (StringUtils.isBlank(examTest.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExamTest.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", examTest.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examTestService.updateExamTest(example, examTest));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新试题测试
	 * @param examTest 考试对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExamTest")
	public RetDataBean updateExamTest(ExamTest examTest) {
		try {
			if (StringUtils.isBlank(examTest.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExamTest.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", examTest.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examTestService.updateExamTest(userInfo,example, examTest));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加试卷试题
	 * @param examQuestionRecord 试卷试题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExamQuestionRecord")
	public RetDataBean insertExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			examQuestionRecord.setRecordId(SysTools.getGUID());
			examQuestionRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			examQuestionRecord.setCreateUser(userInfo.getAccountId());
			examQuestionRecord.setStatus("0");
			examQuestionRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examQuestionRecordService.insertExamQuestionRecord(examQuestionRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除试卷试题
	 * @param examQuestionRecord 试卷试题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExamQuestionRecord")
	public RetDataBean deleteExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
		try {
			if (StringUtils.isBlank(examQuestionRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			examQuestionRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examQuestionRecordService.deleteExamQuestionRecord(examQuestionRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新试卷试题
	 * @param examQuestionRecord 试卷试题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExamQuestionRecord")
	public RetDataBean updateExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
		try {
			if (StringUtils.isBlank(examQuestionRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExamQuestionRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", examQuestionRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examQuestionRecordService.updateExamQuestionRecord(example, examQuestionRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加考试类型
	 * @param examTestSort 试卷分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExamTestSort")
	public RetDataBean insertExamTestSort(ExamTestSort examTestSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			examTestSort.setSortId(SysTools.getGUID());
			examTestSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			if(StringUtils.isBlank(examTestSort.getParentId()))
			{
				examTestSort.setParentId("0");
			}
			examTestSort.setCreateUser(userInfo.getAccountId());
			examTestSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examTestSortService.insertExamTestSort(examTestSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除考试类型
	 * @param examTestSort 试卷分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExamTestSort")
	public RetDataBean deleteExamTestSort(ExamTestSort examTestSort) {
		try {
			if (StringUtils.isBlank(examTestSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			examTestSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examTestSortService.deleteExamTestSort(examTestSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新试题分类
	 * @param examTestSort 试卷分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExamTestSort")
	public RetDataBean updateExamTestSort(ExamTestSort examTestSort) {
		try {
			if (StringUtils.isBlank(examTestSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isBlank(examTestSort.getParentId()))
			{
				examTestSort.setParentId("0");
			}
			Example example = new Example(ExamTestSort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", examTestSort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examTestSortService.updateExamTestSort(example, examTestSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加试题
	 * @param examQuestions 试卷试题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExamQuestions")
	public RetDataBean insertExamQuestions(ExamQuestions examQuestions) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			examQuestions.setRecordId(SysTools.getGUID());
			examQuestions.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			examQuestions.setCreateUser(userInfo.getAccountId());
			examQuestions.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examQuestionsService.insertExamQuestions(examQuestions));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除试题
	 * @param examQuestions 试卷试题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExamQuestions")
	public RetDataBean deleteExamQuestions(ExamQuestions examQuestions) {
		try {
			if (StringUtils.isBlank(examQuestions.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			examQuestions.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examQuestionsService.deleteExamQuestions(examQuestions));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  批量删除试题
	 * @param ids 试题Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExamQuestionsByIds")
	public RetDataBean deleteDiscussNoticeIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return examQuestionsService.deleteExamQuestionsByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新试题内容
	 * @param examQuestions 试卷试题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExamQuestions")
	public RetDataBean updateExamQuestions(ExamQuestions examQuestions) {
		try {
			if (StringUtils.isBlank(examQuestions.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExamQuestions.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", examQuestions.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examQuestionsService.updateExamQuestions(example, examQuestions));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加试题类型
	 * @param examSort 试题类型对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertExamSort")
	public RetDataBean insertExamSort(ExamSort examSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			examSort.setSortId(SysTools.getGUID());
			if(StringUtils.isBlank(examSort.getParentId()))
			{
				examSort.setParentId("0");
			}
			examSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			examSort.setCreateUser(userInfo.getAccountId());
			examSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, examSortService.insertExamSort(examSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除试题类型
	 * @param examSort 试题类型对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteExamSort")
	public RetDataBean deleteExamSort(ExamSort examSort) {
		try {
			if (StringUtils.isBlank(examSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			examSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, examSortService.deleteExamSort(examSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新试题分类
	 * @param examSort 试题类型对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateExamSort")
	public RetDataBean updateExamSort(ExamSort examSort) {
		try {
			if (StringUtils.isBlank(examSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if(StringUtils.isBlank(examSort.getParentId()))
			{
				examSort.setParentId("0");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ExamSort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", examSort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, examSortService.updateExamSort(example, examSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加子任务处理结果
	 * @param taskProcess 子任务处理对象
	 * @param progress 处理结果对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertTaskProcess")
	public RetDataBean insertTaskProcess(TaskProcess taskProcess, Double progress) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			TaskGanttData taskGanttData = new TaskGanttData();
			taskGanttData.setProgress(progress);
			taskGanttData.setOrgId(userInfo.getOrgId());
			Example example = new Example(TaskGanttData.class);
			example.createCriteria().andEqualTo("orgId", taskGanttData.getOrgId()).andEqualTo("taskDataId", taskProcess.getTaskDataId());
			taskGanttDataService.updateTaskGanttData(example, taskGanttData);
			taskProcess.setProcessId(SysTools.getGUID());
			taskProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			taskProcess.setCreateUser(userInfo.getAccountId());
			taskProcess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, taskProcessService.insertTaskProcess(taskProcess));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除子任务处理结果
	 * @param taskProcess 任务处理结果对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteTaskProcess")
	public RetDataBean deleteTaskProcess(TaskProcess taskProcess) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(taskProcess.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (!userInfo.getOpFlag().equals("1")) {
				taskProcess.setCreateUser(userInfo.getAccountId());
			}
			taskProcess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, taskProcessService.deleteTaskProcess(taskProcess));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 修改子任务的处理过程
	 * @param taskProcess 子任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateTaskProcess")
	public RetDataBean updateTaskGanttLink(TaskProcess taskProcess) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(taskProcess.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(TaskProcess.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", taskProcess.getProcessId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, taskProcessService.updateTaskProcess(example, taskProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建关联
	 * @param taskGanttLink 任务关联对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertTaskGanttLink")
	public RetDataBean insertTaskGanttLink(TaskGanttLink taskGanttLink) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isBlank(taskGanttLink.getTaskId()))
			{
				TaskGanttData t = new TaskGanttData();
				t.setOrgId(userInfo.getOrgId());
				t.setTaskDataId(taskGanttLink.getSource());
				t = taskGanttDataService.selectOneTaskGanttData(t);
				taskGanttLink.setTaskId(t.getTaskId());
			}
			taskGanttLink.setTaskLinkId(SysTools.getGUID());
			taskGanttLink.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			taskGanttLink.setCreateUser(userInfo.getAccountId());
			taskGanttLink.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, taskGanttLinkService.insertTaskGanttLink(taskGanttLink));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除关联关系
	 * @param taskGanttLink 任务关联对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteTaskGanttLink")
	public RetDataBean deleteTaskGanttLink(TaskGanttLink taskGanttLink) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(taskGanttLink.getTaskLinkId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (!userInfo.getOpFlag().equals("1")) {
				taskGanttLink.setCreateUser(userInfo.getAccountId());
			}
			taskGanttLink.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, taskGanttLinkService.deleteTaskGanttLink(taskGanttLink));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新子任务路径
	 * @param taskGanttLink 任务路径对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateTaskGanttLink")
	public RetDataBean updateTaskGanttLink(TaskGanttLink taskGanttLink) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(taskGanttLink.getTaskLinkId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(TaskGanttLink.class);
			if (userInfo.getOpFlag().equals("1")) {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskLinkId", taskGanttLink.getTaskLinkId());
			} else {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskLinkId", taskGanttLink.getTaskLinkId()).andEqualTo("createUser").andEqualTo(userInfo.getAccountId());
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, taskGanttLinkService.updateTaskGanttLink(example, taskGanttLink));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建子任务
	 * @param taskGanttData 任务关联子任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertTaskGanttData")
	public RetDataBean insertTaskGanttData(TaskGanttData taskGanttData,String id) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isBlank(taskGanttData.getTaskId()))
			{
				TaskGanttData t = new TaskGanttData();
				t.setOrgId(userInfo.getOrgId());
				t.setTaskDataId(taskGanttData.getParent());
				t = taskGanttDataService.selectOneTaskGanttData(t);
				taskGanttData.setTaskId(t.getTaskId());
			}
			taskGanttData.setTaskDataId(id);
			taskGanttData.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			taskGanttData.setCreateUser(userInfo.getAccountId());
			taskGanttData.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, taskGanttDataService.insertTaskGanttData(taskGanttData));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除子任务
	 * @param taskGanttData 任务关联子任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteTaskGanttData")
	public RetDataBean TaskGanttData(TaskGanttData taskGanttData) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(taskGanttData.getTaskDataId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (!userInfo.getOpFlag().equals("1")) {
				taskGanttData.setCreateUser(userInfo.getAccountId());
			}
			taskGanttData.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, taskGanttDataService.deleteTaskGanttData(taskGanttData));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新子任务
	 * @param taskGanttData 任务关联子任务对象
	 * @return 消息结构
	 */

	@PostMapping(value = "/updateTaskGanttData")
	public RetDataBean updateTaskGanttData(TaskGanttData taskGanttData) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(taskGanttData.getTaskDataId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(TaskGanttData.class);
			if (userInfo.getOpFlag().equals("1")) {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskDataId", taskGanttData.getTaskDataId());
			} else {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskDataId", taskGanttData.getTaskDataId()).andEqualTo("createUser").andEqualTo(userInfo.getAccountId());
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, taskGanttDataService.updateTaskGanttData(example, taskGanttData));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建任务
	 * @param task 任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertTask")
	public RetDataBean insertTask(Task task) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			task.setTaskId(SysTools.getGUID());
			UserInfo userInfo = userInfoService.getRedisUser();
			task.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			task.setStatus("0");
			Document htmlDoc = Jsoup.parse(task.getContent());
			String subheading = htmlDoc.text();
			if (subheading.length() > 50) {
				subheading = subheading.substring(0, 50) + "...";
			}
			task.setSubheading(subheading);
			task.setCreateUser(userInfo.getAccountId());
			task.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, taskService.addTask(userInfo, task));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除任务
	 * @param ids 任务Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteTaskByIds")
	public RetDataBean deleteTaskByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return taskService.deleteTaskByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 删除任务
	 * @param task 作务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteTask")
	public RetDataBean deleteTask(Task task) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(task.getTaskId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (!userInfo.getOpFlag().equals("1")) {
				task.setCreateUser(userInfo.getAccountId());
			}
			task.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, taskService.deleteTask(task));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新任务
	 * @param task 任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateTask")
	public RetDataBean updateTask(Task task) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Document htmlDoc = Jsoup.parse(task.getContent());
			String subheading = htmlDoc.text();
			if (subheading.length() > 50) {
				subheading = subheading.substring(0, 50) + "...";
			}
			task.setSubheading(subheading);
			if (StringUtils.isBlank(task.getTaskId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(Task.class);
			if (userInfo.getOpFlag().equals("1")) {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskId", task.getTaskId());
			} else {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskId", task.getTaskId()).andEqualTo("createUser").andEqualTo(userInfo.getAccountId());
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, taskService.updateTask(userInfo, task, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 设置调度员
	 * @param optUser 调度员账号
	 * @return 消息结构
	 */
	@PostMapping(value = "/setVehicleOperator")
	public RetDataBean setVehicleOperator(String optUser) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return vehicleOperatorService.setVehicleOperator(userInfo, optUser);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 车辆归还
	 * @param vehicleApply 车辆申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setReturnVehicle")
	public RetDataBean setReturnVehicle(VehicleApply vehicleApply,Double returnMileage,Double returnBalance) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			vehicleApply.setReturnTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			vehicleApply.setOrgId(userInfo.getOrgId());
			return vehicleApplyService.setReturnVehicle(vehicleApply,returnMileage,returnBalance);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 调度员审批
	 * @param vehicleApply 车辆使用申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setApprovalStatus")
	public RetDataBean setApprovalStatus(VehicleApply vehicleApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if(vehicleApply.getStatus().equals("1"))
			{
				if (StringUtils.isBlank(vehicleApply.getVehicleId()) || StringUtils.isBlank(vehicleApply.getApplyId())) {
					return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
				}
			}else
			{
				if (StringUtils.isBlank(vehicleApply.getApplyId())) {
					return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
				}
			}
			vehicleApply.setOrgId(userInfo.getOrgId());
			return vehicleApplyService.setApprovalStatus(vehicleApply);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 车辆使用申请
	 * @param vehicleApply 车辆使用申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertVehicleApply")
	public RetDataBean insertVehicleInfo(VehicleApply vehicleApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			vehicleApply.setApplyId(SysTools.getGUID());
			vehicleApply.setCreateUser(userInfo.getAccountId());
			vehicleApply.setStatus("0");
			vehicleApply.setReturnFlag("0");
			vehicleApply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			vehicleApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, vehicleApplyService.insertVehicleApply(vehicleApply));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除车辆使用申请
	 * @param vehicleApply 车辆使用申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVehicleApply")
	public RetDataBean deleteVehicleApply(VehicleApply vehicleApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			vehicleApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleApplyService.deleteVehicleApply(vehicleApply));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新车辆使用申请
	 * @param vehicleApply 车辆使用申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateVehicleApply")
	public RetDataBean updateVehicleApply(VehicleApply vehicleApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(VehicleApply.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("applyId", vehicleApply.getApplyId());
			vehicleApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleApplyService.updateVehicleApply(example, vehicleApply));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加维修记录
	 *
	 * @param vehicleRepairRecord 维修记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertVehicleRepairRecord")
	public RetDataBean insertVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			vehicleRepairRecord.setRecordId(SysTools.getGUID());
			vehicleRepairRecord.setCreateUser(userInfo.getAccountId());
			vehicleRepairRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			vehicleRepairRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, vehicleRepairRecordService.insertVehicleRepairRecord(vehicleRepairRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除维修记录
	 *
	 * @param vehicleRepairRecord 维修记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVehicleRepairRecord")
	public RetDataBean deleteVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleRepairRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			vehicleRepairRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleRepairRecordService.deleteVehicleRepairRecord(vehicleRepairRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除车辆保养记录
	 *
	 * @param ids 车辆保养记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVehicleRepairRecordByIds")
	public RetDataBean deleteVehicleRepairRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return vehicleRepairRecordService.deleteVehicleRepairRecordByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新维修记录
	 *
	 * @param vehicleRepairRecord 维修记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateVehicleRepairRecord")
	public RetDataBean updateVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleRepairRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(VehicleRepairRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", vehicleRepairRecord.getRecordId());
			vehicleRepairRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleRepairRecordService.updateVehicleRepairRecord(example, vehicleRepairRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加车辆信息
	 *
	 * @param vehicleInfo 车辆信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertVehicleInfo")
	public RetDataBean insertVehicleInfo(VehicleInfo vehicleInfo) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vehicle:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			vehicleInfo.setVehicleId(SysTools.getGUID());
			vehicleInfo.setCreateUser(userInfo.getAccountId());
			vehicleInfo.setStatus("0");
			vehicleInfo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			vehicleInfo.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, vehicleInfoService.insertVehicleInfo(vehicleInfo));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除车辆信息
	 *
	 * @param vehicleInfo 车辆信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVehicleInfo")
	public RetDataBean deleteVehicleInfo(VehicleInfo vehicleInfo) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vehicle:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleInfo.getVehicleId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			vehicleInfo.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleInfoService.deleteVehicleInfo(vehicleInfo));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除车辆信息
	 *
	 * @param ids 车辆记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVehicleInfoByIds")
	public RetDataBean deleteVehicleInfoByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vehicle:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return vehicleInfoService.deleteVehicleInfoByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新车辆信息
	 *
	 * @param vehicleInfo 车辆信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateVehicleInfo")
	public RetDataBean updateVehicleInfo(VehicleInfo vehicleInfo) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vehicle:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleInfo.getVehicleId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(VehicleInfo.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("vehicleId", vehicleInfo.getVehicleId());
			vehicleInfo.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleInfoService.updateVehicleInfo(example, vehicleInfo));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加司机
	 *
	 * @param vehicleDriver 司机对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertVehicleDriver")
	public RetDataBean insertVehicleDriver(VehicleDriver vehicleDriver) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			vehicleDriver.setDriverId(SysTools.getGUID());
			vehicleDriver.setStatus("0");
			vehicleDriver.setCreateUser(userInfo.getAccountId());
			vehicleDriver.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			vehicleDriver.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, vehicleDriverService.insertVehicleDriver(vehicleDriver));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除司机记录
	 *
	 * @param vehicleDriver 司机对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVehicleDriver")
	public RetDataBean deleteVehicleDriver(VehicleDriver vehicleDriver) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleDriver.getDriverId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			vehicleDriver.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleDriverService.deleteVehicleDriver(vehicleDriver));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新司机记录
	 *
	 * @param vehicleDriver 司机对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateVehicleDriver")
	public RetDataBean updateVehicleDriver(VehicleDriver vehicleDriver) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleDriver.getDriverId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(VehicleDriver.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("driverId", vehicleDriver.getDriverId());
			vehicleDriver.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleDriverService.updateVehicleDriver(example, vehicleDriver));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除领导日程
	 *
	 * @param ids 领导日程Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVehicleOilCardByIds")
	public RetDataBean deleteVehicleOilCardByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return vehicleOilCardService.deleteVehicleOilCardByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加油卡信息
	 *
	 * @param vehicleOilCard 油卡信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertVehicleOilCard")
	public RetDataBean insertVehicleOilCard(VehicleOilCard vehicleOilCard) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			vehicleOilCard.setCardId(SysTools.getGUID());
			vehicleOilCard.setCreateUser(userInfo.getAccountId());
			vehicleOilCard.setStatus("0");
			vehicleOilCard.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			vehicleOilCard.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, vehicleOilCardService.insertVehicleOilCard(vehicleOilCard));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除油卡信息
	 *
	 * @param vehicleOilCard 油卡信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVehicleOilCard")
	public RetDataBean deleteVehicleOilCard(VehicleOilCard vehicleOilCard) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleOilCard.getCardId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			vehicleOilCard.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleOilCardService.deleteVehicleOilCard(vehicleOilCard));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新油卡信息
	 *
	 * @param vehicleOilCard 油卡信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateVehicleOilCard")
	public RetDataBean updateVehicleOilCard(VehicleOilCard vehicleOilCard) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(vehicleOilCard.getCardId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(VehicleOilCard.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("cardId", vehicleOilCard.getCardId());
			vehicleOilCard.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleOilCardService.updateVehicleOilCard(example, vehicleOilCard));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 资产调拨
	 *
	 * @param fixedAssetsAllocation 资产调拨对象
	 * @param applyId 申请记录Id
	 * @return 消息结构
	 */
	@PostMapping(value = "/addFixedAssetsAllocation")
	public RetDataBean addFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation, String applyId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			fixedAssetsAllocation.setRecordId(SysTools.getGUID());
			fixedAssetsAllocation.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			fixedAssetsAllocation.setCreateUser(userInfo.getAccountId());
			fixedAssetsAllocation.setOrgId(userInfo.getOrgId());
			return fixedAssetsAllocationService.addFixedAssetsAllocation(fixedAssetsAllocation, applyId);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 发起维修
	 *
	 * @param fixedAssetsRepair 维修记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertFixedAssetsRepair")
	public RetDataBean insertFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			fixedAssetsRepair.setRepairId(SysTools.getGUID());
			fixedAssetsRepair.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			fixedAssetsRepair.setCreateUser(userInfo.getAccountId());
			fixedAssetsRepair.setStatus("0");
			fixedAssetsRepair.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsRepairService.addFixedAssetsRepair(fixedAssetsRepair));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除固定资产
	 *
	 * @param ids 固定资产Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteFixedAssetsRepairByIds")
	public RetDataBean deleteFixedAssetsRepairByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return fixedAssetsRepairService.deleteFixedAssetsRepairByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除维修
	 *
	 * @param fixedAssetsRepair 固定资产维修记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteFixedAssetsRepair")
	public RetDataBean deleteFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(fixedAssetsRepair.getRepairId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			fixedAssetsRepair.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsRepairService.deleteFixedAssetsRepair(fixedAssetsRepair));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新维修记录
	 * @param fixedAssetsRepair 固定资产维修记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateFixedAssetsRepair")
	public RetDataBean updateFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
		try {
			if (StringUtils.isBlank(fixedAssetsRepair.getRepairId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(FixedAssetsRepair.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("repairId", fixedAssetsRepair.getRepairId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsRepairService.updateFixedAssetsRepair(example, fixedAssetsRepair));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新申请
	 *
	 * @param fixedAssetsApply 固定资产申请记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateFixedAssetsApply")
	public RetDataBean updateFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
		try {
			if (StringUtils.isBlank(fixedAssetsApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(FixedAssetsApply.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("applyId", fixedAssetsApply.getApplyId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetApplyService.updateFixedAssetsApply(example, fixedAssetsApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 册除申请
	 *
	 * @param fixedAssetsApply 固定资产申请记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteFixedAssetsApply")
	public RetDataBean deleteFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(fixedAssetsApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			fixedAssetsApply.setCreateUser(userInfo.getAccountId());
			fixedAssetsApply.setStatus("0");
			fixedAssetsApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetApplyService.deleteFixedAssetsApply(fixedAssetsApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 发起申请
	 *
	 * @param fixedAssetsApply 固定资产申请记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertFixedAssetsApply")
	public RetDataBean insertFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			fixedAssetsApply.setApplyId(SysTools.getGUID());
			fixedAssetsApply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			fixedAssetsApply.setCreateUser(userInfo.getAccountId());
			fixedAssetsApply.setStatus("0");
			fixedAssetsApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetApplyService.insertFixedAssetsApply(fixedAssetsApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 固定资产申请审批
	 *
	 * @param fixedAssetsApply 固定资产申请记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/approvalFixedAssetsApply")
	public RetDataBean approvalFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
		try {
			if (StringUtils.isBlank(fixedAssetsApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			fixedAssetsApply.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			fixedAssetsApply.setApprovalUser(userInfo.getAccountId());
			Example example = new Example(FixedAssetsApply.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("applyId", fixedAssetsApply.getApplyId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetApplyService.updateFixedAssetsApply(example, fixedAssetsApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加固定资产记录
	 *
	 * @param fixedAssets 固定资产记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertFixedAssets")
	public RetDataBean insertFixedAssets(FixedAssets fixedAssets) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			fixedAssets.setAssetsId(SysTools.getGUID());
			fixedAssets.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			fixedAssets.setCreateUser(userInfo.getAccountId());
			fixedAssets.setStatus("0");
			fixedAssets.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsService.insertFixedAssets(fixedAssets));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除固定资产记录
	 *
	 * @param fixedAssets 固定资产记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteFixedAssets")
	public RetDataBean deleteFixedAssets(FixedAssets fixedAssets) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(fixedAssets.getAssetsId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			fixedAssets.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsService.deleteFixedAssets(fixedAssets));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 批量删除固定资产
	 *
	 * @param ids 固定资产Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteFixedAssetsByIds")
	public RetDataBean deleteFixedAssetsByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return fixedAssetsService.deleteFixedAssetsByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 收回固定资产
	 * @param fixedAssets 固定资产对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setRecoverFixedAssetsById")
	public RetDataBean setRecoverFixedAssetsById(FixedAssets fixedAssets) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(fixedAssets.getAssetsId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			fixedAssets.setStatus("0");
			fixedAssets.setOwnDept("");
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(FixedAssets.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("assetsId", fixedAssets.getAssetsId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsService.updateFixedAssets(example, fixedAssets));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新固定资产记录
	 *
	 * @param fixedAssets 固定资产对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateFixedAssets")
	public RetDataBean updateFixedAssets(FixedAssets fixedAssets) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(fixedAssets.getAssetsId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(FixedAssets.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("assetsId", fixedAssets.getAssetsId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsService.updateFixedAssets(example, fixedAssets));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建固定资产分类
	 *
	 * @param fixedAssetsSort 固定资产分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertFixedAssetsSort")
	public RetDataBean insertFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			fixedAssetsSort.setSortId(SysTools.getGUID());
			fixedAssetsSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			if (StringUtils.isBlank(fixedAssetsSort.getParentId())) {
				fixedAssetsSort.setParentId("0");
			}
			fixedAssetsSort.setCreateUser(userInfo.getAccountId());
			fixedAssetsSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsSortService.insertFixedAssetsSort(fixedAssetsSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除固定资产分类
	 *
	 * @param fixedAssetsSort 固定资产分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteFixedAssetsSort")
	public RetDataBean deleteFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(fixedAssetsSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			fixedAssetsSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsSortService.deleteFixedAssetsSort(fixedAssetsSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新固定资产
	 *
	 * @param fixedAssetsSort 固定资产分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateFixedAssetsSort")
	public RetDataBean updateFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
		try {
			if (StringUtils.isBlank(fixedAssetsSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (fixedAssetsSort.getSortId().equals(fixedAssetsSort.getParentId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
			}
			if (StringUtils.isBlank(fixedAssetsSort.getParentId())) {
				fixedAssetsSort.setParentId("0");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(FixedAssetsSort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", fixedAssetsSort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsSortService.updateFixedAssetsSort(example, fixedAssetsSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建仓库
	 *
	 * @param fixedAssetsStorage 固定资产仓库对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertFixedAssetsStorage")
	public RetDataBean insertFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			fixedAssetsStorage.setStorageId(SysTools.getGUID());
			fixedAssetsStorage.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			fixedAssetsStorage.setCreateUser(userInfo.getAccountId());
			fixedAssetsStorage.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fixedAssetsStorageService.insertFixedAssetsStorage(fixedAssetsStorage));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除仓库
	 *
	 * @param fixedAssetsStorage 固定资产仓库对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteFixedAssetsStorage")
	public RetDataBean deleteFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(fixedAssetsStorage.getStorageId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			fixedAssetsStorage.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, fixedAssetsStorageService.deleteFixedAssetsStorage(fixedAssetsStorage));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新仓库信息
	 *
	 * @param fixedAssetsStorage 固定资产仓库对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateFixedAssetsStorage")
	public RetDataBean updateFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
		try {
			if (StringUtils.isBlank(fixedAssetsStorage.getStorageId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(FixedAssetsStorage.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("storageId", fixedAssetsStorage.getStorageId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fixedAssetsStorageService.updateFixedAssetsStorage(example, fixedAssetsStorage));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加会议记要
	 * @param meetingNotes 会议记要对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertMeetingNotes")
	public RetDataBean insertMeetingNotes(MeetingNotes meetingNotes) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			meetingNotes.setNotesId(SysTools.getGUID());
			meetingNotes.setCreateUser(userInfo.getAccountId());
			meetingNotes.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			meetingNotes.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, meetingNotesService.insertMeetingNotes(meetingNotes));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除会议记要
	 *
	 * @param meetingNotes 会议记要对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteMeetingNotes")
	public RetDataBean deleteMeetingNotes(MeetingNotes meetingNotes) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meetingNotes.getNotesId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			meetingNotes.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, meetingNotesService.deleteMeetingNotes(meetingNotes));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除会议纪要
	 *
	 * @param ids 会议记要Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteMeetingNotesByIds")
	public RetDataBean deleteMeetingNotesByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return meetingNotesService.deleteMeetingNotesByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新会议记要
	 *
	 * @param meetingNotes 会议记要对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateMeetingNotes")
	public RetDataBean updateMeetingNotes(MeetingNotes meetingNotes) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meetingNotes.getNotesId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(MeetingNotes.class);
			if (userInfo.getOpFlag().equals("1")) {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("notesId", meetingNotes.getNotesId());
			} else {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("notesId", meetingNotes.getNotesId()).andEqualTo("createUser", meetingNotes.getCreateUser());
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, meetingNotesService.updateMeetingNotes(example, meetingNotes));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 会议取消通知
	 * @param meeting 会义记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/cancelMeeting")
	public RetDataBean cancelMeeting(Meeting meeting) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			meeting.setOrgId(userInfo.getOrgId());
			meeting = meetingService.selectOneMeeting(meeting);
			meeting.setStatus("3");
			Example example = new Example(Meeting.class);
			example.createCriteria().andEqualTo("orgId", meeting.getOrgId()).andEqualTo("meetingId", meeting.getMeetingId());
			meetingService.sendCancelMeetingMsg(userInfo, meeting);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, meetingService.updateMeeting(example, meeting));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 发送会议通知
	 * @param meeting 会义记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/sendMeetingMsg")
	public RetDataBean sendMeetingMsg(Meeting meeting) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			meeting.setOrgId(userInfo.getOrgId());
			meeting = meetingService.selectOneMeeting(meeting);
			meetingService.sendMeetingMsg(userInfo, meeting);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 申请会议
	 * @param meeting 会义记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertMeeting")
	public RetDataBean insertMeeting(Meeting meeting) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			meeting.setMeetingId(SysTools.getGUID());
			meeting.setCreateUser(userInfo.getAccountId());
			if (StringUtils.isNotBlank(meeting.getBeginTime())) {
				meeting.setMeetingDay(meeting.getBeginTime().split(" ")[0]);
				meeting.setMeetingBegin(SysTools.convertTimeToInt48(meeting.getBeginTime().split(" ")[1]));
			}
			if (StringUtils.isNotBlank(meeting.getEndTime())) {
				meeting.setMeetingEnd(SysTools.convertTimeToInt48(meeting.getEndTime().split(" ")[1]));
			}
			meeting.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			meeting.setOrgId(userInfo.getOrgId());
			meeting.setStatus("0");
			return meetingService.addMeeting(meeting);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 撤销会议
	 * @param meeting 会义记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteMeeting")
	public RetDataBean deleteMeeting(Meeting meeting) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meeting.getMeetingId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			meeting.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, meetingService.deleteMeeting(meeting));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新会议
	 *
	 * @param meeting 会义记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateMeeting")
	public RetDataBean updateMeeting(Meeting meeting) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meeting.getMeetingId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (StringUtils.isNotBlank(meeting.getBeginTime())) {
				meeting.setMeetingDay(meeting.getBeginTime().split(" ")[0]);
				meeting.setMeetingBegin(SysTools.convertTimeToInt48(meeting.getBeginTime().split(" ")[1]));
			}
			if (StringUtils.isNotBlank(meeting.getEndTime())) {
				meeting.setMeetingEnd(SysTools.convertTimeToInt48(meeting.getEndTime().split(" ")[1]));
			}
			Example example = new Example(Meeting.class);
			if (userInfo.getOpFlag().equals("1")) {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("meetingId", meeting.getMeetingId());
			} else {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("meetingId", meeting.getMeetingId()).andEqualTo("createUser", meeting.getCreateUser());
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, meetingService.updateMeeting(example, meeting));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加会议室设备
	 * @param meetingDevice 会设备对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertMeetingDevice")
	public RetDataBean insertMeetingDevice(MeetingDevice meetingDevice) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			meetingDevice.setDeviceId(SysTools.getGUID());
			meetingDevice.setStatus("0");
			meetingDevice.setCreateUser(userInfo.getAccountId());
			meetingDevice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			meetingDevice.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, meetingDeviceService.insertMeetingDevice(meetingDevice));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除会议室设备
	 *
	 * @param meetingDevice 会设备对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteMeetingDevice")
	public RetDataBean deleteMeetingDevice(MeetingDevice meetingDevice) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meetingDevice.getDeviceId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			meetingDevice.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, meetingDeviceService.deleteMeetingDevice(meetingDevice));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新设备信息
	 * @param meetingDevice 会设备对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateMeetingDevice")
	public RetDataBean updateMeetingDevice(MeetingDevice meetingDevice) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meetingDevice.getDeviceId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(MeetingDevice.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("deviceId", meetingDevice.getDeviceId());
			meetingDevice.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, meetingDeviceService.updateMeetingDevice(example, meetingDevice));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加会议室
	 * @param meetingRoom 会议室对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertMeetingRoom")
	public RetDataBean insertMeetingRoom(MeetingRoom meetingRoom) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			meetingRoom.setRoomId(SysTools.getGUID());
			meetingRoom.setStatus("0");
			meetingRoom.setCreateUser(userInfo.getAccountId());
			meetingRoom.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			meetingRoom.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, meetingRoomService.insertMeetingRoom(meetingRoom));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除会议室
	 *
	 * @param meetingRoom 会议室对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteMeetingRoom")
	public RetDataBean deleteMeetingRoom(MeetingRoom meetingRoom) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meetingRoom.getRoomId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			meetingRoom.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, meetingRoomService.deleteMeetingRoom(meetingRoom));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新会议室
	 * @param meetingRoom 会议室对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateMeetingRoom")
	public RetDataBean updateMeetingRoom(MeetingRoom meetingRoom) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meetingRoom.getRoomId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(MeetingRoom.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("roomId", meetingRoom.getRoomId());
			meetingRoom.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, meetingRoomService.updateMeetingRoom(example, meetingRoom));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加发放记录
	 *
	 * @param officeSuppliesGrant 物资发放对象
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
	@PostMapping(value = "/insertOfficeSuppliesGrant")
	public RetDataBean insertOfficeSuppliesGrant(OfficeSuppliesGrant officeSuppliesGrant) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			OfficeSuppliesApply officeSuppliesApply = new OfficeSuppliesApply();
			officeSuppliesApply.setOrgId(userInfo.getOrgId());
			officeSuppliesApply.setApplyId(officeSuppliesGrant.getApplyId());
			officeSuppliesApply = officeSuppliesApplyService.selectOneOfficeSuppliesApply(officeSuppliesApply);
			int quantity = officeSuppliesApply.getApprovalQuantity();
			int count = officeSuppliesGrantService.getGrantCount(userInfo.getOrgId(), officeSuppliesGrant.getApplyId()) + officeSuppliesGrant.getQuantity();
			if (count <= quantity) {
				if (count == quantity) {
					OfficeSuppliesApply officeSuppliesApply1 = new OfficeSuppliesApply();
					officeSuppliesApply1.setOrgId(officeSuppliesApply.getOrgId());
					officeSuppliesApply1.setStatus("3");
					officeSuppliesApply1.setApplyId(officeSuppliesApply.getApplyId());
					Example example = new Example(OfficeSuppliesApply.class);
					example.createCriteria().andEqualTo("orgId", officeSuppliesApply.getApplyId()).andEqualTo("applyId", officeSuppliesApply.getApplyId());
					officeSuppliesApplyService.updateOfficeSuppliesApply(example, officeSuppliesApply1);
				}

				OfficeSupplies officeSupplies = new OfficeSupplies();
				officeSupplies.setOrgId(userInfo.getOrgId());
				officeSupplies.setSuppliesId(officeSuppliesApply.getSuppliesId());
				officeSupplies = officeSuppliesService.selectOneOfficeSupplies(officeSupplies);
				officeSupplies.setQuantity(officeSupplies.getQuantity() - officeSuppliesGrant.getQuantity());
				Example example1 = new Example(OfficeSupplies.class);
				example1.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("suppliesId", officeSupplies.getSuppliesId());
				officeSuppliesService.updateOfficeSupplies(example1, officeSupplies);

				officeSuppliesGrant.setGrantId(SysTools.getGUID());
				officeSuppliesGrant.setSuppliesId(officeSuppliesApply.getSuppliesId());
				officeSuppliesGrant.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				officeSuppliesGrant.setCreateUser(userInfo.getAccountId());
				officeSuppliesGrant.setOrgId(userInfo.getOrgId());
				return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, officeSuppliesGrantService.insertOfficeSuppliesGrant(officeSuppliesGrant));
			} else {
				return RetDataTools.NotOk(MessageCode.MSG_00020);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 发起申请
	 *
	 * @param officeSuppliesApply 物资申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertOfficeSuppliesApply")
	public RetDataBean insertOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			officeSuppliesApply.setApplyId(SysTools.getGUID());
			officeSuppliesApply.setStatus("0");
			officeSuppliesApply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			officeSuppliesApply.setCreateUser(userInfo.getAccountId());
			officeSuppliesApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, officeSuppliesApplyService.insertOfficeSuppliesApply(officeSuppliesApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除申请
	 *
	 * @param officeSuppliesApply 申请记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteOfficeSuppliesApply")
	public RetDataBean deleteOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(officeSuppliesApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			officeSuppliesApply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, officeSuppliesApplyService.deleteOfficeSuppliesApply(officeSuppliesApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新申请
	 *
	 * @param officeSuppliesApply 物资申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateOfficeSuppliesApply")
	public RetDataBean updateOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
		try {
			if (StringUtils.isBlank(officeSuppliesApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(OfficeSuppliesApply.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("applyId", officeSuppliesApply.getApplyId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesApplyService.updateOfficeSuppliesApply(example, officeSuppliesApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 领用物资申请审批
	 *
	 * @param officeSuppliesApply 物资申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/approvalOfficeSuppliesApply")
	public RetDataBean approvalOfficeSuppliesApply(OfficeSuppliesApply officeSuppliesApply) {
		try {
			if (StringUtils.isBlank(officeSuppliesApply.getApplyId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			officeSuppliesApply.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			officeSuppliesApply.setApprovalUser(userInfo.getAccountId());
			Example example = new Example(OfficeSuppliesApply.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("applyId", officeSuppliesApply.getApplyId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesApplyService.updateOfficeSuppliesApply(example, officeSuppliesApply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加办公用品
	 *
	 * @param officeSupplies 办公用品对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertOfficeSupplies")
	public RetDataBean insertOfficeSupplies(OfficeSupplies officeSupplies) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("officesupplies:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			officeSupplies.setSuppliesId(SysTools.getGUID());
			officeSupplies.setStatus("0");
			officeSupplies.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			officeSupplies.setCreateUser(userInfo.getAccountId());
			officeSupplies.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, officeSuppliesService.insertOfficeSupplies(officeSupplies));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除办公用品
	 *
	 * @param officeSupplies 办公用品对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteOfficeSupplies")
	public RetDataBean deleteOfficeSupplies(OfficeSupplies officeSupplies) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("officesupplies:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(officeSupplies.getSuppliesId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			officeSupplies.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, officeSuppliesService.deleteOfficeSupplies(officeSupplies));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除物资记录
	 *
	 * @param ids 物资记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteOfficeSuppliesByIds")
	public RetDataBean deleteOfficeSuppliesByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("officesupplies:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return officeSuppliesService.deleteOfficeSuppliesByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新办公用品信息
	 *
	 * @param officeSupplies 公用品信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateOfficeSupplies")
	public RetDataBean updateOfficeSupplies(OfficeSupplies officeSupplies) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("officesupplies:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(OfficeSupplies.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("suppliesId", officeSupplies.getSuppliesId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesService.updateOfficeSupplies(example, officeSupplies));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加办公用分类
	 *
	 * @param officeSuppliesSort 办公用品分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertOfficeSuppliesSort")
	public RetDataBean insertOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			officeSuppliesSort.setSortId(SysTools.getGUID());
			officeSuppliesSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			officeSuppliesSort.setCreateUser(userInfo.getAccountId());
			officeSuppliesSort.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(officeSuppliesSort.getParentId())) {
				officeSuppliesSort.setParentId("0");
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, officeSuppliesSortService.insertOfficeSuppliesSort(officeSuppliesSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除办公用分类
	 *
	 * @param officeSuppliesSort 办公用品分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteOfficeSuppliesSort")
	public RetDataBean deleteOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(officeSuppliesSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			officeSuppliesSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, officeSuppliesSortService.deleteOfficeSuppliesSort(officeSuppliesSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新办公用分类
	 *
	 * @param officeSuppliesSort 办公用品分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateOfficeSuppliesSort")
	public RetDataBean updateOfficeSuppliesSort(OfficeSuppliesSort officeSuppliesSort) {
		try {
			if (StringUtils.isBlank(officeSuppliesSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (officeSuppliesSort.getSortId().equals(officeSuppliesSort.getParentId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(officeSuppliesSort.getParentId())) {
				officeSuppliesSort.setParentId("0");
			}
			Example example = new Example(OfficeSuppliesSort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", officeSuppliesSort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, officeSuppliesSortService.updateOfficeSuppliesSort(example, officeSuppliesSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 办公用品导入
	 * @param file 文件
	 * @return 消息结构
	 */
	@PostMapping(value = "/importOfficeSuppliesInfo")
	public RetDataBean importOfficeSuppliesInfo(MultipartFile file) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("officesupplies:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return officeSuppliesService.importOfficeSuppliesInfo(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加日志评论
	 *
	 * @param diaryComments 日志评论对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDiaryComments")
	public RetDataBean insertDiaryComments(DiaryComments diaryComments) {
		try {
			if (StringUtils.isBlank(diaryComments.getDiaryId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			diaryComments.setCommId(SysTools.getGUID());
			diaryComments.setCreateUser(userInfo.getAccountId());
			diaryComments.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			diaryComments.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, diaryCommentsService.insertDiaryComments(diaryComments));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除自己的评论
	 *
	 * @param diaryComments 工作日志评论对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDiaryComments")
	public RetDataBean deleteDiaryComments(DiaryComments diaryComments) {
		try {
			if (StringUtils.isBlank(diaryComments.getCommId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			diaryComments.setCreateUser(userInfo.getAccountId());
			diaryComments.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, diaryCommentsService.deleteDiaryComments(diaryComments));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置工作日志权限
	 *
	 * @param diaryConfig 日志权限对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setDiaryConfig")
	public RetDataBean setDiaryConfig(DiaryConfig diaryConfig) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			diaryConfig.setCreateUser(userInfo.getAccountId());
			diaryConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			diaryConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, diaryConfigService.setDiaryConfig(diaryConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建工作日志
	 *
	 * @param diary 工作日志对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/createDiary")
	public RetDataBean createDiary(Diary diary) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			diary.setDiaryId(SysTools.getGUID());
			if (StringUtils.isBlank(diary.getTitle())) {
				diary.setTitle(MessageCode.MESSAGE_TITLE_IS_EMPTY);
			}
			if (StringUtils.isBlank(diary.getDiaryDay())) {
				diary.setDiaryDay(SysTools.getTime("yyyy-MM-dd"));
			}
			if (diary.getDiaryType().equals("2")) {
				diary.setUserRole("");
				diary.setDeptRole("");
				diary.setLevelRole("");
			}
			diary.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			diary.setCreateUser(userInfo.getAccountId());
			Document htmlDoc = Jsoup.parse(diary.getContent());
			String subheading = htmlDoc.text();
			if (subheading.length() > 50) {
				subheading = subheading.substring(0, 50) + "...";
			}
			diary.setSubheading(subheading);
			diary.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, diaryService.createDiary(userInfo, diary));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除日志
	 * @param diary 工作日志对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDiary")
	public RetDataBean deleteDiary(Diary diary) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			if (StringUtils.isBlank(diary.getDiaryId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			diary.setCreateUser(userInfo.getAccountId());
			diary.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, diaryService.deleteDiary(diary));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 工作日志更新
	 *
	 * @param diary 工作日志对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDiary")
	public RetDataBean updateDiary(Diary diary) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(diary.getDiaryId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			diary.setCreateUser(userInfo.getAccountId());
			diary.setOrgId(userInfo.getOrgId());
			Document htmlDoc = Jsoup.parse(diary.getContent());
			String subheading = htmlDoc.text();
			if (subheading.length() > 50) {
				subheading = subheading.substring(0, 50) + "...";
			}
			diary.setSubheading(subheading);
			DiaryConfig diaryConfig = new DiaryConfig();
			diaryConfig.setOrgId(userInfo.getOrgId());
			diaryConfig = diaryConfigService.selectOneDiaryConfig(diaryConfig);
			Example example = new Example(Diary.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("diaryId", diary.getDiaryId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, diaryService.updateDiary(userInfo, example, diary, diaryConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建工作计划
	 *
	 * @param workPlan 工作计划对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertWorkPlan")
	public RetDataBean insertWorkPlan(WorkPlan workPlan) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			workPlan.setPlanId(SysTools.getGUID());
			workPlan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			workPlan.setCreateUser(userInfo.getAccountId());
			workPlan.setOrgId(userInfo.getOrgId());
			workPlan.setStatus("0");
			if (StringUtils.isNotBlank(workPlan.getContent())) {
				Document htmlDoc = Jsoup.parse(workPlan.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				workPlan.setSubheading(subheading);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, workPlanService.createWorkPlan(userInfo, workPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除工作计划
	 *
	 * @param workPlan 工作计划对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteWorkPlan")
	public RetDataBean deleteWorkPlan(WorkPlan workPlan) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			if (StringUtils.isBlank(workPlan.getPlanId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			workPlan.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, workPlanService.deleteWorkPlan(workPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除工作计划
	 *
	 * @param ids 工作计划Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteWorkPlanByIds")
	public RetDataBean deleteWorkPlanByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return workPlanService.deleteWorkPlanByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新工作计划
	 *
	 * @param workPlan 工作计划对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateWorkPlan")
	public RetDataBean updateWorkPlan(WorkPlan workPlan) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(workPlan.getPlanId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (StringUtils.isNotBlank(workPlan.getContent())) {
				Document htmlDoc = Jsoup.parse(workPlan.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				workPlan.setSubheading(subheading);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(WorkPlan.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("planId", workPlan.getPlanId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, workPlanService.updateWorkPlan(example, workPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加计划完成情况
	 *
	 * @param workPlanProcess 工作计划进度对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertWorkPlanProcess")
	public RetDataBean insertWorkPlanProcess(WorkPlanProcess workPlanProcess) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			workPlanProcess.setProcessId(SysTools.getGUID());
			workPlanProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			workPlanProcess.setCreateUser(userInfo.getAccountId());
			workPlanProcess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, workPlanProcessService.insertWorkPlanProcess(workPlanProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除工作计划处理结果
	 *
	 * @param workPlanProcess 工作计划对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteWorkPlanProcess")
	public RetDataBean deleteWorkPlanProcess(WorkPlanProcess workPlanProcess) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			if (StringUtils.isBlank(workPlanProcess.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			workPlanProcess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, workPlanProcessService.deleteWorkPlanProcess(workPlanProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新工作计划处理结果
	 *
	 * @param workPlanProcess 工作计划处理对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateWorkPlanProcess")
	public RetDataBean updateWorkPlanProcess(WorkPlanProcess workPlanProcess) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(workPlanProcess.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(WorkPlanProcess.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", workPlanProcess.getProcessId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, workPlanProcessService.updateWorkPlanProcess(example, workPlanProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加考勤记录
	 * @param attend 考勤记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertAttend")
	public RetDataBean insertAttend(Attend attend) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, attendService.addAttendRecord(userInfo,attend));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建招待记录
	 *
	 * @param entertain 招待记录
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertEntertain")
	public RetDataBean insertEntertain(Entertain entertain) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			entertain.setRecordId(SysTools.getGUID());
			entertain.setStatus("0");
			entertain.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			entertain.setCreateUser(userInfo.getAccountId());
			entertain.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, entertainService.insertEntertain(entertain));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除招待记录
	 *
	 * @param entertain 招待记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteEntertain")
	public RetDataBean deleteEntertain(Entertain entertain) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(entertain.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			entertain.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, entertainService.deleteEntertain(entertain));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除招待记录
	 *
	 * @param ids 招待记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteEntertainByIds")
	public RetDataBean deleteEntertainByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return entertainService.deleteEntertainByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新招待记录
	 *
	 * @param entertain 招待记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateEntertain")
	public RetDataBean updateEntertain(Entertain entertain) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(Entertain.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", entertain.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, entertainService.updateEntertain(example, entertain));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


}
