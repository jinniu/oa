package com.core136.controller.info;

import com.core136.bean.account.UserInfo;
import com.core136.bean.info.*;
import com.core136.bean.office.DutySort;
import com.core136.bean.system.SysWebSite;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.info.*;
import com.core136.service.system.SysWebSiteService;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/info")
@CrossOrigin
@Api(value="信息SetController",tags={"信息更新数据接口"})
public class ApiSetInfoController {
	private NewsService newsService;
	private UserInfoService userInfoService;
	private NoticeTemplateService noticeTemplateService;
	private NoticeService noticeService;
	private NoticeConfigService noticeConfigService;
	private SysWebSiteService sysWebSiteService;
	private VoteService voteService;
	private LeadActivityService leadActivityService;
	private LeaderMailboxConfigService leaderMailboxConfigService;
	private LeaderMailboxService leaderMailboxService;
	private DataUploadInfoService dataUploadInfoService;
	private DataUploadHandleService dataUploadHandleService;
	private AddressBookService addressBookService;
	private VoteResultService voteResultService;
	private BigStoryService bigStoryService;
	private DiscussService discussService;
	private DiscussNoticeService discussNoticeService;
	private DiscussRecordService discussRecordService;
	private DynamicsRecordService dynamicsRecordService;
	private DynamicsCommentService dynamicsCommentService;
	private EmergencyService emergencyService;
	private BookMarkService bookMarkService;
	private OpenInfoService openInfoService;
	@Autowired
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setNoticeTemplateService(NoticeTemplateService noticeTemplateService) {
		this.noticeTemplateService = noticeTemplateService;
	}
	@Autowired
	public void setNoticeService(NoticeService noticeService) {
		this.noticeService = noticeService;
	}
	@Autowired
	public void setNoticeConfigService(NoticeConfigService noticeConfigService) {
		this.noticeConfigService = noticeConfigService;
	}
	@Autowired
	public void setSysWebSiteService(SysWebSiteService sysWebSiteService) {
		this.sysWebSiteService = sysWebSiteService;
	}
	@Autowired
	public void setVoteService(VoteService voteService) {
		this.voteService = voteService;
	}
	@Autowired
	public void setLeadActivityService(LeadActivityService leadActivityService) {
		this.leadActivityService = leadActivityService;
	}
	@Autowired
	public void setLeaderMailboxConfigService(LeaderMailboxConfigService leaderMailboxConfigService) {
		this.leaderMailboxConfigService = leaderMailboxConfigService;
	}
	@Autowired
	public void setLeaderMailboxService(LeaderMailboxService leaderMailboxService) {
		this.leaderMailboxService = leaderMailboxService;
	}
	@Autowired
	public void setDataUploadInfoService(DataUploadInfoService dataUploadInfoService) {
		this.dataUploadInfoService = dataUploadInfoService;
	}
	@Autowired
	public void setDataUploadHandleService(DataUploadHandleService dataUploadHandleService) {
		this.dataUploadHandleService = dataUploadHandleService;
	}
	@Autowired
	public void setAddressBookService(AddressBookService addressBookService) {
		this.addressBookService = addressBookService;
	}
	@Autowired
	public void setVoteResultService(VoteResultService voteResultService) {
		this.voteResultService = voteResultService;
	}
	@Autowired
	public void setBigStoryService(BigStoryService bigStoryService) {
		this.bigStoryService = bigStoryService;
	}
	@Autowired
	public void setDiscussService(DiscussService discussService) {
		this.discussService = discussService;
	}
	@Autowired
	public void setDiscussNoticeService(DiscussNoticeService discussNoticeService) {
		this.discussNoticeService = discussNoticeService;
	}
	@Autowired
	public void setDiscussRecordService(DiscussRecordService discussRecordService) {
		this.discussRecordService = discussRecordService;
	}
	@Autowired
	public void setDynamicsRecordService(DynamicsRecordService dynamicsRecordService) {
		this.dynamicsRecordService = dynamicsRecordService;
	}
	@Autowired
	public void setDynamicsCommentService(DynamicsCommentService dynamicsCommentService) {
		this.dynamicsCommentService = dynamicsCommentService;
	}
	@Autowired
	public void setEmergencyService(EmergencyService emergencyService) {
		this.emergencyService = emergencyService;
	}
	@Autowired
	public void setBookMarkService(BookMarkService bookMarkService) {
		this.bookMarkService = bookMarkService;
	}
	@Autowired
	public void setOpenInfoService(OpenInfoService openInfoService) {
		this.openInfoService = openInfoService;
	}

	/**
	 * 更新工作便签信息
	 * @param bookMark 工作便签对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setBookMark")
	public RetDataBean setBookMark(BookMark bookMark) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			bookMark.setCreateUser(userInfo.getAccountId());
			bookMark.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, bookMarkService.setBookMark(bookMark));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 发布紧急事件
	 * @param emergency 紧急事件对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertEmergency")
	public RetDataBean insertEmergency(Emergency emergency) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			emergency.setRecordId(SysTools.getGUID());
			emergency.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			emergency.setCreateUser(userInfo.getAccountId());
			emergency.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, emergencyService.insertEmergency(emergency));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除紧急事件
	 * @param emergency 紧急事件对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteEmergency")
	public RetDataBean deleteEmergency(Emergency emergency) {
		try {
			if (StringUtils.isBlank(emergency.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			emergency.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, emergencyService.deleteEmergency(emergency));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新紧急事件
	 * @param emergency 紧急事件对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateEmergency")
	public RetDataBean updateEmergency(Emergency emergency) {
		try {
			if (StringUtils.isBlank(emergency.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(Emergency.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", emergency.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, emergencyService.updateEmergency(example, emergency));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除紧急事件
	 * @param ids 紧急事件Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteEmergencyByIds")
	public RetDataBean deleteEmergencyByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return emergencyService.deleteEmergencyByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 发表评论
	 * @param dynamicsComment 讨论区评论对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDynamicsComment")
	public RetDataBean insertDynamicsComment(DynamicsComment dynamicsComment) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			dynamicsComment.setRecordId(SysTools.getGUID());
			dynamicsComment.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			dynamicsComment.setCreateUser(userInfo.getAccountId());
			dynamicsComment.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, dynamicsCommentService.insertDynamicsComment(dynamicsComment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除评论
	 * @param dynamicsComment 讨论区评论对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDynamicsComment")
	public RetDataBean deleteDynamicsComment(DynamicsComment dynamicsComment) {
		try {
			if (StringUtils.isBlank(dynamicsComment.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dynamicsComment.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, dynamicsCommentService.deleteDynamicsComment(dynamicsComment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新评论
	 * @param dynamicsComment 讨论区评论对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDynamicsComment")
	public RetDataBean updateDynamicsComment(DynamicsComment dynamicsComment) {
		try {
			if (StringUtils.isBlank(dynamicsComment.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(DutySort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", dynamicsComment.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, dynamicsCommentService.updateDynamicsComment(example, dynamicsComment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建动态信息
	 * @param dynamicsRecord 动态信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDynamicsRecord")
	public RetDataBean insertDynamicsRecord(DynamicsRecord dynamicsRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			dynamicsRecord.setRecordId(SysTools.getGUID());
			dynamicsRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			dynamicsRecord.setCreateUser(userInfo.getAccountId());
			dynamicsRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, dynamicsRecordService.insertDynamicsRecord(dynamicsRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除动态信息
	 * @param dynamicsRecord 动态信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDynamicsRecord")
	public RetDataBean deleteDynamicsRecord(DynamicsRecord dynamicsRecord) {
		try {
			if (StringUtils.isBlank(dynamicsRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dynamicsRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, dynamicsRecordService.deleteDynamicsRecord(dynamicsRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新动态信息
	 * @param dynamicsRecord 动态信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDynamicsRecord")
	public RetDataBean updateDynamicsRecord(DynamicsRecord dynamicsRecord) {
		try {
			if (StringUtils.isBlank(dynamicsRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(DynamicsRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", dynamicsRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, dynamicsRecordService.updateDynamicsRecord(example, dynamicsRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 回帖
	 * @param discussRecord 讨论区帖子对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/revDiscussRecord")
	public RetDataBean revDiscussRecord(DiscussRecord discussRecord) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			DiscussRecord tempDiscussRecord = new DiscussRecord();
			tempDiscussRecord.setRecordId(discussRecord.getRecordId());
			tempDiscussRecord.setOrgId(discussRecord.getOrgId());
			tempDiscussRecord = discussRecordService.selectOneDiscussRecord(tempDiscussRecord);
			discussRecord.setLevelId(tempDiscussRecord.getRecordId());
			discussRecord.setDiscussId(tempDiscussRecord.getDiscussId());
			discussRecord.setAccountId(userInfo.getAccountId());
			discussRecord.setStatus(tempDiscussRecord.getStatus());
			Document htmlDoc = Jsoup.parse(discussRecord.getContent());
			String subheading = htmlDoc.text();
			if (subheading.length() > 100) {
				subheading = subheading.substring(0, 100) + "...";
			}
			discussRecord.setSubheading(subheading);
			discussRecord.setRecordId(SysTools.getGUID());
			discussRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			discussRecord.setCreateUser(userInfo.getAccountId());
			discussRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, discussRecordService.insertDiscussRecord(discussRecord));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置新闻查看状态
	 * @param news 单位新闻对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setNewsReadStatus")
	public RetDataBean setNewsReadStatus(News news) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			news.setOrgId(userInfo.getOrgId());
			return newsService.setNewsReadStatus(userInfo,news.getNewsId());
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 发贴
	 * @param discussRecord 讨论区帖子对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDiscussRecord")
	public RetDataBean insertDiscussRecord(DiscussRecord discussRecord) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			discussRecord.setAccountId(userInfo.getAccountId());
			if (StringUtils.isNotBlank(discussRecord.getLevelId())) {
				discussRecord.setStatus("1");
				discussRecord.setLevelId("0");
			} else {
				discussRecord.setLevelId("0");
				Discuss discuss = new Discuss();
				discuss.setOrgId(userInfo.getOrgId());
				discuss.setRecordId(discussRecord.getDiscussId());
				discuss = discussService.selectOneDiscuss(discuss);
				if (discuss.getNeedApproval().equals("1")) {
					discussRecord.setStatus("0");
				} else {
					discussRecord.setStatus("1");
				}
			}
			Document htmlDoc = Jsoup.parse(discussRecord.getContent());
			String subheading = htmlDoc.text();
			if (subheading.length() > 100) {
				subheading = subheading.substring(0, 100) + "...";
			}
			discussRecord.setSubheading(subheading);
			discussRecord.setRecordId(SysTools.getGUID());
			discussRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			discussRecord.setCreateUser(userInfo.getAccountId());
			discussRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, discussRecordService.insertDiscussRecord(discussRecord));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删帖
	 * @param discussRecord 讨论区帖子对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDiscussRecord")
	public RetDataBean deleteDiscussRecord(DiscussRecord discussRecord) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			discussRecord.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(discussRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, discussRecordService.deleteDiscussRecord(discussRecord));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 更新帖子
	 * @param discussRecord 讨论区帖子对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDiscussRecord")
	public RetDataBean updateDiscussRecord(DiscussRecord discussRecord) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(discussRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				Document htmlDoc = Jsoup.parse(discussRecord.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 100) {
					subheading = subheading.substring(0, 100) + "...";
				}
				discussRecord.setSubheading(subheading);
				Example example = new Example(DiscussRecord.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", discussRecord.getRecordId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, discussRecordService.updateDiscussRecord(example, discussRecord));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 发布通知公告
	 * @param discussNotice 讨论区通知公告对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDiscussNotice")
	public RetDataBean insertDiscussNotice(DiscussNotice discussNotice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			discussNotice.setRecordId(SysTools.getGUID());
			discussNotice.setStatus("0");
			discussNotice.setReader("");
			discussNotice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			discussNotice.setCreateUser(userInfo.getAccountId());
			discussNotice.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, discussNoticeService.insertDiscussNotice(discussNotice));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  批量删除讨论区通知公告
	 * @param ids 讨论区通知公告Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDiscussNoticeIds")
	public RetDataBean deleteDiscussNoticeIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return discussNoticeService.deleteDiscussNoticeIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 删除通知公告
	 * @param discussNotice 讨论区通知对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDiscussNotice")
	public RetDataBean deleteDiscussNotice(DiscussNotice discussNotice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			discussNotice.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(discussNotice.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, discussNoticeService.deleteDiscussNotice(discussNotice));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新通知公告
	 * @param discussNotice 讨论区通知对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDiscussNotice")
	public RetDataBean updateDiscussNotice(DiscussNotice discussNotice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(discussNotice.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(DiscussNotice.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", discussNotice.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, discussNoticeService.updateDiscussNotice(example, discussNotice));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建讨论区
	 * @param discuss 讨论区对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDiscuss")
	public RetDataBean insertDiscuss(Discuss discuss) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			discuss.setRecordId(SysTools.getGUID());
			discuss.setStatus("1");
			discuss.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			discuss.setCreateUser(userInfo.getAccountId());
			discuss.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, discussService.insertDiscuss(discuss));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 删除讨论区
	 * @param discuss 讨论区对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDiscuss")
	public RetDataBean deleteDiscuss(Discuss discuss) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			discuss.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(discuss.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, discussService.deleteDiscuss(discuss));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新讨论区
	 * @param discuss 讨论区对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDiscuss")
	public RetDataBean updateDiscuss(Discuss discuss) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(discuss.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				Example example = new Example(Discuss.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", discuss.getRecordId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, discussService.updateDiscuss(example, discuss));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除大记事
	 * @param ids 大记事Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteBigStoryByIds")
	public RetDataBean deleteBigStoryByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("bigstory:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return bigStoryService.deleteBigStoryByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 导入大记事
	 * @param file 文件
	 * @return 消息结构
	 */
	@PostMapping(value = "/importBigStory")
	public RetDataBean importBigStory(MultipartFile file) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("bigstory:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, bigStoryService.importBigStory(userInfo, file));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建大纪事
	 * @param bigStory 大纪事对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertBigStory")
	public RetDataBean insertBigStory(BigStory bigStory) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("bigstory:INSERT")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			bigStory.setRecordId(SysTools.getGUID());
			Document htmlDoc = Jsoup.parse(bigStory.getContent());
			String subheading = htmlDoc.text();
			if (subheading.length() > 50) {
				subheading = subheading.substring(0, 50) + "...";
			}
			bigStory.setSubheading(subheading);
			if (StringUtils.isNotBlank(bigStory.getHappenTime())) {
				bigStory.setHappenYear(bigStory.getHappenTime().substring(0, 4));
			}
			bigStory.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			bigStory.setCreateUser(userInfo.getAccountId());
			bigStory.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, bigStoryService.insertBigStory(bigStory));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除大纪事
	 * @param bigStory 大纪事对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteBigStory")
	public RetDataBean deleteBigStory(BigStory bigStory) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("bigstory:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			bigStory.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(bigStory.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, bigStoryService.deleteBigStory(bigStory));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 大纪事更新
	 * @param bigStory 大纪事对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateBigStory")
	public RetDataBean updateBigStory(BigStory bigStory) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("bigstory:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(bigStory.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				Document htmlDoc = Jsoup.parse(bigStory.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				bigStory.setSubheading(subheading);
				if (StringUtils.isNotBlank(bigStory.getHappenTime())) {
					bigStory.setHappenYear(bigStory.getHappenTime().substring(0, 4));
				}
				Example example = new Example(BigStory.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", bigStory.getRecordId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, bigStoryService.updateBigStory(example, bigStory));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 人员投票
	 * @param voteId 投票记录Id
	 * @param result 投票结果
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertVoteResult")
	public RetDataBean insertVoteResult(String voteId, String result) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return voteResultService.addVoteResultRecord(userInfo, voteId, result);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加投票记录
	 * @param vote 投票对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertVote")
	public RetDataBean insertVote(Vote vote) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			vote.setVoteId(SysTools.getGUID());
			Document htmlDoc = Jsoup.parse(vote.getContent());
			String subheading = htmlDoc.text();
			if (subheading.length() > 50) {
				subheading = subheading.substring(0, 50) + "...";
			}
			vote.setSubheading(subheading);
			vote.setStatus("0");
			vote.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			vote.setCreateUser(userInfo.getAccountId());
			vote.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, voteService.insertVote(vote));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除投票记录
	 * @param vote 投票记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVote")
	public RetDataBean deleteVote(Vote vote) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			if (StringUtils.isBlank(vote.getVoteId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			vote.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, voteService.deleteVote(vote));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除投票记录
	 * @param ids 投票记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteVoteByIds")
	public RetDataBean deleteVoteByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return voteService.deleteVoteByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新投票记录
	 * @param vote 投票对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateVote")
	public RetDataBean updateVote(Vote vote) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(vote.getVoteId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isNotBlank(vote.getContent())) {
				Document htmlDoc = Jsoup.parse(vote.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				vote.setSubheading(subheading);
			}
			Example example = new Example(Vote.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("voteId", vote.getVoteId());
			if (voteService.updateVote(example, vote) > 0 && StringUtils.isNotBlank(vote.getStatus()) && vote.getStatus().equals("1")) {
				voteService.sendMsgForUser(vote, userInfo);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 通讯录导入
	 * @param file 文件
	 * @return 消息结构
	 */
	@PostMapping(value = "/importAddressBookForExcel")
	public RetDataBean importUnitDept(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return addressBookService.importAddressBookForExcel(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建通讯录人员
	 * @param addressBook 通讯录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertAddressBook")
	public RetDataBean insertAddressBook(AddressBook addressBook) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			addressBook.setRecordId(SysTools.getGUID());
			addressBook.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			String pinYin = StrTools.getPinYinHeadChar(addressBook.getUserName());
			addressBook.setPinYin(pinYin);
			addressBook.setCreateUser(userInfo.getAccountId());
			addressBook.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, addressBookService.insertAddressBook(addressBook));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除通讯录联系方式
	 * @param addressBook 通讯录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteAddressBook")
	public RetDataBean deleteAddressBook(AddressBook addressBook) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			addressBook.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(addressBook.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, addressBookService.deleteAddressBook(addressBook));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新通讯录
	 * @param addressBook 通讯录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateAddressBook")
	public RetDataBean updateAddressBook(AddressBook addressBook) {
		try {
			if (StringUtils.isBlank(addressBook.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				String pinYin = StrTools.getPinYinHeadChar(addressBook.getUserName());
				addressBook.setPinYin(pinYin);
				Example example = new Example(AddressBook.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", addressBook.getRecordId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, addressBookService.updateAddressBook(example, addressBook));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加事件处理结果
	 * @param dataUploadHandle 事件处理结果对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDataUploadHandle")
	public RetDataBean insertDataUploadHandle(DataUploadHandle dataUploadHandle) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dataUploadHandle.setProcessId(SysTools.getGUID());
			dataUploadHandle.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			dataUploadHandle.setCreateUser(userInfo.getAccountId());
			dataUploadHandle.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, dataUploadHandleService.processDataInfo(dataUploadHandle));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除事件处理结果
	 * @param dataUploadHandle 事件处理结果对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDataUploadHandle")
	public RetDataBean deleteDataUploadHandle(DataUploadHandle dataUploadHandle) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			if (StringUtils.isBlank(dataUploadHandle.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dataUploadHandle.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, dataUploadHandleService.deleteDataUploadHandle(dataUploadHandle));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新处理结果
	 * @param dataUploadHandle 事件处理结果对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDataUploadHandle")
	public RetDataBean updateDataUploadHandle(DataUploadHandle dataUploadHandle) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(dataUploadHandle.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(DataUploadHandle.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", dataUploadHandle.getProcessId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, dataUploadHandleService.updateDataUploadHandle(example, dataUploadHandle));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 上报信息
	 * @param dataUploadInfo 上报信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertDataUploadInfo")
	public RetDataBean insertDataUploadInfo(DataUploadInfo dataUploadInfo) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dataUploadInfo.setRecordId(SysTools.getGUID());
			dataUploadInfo.setStatus("0");
			dataUploadInfo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			if(StringUtils.isNotBlank(dataUploadInfo.getContent())) {
				Document htmlDoc = Jsoup.parse(dataUploadInfo.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				dataUploadInfo.setSubheading(subheading);
			}
			dataUploadInfo.setCreateUser(userInfo.getAccountId());
			dataUploadInfo.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, dataUploadInfoService.dataUploadInfo(userInfo, dataUploadInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除上报信息
	 * @param dataUploadInfo 上报信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteDataUploadInfo")
	public RetDataBean deleteDataUploadInfo(DataUploadInfo dataUploadInfo) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			if (StringUtils.isBlank(dataUploadInfo.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dataUploadInfo.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, dataUploadInfoService.deleteDataUploadInfo(dataUploadInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新上报信息
	 * @param dataUploadInfo 上报信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateDataUploadInfo")
	public RetDataBean updateDataUploadInfo(DataUploadInfo dataUploadInfo) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(dataUploadInfo.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isNotBlank(dataUploadInfo.getContent())) {
				Document htmlDoc = Jsoup.parse(dataUploadInfo.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				dataUploadInfo.setSubheading(subheading);
			}
			Example example = new Example(DataUploadInfo.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", dataUploadInfo.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, dataUploadInfoService.updateDataUploadInfo(userInfo, example, dataUploadInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 发送领导信息
	 * @param leaderMailbox 领导信箱对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setLeaderMailBox")
	public RetDataBean setLeaderMailBox(LeaderMailbox leaderMailbox) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadbox:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			leaderMailbox.setRecordId(SysTools.getGUID());
			leaderMailbox.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			leaderMailbox.setStatus("0");
			leaderMailbox.setCreateUser(userInfo.getAccountId());
			leaderMailbox.setOrgId(userInfo.getOrgId());
			return leaderMailboxService.setLeaderMailBox(userInfo, leaderMailbox);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除领导信箱信息
	 * @param leaderMailbox 领导信箱信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLeaderMailbox")
	public RetDataBean deleteLeaderMailbox(LeaderMailbox leaderMailbox) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadbox:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			leaderMailbox.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(leaderMailbox.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, leaderMailboxService.deleteLeaderMailbox(leaderMailbox));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新领导信箱信息
	 * @param leaderMailbox 领导信箱对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateLeaderMailbox")
	public RetDataBean updateLeaderMailbox(LeaderMailbox leaderMailbox) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadbox:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(leaderMailbox.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			leaderMailbox.setOrgId(userInfo.getOrgId());
			Example example = new Example(LeaderMailbox.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", leaderMailbox.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, leaderMailboxService.updateLeaderMailbox(example, leaderMailbox));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新已读状态
	 * @param leaderMailbox 领导信箱对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setReadLeaderMailbox")
	public RetDataBean setReadLeaderMailbox(LeaderMailbox leaderMailbox) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadbox:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(leaderMailbox.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			leaderMailbox.setOrgId(userInfo.getOrgId());
			leaderMailbox.setStatus("1");
			Example example = new Example(LeaderMailbox.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", leaderMailbox.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, leaderMailboxService.updateLeaderMailbox(example, leaderMailbox));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 更新领导信箱配置
	 * @param leaderMailboxConfig 领导信箱配置配置
	 * @return 消息结构
	 */
	@PostMapping(value = "/setLeaderMailboxConfig")
	public RetDataBean setLeaderMailboxConfig(LeaderMailboxConfig leaderMailboxConfig) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadbox:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(leaderMailboxConfig.getIsAnonymous())) {
				leaderMailboxConfig.setIsAnonymous("0");
			}
			leaderMailboxConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			leaderMailboxConfig.setCreateUser(userInfo.getAccountId());
			leaderMailboxConfig.setOrgId(userInfo.getOrgId());
			return leaderMailboxConfigService.setLeaderMailboxConfig(leaderMailboxConfig);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建领导行程
	 * @param leadActivity 领导行程对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertLeadActivity")
	public RetDataBean insertLeadActivity(LeadActivity leadActivity) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			leadActivity.setRecordId(SysTools.getGUID());
			leadActivity.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			leadActivity.setCreateUser(userInfo.getAccountId());
			leadActivity.setStatus("1");
			leadActivity.setOrgId(userInfo.getOrgId());
			return leadActivityService.sendLeadActivity(userInfo,leadActivity);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除领导行程
	 * @param leadActivity 领导行程对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLeadActivity")
	public RetDataBean deleteLeadActivity(LeadActivity leadActivity) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			if (StringUtils.isBlank(leadActivity.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			leadActivity.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, leadActivityService.deleteLeadActivity(leadActivity));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除领导日程
	 * @param ids 领导行程Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteLeadActivityByIds")
	public RetDataBean deleteLeadActivityByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return leadActivityService.deleteLeadActivityByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新领导行程
	 * @param leadActivity 领导行程对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateLeadActivity")
	public RetDataBean updateLeadActivity(LeadActivity leadActivity) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(leadActivity.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(LeadActivity.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", leadActivity.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, leadActivityService.updateLeadActivityAndSendMessage(userInfo,example, leadActivity));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
    /**
     * 更新新闻
     * @param news 单位新闻对象
     * @return 消息结构
     */
    @PostMapping(value = "/updateNews")
    public RetDataBean updateNews(News news) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
            if (StringUtils.isBlank(news.getNewsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = userInfoService.getRedisUser();
            news.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            if(StringUtils.isNotBlank(news.getContent())) {
                Document htmlDoc = Jsoup.parse(news.getContent());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                news.setSubheading(subheading);
            }
            if (StringUtils.isNotBlank(news.getIsTop())) {
                if (news.getIsTop().equals("1")) {
                    news.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                }
            }
            Example example = new Example(News.class);
            example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("newsId", news.getNewsId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, newsService.updateNews(news, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 发布新闻
     * @param news 单位新闻对象
     * @return 消息结构
     */
    @PostMapping(value = "/insertNews")
    public RetDataBean insertNews(News news) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
            UserInfo userInfo = userInfoService.getRedisUser();
            news.setNewsId(SysTools.getGUID());
            String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            news.setCreateUser(userInfo.getAccountId());
            news.setDelFlag("0");
            news.setStatus("1");
            news.setOnclickCount(0);
            Document htmlDoc = Jsoup.parse(news.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            news.setSubheading(subheading);
            news.setCreateTime(createTime);
            news.setOrgId(userInfo.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, newsService.sendNews(news, userInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除新闻
     * @param news 单位新闻对象
     * @return 消息结构
     */
    @PostMapping(value = "/deleteNews")
    public RetDataBean deleteNews(News news) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
            if (StringUtils.isBlank(news.getNewsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = userInfoService.getRedisUser();
            news.setOrgId(userInfo.getOrgId());
            if (!userInfo.getOpFlag().equals("1")) {
                news.setCreateUser(userInfo.getAccountId());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, newsService.deleteNews(news));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 批量删除新闻
	 * @param ids 新闻Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteNewsByIds")
	public RetDataBean deleteNewsByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return newsService.deleteNewsByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加通知公告红头模版
	 * @param noticeTemplate 通知公告红头模版对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertNoticeTemplate")
	public RetDataBean insertNoticeTemplate(NoticeTemplate noticeTemplate) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			noticeTemplate.setTemplateId(SysTools.getGUID());
			noticeTemplate.setOrgId(userInfo.getOrgId());
			noticeTemplate.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			noticeTemplate.setCreateUser(userInfo.getAccountId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, noticeTemplateService.insertNoticeTemplate(noticeTemplate));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除红头模版
	 * @param noticeTemplate 通知公告红头模版对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteNoticeTemplate")
	public RetDataBean deleteNoticeTemplate(NoticeTemplate noticeTemplate) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(noticeTemplate.getTemplateId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (!userInfo.getOpFlag().equals("1")) {
				noticeTemplate.setCreateUser(userInfo.getAccountId());
			}
			noticeTemplate.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, noticeTemplateService.deleteNoticeTemplate(noticeTemplate));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除通知公告模版
	 * @param ids 通知公告模版Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteNoticeTemplateByIds")
	public RetDataBean deleteNoticeTemplateByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return noticeTemplateService.deleteNoticeTemplateByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除通知公告
	 * @param ids 通知公告模版Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteNoticeByIds")
	public RetDataBean deleteNoticeByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return noticeService.deleteNoticeByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新模版信息
	 * @param noticeTemplate 通知公告模版对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateNoticeTemplate")
	public RetDataBean updateNoticeTemplate(NoticeTemplate noticeTemplate) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(noticeTemplate.getTemplateId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Example example = new Example(NoticeTemplate.class);
			if (!userInfo.getOpFlag().equals("1")) {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("templateId", noticeTemplate.getTemplateId()).andEqualTo("createUser", userInfo.getAccountId());
			} else {
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("templateId", noticeTemplate.getTemplateId());
			}
			noticeTemplate.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, noticeTemplateService.updateNoticeTemplate(noticeTemplate, example));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置通知公告配置信息
	 * @param noticeConfig 通知公告配置对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateNoticeConfig")
	public RetDataBean updateNoticeConfig(NoticeConfig noticeConfig) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(noticeConfig.getNoticeType())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if(noticeConfigService.isExistConfig(userInfo.getOrgId(),noticeConfig.getNoticeType()))
			{
				Example example = new Example(NoticeConfig.class);
				example.createCriteria().andEqualTo("orgId",userInfo.getOrgId()).andEqualTo("noticeType",noticeConfig.getNoticeType());
				return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, noticeConfigService.updateNoticeConfig(noticeConfig,example));
			}else {
				noticeConfig.setConfigId(SysTools.getGUID());
				noticeConfig.setOrgId(userInfo.getOrgId());
				noticeConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				noticeConfig.setCreateUser(userInfo.getAccountId());
				return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, noticeConfigService.insertNoticeConfig(noticeConfig));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 发布公告
	 * @param notice 通知公告对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/sendNotice")
	public RetDataBean sendNotice(Notice notice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			if (StringUtils.isBlank(notice.getNoticeType())) {
				return RetDataTools.NotOk(MessageCode.MSG_00019);
			}
			if (StringUtils.isNotBlank(notice.getContent())) {
				Document htmlDoc = Jsoup.parse(notice.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				notice.setSubheading(subheading);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			notice.setNoticeId(SysTools.getGUID());
			notice.setCreateUser(userInfo.getAccountId());
			notice.setDelFlag("0");
			notice.setOnclickCount("0");
			notice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			notice.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, noticeService.sendNotice(notice, userInfo));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新通知公告状态
	 * @param notice 通知公告对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setNoticeStatus")
	public RetDataBean setNoticeStatus(Notice notice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(notice.getNoticeId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(Notice.class);
			Example.Criteria criteria = example.createCriteria();
			criteria.andEqualTo("noticeId", notice.getNoticeId()).andEqualTo("orgId", userInfo.getOrgId());
			notice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, noticeService.setNoticeStatus(userInfo,notice, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新通知公告
	 * @param notice 通知公告对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateNotice")
	public RetDataBean updateNotice(Notice notice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(notice.getNoticeId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (StringUtils.isNotBlank(notice.getContent())) {
				Document htmlDoc = Jsoup.parse(notice.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				notice.setSubheading(subheading);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			notice.setOrgId(userInfo.getOrgId());
			Example example = new Example(Notice.class);
			Example.Criteria criteria = example.createCriteria();
			if (!userInfo.getOpFlag().equals("1")) {
				criteria.andEqualTo("createUser", userInfo.getAccountId());
			}
			if (StringUtils.isNotBlank(notice.getIsTop()) && notice.getIsTop().equals("1")) {
				notice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			}
			criteria.andEqualTo("noticeId", notice.getNoticeId()).andEqualTo("orgId", userInfo.getOrgId());
			notice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, noticeService.reEditNotice(notice, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 设置通知公告查看状态
	 * @param notice 通知公告对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setNoticeReadStatus")
	public RetDataBean setNoticeReadStatus(Notice notice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(notice.getNoticeId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			notice.setOrgId(userInfo.getOrgId());
			return noticeService.setNoticeReadStatus(userInfo, notice);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 删除通知公告
	 * @param notice 通知公告对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteNotice")
	public RetDataBean deleteNotice(Notice notice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(notice.getNoticeId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (!userInfo.getOpFlag().equals("1")) {
				notice.setCreateUser(userInfo.getAccountId());
			}
			notice.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, noticeService.deleteNotice(notice));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 审批通知公告
	 * @param notice 通知公告对象
	 * @return 消息结构
	 */

	@PostMapping(value = "/approvalNotice")
	public RetDataBean approvalNotice(Notice notice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(notice.getNoticeId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (StringUtils.isBlank(notice.getStatus())) {
				notice.setStatus("0");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			notice.setApprovalStaff(userInfo.getAccountId());
			notice.setOrgId(userInfo.getOrgId());
			Example example = new Example(Notice.class);
			example.createCriteria().andEqualTo("noticeId", notice.getNoticeId()).andEqualTo("orgId", notice.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, noticeService.updateNotice(notice, example));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 删除常用网址
	 * @param sysWebSite 常用网址对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSysWebSite")
	public RetDataBean deleteSysWebSite(SysWebSite sysWebSite) {
		try {
			if (StringUtils.isBlank(sysWebSite.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			sysWebSite.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, sysWebSiteService.deleteSysWebSite(sysWebSite));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除常用网址
	 * @param ids 常用网址Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSysWebSiteBatch")
	public RetDataBean deleteSysWebSiteBatch(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			List<String> list = new ArrayList<>(Arrays.asList(ids));
			return sysWebSiteService.deleteSysWebSiteBatch(userInfo,list);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加常用网址
	 * @param sysWebSite 常用网址对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertSysWebSite")
	public RetDataBean insertSysWebSite(SysWebSite sysWebSite) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			sysWebSite.setRecordId(SysTools.getGUID());
			sysWebSite.setAccountId(userInfo.getAccountId());
			sysWebSite.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			sysWebSite.setCreateUser(userInfo.getAccountId());
			sysWebSite.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysWebSiteService.insertSysWebSite(sysWebSite));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新常用网址
	 * @param sysWebSite 常用网址对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSysWebSite")
	public RetDataBean updateSysWebSite(SysWebSite sysWebSite) {
		try {
			if (StringUtils.isBlank(sysWebSite.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(SysWebSite.class);
			example.createCriteria().andEqualTo("recordId", sysWebSite.getRecordId()).andEqualTo("orgId", userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysWebSiteService.updateSysWebSite(example, sysWebSite));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 发布政务公开信息
	 * @param openInfo 政务公开信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertOpenInfo")
	public RetDataBean insertOpenInfo(OpenInfo openInfo) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			openInfo.setRecordId(SysTools.getGUID());
			if(StringUtils.isNotBlank(openInfo.getContent())) {
				Document htmlDoc = Jsoup.parse(openInfo.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				openInfo.setSubheading(subheading);
			}
			openInfo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			openInfo.setCreateUser(userInfo.getAccountId());
			openInfo.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, openInfoService.insertOpenInfo(openInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新政务公开信息
	 * @param openInfo 政务公开信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateOpenInfo")
	public RetDataBean updateOpenInfo(OpenInfo openInfo) {
		try {
			if (StringUtils.isBlank(openInfo.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if(StringUtils.isNotBlank(openInfo.getContent())) {
				Document htmlDoc = Jsoup.parse(openInfo.getContent());
				String subheading = htmlDoc.text();
				if (subheading.length() > 50) {
					subheading = subheading.substring(0, 50) + "...";
				}
				openInfo.setSubheading(subheading);
			}
			Example example = new Example(OpenInfo.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", openInfo.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, openInfoService.updateOpenInfo(example, openInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 删除政务公开信息
	 * @param openInfo 政务公开信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteOpenInfo")
	public RetDataBean deleteOpenInfo(OpenInfo openInfo) {
		try {
			if (StringUtils.isBlank(openInfo.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			openInfo.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, openInfoService.deleteOpenInfo(openInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除政务信息
	 * @param ids 政务信息Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteOpenInfoByIds")
	public RetDataBean deleteOpenInfoByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return openInfoService.deleteOpenInfoByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *设置查阅人员
	 * @param recordId 政务信息Id
	 * @return 消息结构
	 */
	@PostMapping(value = "/setOpenInfoReaderUser")
	public RetDataBean setOpenInfoReaderUser(String recordId) {
		try {
			if (StringUtils.isBlank(recordId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			openInfoService.setReaderUser(userInfo.getOrgId(),userInfo.getAccountId(),recordId);
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
}
