package com.core136.controller.finance;

import com.core136.bean.account.UserInfo;
import com.core136.bean.finance.BudgetConfig;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.finance.*;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/get/finance")
@CrossOrigin
@Api(value="财务GetController",tags={"财务获取数据接口"})
public class ApiGetFinanceController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
    private UserInfoService userInfoService;
    private BudgetConfigService budgetConfigService;
    private BudgetAccountService budgetAccountService;
    private BudgetProjectService budgetProjectService;
	private BudgetAdjustmentService budgetAdjustmentService;
	private BudgetCostApplyService budgetCostApplyService;
	private BudgetCostService budgetCostService;
	private ContractSortService contractSortService;
	private ContractService contractService;
	private ContractReceivablesService contractReceivablesService;
	private ContractReceivablesRecordService contractReceivablesRecordService;
	private ContractPayableService contractPayableService;
	private ContractPayableRecordService contractPayableRecordService;
	private ContractBillService contractBillService;
	private ContractDeliveryService contractDeliveryService;
	private ExpenseAccountService expenseAccountService;
	private ExpenseComConfigService expenseComConfigService;
	private ExpenseUserConfigService expenseUserConfigService;
	private ExpenseDeptConfigService expenseDeptConfigService;
	private ExpenseClaimService expenseClaimService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
		this.budgetConfigService = budgetConfigService;
	}
	@Autowired
	public void setBudgetAccountService(BudgetAccountService budgetAccountService) {
		this.budgetAccountService = budgetAccountService;
	}
	@Autowired
	public void setBudgetProjectService(BudgetProjectService budgetProjectService) {
		this.budgetProjectService = budgetProjectService;
	}
	@Autowired
	public void setBudgetAdjustmentService(BudgetAdjustmentService budgetAdjustmentService) {
		this.budgetAdjustmentService = budgetAdjustmentService;
	}
	@Autowired
	public void setBudgetCostApplyService(BudgetCostApplyService budgetCostApplyService) {
		this.budgetCostApplyService = budgetCostApplyService;
	}
	@Autowired
	public void setBudgetCostService(BudgetCostService budgetCostService) {
		this.budgetCostService = budgetCostService;
	}
	@Autowired
	public void setContractSortService(ContractSortService contractSortService) {
		this.contractSortService = contractSortService;
	}
	@Autowired
	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	}
	@Autowired
	public void setContractReceivablesService(ContractReceivablesService contractReceivablesService) {
		this.contractReceivablesService = contractReceivablesService;
	}
	@Autowired
	public void setContractReceivablesRecordService(ContractReceivablesRecordService contractReceivablesRecordService) {
		this.contractReceivablesRecordService = contractReceivablesRecordService;
	}
	@Autowired
	public void setContractPayableService(ContractPayableService contractPayableService) {
		this.contractPayableService = contractPayableService;
	}
	@Autowired
	public void setContractPayableRecordService(ContractPayableRecordService contractPayableRecordService) {
		this.contractPayableRecordService = contractPayableRecordService;
	}
	@Autowired
	public void setContractBillService(ContractBillService contractBillService) {
		this.contractBillService = contractBillService;
	}
	@Autowired
	public void setContractDeliveryService(ContractDeliveryService contractDeliveryService) {
		this.contractDeliveryService = contractDeliveryService;
	}
	@Autowired
	public void setExpenseAccountService(ExpenseAccountService expenseAccountService) {
		this.expenseAccountService = expenseAccountService;
	}
	@Autowired
	public void setExpenseComConfigService(ExpenseComConfigService expenseComConfigService) {
		this.expenseComConfigService = expenseComConfigService;
	}
	@Autowired
	public void setExpenseUserConfigService(ExpenseUserConfigService expenseUserConfigService) {
		this.expenseUserConfigService = expenseUserConfigService;
	}
	@Autowired
	public void setExpenseDeptConfigService(ExpenseDeptConfigService expenseDeptConfigService) {
		this.expenseDeptConfigService = expenseDeptConfigService;
	}
	@Autowired
	public void setExpenseClaimService(ExpenseClaimService expenseClaimService) {
		this.expenseClaimService = expenseClaimService;
	}

	/**
	 * 获取人员报销列表
	 * @param pageParam 分页参数
	 * @param deptId 部门Id
	 * @param accountId 用户账号
	 * @param expenseYear 查询年度
	 * @param expenseType 费用类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExpenseClaimList")
	public RetDataBean getExpenseClaimList(PageParam pageParam,String deptId,String accountId,String expenseYear,String expenseType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = expenseClaimService.getExpenseClaimList(pageParam,deptId,accountId,expenseYear,expenseType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人员年度预算列表
	 * @param pageParam 分页参数
	 * @param deptId 部门Id
	 * @param expenseYear 年度
	 * @return 预算列表
	 */
	@GetMapping(value = "/getExpenseDeptConfigList")
	public RetDataBean getExpenseDeptConfigList(PageParam pageParam,String deptId,String expenseYear) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = expenseDeptConfigService.getExpenseDeptConfigList(pageParam,deptId,expenseYear);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人员年度预算列表
	 * @param pageParam 分页参数
	 * @param deptId 部门Id
	 * @param accountId 人员账号
	 * @param expenseYear 年度
	 * @return 预算列表
	 */
	@GetMapping(value = "/getExpenseUserConfigList")
	public RetDataBean getExpenseUserConfigList(PageParam pageParam,String deptId,String accountId,String expenseYear) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = expenseUserConfigService.getExpenseUserConfigList(pageParam,deptId,accountId,expenseYear);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取公司年度费用预算列表
	 * @param pageParam 分页参数
	 * @param expenseYear 年度
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExpenseComConfigList")
	public RetDataBean getExpenseComConfigList(PageParam pageParam,String expenseYear) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = expenseComConfigService.getExpenseComConfigList(pageParam,expenseYear);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取科目树型结构
	 * @return 科目列表
	 */
	@GetMapping(value = "/getExpenseAccountTree")
	public RetDataBean getExpenseAccountTree(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, expenseAccountService.getExpenseAccountTree(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * 获取近期的合同列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractTop")
	public RetDataBean getContractTop() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractService.getContractTop(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取近期发票列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractBillTop")
	public RetDataBean getContractBillTop() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractBillService.getContractBillTop(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 近期付款记录
	 * @return 消息结构
	 */
	@GetMapping(value = "/getPayableRecordTop")
	public RetDataBean getPayableRecordTop() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractPayableRecordService.getPayableRecordTop(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 收款记录
	 * @return 消息结构
	 */
	@GetMapping(value = "/getReceivablesRecordTop")
	public RetDataBean getReceivablesRecordTop() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractReceivablesRecordService.getReceivablesRecordTop(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取发货列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类期
	 * @param contractType 合同类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractDeliveryList")
	public RetDataBean getContractDeliveryList(PageParam pageParam,String date,String contractType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("g.send_date");
			} else {
				pageParam.setProp("g."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = contractDeliveryService.getContractDeliveryList(pageParam, contractType,date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取票据列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param isOpen 是否开票
	 * @param billType 发票类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractBillList")
	public RetDataBean getContractBillList(PageParam pageParam,String date,String isOpen,String billType,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.create_time");
			} else {
				pageParam.setProp("b."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = contractBillService.getContractBillList(pageParam, isOpen, status, billType,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取付款记录
	 * @param pageParam 分页参数
	 * @param payableId 支付记录Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractPayableRecordList")
	public RetDataBean getContractPayableRecordList(PageParam pageParam,String payableId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.payable_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = contractPayableRecordService.getContractPayableRecordList(pageParam, payableId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取历史付款记录
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param payReceivedType 付款种类
	 * @return 付款记录列表
	 */
	@GetMapping(value = "/getAllContractPayableRecordList")
	public RetDataBean getAllContractPayableRecordList(PageParam pageParam,String date,String payReceivedType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.payable_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = contractPayableRecordService.getAllContractPayableRecordList(pageParam,payReceivedType,date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取应付款列表
	 * @param pageParam 分页参数
	 * @param accountId 用户账号
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractPayableList")
	public RetDataBean getContractPayableList(PageParam pageParam,String accountId,String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.payable_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(accountId);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = contractPayableService.getContractPayableList(pageParam,date,timeArr[0] , timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取收款记录
	 * @param pageParam 分页参数
	 * @param receivablesId 收款记录Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractReceivablesRecordList")
	public RetDataBean getContractReceivablesRecordList(PageParam pageParam,String receivablesId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.payee_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = contractReceivablesRecordService.getContractReceivablesRecordList(pageParam, receivablesId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 查询历史收款记录列表
	 * @param pageParam 分页参数
	 * @param payReceivedType 付款种类
	 * @param date 日期范围
	 * @return 收款记录列表
	 */
	@GetMapping(value = "/getAllContractReceivablesRecordList")
	public RetDataBean getAllContractReceivablesRecordList(PageParam pageParam,String date,String payReceivedType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.payee_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = contractReceivablesRecordService.getAllContractReceivablesRecordList(pageParam, payReceivedType,date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取应收款列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param status 状态
	 * @param accountId 用户账号
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractReceivablesList")
	public RetDataBean getContractReceivablesList(PageParam pageParam,String date,String status,String accountId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.receivables_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(accountId);
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, Object>> pageInfo = contractReceivablesService.getContractReceivablesList(pageParam,date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取试卷列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractForSelect")
	public RetDataBean getContractForSelect(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("create_time");
			} else {
				pageParam.setProp( StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = contractService.getContractForSelect(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取合同详情
	 * @param contractId 合同Id
	 * @return 合同详情
	 */
	@GetMapping(value = "/getContractById")
	public RetDataBean getContractById(String contractId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractService.getContractById(userInfo.getOrgId(),contractId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 合同查询列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param sortId 分类Id
	 * @param contractType 合同类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/queryContract")
	public RetDataBean queryContract(PageParam pageParam,String date,String sortId,String contractType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.sign_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = contractService.queryContract(pageParam, sortId,date,timeArr[0],timeArr[1], contractType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取合同管理列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param sortId 合同类型
	 * @param contractType 合同类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractManageList")
	public RetDataBean getContractManageList(PageParam pageParam,String date,String sortId,String contractType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.sign_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = contractService.getContractManageList(pageParam,sortId,date,timeArr[0] , timeArr[1], contractType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取合同分类树结构
	 * @return 消息结构
	 */
	@GetMapping(value = "/getContractSortTree")
	public RetDataBean getContractSortTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractSortService.getContractSortTree(userInfo.getOrgId()));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 获取项目预算费用支出列表
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBudgetCostList")
	public RetDataBean getBudgetCostList(PageParam pageParam, String projectId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = budgetCostService.getBudgetCostList(pageParam, projectId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取子项目费用支出列表
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getChildBudgetCostList")
	public RetDataBean getChildBudgetCostList(PageParam pageParam, String projectId, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.happen_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = budgetCostService.getChildBudgetCostList(pageParam, projectId,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取预算费用审批记录
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBudgetCostApprovalList")
	public RetDataBean getBudgetCostApprovalList(PageParam pageParam, String projectId, String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = budgetCostApplyService.getBudgetCostApprovalList(pageParam, projectId,date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取费用预算申请审批人
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCostApprovalUser")
	public RetDataBean getCostApprovalUser() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetConfigService.getCostApprovalUser(userInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取预算申请记录列表
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBudgetCostApplyList")
	public RetDataBean getBudgetCostApplyList(PageParam pageParam, String projectId ,String date, String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = budgetCostApplyService.getBudgetCostApplyList(pageParam, projectId,date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取预算调整审批人员列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAdjustmentApprovalUser")
	public RetDataBean getAdjustmentApprovalUser() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetConfigService.getAdjustmentApprovalUser(userInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有父级项目
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllParentBudgetProject")
	public RetDataBean getAllParentBudgetProject() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("budget:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetProjectService.getAllParentBudgetProject(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取等审批列表
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAdjustmentApprovalList")
	public RetDataBean getAdjustmentApprovalList(PageParam pageParam, String projectId, String date, String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = budgetAdjustmentService.getAdjustmentApprovalList(pageParam, status,projectId,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取申请列表
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAdjustmentApplyList")
	public RetDataBean getAdjustmentApplyList(PageParam pageParam, String projectId, String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			PageInfo<Map<String, String>> pageInfo = budgetAdjustmentService.getAdjustmentApplyList(pageParam, status, projectId, date,timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

    /**
     * 获取项目列表
     * @param pageParam 分页参数
     * @param date 日期范围类型
     * @param projectType 项目类型
     * @param budgetAccount 账套
     * @return 消息结构
     */
    @GetMapping(value = "/getBudgetProjectList")
    public RetDataBean getBudgetProjectList(PageParam pageParam, String date, String projectType, String budgetAccount) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("budget:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("p.begin_time");
			} else {
				pageParam.setProp("p."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = budgetProjectService.getBudgetProjectList(pageParam,projectType,budgetAccount,date,timeArr[0], timeArr[1]);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 获取预算项目列表
	 * @param keyword 查询关键词
	 * @return 项目列表
	 */
	@GetMapping(value = "/getAllBudgetProjectList")
	public RetDataBean getAllBudgetProjectList(String keyword) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("budget:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetProjectService.getAllBudgetProjectList(userInfo.getOrgId(), keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
    /**
     * 获取账套树结构
     * @param keyword 查询关键词
     * @return 消息结构
     */
    @GetMapping(value = "/getAllBudgetAccountList")
    public RetDataBean getAllBudgetAccountList(String keyword) {
        try {
            UserInfo userInfo = userInfoService.getRedisUser();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetAccountService.getAllBudgetAccountList(userInfo.getOrgId(), keyword));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 获取账套列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBudgetAccountForSelect")
	public RetDataBean getBudgetAccountForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetAccountService.getBudgetAccountForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

    /**
     * 获取费用预算配置详情
     * @param budgetConfig 费用预算配置对象
     * @return 消息结构
     */
    @GetMapping(value = "/getBudgetConfig")
    public RetDataBean getBudgetConfig(BudgetConfig budgetConfig) {
        try {
            UserInfo userInfo = userInfoService.getRedisUser();
            budgetConfig.setOrgId(userInfo.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetConfigService.selectOneBudgetConfig(budgetConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 获取报销费用类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExpenseAccountForSelect")
	public RetDataBean getExpenseAccountForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, expenseAccountService.getExpenseAccountForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

}
