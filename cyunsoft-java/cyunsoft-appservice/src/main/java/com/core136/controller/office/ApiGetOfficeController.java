package com.core136.controller.office;

import com.alibaba.fastjson.JSON;
import com.core136.bean.account.UserInfo;
import com.core136.bean.office.*;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.office.*;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/get/office")
@CrossOrigin
@Api(value="行政GetController",tags={"行政获取数据接口"})
public class ApiGetOfficeController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private UserInfoService userInfoService;
	private DiaryConfigService diaryConfigService;
	private DiaryService diaryService;
	private DiaryCommentsService diaryCommentsService;
	private OfficeSuppliesSortService officeSuppliesSortService;
	private OfficeSuppliesService officeSuppliesService;
	private OfficeSuppliesApplyService officeSuppliesApplyService;
	private OfficeSuppliesGrantService officeSuppliesGrantService;
	private MeetingRoomService meetingRoomService;
	private MeetingDeviceService meetingDeviceService;
	private MeetingNotesService meetingNotesService;
	private WorkPlanService workPlanService;
	private WorkPlanProcessService workPlanProcessService;
	private MeetingService meetingService;
	private FixedAssetsStorageService fixedAssetsStorageService;
	private FixedAssetsSortService fixedAssetsSortService;
	private FixedAssetsService fixedAssetsService;
	private FixedAssetsApplyService fixedAssetApplyService;
	private FixedAssetsRepairService fixedAssetsRepairService;
	private FixedAssetsAllocationService fixedAssetsAllocationService;
	private VehicleOilCardService vehicleOilCardService;
	private VehicleDriverService vehicleDriverService;
	private VehicleInfoService vehicleInfoService;
	private VehicleRepairRecordService vehicleRepairRecordService;
	private VehicleApplyService vehicleApplyService;
	private VehicleOperatorService vehicleOperatorService;
	private TaskProcessService taskProcessService;
	private TaskService taskService;
	private TaskGanttDataService taskGanttDataService;
	private AttendService attendService;
	private ExamSortService examSortService;
	private ExamQuestionsService examQuestionsService;
	private ExamTestSortService examTestSortService;
	private ExamQuestionRecordService examQuestionRecordService;
	private ExamTestService examTestService;
	private ExamGradeService examGradeService;
	private DutySortService dutySortService;
	private DutyRecordService dutyRecordService;
	private LicenceRecordService licenceRecordService;
	private LicenceBorrowingService licenceBorrowingService;
	private LicenceTypeService licenceTypeService;
	private EntertainService entertainService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setDiaryConfigService(DiaryConfigService diaryConfigService) {
		this.diaryConfigService = diaryConfigService;
	}
	@Autowired
	public void setDiaryService(DiaryService diaryService) {
		this.diaryService = diaryService;
	}
	@Autowired
	public void setDiaryCommentsService(DiaryCommentsService diaryCommentsService) {
		this.diaryCommentsService = diaryCommentsService;
	}
	@Autowired
	public void setOfficeSuppliesSortService(OfficeSuppliesSortService officeSuppliesSortService) {
		this.officeSuppliesSortService = officeSuppliesSortService;
	}
	@Autowired
	public void setOfficeSuppliesService(OfficeSuppliesService officeSuppliesService) {
		this.officeSuppliesService = officeSuppliesService;
	}
	@Autowired
	public void setOfficeSuppliesApplyService(OfficeSuppliesApplyService officeSuppliesApplyService) {
		this.officeSuppliesApplyService = officeSuppliesApplyService;
	}
	@Autowired
	public void setOfficeSuppliesGrantService(OfficeSuppliesGrantService officeSuppliesGrantService) {
		this.officeSuppliesGrantService = officeSuppliesGrantService;
	}
	@Autowired
	public void setMeetingRoomService(MeetingRoomService meetingRoomService) {
		this.meetingRoomService = meetingRoomService;
	}
	@Autowired
	public void setMeetingDeviceService(MeetingDeviceService meetingDeviceService) {
		this.meetingDeviceService = meetingDeviceService;
	}
	@Autowired
	public void setMeetingNotesService(MeetingNotesService meetingNotesService) {
		this.meetingNotesService = meetingNotesService;
	}
	@Autowired
	public void setWorkPlanService(WorkPlanService workPlanService) {
		this.workPlanService = workPlanService;
	}
	@Autowired
	public void setWorkPlanProcessService(WorkPlanProcessService workPlanProcessService) {
		this.workPlanProcessService = workPlanProcessService;
	}
	@Autowired
	public void setMeetingService(MeetingService meetingService) {
		this.meetingService = meetingService;
	}
	@Autowired
	public void setFixedAssetsStorageService(FixedAssetsStorageService fixedAssetsStorageService) {
		this.fixedAssetsStorageService = fixedAssetsStorageService;
	}
	@Autowired
	public void setFixedAssetsSortService(FixedAssetsSortService fixedAssetsSortService) {
		this.fixedAssetsSortService = fixedAssetsSortService;
	}
	@Autowired
	public void setFixedAssetsService(FixedAssetsService fixedAssetsService) {
		this.fixedAssetsService = fixedAssetsService;
	}
	@Autowired
	public void setFixedAssetApplyService(FixedAssetsApplyService fixedAssetApplyService) {
		this.fixedAssetApplyService = fixedAssetApplyService;
	}
	@Autowired
	public void setFixedAssetsRepairService(FixedAssetsRepairService fixedAssetsRepairService) {
		this.fixedAssetsRepairService = fixedAssetsRepairService;
	}
	@Autowired
	public void setFixedAssetsAllocationService(FixedAssetsAllocationService fixedAssetsAllocationService) {
		this.fixedAssetsAllocationService = fixedAssetsAllocationService;
	}
	@Autowired
	public void setVehicleOilCardService(VehicleOilCardService vehicleOilCardService) {
		this.vehicleOilCardService = vehicleOilCardService;
	}
	@Autowired
	public void setVehicleDriverService(VehicleDriverService vehicleDriverService) {
		this.vehicleDriverService = vehicleDriverService;
	}
	@Autowired
	public void setVehicleInfoService(VehicleInfoService vehicleInfoService) {
		this.vehicleInfoService = vehicleInfoService;
	}
	@Autowired
	public void setVehicleRepairRecordService(VehicleRepairRecordService vehicleRepairRecordService) {
		this.vehicleRepairRecordService = vehicleRepairRecordService;
	}
	@Autowired
	public void setVehicleApplyService(VehicleApplyService vehicleApplyService) {
		this.vehicleApplyService = vehicleApplyService;
	}
	@Autowired
	public void setVehicleOperatorService(VehicleOperatorService vehicleOperatorService) {
		this.vehicleOperatorService = vehicleOperatorService;
	}
	@Autowired
	public void setTaskProcessService(TaskProcessService taskProcessService) {
		this.taskProcessService = taskProcessService;
	}
	@Autowired
	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
	@Autowired
	public void setTaskGanttDataService(TaskGanttDataService taskGanttDataService) {
		this.taskGanttDataService = taskGanttDataService;
	}
	@Autowired
	public void setAttendService(AttendService attendService) {
		this.attendService = attendService;
	}
	@Autowired
	public void setExamSortService(ExamSortService examSortService) {
		this.examSortService = examSortService;
	}
	@Autowired
	public void setExamQuestionsService(ExamQuestionsService examQuestionsService) {
		this.examQuestionsService = examQuestionsService;
	}
	@Autowired
	public void setExamTestSortService(ExamTestSortService examTestSortService) {
		this.examTestSortService = examTestSortService;
	}
	@Autowired
	public void setExamQuestionRecordService(ExamQuestionRecordService examQuestionRecordService) {
		this.examQuestionRecordService = examQuestionRecordService;
	}
	@Autowired
	public void setExamTestService(ExamTestService examTestService) {
		this.examTestService = examTestService;
	}
	@Autowired
	public void setExamGradeService(ExamGradeService examGradeService) {
		this.examGradeService = examGradeService;
	}
	@Autowired
	public void setDutySortService(DutySortService dutySortService) {
		this.dutySortService = dutySortService;
	}
	@Autowired
	public void setDutyRecordService(DutyRecordService dutyRecordService) {
		this.dutyRecordService = dutyRecordService;
	}
	@Autowired
	public void setLicenceRecordService(LicenceRecordService licenceRecordService) {
		this.licenceRecordService = licenceRecordService;
	}
	@Autowired
	public void setLicenceBorrowingService(LicenceBorrowingService licenceBorrowingService) {
		this.licenceBorrowingService = licenceBorrowingService;
	}
	@Autowired
	public void setLicenceTypeService(LicenceTypeService licenceTypeService) {
		this.licenceTypeService = licenceTypeService;
	}
	@Autowired
	public void setEntertainService(EntertainService entertainService) {
		this.entertainService = entertainService;
	}

	/**
	 * 获取桌面待办招待事件
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyEntertainListForDesk")
	public RetDataBean getMyEntertainListForDesk() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, entertainService.getMyEntertainListForDesk(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 查询招待事件列表
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @param diningType 用餐类型
	 * @param entertainType 招待类型
	 * @param status 状态
	 * @return 消息结构 招待列表
	 */
	@GetMapping(value = "/getEntertainListForSearch")
	public RetDataBean getEntertainListForSearch(PageParam pageParam,String date,String diningType,String entertainType,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("e.create_time");
			} else {
				pageParam.setProp("e." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			String[] timeArr = SysTools.convertDateToTime(date);
			PageInfo<Map<String, String>> pageInfo = entertainService.getEntertainListForSearch(pageParam,date,timeArr[0],timeArr[1],diningType,entertainType,status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取待审批事件
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @param diningType 用餐类型
	 * @param entertainType 招待类型
	 * @return 消息结构 招待事件列表
	 */
	@GetMapping(value = "/getEntertainListForApprove")
	public RetDataBean getEntertainListForApprove(PageParam pageParam,String date,String diningType,String entertainType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("e.create_time");
			} else {
				pageParam.setProp("e." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			String[] timeArr = SysTools.convertDateToTime(date);
			PageInfo<Map<String, String>> pageInfo = entertainService.getEntertainListForApprove(pageParam,date,timeArr[0],timeArr[1],diningType,entertainType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取接待事件列表
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @param status 事件状态
	 * @param diningType 就餐类型
	 * @param entertainType 接待类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getEntertainListForManage")
	public RetDataBean getEntertainListForManage(PageParam pageParam,String date,String status,String diningType,String entertainType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("e.create_time");
			} else {
				pageParam.setProp("e." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			String[] timeArr = SysTools.convertDateToDay(date);
			PageInfo<Map<String, String>> pageInfo = entertainService.getEntertainListForManage(pageParam,date,timeArr[0],timeArr[1],status,diningType,entertainType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取证照出借记录
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @param status 出借状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLicenceBorrowingList")
	public RetDataBean getLicenceBorrowingList(PageParam pageParam,String date,String status,String licType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.create_time");
			} else {
				pageParam.setProp("b." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			String[] timeArr = SysTools.convertDateToDay(date);
			PageInfo<Map<String, String>> pageInfo = licenceBorrowingService.getLicenceBorrowingList(pageParam,date,timeArr[0],timeArr[1],status,licType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取单位证照列表
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @param licType 证照类型
	 * @param status 证照状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLicenceRecordList")
	public RetDataBean getLicenceRecordList(PageParam pageParam,String date,String licType,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.create_time");
			} else {
				pageParam.setProp("r." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			String[] timeArr = SysTools.convertDateToDay(date);
			PageInfo<Map<String, String>> pageInfo = licenceRecordService.getLicenceRecordList(pageParam,date,timeArr[0],timeArr[1],licType,status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取单位证照备选列表
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLicenceRecordListForSelect")
	public RetDataBean getLicenceRecordListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  licenceRecordService.getLicenceRecordListForSelect(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取分类列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLicenceTypeList")
	public RetDataBean getLicenceTypeList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  licenceTypeService.getLicenceTypeList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取分类树型结构
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLicenceTypeTree")
	public RetDataBean getLicenceTypeTree(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  licenceTypeService.getLicenceTypeTree(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取值班记录详情
	 * @param recordId 值班记录Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDutyRecordById")
	public RetDataBean getDutyRecordById(String recordId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dutyRecordService.getDutyRecordById(userInfo.getOrgId(),recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取当前值班列表
	 * @param sortId 值班类型Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDutyListBySortId")
	public RetDataBean getDutyListBySortId(String sortId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dutyRecordService.getDutyListBySortId(userInfo.getOrgId(),sortId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取值班待审批列表
	 * @param pageParam 分页参数 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDutyRecordListForApproval")
	public RetDataBean getDutyRecordListForApproval(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.create_time");
			} else {
				pageParam.setProp("r." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = dutyRecordService.getDutyRecordListForApproval(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取值班列表
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @param sortId 值班分类
	 * @param status 值班状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDutyRecordList")
	public RetDataBean getDutyRecordList(PageParam pageParam,String date,String sortId,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.create_time");
			} else {
				pageParam.setProp("r." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = dutyRecordService.getDutyRecordList(pageParam,sortId,status,date, timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取值班分类
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDutySortList")
	public RetDataBean getDutySortList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dutySortService.getDutySortList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取已启用的分类列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDutySortListForSelect")
	public RetDataBean getDutySortListForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dutySortService.getDutySortListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人上班考勤是否打卡
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAttendStatusDay")
	public RetDataBean getAttendStatusDay() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendService.getAttendStatusDay(userInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人下班考勤是否打卡
	 * @return 消息结构
	 */
	@GetMapping(value = "/getOffWorkAttendStatusDay")
	public RetDataBean getOffWorkAttendStatusDay() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendService.getOffWorkAttendStatusDay(userInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取加班列表
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyOverTimeList")
	public RetDataBean getMyOverTimeList(PageParam pageParam,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.create_time");
			} else {
				pageParam.setProp("b." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = attendService.getMyOverTimeList(pageParam,date, timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取值班列表
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyDutyList")
	public RetDataBean getMyDutyList(PageParam pageParam,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.create_time");
			} else {
				pageParam.setProp("b." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = attendService.getMyDutyList(pageParam, date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取出差列表
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyTravelList")
	public RetDataBean getMyTravelList(PageParam pageParam,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.create_time");
			} else {
				pageParam.setProp("b." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = attendService.getMyTravelList(pageParam,date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取人员请假列表
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围 日期范围类型
	 * @param attendType 类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyLeaveList")
	public RetDataBean getMyLeaveList(PageParam pageParam,String date,String attendType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.create_time");
			} else {
				pageParam.setProp("b." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = attendService.getMyLeaveList(pageParam, date,timeArr[0], timeArr[1], attendType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取外出列表
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyOutAttendList")
	public RetDataBean getMyOutAttendList(PageParam pageParam,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.create_time");
			} else {
				pageParam.setProp("b." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = attendService.getMyOutAttendList(pageParam,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取移动端考勤记录
	 * @param page 当前页码
	 * @param time 查询时间
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyAttendListForApp")
	public RetDataBean getMyAttendListForApp(Integer page,String time) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendService.getMyAttendListForApp(userInfo.getOrgId(), userInfo.getAccountId(),time, page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人考勤的全部年份
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyAttendYear")
	public RetDataBean getMyAttendYear() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, attendService.getMyAttendYear(userInfo.getOrgId(), userInfo.getAccountId()));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人考勤
	 * @param year 年份
	 * @param attendType 考勤方式
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyAttendList")
	public RetDataBean getMyAttendList(PageParam pageParam, String attendType, String status, String year,String month) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = attendService.getMyAttendList(pageParam,attendType, status, year,month);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取移动端待办会议
	 * @param page 当前页码
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyMeetingListForApp")
	public RetDataBean getMyMeetingListForApp(Integer page) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getMyMeetingListForApp(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(), page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取会议详情
	 * @param meetingId 会议Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyMeetingById")
	public RetDataBean getMyMeetingById(String meetingId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(meetingId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getMyMeetingById(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(), meetingId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人工作日志列表
	 * @param page 当前页码
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyDiaryListForApp")
	public RetDataBean getMobileMyDiaryList(Integer page) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryService.getMyDiaryListForApp(userInfo.getOrgId(), userInfo.getAccountId(), page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人日志总数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyDiaryCount")
	public RetDataBean getMyDiaryCount() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryService.getMyDiaryCount(userInfo.getOrgId(), userInfo.getAccountId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取工作日志详情
	 * @param diaryId 工作日志Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyDiaryById")
	public RetDataBean getMyDiaryById(String diaryId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(diaryId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryService.getMyDiaryById(userInfo.getOrgId(), userInfo.getOpFlag(),userInfo.getAccountId(),userInfo.getDeptId(),userInfo.getUserLevel(),diaryId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取考试详情
	 * @param recordId 考试记录Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyExamTestById")
	public RetDataBean getMyExamTestById(String recordId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(recordId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examTestService.getMyExamTestById(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(),recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取考试成绩
	 * @param examGrade 考试成绩对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamGradeById")
	public RetDataBean getExamGradeById(ExamGrade examGrade) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			examGrade.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examGradeService.selectOneExamGrade(examGrade));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取考试列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamTestListForSelect")
	public RetDataBean getExamTestListForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examTestService.getExamTestListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 历史成绩管理
	 * @param pageParam 分页参数
	 * @param accountId 用户账号
	 * @param examTestId 考试Id
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamGradeOldList")
	public RetDataBean getExamGradeOldList(PageParam pageParam, String accountId, String examTestId, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("g.begin_time");
			} else {
				pageParam.setProp("g." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(accountId);
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = examGradeService.getExamGradeOldList(pageParam, examTestId,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取考试成绩列表
	 * @param pageParam 分页参数
	 * @param accountId 用户账号
	 * @param examTestId 考试Id
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamGradeList")
	public RetDataBean getExamGradeList(PageParam pageParam, String accountId, String examTestId, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("g.begin_time");
			} else {
				pageParam.setProp("g." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(accountId);
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = examGradeService.getExamGradeList(pageParam, examTestId,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取试卷详情
	 * @param examTest 获取试题对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamTestById")
	public RetDataBean getExamTestById(ExamTest examTest) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			examTest.setOrgId(userInfo.getOrgId());
			if(examGradeService.getInitExamTestStatus(userInfo.getOrgId(),examTest.getRecordId(),userInfo.getAccountId())) {
				return RetDataTools.Fail(MessageCode.MESSAGE_RECORD_EXIST);
			}else {
				String beginTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
				ExamGrade examGrade = new ExamGrade();
				examGrade.setRecordId(SysTools.getGUID());
				examGrade.setExamTestId(examTest.getRecordId());
				examGrade.setStatus("0");
				examGrade.setAccountId(userInfo.getAccountId());
				examGrade.setCreateTime(beginTime);
				examGrade.setBeginTime(beginTime);
				examGrade.setCreateUser(userInfo.getAccountId());
				examGrade.setOrgId(userInfo.getOrgId());
				examGradeService.insertExamGrade(examGrade);
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examTestService.selectOneExamTest(examTest));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  获取试题列表
	 * @param examTest 获取试题对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamQuestionListById")
	public RetDataBean getExamQuestionListById(ExamTest examTest) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			examTest.setOrgId(userInfo.getOrgId());
			examTest = examTestService.selectOneExamTest(examTest);
			String recordId = JSON.parseObject(examTest.getQuestionRecordId()).getString("recordId");
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examQuestionRecordService.getExamQuestionListById(userInfo.getOrgId(), recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人考试表表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyExamTestList")
	public RetDataBean getMyExamTestList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examTestService.getMyExamTestList(userInfo.getOrgId(),userInfo.getAccountId(),userInfo.getDeptId(),userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取试卷列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamQuestionRecordForSelect")
	public RetDataBean getExamQuestionRecordForSelect(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("create_time");
			} else {
				pageParam.setProp( StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = examQuestionRecordService.getExamQuestionRecordForSelect(pageParam);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取试卷管理列表
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamTestManageList")
	public RetDataBean getExamTestManageList(PageParam pageParam, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.create_time");
			} else {
				pageParam.setProp("t." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = examTestService.getExamTestManageList(pageParam, date,timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取试卷列表
	 *
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamQuestionRecordList")
	public RetDataBean getExamQuestionRecordList(PageParam pageParam,String date,String sortId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.create_time");
			} else {
				pageParam.setProp("n." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = examQuestionRecordService.getExamQuestionRecordList(pageParam,date,timeArr[0],timeArr[1],sortId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取试卷分类树结构
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamTestSortTree")
	public RetDataBean getExamTestSortTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  examTestSortService.getExamTestSortTree(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按分类获取试题列表
	 * @param pageParam 分页参数
	 * @param sortId 试卷分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamQuestionsListBySortId")
	public RetDataBean getExamQuestionsListBySortId(PageParam pageParam, String sortId,String examType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("q.sort_no");
			} else {
				pageParam.setProp("q." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = examQuestionsService.getExamQuestionsListBySortId(pageParam, sortId,examType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取试题分类树结构
	 * @return 消息结构
	 */
	@GetMapping(value = "/getExamSortTree")
	public RetDataBean getExamSortTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  examSortService.getExamSortTree(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取考勤列表
	 * @param pageParam 分页参数
	 * @param attendType 考勤类型
	 * @param date 日期范围
	 * @param deptId 部门Id
	 * @param createUser 考勤人
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTotalAttendList")
	public RetDataBean getTotalAttendList(PageParam pageParam, String attendType, @RequestParam(value = "date[]") String[] date, String deptId, String createUser) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("create_time");
			} else {
				pageParam.setProp(StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			if(date==null) {
				date = new String[2];
			}
			PageInfo<Map<String, String>> pageInfo = attendService.getTotalAttendList(pageParam, attendType, date[0], date[1], deptId, createUser);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面个人任务
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyTaskListForDesk")
	public RetDataBean getMyTaskListForDesk() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskGanttDataService.getMyTaskListForDesk(userInfo.getOrgId(), userInfo.getAccountId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取处理事件详情
	 * @param taskId 事件处理记录Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTaskProcessInfo")
	public RetDataBean getTaskProcessInfo(String taskId) {
		try {
			if (StringUtils.isBlank(taskId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskProcessService.getProcessInfo(userInfo.getOrgId(), taskId, userInfo.getAccountId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取Gantt详情
	 * @param task Gantt任务对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getGanttInfoByTaskId")
	public RetDataBean getGanttInfoByTaskId(Task task) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(task.getTaskId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskService.getGanttInfoByTaskId(userInfo.getOrgId(),task.getTaskId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取任务详情
	 * @param task 任务对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTaskById")
	public RetDataBean getTaskById(Task task) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(task.getTaskId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			task.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskService.getTaskById(task));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 任务查询
	 * @param pageParam 分页参数
	 * @param status 任务状态
	 * @param taskType 任务类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/searchTaskList")
	public RetDataBean searchTaskList(PageParam pageParam, String status,String taskType, String date,String queryType) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.create_time");
			} else {
				pageParam.setProp("t." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			if(StringUtils.isBlank(queryType)) {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,taskService.getMyJoinTaskList(pageParam, taskType, status,date,timeArr[0], timeArr[1]));
			}else {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,taskService.getMyShareTaskList(pageParam, taskType, status,date,timeArr[0], timeArr[1]));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取我的任务
	 * @param pageParam 分页参数
	 * @param status 任务状态
	 * @param taskType 任务类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyTaskWorkList")
	public RetDataBean getMyTaskWorkList(PageParam pageParam, String status,String taskType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.create_time");
			} else {
				pageParam.setProp("t." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = taskGanttDataService.getMyTaskWorkList(pageParam, taskType, status,date,timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取移动端我的任务
	 * @param page 当前页码
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyTaskWorkListForApp")
	public RetDataBean getMyTaskWorkListForApp(Integer page,String keyword) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			List<Map<String, String>> pageInfo = taskGanttDataService.getMyTaskWorkListForApp(userInfo.getOrgId(), userInfo.getAccountId(),keyword,page);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人负责的任务
	 *
	 * @param pageParam 分页参数
	 * @param status 什务状态
	 * @param taskType 任务类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyChargeTaskList")
	public RetDataBean getMyChargeTaskList(PageParam pageParam, String queryType, String status, String taskType, String date) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.create_time");
			} else {
				pageParam.setProp("t." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(queryType)) {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskService.getMyChargeTaskList(pageParam, status, taskType, date, timeArr[0], timeArr[1]));
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskService.getMySupervisorTaskList(pageParam, status, taskType, date, timeArr[0], timeArr[1]));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有待分配子任务列表
	 *
	 * @param taskType 任务类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTaskAllocationList")
	public RetDataBean getTaskAllocationList(String taskType, String date, String keyword) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String[] timeArr = SysTools.convertDateToDayAfter(date);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskService.getTaskAllocationList(userInfo.getOrgId(), userInfo.getAccountId(), taskType, date, timeArr[0], timeArr[1], keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取待分解任务列表
	 *
	 * @param taskType 任务类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAssignmentTaskList")
	public RetDataBean getAssignmentTaskList(String taskType, String date) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String[] timeArr = SysTools.convertDateToDayAfter(date);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, taskService.getAssignmentTaskList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getOpFlag(), taskType, date, timeArr[0], timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取任务列表
	 *
	 * @param pageParam 分页参数
	 * @param status 任务状态
	 * @param taskType 任务类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getManageTaskList")
	public RetDataBean getManageTaskList(PageParam pageParam, String status, String taskType, String date) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("task:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.create_time");
			} else {
				pageParam.setProp("t." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = taskService.getManageTaskList(pageParam, status, taskType, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取子任务的处理过程列表
	 *
	 * @param pageParam 分页参数
	 * @param createUser 处理人
	 * @param taskType 任务类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyTaskProcessList")
	public RetDataBean getMyTaskProcessList(PageParam pageParam, String createUser, String taskType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("p.complete_time");
			} else {
				pageParam.setProp("p." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = taskProcessService.getMyTaskProcessList(pageParam, createUser, taskType, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取待还车记录
	 *
	 * @param pageParam 分页参数
	 * @param returnFlag 还车状态
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleReturnList")
	public RetDataBean getVehicleReturnList(PageParam pageParam, String carType, String returnFlag, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = vehicleApplyService.getVehicleReturnList(pageParam, carType, returnFlag, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取调度人员列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleOperatorUserList")
	public RetDataBean getVehicleOperatorUserList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			VehicleOperator vehicleOperator = new VehicleOperator();
			vehicleOperator.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleOperatorService.getVehicleOperatorUserList(vehicleOperator));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取审批结果
	 *
	 * @param applyId 申请记录Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleApprovalResult")
	public RetDataBean getVehicleApprovalResult(String applyId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(applyId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleApplyService.getVehicleApprovalResult(userInfo.getOrgId(), applyId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取车辆调度人员
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleOperatorByOrgId")
	public RetDataBean getVehicleOperatorByOrgId() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			VehicleOperator vehicleOperator = new VehicleOperator();
			vehicleOperator.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleOperatorService.selectOneVehicleOperator(vehicleOperator));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取车辆申请审批列表
	 *
	 * @param pageParam 分页参数
	 * @param status 审批状态
	 * @param date 日期范围
	 * @param carType 车辆类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleApprovedList")
	public RetDataBean getVehicleApprovedList(PageParam pageParam, String status, String date, String carType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = vehicleApplyService.getVehicleApprovedList(pageParam, status, date, timeArr[0], timeArr[1], carType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取申请记录
	 *
	 * @param pageParam 分页参数
	 * @param status 申请状态
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleApplyList")
	public RetDataBean getVehicleApplyList(PageParam pageParam, String status, String date, String carType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = vehicleApplyService.getVehicleApplyList(pageParam, status, date, timeArr[0], timeArr[1], carType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	@GetMapping(value = "/getVehicleDriverListForSelect")
	public RetDataBean getVehicleDriverListForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleDriverService.getVehicleDriverListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取可调度车辆
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCanUsedVehicleList")
	public RetDataBean getCanUsedVehicleList(String carType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleInfoService.getCanUsedVehicleList(userInfo.getOrgId(), carType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取车辆保养经办人
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleRepairUserList")
	public RetDataBean getVehicleRepairUserList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleRepairRecordService.getVehicleRepairUserList(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取维修列表
	 *
	 * @param pageParam 分页参数
	 * @param repairType 维修时间
	 * @param repairUser 维修人
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleRepairRecordList")
	public RetDataBean getVehicleRepairRecordList(PageParam pageParam, String repairType, String repairUser, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.repair_time");
			} else {
				pageParam.setProp("r." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = vehicleRepairRecordService.getVehicleRepairRecordList(pageParam, repairUser, repairType, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取车辆下接列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleListForSelect")
	public RetDataBean getVehicleListForSelect() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vehicle:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleInfoService.getVehicleListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取车辆列表
	 *
	 * @param pageParam 分页参数
	 * @param onwer 归属人
	 * @param carType 车辆类型
	 * @param nature 车辆性质
	 * @param date1 日期范围1
	 * @param date2 日期范围2
	 * @return 消息结构
	 */
	@GetMapping(value = "/getManageVehicleInfoList")
	public RetDataBean getManageVehicleInfoList(PageParam pageParam, String onwer, String carType, String nature, String date1, String date2) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vehicle:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("v.create_time");
			} else {
				pageParam.setProp("v." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr1 = SysTools.convertDateToTimeAfter(date1);
			String[] timeArr2 = SysTools.convertDateToTime(date2);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = vehicleInfoService.getManageVehicleInfoList(pageParam, onwer, carType, nature, date1, timeArr1[0], timeArr1[1], date2, timeArr2[0], timeArr2[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取司机列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleDriverList")
	public RetDataBean getVehicleDriverList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleDriverService.getVehicleDriverList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取油卡列表
	 *
	 * @param pageParam 分页参数
	 * @param oilType 油卡类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVehicleOilCardList")
	public RetDataBean getVehicleOilCardList(PageParam pageParam, String oilType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("v.card_time");
			} else {
				pageParam.setProp("v." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = vehicleOilCardService.getVehicleOilCardList(pageParam, oilType, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取可通的油卡列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCanUsedOilCardList")
	public RetDataBean getCanUsedOilCardList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleOilCardService.getCanUsedOilCardList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 历史调拨记录
	 *
	 * @param pageParam 分页参数
	 * @param deptId 部门Id
	 * @param sortId 分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getFixedAssetsAllocationOldList")
	public RetDataBean getFixedAssetsAllocationOldList(PageParam pageParam, String date, String deptId, String sortId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = fixedAssetsAllocationService.getFixedAssetsAllocationOldList(pageParam, date, timeArr[0], timeArr[1], deptId, sortId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取固定资产详情
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getFixedAssetsById")
	public RetDataBean getFixedAssetsById(String assetsId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, fixedAssetsService.getFixedAssetsById(userInfo.getOrgId(), assetsId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取维修列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param sortId 分类Id
	 * @param status 维修状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getFixedAssetsRepairList")
	public RetDataBean getFixedAssetsRepairList(PageParam pageParam, String date, String sortId, String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.create_time");
			} else {
				pageParam.setProp("r." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = fixedAssetsRepairService.getFixedAssetsRepairList(pageParam, date, timeArr[0], timeArr[1], sortId, status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取调拨列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param sortId 分类Id
	 * @param applyUser 申请人
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllocationList")
	public RetDataBean getAllocationList(PageParam pageParam, String date, String sortId, String applyUser) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = fixedAssetsService.getAllocationList(pageParam, date, timeArr[0], timeArr[1], sortId, applyUser);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取申请列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param status 申请状态
	 * @param sortId 分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getFixedAssetsApplyOldList")
	public RetDataBean getFixedAssetsApplyOldList(PageParam pageParam, String date, String status, String sortId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = fixedAssetApplyService.getFixedAssetsApplyOldList(pageParam, status, date, timeArr[0], timeArr[1], sortId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取申请列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param status 申请状态
	 * @param sortId 分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getFixedAssetsApplyList")
	public RetDataBean getFixedAssetsApplyList(PageParam pageParam, String date, String status, String sortId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = fixedAssetApplyService.getFixedAssetsApplyList(pageParam, status, date, timeArr[0], timeArr[1], sortId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 查询固定资产列表
	 *
	 * @param pageParam 分页参数
	 * @param sortId 分类Id
	 * @param ownDept 所属部门
	 * @param status 资产状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/queryFixedAssetsList")
	public RetDataBean queryFixedAssetsList(PageParam pageParam, String sortId, String ownDept, String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("f.sort_no");
			} else {
				pageParam.setProp("f." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = fixedAssetsService.queryFixedAssetsList(pageParam, sortId, ownDept, status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取固定资产列表
	 *
	 * @param pageParam 分页参数
	 * @param sortId 固定资产分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getFixedAssetsList")
	public RetDataBean getFixedAssetsList(PageParam pageParam, String sortId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("f.sort_no");
			} else {
				pageParam.setProp("f." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsService.getFixedAssetsList(pageParam, sortId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取可申请的固定资产的列表
	 *
	 * @param pageParam 分页参数
	 * @param sortId 固定资产分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getApplyFixedAssetsList")
	public RetDataBean getApplyFixedAssetsList(PageParam pageParam, String sortId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("fixedassets:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("f.sort_no");
			} else {
				pageParam.setProp("f." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = fixedAssetsService.getApplyFixedAssetsList(pageParam, sortId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取固定资产的分类
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getFixedAssetsSortTree")
	public RetDataBean getFixedAssetsSortTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, fixedAssetsSortService.getFixedAssetsSortTree(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取仓库列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getFixedAssetsStorageList")
	public RetDataBean getFixedAssetsStorageList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsStorageService.getFixedAssetsStorageList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取仓库列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllFixedAssetsStorageList")
	public RetDataBean getAllFixedAssetsStorageList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsStorageService.getAllFixedAssetsStorageList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面会议记要
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyMeetingNotesListForDesk")
	public RetDataBean getMyMeetingNotesListForDesk() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingNotesService.getMyMeetingNotesListForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 会议记要查询
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param meetingType 会议类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/queryMeetingNotesList")
	public RetDataBean queryMeetingNotesList(PageParam pageParam, String date, String meetingType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.create_time");
			} else {
				pageParam.setProp("n." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = meetingNotesService.queryMeetingNotesList(pageParam, meetingType, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取没有会议纪要的会议列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getNotNotesMeetingList")
	public RetDataBean getNotNotesMeetingList() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getNotNotesMeetingList(userInfo.getOrgId(), userInfo.getAccountId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面会议
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyMeetingListForDesk")
	public RetDataBean getMyMeetingListForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getMyMeetingListForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取当前用户待参加会议
	 *
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyMeetingList")
	public RetDataBean getMyMeetingList(PageParam pageParam, String date) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("m.create_time");
			} else {
				pageParam.setProp("m." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTimeAfter(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = meetingService.getMyMeetingList(pageParam, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取待审批的会议列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param status 会议状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getApplyMeetingList")
	public RetDataBean getApplyMeetingList(PageParam pageParam, String date, String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("meeting:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("m.create_time");
			} else {
				pageParam.setProp("m." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = meetingService.getApplyMeetingList(pageParam, status, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取会议记要列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMeetingNotesList")
	public RetDataBean getMeetingNotesList(PageParam pageParam, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.create_time");
			} else {
				pageParam.setProp("n." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = meetingNotesService.getMeetingNotesList(pageParam, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按日期获取当前会议室使用情况
	 *
	 * @param dayStr 会议时间段
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMeetingListByDayStr")
	public RetDataBean getMeetingListByDayStr(String dayStr) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(dayStr)) {
				dayStr = SysTools.getTime("yyyy-MM-dd");
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getMeetingListByDayStr(userInfo.getOrgId(), dayStr, userInfo.getAccountId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人历史会议申请记录
	 *
	 * @param pageParam 分页参数
	 * @param roomId 会议室Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyApplyMeetingList")
	public RetDataBean getMyApplyMeetingList(PageParam pageParam, String roomId, String meetingType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("m.create_time");
			} else {
				pageParam.setProp("m." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = meetingService.getMyApplyMeetingList(pageParam, date, timeArr[0], timeArr[1], roomId, meetingType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取设备名称列表
	 *
	 * @param deviceIds 设置Id 字符串
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMeetingDeviceListName")
	public RetDataBean getMeetingDeviceListName(String deviceIds) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingDeviceService.getMeetingDeviceListName(userInfo.getOrgId(), deviceIds));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取权限内可用的会议室设备
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCanUseDeviceList")
	public RetDataBean getCanUseDeviceList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (userInfo.getOpFlag().equals("1")) {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingDeviceService.getCanUseDeviceList(userInfo.getOrgId(), null));
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingDeviceService.getCanUseDeviceList(userInfo.getOrgId(), userInfo.getDeptId()));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取会议设备列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMeetingDeviceList")
	public RetDataBean getMeetingDeviceList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingDeviceService.getMeetingDeviceList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取当前用户可用的会议室
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getCanUseMeetingRoomList")
	public RetDataBean getCanUseMeetingRoomList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingRoomService.getCanUseMeetingRoomList(userInfo.getOrgId(), userInfo.getDeptId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取我历史申请过的会议室列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyApplyMeetingRoomList")
	public RetDataBean getMyApplyMeetingRoomList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingRoomService.getMyApplyMeetingRoomList(userInfo));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 会议室列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMeetingRoomList")
	public RetDataBean getMeetingRoomList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingRoomService.getMeetingRoomList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取禁用的会议时间段
	 *
	 * @param dayStr 会议时间段
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMeetingByDay")
	public RetDataBean getMeetingByDay(String dayStr) {
		try {
			if (StringUtils.isBlank(dayStr)) {
				dayStr = SysTools.getTime("yyyy-MM-dd");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, meetingService.getMeetingByDay(userInfo.getOrgId(), userInfo.getDeptId(), dayStr));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人历史申请记录
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param status 申请状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyApplyOfficeSuppliesList")
	public RetDataBean getMyApplyOfficeSuppliesList(PageParam pageParam, String date, String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			PageInfo<Map<String, String>> pageInfo = officeSuppliesApplyService.getMyApplyOfficeSuppliesList(pageParam, date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取办公用品发放列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param accountId 用户账号
	 * @return 消息结构
	 */
	@GetMapping(value = "/getGrantOfficeList")
	public RetDataBean getGrantOfficeList(PageParam pageParam, String date, String accountId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(accountId);
			PageInfo<Map<String, String>> pageInfo = officeSuppliesGrantService.getGrantOfficeList(pageParam, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 待发放用品列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getGrantOfficeSuppliesList")
	public RetDataBean getGrantOfficeSuppliesList(PageParam pageParam, String accountId, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(accountId);
			PageInfo<Map<String, String>> pageInfo = officeSuppliesApplyService.getGrantOfficeSuppliesList(pageParam, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取审批列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param status 审批状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getOfficeSuppliesApplyRecordList")
	public RetDataBean getOfficeSuppliesApplyRecordList(PageParam pageParam, String date, String status, String accountId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(accountId);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = officeSuppliesApplyService.getOfficeSuppliesApplyRecordList(pageParam, date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取办公用品列表
	 *
	 * @param sortId 办公用品类型
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getOfficeSuppliesListBySortId")
	public RetDataBean getOfficeSuppliesListBySortId(PageParam pageParam, String sortId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("officesupplies:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("sort_no");
			} else {
				pageParam.setProp(StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = officeSuppliesService.getOfficeSuppliesListBySortId(pageParam, sortId);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取可以领用的办公用品列表
	 *
	 * @param pageParam 分页参数
	 * @param sortId 办公用品类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getApplyOfficeSuppliesList")
	public RetDataBean getApplyOfficeSuppliesList(PageParam pageParam, String sortId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("officesupplies:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("sort_no");
			} else {
				pageParam.setProp(StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = officeSuppliesService.getApplyOfficeSuppliesList(pageParam, sortId, userInfo.getDeptId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取办公用品分类
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getOfficeSuppliesSortTree")
	public RetDataBean getOfficeSuppliesSortTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(officeSuppliesSortService.getOfficeSuppliesSortTree(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取日志评论
	 *
	 * @param diaryId 工作日志Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDiaryCommentsList")
	public RetDataBean getDiaryCommentsList(String diaryId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryCommentsService.getDiaryCommentsList(userInfo.getOrgId(), diaryId, userInfo.getAccountId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取共享人员列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getShareDiaryUserList")
	public RetDataBean getShareDiaryUserList() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryService.getShareDiaryUserList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取他人分享的工作日志
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getShareDiaryList")
	public RetDataBean getShareDiaryList(PageParam pageParam, String date, String createUser) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.create_time");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = diaryService.getShareDiaryList(pageParam, date, timeArr[0], timeArr[0], createUser);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取当前用户的历史工作日志
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyDiaryList")
	public RetDataBean getMyDiaryList(PageParam pageParam, String date, String diaryDay) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.create_time");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, Object>> pageInfo = diaryService.getMyDiaryList(pageParam, date, timeArr[0], timeArr[1], diaryDay);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取下属的工作日志
	 *
	 * @param pageParam 分页参数
	 * @param createUser 工作日志创建人
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMySubordinatesDiaryList")
	public RetDataBean getMySubordinatesDiaryList(PageParam pageParam, String createUser, String date) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("diary:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.create_time");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = diaryService.getMySubordinatesDiaryList(pageParam, createUser, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取工作日志设置
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDiaryConfig")
	public RetDataBean getDiaryConfig() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			DiaryConfig diaryConfig = new DiaryConfig();
			diaryConfig.setOrgId(userInfo.getOrgId());
			diaryConfig = diaryConfigService.selectOneDiaryConfig(diaryConfig);
			if(diaryConfig==null) {
				DiaryConfig tempDiaryConfig = new DiaryConfig();
				tempDiaryConfig.setConfigId(SysTools.getGUID());
				tempDiaryConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				tempDiaryConfig.setCreateUser(userInfo.getCreateUser());
				tempDiaryConfig.setLockDay(2);
				tempDiaryConfig.setShareStatus(2);
				tempDiaryConfig.setCommStatus(1);
				diaryConfigService.insertDiaryConfig(tempDiaryConfig);
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, tempDiaryConfig);
			}else {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, diaryConfig);
			}

		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取工作维护列表
	 *
	 * @param pageParam 分页参数
	 * @param status 工作计划状态
	 * @param planType 工作计划类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getManageWorkPlanList")
	public RetDataBean getManageWorkPlanList(PageParam pageParam, String status, String planType, String date) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("w.create_time");
			} else {
				pageParam.setProp("w." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = workPlanService.getManageWorkPlanList(pageParam, date, timeArr[0], timeArr[1], status, planType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取工作计划详情
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyWorkPlanById")
	public RetDataBean getMyWorkPlanById(String planId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(planId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, workPlanService.getMyWorkPlanById(userInfo.getOrgId(),userInfo.getOpFlag(),userInfo.getAccountId(),userInfo.getDeptId(),userInfo.getUserLevel(),planId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 我负责的计划列表
	 *
	 * @param pageParam 分页参数
	 * @param status 计划状态
	 * @param planType 计划类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHoldWorkPlanList")
	public RetDataBean getHoldWorkPlanList(PageParam pageParam, String status, String planType, String date) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("w.create_time");
			} else {
				pageParam.setProp("w." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = workPlanService.getHoldWorkPlanList(pageParam, date, timeArr[0], timeArr[1], status, planType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 我督查的计划列表
	 *
	 * @param pageParam 分页参数
	 * @param status 计划状态
	 * @param planType 计划类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupWorkPlanList")
	public RetDataBean getSupWorkPlanList(PageParam pageParam, String status, String planType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("w.create_time");
			} else {
				pageParam.setProp("w." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = workPlanService.getSupWorkPlanList(pageParam, date, timeArr[0], timeArr[1], status, planType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面的工作计划
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyWorkPlanForDesk")
	public RetDataBean getMyWorkPlanForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, workPlanService.getMyWorkPlanForDesk(userInfo.getOrgId(), userInfo.getAccountId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 我参与的工作计划
	 *
	 * @param pageParam 分页参数
	 * @param status 工作计划状态
	 * @param planType 工作计划类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyWorkPlanList")
	public RetDataBean getMyWorkPlanList(PageParam pageParam, String status, String planType, String date, String type) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("w.create_time");
			} else {
				pageParam.setProp("w." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, workPlanService.getMyWorkPlanList(pageParam, type, date, timeArr[0], timeArr[1], status, planType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 移动端我参与的工作计划
	 * @param page 当前页码
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyWorkPlanListForApp")
	public RetDataBean getMyWorkPlanListForApp(Integer page,String keyword) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, workPlanService.getMyWorkPlanListForApp(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(),keyword, page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取工作计划处理反馈列表
	 *
	 * @param workPlanProcess 工作处理记录对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getWorkPlanProcessList")
	public RetDataBean getWorkPlanProcessList(WorkPlanProcess workPlanProcess) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("workplan:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			workPlanProcess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, workPlanProcessService.getWorkPlanProcessList(workPlanProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 导出考勤详情
	 * @param response HttpServletResponse
	 * @param beginTime 开始时间
	 * @param attendType 考勤类型
	 * @param createUser 考勤人
	 * @param deptId 考勤部门Id
	 */
	@PostMapping(value = "/getExpAttendRecord")
	public void getExpAttendRecord(HttpServletResponse response, String beginTime,String attendType,String createUser, String deptId,String configId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			attendService.getExpAttendRecord(response,userInfo.getOrgId(),beginTime,attendType,createUser,deptId,configId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * 导出考勤汇总
	 * @param response HttpServletResponse
	 * @param beginTime 开始时间
	 * @param attendType 考勤类型
	 * @param createUser 考勤人
	 * @param deptId 考勤部门Id
	 */
	@PostMapping(value = "/getExpAttendRecord2")
	public void getExpAttendRecord2(HttpServletResponse response, String beginTime,String attendType,String createUser, String deptId,String configId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			attendService.getExpAttendRecord2(response,userInfo.getOrgId(),beginTime,attendType,createUser,deptId,configId);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

}
