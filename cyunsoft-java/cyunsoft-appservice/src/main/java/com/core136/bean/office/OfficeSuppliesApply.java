package com.core136.bean.office;

import java.io.Serializable;

/**
 * @author lsq
 *
 */
public class OfficeSuppliesApply implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String applyId;
    private String title;
    private String suppliesId;
    private String remark;
    private Integer quantity;
    private String usedUser;
    private String status;
    private String approvalUser;
    private String approvalTime;
    private String approvalIdea;
    private Integer approvalQuantity;
    private String createUser;
    private String createTime;
    private String orgId;

	public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSuppliesId() {
		return suppliesId;
	}

	public void setSuppliesId(String suppliesId) {
		this.suppliesId = suppliesId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getUsedUser() {
		return usedUser;
	}

	public void setUsedUser(String usedUser) {
		this.usedUser = usedUser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getApprovalUser() {
		return approvalUser;
	}

	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	public String getApprovalTime() {
		return approvalTime;
	}

	public void setApprovalTime(String approvalTime) {
		this.approvalTime = approvalTime;
	}

	public String getApprovalIdea() {
		return approvalIdea;
	}

	public void setApprovalIdea(String approvalIdea) {
		this.approvalIdea = approvalIdea;
	}

	public Integer getApprovalQuantity() {
		return approvalQuantity;
	}

	public void setApprovalQuantity(Integer approvalQuantity) {
		this.approvalQuantity = approvalQuantity;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
