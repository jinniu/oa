package com.core136.bean.office;

import java.io.Serializable;

/**
 * @author lsq
 * 会议记要
 */
public class MeetingNotes implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String notesId;
    private String meetingId;
    private String msgType;
    private String notesTitle;
    private String userRole;
    private String deptRole;
    private String levelRole;
    private String content;
    private String attachId;
    private String attachRole;
    private String createTime;
    private String createUser;
    private String orgId;

	public String getNotesId() {
		return notesId;
	}

	public void setNotesId(String notesId) {
		this.notesId = notesId;
	}

	public String getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(String meetingId) {
		this.meetingId = meetingId;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getNotesTitle() {
		return notesTitle;
	}

	public void setNotesTitle(String notesTitle) {
		this.notesTitle = notesTitle;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getDeptRole() {
		return deptRole;
	}

	public void setDeptRole(String deptRole) {
		this.deptRole = deptRole;
	}

	public String getLevelRole() {
		return levelRole;
	}

	public void setLevelRole(String levelRole) {
		this.levelRole = levelRole;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getAttachRole() {
		return attachRole;
	}

	public void setAttachRole(String attachRole) {
		this.attachRole = attachRole;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
