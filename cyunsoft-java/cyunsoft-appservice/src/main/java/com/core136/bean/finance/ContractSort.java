package com.core136.bean.finance;

import java.io.Serializable;
import java.util.List;

/**
 * @author lsq
 */
public class ContractSort implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String sortId;
    private Integer sortNo;
    private String sortName;
    private String remark;
    private String parentId;
	private transient List<ContractSort> children;
    private String createUser;
    private String createTime;
    private String orgId;

	public String getSortId() {
		return sortId;
	}

	public void setSortId(String sortId) {
		this.sortId = sortId;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public List<ContractSort> getChildren() {
		return children;
	}

	public void setChildren(List<ContractSort> children) {
		this.children = children;
	}
}
