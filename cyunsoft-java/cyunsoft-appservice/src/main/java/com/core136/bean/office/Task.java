package com.core136.bean.office;

import java.io.Serializable;

public class Task implements Serializable {
    /**
     * Time : 2017 2017年10月17日 下午2:38:29
     * Author : LSQ
     *
     * @param request
     * @param response
     * Exception
     */
    private static final long serialVersionUID = 1L;
    private String taskId;
    private String taskName;
    private String subheading;
    private String taskType;
    private String taskLevel;
    private String deptRole;
    private String userRole;
    private String levelRole;
    private String chargeAccountId;
    private String participantAccountId;
    private String supervisorAccountId;
    private String attachId;
    private String attachRole;
    private String beginTime;
    private String endTime;
    private String msgType;
    private String content;
    private String createTime;
    private String createUser;
    private String status;
    private String duration;
    private String orgId;

	public String getSubheading() {
		return subheading;
	}

	public void setSubheading(String subheading) {
		this.subheading = subheading;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getTaskLevel() {
		return taskLevel;
	}

	public void setTaskLevel(String taskLevel) {
		this.taskLevel = taskLevel;
	}

	public String getDeptRole() {
		return deptRole;
	}

	public void setDeptRole(String deptRole) {
		this.deptRole = deptRole;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getLevelRole() {
		return levelRole;
	}

	public void setLevelRole(String levelRole) {
		this.levelRole = levelRole;
	}

	public String getChargeAccountId() {
		return chargeAccountId;
	}

	public void setChargeAccountId(String chargeAccountId) {
		this.chargeAccountId = chargeAccountId;
	}

	public String getParticipantAccountId() {
		return participantAccountId;
	}

	public void setParticipantAccountId(String participantAccountId) {
		this.participantAccountId = participantAccountId;
	}

	public String getSupervisorAccountId() {
		return supervisorAccountId;
	}

	public void setSupervisorAccountId(String supervisorAccountId) {
		this.supervisorAccountId = supervisorAccountId;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getAttachRole() {
		return attachRole;
	}

	public void setAttachRole(String attachRole) {
		this.attachRole = attachRole;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
