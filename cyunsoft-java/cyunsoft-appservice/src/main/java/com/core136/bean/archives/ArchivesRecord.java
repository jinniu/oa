package com.core136.bean.archives;

import java.io.Serializable;
public class ArchivesRecord implements Serializable {
    private String recordId;
    private String title;
    private Integer sortNo;
    private String sortId;
    private String subject;
    private String roomId;
    private String frameId;
	private String frameStatus;
	private String frameTime;
    private String codeNo;
    private String status;
    private String isKind;
    private Integer fileQuantity;
    private Integer pages;

    private String extName;
    private String attachId;

    private String classifiedLevel;

    private String beginTime;

    private String disposalMethod;

    private Integer storagePeriod;

    private String identifyTime;
    private String identifyUser;


    private String borrowingFlag;

    private String queryFlag;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getFrameId() {
        return frameId;
    }

    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public String getCodeNo() {
        return codeNo;
    }

    public void setCodeNo(String codeNo) {
        this.codeNo = codeNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsKind() {
        return isKind;
    }

    public void setIsKind(String isKind) {
        this.isKind = isKind;
    }

    public Integer getFileQuantity() {
        return fileQuantity;
    }

    public void setFileQuantity(Integer fileQuantity) {
        this.fileQuantity = fileQuantity;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public String getExtName() {
        return extName;
    }

    public void setExtName(String extName) {
        this.extName = extName;
    }

    public String getAttachId() {
        return attachId;
    }

    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getClassifiedLevel() {
        return classifiedLevel;
    }

    public void setClassifiedLevel(String classifiedLevel) {
        this.classifiedLevel = classifiedLevel;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getDisposalMethod() {
        return disposalMethod;
    }

    public void setDisposalMethod(String disposalMethod) {
        this.disposalMethod = disposalMethod;
    }

    public String getBorrowingFlag() {
        return borrowingFlag;
    }

    public void setBorrowingFlag(String borrowingFlag) {
        this.borrowingFlag = borrowingFlag;
    }

    public String getQueryFlag() {
        return queryFlag;
    }

    public void setQueryFlag(String queryFlag) {
        this.queryFlag = queryFlag;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getStoragePeriod() {
        return storagePeriod;
    }

    public void setStoragePeriod(Integer storagePeriod) {
        this.storagePeriod = storagePeriod;
    }

    public String getIdentifyTime() {
        return identifyTime;
    }

    public void setIdentifyTime(String identifyTime) {
        this.identifyTime = identifyTime;
    }

    public String getIdentifyUser() {
        return identifyUser;
    }

    public void setIdentifyUser(String identifyUser) {
        this.identifyUser = identifyUser;
    }

	public String getFrameTime() {
		return frameTime;
	}

	public void setFrameTime(String frameTime) {
		this.frameTime = frameTime;
	}

	public String getFrameStatus() {
		return frameStatus;
	}

	public void setFrameStatus(String frameStatus) {
		this.frameStatus = frameStatus;
	}
}
