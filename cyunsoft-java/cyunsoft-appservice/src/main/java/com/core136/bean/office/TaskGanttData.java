package com.core136.bean.office;

import java.io.Serializable;
import java.util.List;

public class TaskGanttData implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String taskDataId;
    private String taskId;
    private Integer sortNo;
    private String text;
    private String startDate;
    private Integer duration;
    private Double progress;
    private String open;
    private transient String userName;
    private String parent;
    private transient List<TaskGanttData> children;
    private String accountId;
    private String createTime;
    private String createUser;
    private String orgId;

	public String getTaskDataId() {
		return taskDataId;
	}

	public void setTaskDataId(String taskDataId) {
		this.taskDataId = taskDataId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Double getProgress() {
		return progress;
	}

	public void setProgress(Double progress) {
		this.progress = progress;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public List<TaskGanttData> getChildren() {
		return children;
	}

	public void setChildren(List<TaskGanttData> children) {
		this.children = children;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
