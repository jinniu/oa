package com.core136.bean.archives;

import java.io.Serializable;
public class ArchivesEnter implements Serializable {
    private String recordId;
    private String title;
    private String attachId;
    private String subject;
    private String status;
    private Integer fileQuantity;
    private Integer pages;

    private String archivesEnterType;
    private String archivesSource;

    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAttachId() {
        return attachId;
    }

    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getArchivesEnterType() {
        return archivesEnterType;
    }

    public void setArchivesEnterType(String archivesEnterType) {
        this.archivesEnterType = archivesEnterType;
    }

    public String getArchivesSource() {
        return archivesSource;
    }

    public void setArchivesSource(String archivesSource) {
        this.archivesSource = archivesSource;
    }

    public Integer getFileQuantity() {
        return fileQuantity;
    }

    public void setFileQuantity(Integer fileQuantity) {
        this.fileQuantity = fileQuantity;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }
}
