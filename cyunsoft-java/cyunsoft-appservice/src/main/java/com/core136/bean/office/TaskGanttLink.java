package com.core136.bean.office;

import java.io.Serializable;

public class TaskGanttLink implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String taskLinkId;
    private String taskId;
    private Integer sortNo;
    private String source;
    private String target;
    private String linkType;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getTaskLinkId() {
        return taskLinkId;
    }

    public void setTaskLinkId(String taskLinkId) {
        this.taskLinkId = taskLinkId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
