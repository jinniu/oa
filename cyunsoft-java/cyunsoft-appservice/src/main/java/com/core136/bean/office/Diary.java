package com.core136.bean.office;

import java.io.Serializable;

/**
 * 工作日志
 * @author lsq
 */
public class Diary implements Serializable {
    private static final long serialVersionUID = 1L;
    private String diaryId;
    private String diaryDay;
    private String title;
    private String content;
    private String subheading;
    private String diaryType;
    private String userRole;
    private String deptRole;
    private String levelRole;
    private String attachId;
    private String attachRole;
    private String msgType;
    private String followUser;
	private String createTime;
	private String createUser;
    private String orgId;

	public String getDiaryId() {
		return diaryId;
	}

	public void setDiaryId(String diaryId) {
		this.diaryId = diaryId;
	}

	public String getDiaryDay() {
		return diaryDay;
	}

	public void setDiaryDay(String diaryDay) {
		this.diaryDay = diaryDay;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSubheading() {
		return subheading;
	}

	public void setSubheading(String subheading) {
		this.subheading = subheading;
	}

	public String getDiaryType() {
		return diaryType;
	}

	public void setDiaryType(String diaryType) {
		this.diaryType = diaryType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getDeptRole() {
		return deptRole;
	}

	public void setDeptRole(String deptRole) {
		this.deptRole = deptRole;
	}

	public String getLevelRole() {
		return levelRole;
	}

	public void setLevelRole(String levelRole) {
		this.levelRole = levelRole;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getAttachRole() {
		return attachRole;
	}

	public void setAttachRole(String attachRole) {
		this.attachRole = attachRole;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getFollowUser() {
		return followUser;
	}

	public void setFollowUser(String followUser) {
		this.followUser = followUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}

