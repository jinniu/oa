package com.core136.bean.office;

import java.io.Serializable;

/**
 * 资固资产调拨记录
 * @author lsq
 */
public class FixedAssetsAllocation implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String assetsId;
    private String oldOwnDept;
    private String newOwnDept;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getAssetsId() {
        return assetsId;
    }

    public void setAssetsId(String assetsId) {
        this.assetsId = assetsId;
    }

    public String getOldOwnDept() {
        return oldOwnDept;
    }

    public void setOldOwnDept(String oldOwnDept) {
        this.oldOwnDept = oldOwnDept;
    }

    public String getNewOwnDept() {
        return newOwnDept;
    }

    public void setNewOwnDept(String newOwnDept) {
        this.newOwnDept = newOwnDept;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
