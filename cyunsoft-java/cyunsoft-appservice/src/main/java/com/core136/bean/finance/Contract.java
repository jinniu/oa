package com.core136.bean.finance;

import java.io.Serializable;

/**
 * @author lsq
 */
public class Contract implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** 合同id*/
    private String contractId;
    /** 合同编号*/
    private String contractCode;
    /** 签订时间*/
    private String signTime;
    /** 签订地址*/
    private String signAddress;
    /** 合同标题*/
    private String title;
    /** 合同类型*/
    private String contractType;
    /** 结算方式(1.进度付款;2.滚动付款;3.款到发货;4.货到付款)*/
    private String payType;
    /** 结算币种(1.人民币;2.美元;3.其它)*/
    private String cashType;
    /** 机构id*/
    private String orgId;
    /** 合同分类id*/
    private String sortId;
    /** 履行期限开始*/
    private String startTime;
    /** 履行期限终止*/
    private String endTime;
    /** 合同条款*/
    private String content;
    /** 需方id*/
    private String customerName;
    /** 需方地址*/
    private String registerAddr;
    /** 需方法人*/
    private String legalPerson;
    /** 需方签订人*/
    private String customerSignUser;
    /** 需方电话*/
    private String mobile;
    /** 需方开户行*/
    private String bank;
    /** 需方账号*/
    private String bankAccount;
    /** 需方税号*/
    private String taxNo;
    /** 供方名称*/
    private String myOrgName;
    /** 供方地址*/
    private String myOrgAdd;
    /** 供方法人*/
    private String myLegalPerson;
    /** 供方签订人*/
    private String mySignUser;
    /** 供方联系电话*/
    private String mySignMobile;
    /** 供方开户行*/
    private String myBank;
    /** 供方账号*/
    private String myBankAccount;
    /** 供方税号*/
    private String myTaxNo;
    /** 录入合同时间*/
    private String createTime;
    /** 录入合同人*/
    private String createUser;
    /** 附件*/
    private String attachId;
    /** */
    private Double total;
    /** */
    private Double realTotal;
    /** 排序*/
    private Integer sortNo;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getSignTime() {
        return signTime;
    }

    public void setSignTime(String signTime) {
        this.signTime = signTime;
    }

    public String getSignAddress() {
        return signAddress;
    }

    public void setSignAddress(String signAddress) {
        this.signAddress = signAddress;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getCashType() {
        return cashType;
    }

    public void setCashType(String cashType) {
        this.cashType = cashType;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

	public String getRegisterAddr() {
		return registerAddr;
	}

	public void setRegisterAddr(String registerAddr) {
		this.registerAddr = registerAddr;
	}

	public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getCustomerSignUser() {
        return customerSignUser;
    }

    public void setCustomerSignUser(String customerSignUser) {
        this.customerSignUser = customerSignUser;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getMyOrgName() {
        return myOrgName;
    }

    public void setMyOrgName(String myOrgName) {
        this.myOrgName = myOrgName;
    }

    public String getMyOrgAdd() {
        return myOrgAdd;
    }

    public void setMyOrgAdd(String myOrgAdd) {
        this.myOrgAdd = myOrgAdd;
    }

    public String getMyLegalPerson() {
        return myLegalPerson;
    }

    public void setMyLegalPerson(String myLegalPerson) {
        this.myLegalPerson = myLegalPerson;
    }

    public String getMySignUser() {
        return mySignUser;
    }

    public void setMySignUser(String mySignUser) {
        this.mySignUser = mySignUser;
    }

    public String getMySignMobile() {
        return mySignMobile;
    }

    public void setMySignMobile(String mySignMobile) {
        this.mySignMobile = mySignMobile;
    }

    public String getMyBank() {
        return myBank;
    }

    public void setMyBank(String myBank) {
        this.myBank = myBank;
    }

    public String getMyBankAccount() {
        return myBankAccount;
    }

    public void setMyBankAccount(String myBankAccount) {
        this.myBankAccount = myBankAccount;
    }

    public String getMyTaxNo() {
        return myTaxNo;
    }

    public void setMyTaxNo(String myTaxNo) {
        this.myTaxNo = myTaxNo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getAttachId() {
        return attachId;
    }

    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getRealTotal() {
        return realTotal;
    }

    public void setRealTotal(Double realTotal) {
        this.realTotal = realTotal;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }


}
