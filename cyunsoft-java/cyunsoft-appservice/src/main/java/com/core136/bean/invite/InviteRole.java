package com.core136.bean.invite;

import java.io.Serializable;

public class InviteRole implements Serializable {
	private String queryUserRole;
	private String queryDeptRole;
	private String queryLevelRole;
	//审批权限
	private String approveRole;
	private String createTime;
	private String createUser;
	private String orgId;

	public String getQueryUserRole() {
		return queryUserRole;
	}

	public void setQueryUserRole(String queryUserRole) {
		this.queryUserRole = queryUserRole;
	}

	public String getQueryDeptRole() {
		return queryDeptRole;
	}

	public void setQueryDeptRole(String queryDeptRole) {
		this.queryDeptRole = queryDeptRole;
	}

	public String getQueryLevelRole() {
		return queryLevelRole;
	}

	public void setQueryLevelRole(String queryLevelRole) {
		this.queryLevelRole = queryLevelRole;
	}

	public String getApproveRole() {
		return approveRole;
	}

	public void setApproveRole(String approveRole) {
		this.approveRole = approveRole;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
