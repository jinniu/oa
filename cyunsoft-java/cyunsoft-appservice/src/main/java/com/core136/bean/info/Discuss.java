package com.core136.bean.info;

import java.io.Serializable;

/**
 * 讨论区
 * @author lsq
 */
public class Discuss implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String title;
    private Integer sortNo;
    private String discussType;
    private String userRole;
    private String deptRole;
    private String levelRole;
    private String edition;
    private String needApproval;
    private String remark;
    private String readRole;
    private String status;
    private String createTime;
    private String createUser;
    private String orgId;

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getDiscussType() {
		return discussType;
	}

	public void setDiscussType(String discussType) {
		this.discussType = discussType;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getDeptRole() {
		return deptRole;
	}

	public void setDeptRole(String deptRole) {
		this.deptRole = deptRole;
	}

	public String getLevelRole() {
		return levelRole;
	}

	public void setLevelRole(String levelRole) {
		this.levelRole = levelRole;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getNeedApproval() {
		return needApproval;
	}

	public void setNeedApproval(String needApproval) {
		this.needApproval = needApproval;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getReadRole() {
		return readRole;
	}

	public void setReadRole(String readRole) {
		this.readRole = readRole;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
