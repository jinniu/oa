package com.core136.bean.office;

import java.io.Serializable;

public class Entertain implements Serializable {
	private String recordId;
	private String title;
	private String remark;
	private String entertainType;
	private String entertainTime;
	private String diningType;
	private String entertainUser;
	private Integer userCount;
	private String status;
	private String createTime;
	private String createUser;
	private String orgId;

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getEntertainType() {
		return entertainType;
	}

	public void setEntertainType(String entertainType) {
		this.entertainType = entertainType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getEntertainTime() {
		return entertainTime;
	}

	public void setEntertainTime(String entertainTime) {
		this.entertainTime = entertainTime;
	}

	public String getDiningType() {
		return diningType;
	}

	public void setDiningType(String diningType) {
		this.diningType = diningType;
	}

	public String getEntertainUser() {
		return entertainUser;
	}

	public void setEntertainUser(String entertainUser) {
		this.entertainUser = entertainUser;
	}

	public Integer getUserCount() {
		return userCount;
	}

	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}
}
