package com.core136.mapper.project;

import com.core136.bean.project.ProProblem;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProProblemMapper extends MyMapper<ProProblem> {
    /**
     * 获取项目任务
     * @param orgId 机构码
     * @param proId 项目Id
     * @param sortId 项目分类Id
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 项目任务
     */
    List<Map<String, String>> getProProblemListByPro(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId, @Param(value = "sortId") String sortId,
                                                     @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime")String beginTime,@Param(value="endTime")String endTime,
                                                     @Param(value = "keyword") String keyword);
}
