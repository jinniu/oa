package com.core136.mapper.project;

import com.core136.bean.project.ProRole;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

public interface ProRoleMapper extends MyMapper<ProRole> {

    /**
     * 判读人员是否在立项权限内
     *
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param deptId 部门Id
     * @param levelId 行政级别Id
     * @return 是否在权限内
     */
    Integer isInRole(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId);
}
