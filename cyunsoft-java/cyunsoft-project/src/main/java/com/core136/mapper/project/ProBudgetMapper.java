package com.core136.mapper.project;

import com.core136.bean.project.ProBudget;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProBudgetMapper extends MyMapper<ProBudget> {

	/**
	 * 项目预算列表
	 *
	 * @param orgId 机构码
	 * @param proId 项目Id
	 * @param keyword 查询关键词
	 * @return 预算列表
	 */
    List<Map<String, String>> getProBudgetListByProId(@Param(value = "orgId") String orgId,@Param(value="sortId")String sortId,@Param(value = "proId") String proId,
													  @Param(value="dateQueryType")String dateQueryType,@Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
													  @Param(value = "keyword") String keyword);
}
