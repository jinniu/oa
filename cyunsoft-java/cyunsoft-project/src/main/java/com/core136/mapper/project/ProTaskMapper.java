package com.core136.mapper.project;

import com.core136.bean.project.ProTask;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProTaskMapper extends MyMapper<ProTask> {
	List<ProTask> getTaskAllocationList(
		@Param(value = "orgId") String orgId,
		@Param(value = "proLevel") String proLevel,
		@Param(value = "proSort") String proSort,
		@Param(value = "dateQueryType") String dateQueryType,
		@Param(value = "beginTime") String beginTime,
		@Param(value = "endTime") String endTime,
		@Param(value = "keyword") String keyword);


	Map<String, String> getProTaskById(@Param(value = "orgId") String orgId, @Param(value = "taskId") String taskId);
    /**
     * 获取项目子任务列表
     *
     * @param orgId 机构码
     * @param proId 项目Id
     * @return 子任务列表
     */
    List<Map<String, String>> getProTaskList(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);

    /**
     * 获取个人待办任务列表
     *
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param proSort 项目分类
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 待办任务列表
     */
    List<Map<String, String>> getMyTaskWorkList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "proSort") String proSort,@Param(value="dateQueryType")String dateQueryType,
                                                @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);


    List<Map<String, String>> getTaskByProForSelect(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);

}
