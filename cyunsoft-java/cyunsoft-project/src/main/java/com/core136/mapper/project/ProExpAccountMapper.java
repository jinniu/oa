package com.core136.mapper.project;

import com.core136.bean.project.ProExpAccount;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProExpAccountMapper extends MyMapper<ProExpAccount> {


	List<ProExpAccount> getAllProExpAccount(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

	/**
	 * 判断是否还有子集
	 * @param orgId 机构码
	 * @param sortId 项目分类Id
	 * @return 是否有子集
	 */
	int isExistChild(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);
}
