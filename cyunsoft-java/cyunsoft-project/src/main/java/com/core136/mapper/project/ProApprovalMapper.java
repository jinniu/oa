package com.core136.mapper.project;

import com.core136.bean.project.ProApproval;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProApprovalMapper extends MyMapper<ProApproval> {

    /**
     * 获取项目历史审批记录
     *
     * @param orgId 机构码
     * @param status 状态
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 历史审批记录
     */
    List<Map<String, String>> getOldProApprovalList(@Param(value = "orgId") String orgId, @Param(value = "status") String status, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);

}
