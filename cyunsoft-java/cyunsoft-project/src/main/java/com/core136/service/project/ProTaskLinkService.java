package com.core136.service.project;

import com.core136.bean.project.ProTaskLink;
import com.core136.mapper.project.ProTaskLinkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProTaskLinkService {
    private ProTaskLinkMapper proTaskLinkMapper;
	@Autowired
	public void setProTaskLinkMapper(ProTaskLinkMapper proTaskLinkMapper) {
		this.proTaskLinkMapper = proTaskLinkMapper;
	}

	public int insertProTaskLink(ProTaskLink proTaskLink) {
        return proTaskLinkMapper.insert(proTaskLink);
    }

    public int deleteProTaskLink(ProTaskLink proTaskLink) {
        return proTaskLinkMapper.delete(proTaskLink);
    }

    public int updateProTaskLink(Example example, ProTaskLink proTaskLink) {
        return proTaskLinkMapper.updateByExampleSelective(proTaskLink, example);
    }

    public ProTaskLink selectOneProTaskLink(ProTaskLink proTaskLink) {
        return proTaskLinkMapper.selectOne(proTaskLink);
    }

    /**
     * 获取任务关联列表
     *
     * @param orgId 机构码
     * @param proId 项目Id
     * @return 消息结构
     */
    public List<Map<String, String>> getProTaskLinkList(String orgId, String proId) {
        return proTaskLinkMapper.getProTaskLinkList(orgId, proId);
    }

}
