package com.core136.service.project;

import com.core136.bean.project.ProRecord;
import com.core136.bean.project.ProTask;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProTaskMapper;
import com.core136.service.account.UserInfoService;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
public class ProTaskService {
    private ProTaskMapper proTaskMapper;
	private ProRecordService proRecordService;
	private UserInfoService userInfoService;
    private ProTaskLinkService proTaskLinkService;

	@Autowired
	public void setProTaskMapper(ProTaskMapper proTaskMapper) {
		this.proTaskMapper = proTaskMapper;
	}

	@Autowired
	public void setProRecordService(ProRecordService proRecordService) {
		this.proRecordService = proRecordService;
	}

	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	@Autowired
	public void setProTaskLinkService(ProTaskLinkService proTaskLinkService) {
		this.proTaskLinkService = proTaskLinkService;
	}

	public int insertProTask(ProTask proTask) {
        return proTaskMapper.insert(proTask);
    }

    public int deleteProTask(ProTask proTask) {
        return proTaskMapper.delete(proTask);
    }

    public int updateProTask(Example example, ProTask proTask) {
        return proTaskMapper.updateByExampleSelective(proTask, example);
    }

    public ProTask selectOneProTask(ProTask proTask) {
        return proTaskMapper.selectOne(proTask);
    }

	public Map<String,String> getProTaskById(String orgId,String taskId) {
		return proTaskMapper.getProTaskById(orgId,taskId);
	}


	public List<ProTask> getTaskAllocationList(String orgId, String proLevel, String proSort, String dateQueryType, String beginTime, String endTime, String keyword) {
		if (StringUtils.isBlank(keyword)) {
			keyword = "%%";
		} else {
			keyword = "%" + keyword + "%";
		}
		List<ProTask> list = proTaskMapper.getTaskAllocationList(orgId, proLevel, proSort, dateQueryType, beginTime, endTime, keyword);
		List<ProTask> proTasksList = new ArrayList<>();
		for (ProTask task : list) {
			if (StringUtils.isNotBlank(task.getAccountId())) {
				task.setUserName(userInfoService.getUserNamesByAccountIds(orgId, Arrays.asList(task.getAccountId().split(","))));
			}
			if (task.getParent().equals("0")) {
				proTasksList.add(task);
			}
		}
		for (ProTask proTask : proTasksList) {
			proTask.setChildren(getChildProTaskList(proTask.getTaskId(), list));
		}
		return proTasksList;
	}

	/**
	 * 获取任务列表的父子结构
	 * @param taskDataId 作务Id
	 * @param rootProTask 项目任务列表
	 * @return 任务列表结构
	 */
	public List<ProTask> getChildProTaskList(String taskDataId, List<ProTask> rootProTask) {
		List<ProTask> childList = new ArrayList<>();
		for (ProTask proTaskData : rootProTask) {
			if (proTaskData.getParent().equals(taskDataId)) {
				childList.add(proTaskData);
			}
		}
		for (ProTask proTaskData : childList) {
			proTaskData.setChildren(getChildProTaskList(proTaskData.getTaskId(), rootProTask));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}
    /**
     * 获取子任甘特图信息
     *
     * @param orgId 机构码
     * @param proId 项目Id
     * @return 甘特图信息
     */
    public Map<String,Object> getProTaskInfo(String orgId, String proId) {
		ProRecord proRecord = new ProRecord();
		proRecord.setProId(proId);
		proRecord.setOrgId(orgId);
        Map<String, Object> map = new HashMap<>();
        map.put("proRecord",proRecordService.selectOneProRecord(proRecord));
        map.put("data", getProTaskList(orgId, proId));
        map.put("links", proTaskLinkService.getProTaskLinkList(orgId, proId));
        return map;
    }


    /**
     * 获取项目子任务列表
     *
     * @param orgId 机构码
     * @param proId 项目Id
     * @return 子任务列表
     */
    public List<Map<String, String>> getProTaskList(String orgId, String proId) {
        return proTaskMapper.getProTaskList(orgId, proId);
    }

    /**
     * 获取项目子任务列表
     *
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param proSort 项目分类对象
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 子任务列表
     */
    public List<Map<String, String>> getMyTaskWorkList(String orgId, String accountId, String proSort, String dateQueryType,String beginTime, String endTime, String keyword) {
        return proTaskMapper.getMyTaskWorkList(orgId, accountId, proSort, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取项目子任务列表
     *
     * @param pageParam 分页参数
     * @param proSort 项目分类对象
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return 子任务列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getMyTaskWorkList(PageParam pageParam, String proSort, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyTaskWorkList(pageParam.getOrgId(), pageParam.getAccountId(), proSort,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


    public List<Map<String, String>> getTaskByProForSelect(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId) {
        return proTaskMapper.getTaskByProForSelect(orgId, proId);
    }

}
