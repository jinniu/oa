package com.core136.service.project;

import com.core136.bean.project.ProApproval;
import com.core136.bean.project.ProRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProApprovalMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;


@Service
public class ProApprovalService {
    private ProApprovalMapper proApprovalMapper;
    private ProRecordService proRecordService;
	@Autowired
	public void setProApprovalMapper(ProApprovalMapper proApprovalMapper) {
		this.proApprovalMapper = proApprovalMapper;
	}
	@Autowired
	public void setProRecordService(ProRecordService proRecordService) {
		this.proRecordService = proRecordService;
	}

	public int insertProApproval(ProApproval proApproval) {
        return proApprovalMapper.insert(proApproval);
    }

    public int deleteProApproval(ProApproval proApproval) {
        return proApprovalMapper.delete(proApproval);
    }

    public int updateProApproval(Example example, ProApproval proApproval) {
        return proApprovalMapper.updateByExampleSelective(proApproval, example);
    }

    public ProApproval selectOneProApproval(ProApproval proApproval) {
        return proApprovalMapper.selectOne(proApproval);
    }

    /**
     * 项目审批成功
     *
     * @param proApproval 项目审批对象
     * @return 消息结构
     */
    @Transactional(value = "generalTM")
    public RetDataBean setApprovalProRecord(ProApproval proApproval) {
        ProRecord proRecord = new ProRecord();
        proRecord.setStatus(proApproval.getStatus());
        Example example = new Example(ProRecord.class);
		example.createCriteria().andEqualTo("orgId",proApproval.getOrgId()).andEqualTo("proId",proApproval.getProId());
        proRecordService.updateProRecord(example, proRecord);
        return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, insertProApproval(proApproval));
    }
	/**
	 * 获取项目历史审批记录
	 *
	 * @param orgId 机构码
	 * @param status 状态
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 历史审批记录
	 */
    public List<Map<String, String>> getOldProApprovalList(String orgId, String status, String beginTime, String endTime, String keyword) {
        return proApprovalMapper.getOldProApprovalList(orgId, status, beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取项目历史审批记录
     *
     * @param pageParam 分页参数
     * @param status 状态
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 历史审批记录
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getOldProApprovalList(PageParam pageParam, String status, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOldProApprovalList(pageParam.getOrgId(), status, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
