package com.core136.service.project;

import com.core136.bean.project.ProBudget;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProBudgetMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProBudgetService {
    private ProBudgetMapper proBudgetMapper;
	@Autowired
	public void setProBudgetMapper(ProBudgetMapper proBudgetMapper) {
		this.proBudgetMapper = proBudgetMapper;
	}

	public int insertProBudget(ProBudget proBudget) {
        return proBudgetMapper.insert(proBudget);
    }

    public int deleteProBudget(ProBudget proBudget) {
        return proBudgetMapper.delete(proBudget);
    }

    public int updateProBudget(Example example, ProBudget proBudget) {
        return proBudgetMapper.updateByExampleSelective(proBudget, example);
    }

    public ProBudget selectOneProBudget(ProBudget proBudget) {
        return proBudgetMapper.selectOne(proBudget);
    }

	/**
	 * 批量删除项目费用
	 * @param orgId 机构码
	 * @param list 项目费用Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteProBudgetByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(ProBudget.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, proBudgetMapper.deleteByExample(example));
		}
	}

    /**
     * 项目预算列表
     *
     * @param orgId 机构码
     * @param proId 项目Id
     * @param keyword 查询关键词
     * @return 预算列表
     */
    public List<Map<String, String>> getProBudgetListByProId(String orgId, String sortId,String proId,String dateQueryType,String beginTime,String endTime, String keyword) {
        return proBudgetMapper.getProBudgetListByProId(orgId,sortId, proId,dateQueryType,beginTime,endTime, keyword);
    }

    /**
     * 项目预算列表
     *
     * @param pageParam 分页参数
     * @param proId 项目Id
     * @return 预算列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getProBudgetListByProId(PageParam pageParam,String sortId, String proId,String dateQueryType,String beginTime,String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProBudgetListByProId(pageParam.getOrgId(), sortId,proId,dateQueryType,beginTime,endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
