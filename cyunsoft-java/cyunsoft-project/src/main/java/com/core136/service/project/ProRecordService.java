package com.core136.service.project;

import com.core136.bean.project.ProRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProRecordMapper;
import com.core136.service.account.UserInfoService;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProRecordService {
    private ProRecordMapper proRecordMapper;
    private UserInfoService userInfoService;
	@Autowired
	public void setProRecordMapper(ProRecordMapper proRecordMapper) {
		this.proRecordMapper = proRecordMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertProRecord(ProRecord proRecord) {
        return proRecordMapper.insert(proRecord);
    }

    public int deleteProRecord(ProRecord proRecord) {
        return proRecordMapper.delete(proRecord);
    }

	/**
	 * 获取项目选择列表
	 * @param orgId 机构码
	 * @return 项目选择列表
	 */
	public List<Map<String,String>>getProRecordListForSelect(String orgId)
	{
		return proRecordMapper.getProRecordListForSelect(orgId);
	}

	public List<Map<String,String>>getProRecordForFilter(String orgId,String sortId)
	{
		return proRecordMapper.getProRecordForFilter(orgId,sortId);
	}

	/**
	 * 批量删除项目记录
	 * @param orgId 机构码
	 * @param list 项目记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteProRecordByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(ProRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("proId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, proRecordMapper.deleteByExample(example));
		}
	}
    public int updateProRecord(Example example, ProRecord proRecord) {
        return proRecordMapper.updateByExampleSelective(proRecord, example);
    }

    public ProRecord selectOneProRecord(ProRecord proRecord) {
        return proRecordMapper.selectOne(proRecord);
    }

    /**
     * 获取子任务负责人列表
     *
     * @param orgId 机构码
     * @param proId 项目Id
     * @return 任务负责人列表
     */
    public List<Map<String, String>> getTaskUsersList(String orgId, String proId) {
        ProRecord proRecord = new ProRecord();
        proRecord.setOrgId(orgId);
        proRecord.setProId(proId);
        proRecord = selectOneProRecord(proRecord);
        String taskUsers = proRecord.getTaskUsers();
        List<Map<String, String>> tMaps = userInfoService.getAllUserByAccountList(proRecord.getOrgId(), taskUsers);
        List<Map<String, String>> returnList = new ArrayList<>();
		for (Map<String, String> tMap : tMaps) {
			Map<String, String> map = new HashMap<>();
			map.put("key", tMap.get("accountId"));
			map.put("label", tMap.get("userName"));
			returnList.add(map);
		}
        return returnList;
    }


    public Map<String, String> getProRecordStatusCountList(String orgId) {
        return proRecordMapper.getProRecordStatusCountList(orgId);
    }

    /**
     * 按状态获取项目列表
     *
     * @param orgId 机构码
     * @param status 项目状态
     * @param keyword 查询关键词
     * @return 项目列表
     */
    public List<Map<String, String>> getProRecordListByStatus(String orgId, String proLevel, String dateQueryType,String beginTime, String endTime, String status, String keyword) {
        return proRecordMapper.getProRecordListByStatus(orgId, proLevel, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

    /**
     * 按状态获取项目列表
     *
     * @param pageParam 分页参数
     * @param status 项目状态
     * @return 项目列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getProRecordListByStatus(PageParam pageParam, String proLevel,String dateQueryType, String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProRecordListByStatus(pageParam.getOrgId(), proLevel,dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 项目记录查询
     *
     * @param orgId 机构码
     * @param proSort 项目分类对象
     * @param proLevel 项目等级
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status 项目状态
     * @param keyword 查询关键词
     * @return 项目记录列表
     */
    public List<Map<String, String>> getProRecordListByQuery(String orgId, String proSort, String proLevel, String dateQueryType,String beginTime, String endTime, String status, String keyword) {
        return proRecordMapper.getProRecordListByQuery(orgId, proSort, proLevel, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }
	public Map<String, String> getProRecordById(String orgId, String proId) {
		return proRecordMapper.getProRecordById(orgId, proId);
	}

    /**
     * 项目记录查询
     *
     * @param pageParam 分页参数
     * @param proSort 项目分类对象
     * @param proLevel 项目等级
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status 项目状态
     * @return 项目记录
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getProRecordListByQuery(PageParam pageParam, String proSort, String proLevel, String dateQueryType,String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProRecordListByQuery(pageParam.getOrgId(), proSort, proLevel, dateQueryType,beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }
    public List<Map<String, String>> getProRecordListForApproval(String orgId,String proSort, String proLevel, String status,String dateQueryType,String beginTime, String endTime, String keyword) {
        return proRecordMapper.getProRecordListForApproval(orgId, proSort,proLevel, status,dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    public PageInfo<Map<String, String>> getProRecordListForApproval(PageParam pageParam, String proSort,String proLevel, String status,String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProRecordListForApproval(pageParam.getOrgId(), proSort,proLevel,status,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }
}
