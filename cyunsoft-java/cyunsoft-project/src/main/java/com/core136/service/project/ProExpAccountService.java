package com.core136.service.project;

import com.core136.bean.project.ProExpAccount;
import com.core136.mapper.project.ProExpAccountMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ProExpAccountService {
    private ProExpAccountMapper proExpAccountMapper;

	@Autowired
	public void setProExpAccountMapper(ProExpAccountMapper proExpAccountMapper) {
		this.proExpAccountMapper = proExpAccountMapper;
	}

	public int insertProExpAccount(ProExpAccount proExpAccount) {
        return proExpAccountMapper.insert(proExpAccount);
    }

    public int deleteProExpAccount(ProExpAccount proExpAccount) {
        return proExpAccountMapper.delete(proExpAccount);
    }

    public int updateProExpAccount(Example example, ProExpAccount proExpAccount) {
        return proExpAccountMapper.updateByExampleSelective(proExpAccount, example);
    }


	/**
	 * 获取分类树结构
	 * @param orgId 机构码
	 * @return 分类树结构
	 */
	public List<ProExpAccount> getProExpAccountTree(String orgId, String keyword) {
		List<ProExpAccount> list = getAllProExpAccount(orgId,keyword);
		List<ProExpAccount> proSortList = new ArrayList<>();
		for (ProExpAccount proExpAccount : list) {
			if (proExpAccount.getParentId().equals("0")) {
				proSortList.add(proExpAccount);
			}
		}
		for (ProExpAccount proSort : proSortList) {
			proSort.setChildren(getChildProExpAccountList(proSort.getExpId(), list));
		}
		return proSortList;
	}

	/**
	 * 获取分类子表
	 * @param expId 科目Id
	 * @param rootProExpAccount 费用科目对象
	 * @return 项目费用科目分类列表
	 */
	public List<ProExpAccount> getChildProExpAccountList(String expId, List<ProExpAccount> rootProExpAccount) {
		List<ProExpAccount> childList = new ArrayList<>();
		for (ProExpAccount proSort : rootProExpAccount) {
			if (proSort.getParentId().equals(expId)) {
				childList.add(proSort);
			}
		}
		for (ProExpAccount proSort : childList) {
			proSort.setChildren(getChildProExpAccountList(proSort.getExpId(), rootProExpAccount));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	public List<ProExpAccount> getAllProExpAccount(String orgId,String keyword) {
		if(StringUtils.isBlank(keyword)) {
			keyword="";
		}
		return proExpAccountMapper.getAllProExpAccount(orgId,"%"+keyword+"%");
	}

    public ProExpAccount selectOneProExpAccount(ProExpAccount proExpAccount) {
        return proExpAccountMapper.selectOne(proExpAccount);
    }

    /**
     * 判断是否还有子集
     *
     * @param orgId 机构码
     * @param expId 科目Id
     * @return 是否还有子集
     */
    public int isExistChild(String orgId, String expId) {
        return proExpAccountMapper.isExistChild(orgId, expId);
    }

}
