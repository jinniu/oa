package com.core136.service.project;


import com.core136.bean.account.UserInfo;
import com.core136.bean.project.ProRole;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class ProRoleService {
    private ProRoleMapper proRoleMapper;
	@Autowired
	public void setProRoleMapper(ProRoleMapper proRoleMapper) {
		this.proRoleMapper = proRoleMapper;
	}

	public int insertProRole(ProRole proRole) {
        return proRoleMapper.insert(proRole);
    }

    public int deleteProRole(ProRole proRole) {
        return proRoleMapper.delete(proRole);
    }

    public int updateProRole(Example example, ProRole proRole) {
        return proRoleMapper.updateByExampleSelective(proRole, example);
    }

    public ProRole selectOneProRole(ProRole proRole) {
        return proRoleMapper.selectOne(proRole);
    }

    /**
     * 设置项目权限
     *
     * @param user 用户对象
     * @param proRole 项目权限对象
     * @return 消息结构
     */
    public RetDataBean setProRole(UserInfo user, ProRole proRole) {
        ProRole tmpProRole = new ProRole();
        tmpProRole.setOrgId(user.getOrgId());
        tmpProRole = selectOneProRole(tmpProRole);
        if (tmpProRole == null) {
            proRole.setRoleId(SysTools.getGUID());
            proRole.setCreateUser(user.getAccountId());
            proRole.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proRole.setOrgId(user.getOrgId());
            insertProRole(proRole);
        } else {
            Example example = new Example(ProRole.class);
            example.createCriteria().andEqualTo("orgId", user.getOrgId());
            updateProRole(example, proRole);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

    /**
     * 判读人员是否在立项权限内
     *
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param deptId 部门Id
     * @param levelId 行政级别Id
     * @return 是否在立项权限内
     */
    public Integer isInRole(String orgId, String accountId, String deptId, String levelId) {
        return proRoleMapper.isInRole(orgId, accountId, deptId, levelId);
    }


}
